---
title: 'St Joes'
---

## We believe that each person is unique and created in God’s image.  In our school, we provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.

At St Joseph’s we hold Christ at the centre of everything we do and our children know that they are unique and created in God’s image. We celebrate each individual, supporting their journey through school and developing their gifts and talents. Our staff provide excellent opportunities for our children to enjoy and achieve in a happy, safe, and caring environment.

We know that your child will be happy at St Joseph’s and we hope that you as parents and carers will be fully involved in your child’s education and the life of the school in general.

If you need any further information please do not hesitate to look at our school brochure or contact the office and we will do our very best to help.

* w: [https://www.stjosephsrcprimaryschool.net](www.stjosephsrcprimaryschool.net)
* e: [mailto:stjosephsnewtonaycliffe@durhamlearning.net](stjosephsnewtonaycliffe@durhamlearning.net)
* t: +44 (0)1325 300 337