---
content:
    items: '@self.children'
    limit: 6
    order:
        by: default
        dir: asc
    pagination: false
    url_taxonomy_filters: true
    columns: 2
    date: 0
title: Blog
---

