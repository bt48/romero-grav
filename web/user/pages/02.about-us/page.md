---
title: 'About Us'
published: true
---

## Deep-rooted Gospel values are very much at the heart of the Trust’s vision, which serves the Catholic and wider communities of South-West Durham, aiming to provide the best education and opportunities for young people of the area. The Trust’s aim is to develop and grow strong partnerships so that every child is valued, supported and encouraged to develop their God-given talents in order to reach their full potential.
## 
By working in partnership, we are stronger together and are committed to prioritising the welfare and well-being of our children. With outstanding pastoral care and greater access to specialist support, guidance and SEND expertise, we aim to provide a caring environment where children are nurtured and encouraged to flourish. 

As a Trust we will continue to work with other schools and educational organisations on local and national initiatives, which will provide greater access to further opportunities and support for the benefit of children, staff and the individual schools involved. 

The Romero Catholic Education Trust was established in December 2016 by St John’s and the Trust welcomed St Joseph’s Primary School, Newton Aycliffe, into the Trust on 1 April 2018. Currently, the Romero Catholic Education Trust has overarching accountability and governance of:
St John’s School & Sixth Form College – A Catholic Academy
www.stjohnsrc.org.uk
St Joseph’s Primary School – A Catholic Academy
www.stjosephsrcprimaryschool.net
Being members of the Trust enhances our strong commitment to work collaboratively to remove barriers to learning, share expertise, resources and specialist services for the educational benefit of all children and staff. 
