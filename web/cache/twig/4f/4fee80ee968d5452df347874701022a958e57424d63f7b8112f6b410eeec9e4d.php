<?php

/* @particles/carousel.html.twig */
class __TwigTemplate_b098ebb4a0db348b6dd8703b56e476401b9b5e975a2752f2351f0490f8789daa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/carousel.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo " g-owlcarousel-carousel\">

        ";
        // line 6
        if ((($this->getAttribute(($context["particle"] ?? null), "title", []) || $this->getAttribute(($context["particle"] ?? null), "description", [])) || ($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled"))) {
            // line 7
            echo "        <div class=\"g-owlcarousel-carousel-header\">
            ";
            // line 8
            if (($this->getAttribute(($context["particle"] ?? null), "title", []) || ($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled"))) {
                // line 9
                echo "                <div class=\"g-owlcarousel-carousel-title-container\">
                    ";
                // line 10
                if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
                    // line 11
                    echo "                        <div class=\"g-owlcarousel-carousel-title\">
                            ";
                    // line 12
                    echo $this->getAttribute(($context["particle"] ?? null), "title", []);
                    echo "
                        </div>
                    ";
                }
                // line 15
                echo "
                    ";
                // line 16
                if (($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled")) {
                    // line 17
                    echo "                        <div class=\"g-owlcarousel-carousel-controls\" id=\"g-owlcarousel-carousel-controls-";
                    echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
                    echo "\"></div>
                    ";
                }
                // line 19
                echo "                </div>
            ";
            }
            // line 21
            echo "
            ";
            // line 22
            if ($this->getAttribute(($context["particle"] ?? null), "description", [])) {
                // line 23
                echo "                <div class=\"g-owlcarousel-carousel-description\">
                    ";
                // line 24
                echo $this->getAttribute(($context["particle"] ?? null), "description", []);
                echo "
                </div>
            ";
            }
            // line 27
            echo "        </div>
        ";
        }
        // line 29
        echo "
        <div id=\"g-carousel-";
        // line 30
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"g-carousel owl-carousel\">

            ";
        // line 32
        if (($this->getAttribute(($context["particle"] ?? null), "source", []) == "grav")) {
            // line 33
            echo "
                ";
            // line 35
            echo "                ";
            $context["attr_extra"] = $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->attributeArrayFilter($this->getAttribute(($context["particle"] ?? null), "extra", []));
            // line 36
            echo "                ";
            $context["article_settings"] = $this->getAttribute(($context["particle"] ?? null), "article", []);
            // line 37
            echo "                ";
            $context["filter"] = $this->getAttribute(($context["article_settings"] ?? null), "filter", []);
            // line 38
            echo "                ";
            $context["sort"] = $this->getAttribute(($context["article_settings"] ?? null), "sort", []);
            // line 39
            echo "                ";
            $context["limit"] = $this->getAttribute(($context["article_settings"] ?? null), "limit", []);
            // line 40
            echo "                ";
            $context["start"] = $this->getAttribute(($context["limit"] ?? null), "start", []);
            // line 41
            echo "                ";
            $context["display"] = $this->getAttribute(($context["article_settings"] ?? null), "display", []);
            // line 42
            echo "                ";
            $context["collection"] = $this->getAttribute($this->getAttribute(($context["grav"] ?? null), "page", []), "collection", [0 => ["items" => ["@taxonomy.category" => twig_split_filter($this->env, $this->getAttribute(            // line 44
($context["filter"] ?? null), "categories", []), " ")], "order" => ["by" => $this->getAttribute(            // line 45
($context["sort"] ?? null), "orderby", []), "dir" => $this->getAttribute(($context["sort"] ?? null), "ordering", [])], "url_taxonomy_filters" => false], 1 => false], "method");
            // line 49
            echo "                ";
            $context["total"] = $this->getAttribute(($context["collection"] ?? null), "count", [], "method");
            // line 50
            echo "                ";
            $context["pages"] = $this->getAttribute(($context["collection"] ?? null), "slice", [0 => ($context["start"] ?? null), 1 => $this->getAttribute(($context["limit"] ?? null), "total", [])], "method");
            // line 51
            echo "
                ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["pages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 53
                echo "                    <div class=\"g-carousel-item\">
                        <a class=\"g-carousel-link\" href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "url", []));
                echo "\"></a>

                        ";
                // line 56
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "enabled", [])) {
                    // line 57
                    echo "                            <span class=\"g-carousel-title\">";
                    echo (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", [])) ? ($this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", [])), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", []))) : (twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []))));
                    echo "</span>
                        ";
                }
                // line 59
                echo "
                        ";
                // line 60
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", [])) {
                    // line 61
                    echo "                            ";
                    $context["page_text"] = ((($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", []) == "intro")) ? ($this->getAttribute($context["page"], "summary", [])) : ($this->getAttribute($context["page"], "content", [])));
                    // line 62
                    echo "                            <span class=\"g-carousel-description\">
                                ";
                    // line 63
                    if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "formatting", []) == "text")) {
                        // line 64
                        echo "                                    ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                ";
                    } else {
                        // line 66
                        echo "                                    ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateHtml(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                ";
                    }
                    // line 68
                    echo "                            </span>
                        ";
                }
                // line 70
                echo "
                        ";
                // line 71
                if ($this->getAttribute(($context["display"] ?? null), "readmore", [])) {
                    // line 72
                    echo "                            <span class=\"g-carousel-readmore\">";
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["display"] ?? null), "readmore", []), "html", null, true);
                    echo "</span>
                        ";
                }
                // line 74
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "
            ";
        } else {
            // line 78
            echo "
                ";
            // line 80
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "items", []));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 81
                echo "                    <div class=\"g-carousel-item\">
                        ";
                // line 82
                if ( !twig_test_empty($this->getAttribute($context["item"], "link", []))) {
                    // line 83
                    echo "                            <a class=\"g-carousel-link\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["item"], "link", [])));
                    echo "\" target=\"";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "linktarget", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "linktarget", []), "_self")) : ("_self")));
                    echo "\"></a>
                        ";
                }
                // line 85
                echo "
                        ";
                // line 86
                if ($this->getAttribute($context["item"], "icon", [])) {
                    // line 87
                    echo "                            <span class=\"g-carousel-image-icon\">
                                <i class=\"";
                    // line 88
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "icon", []));
                    echo "\"></i>
                            </span>
                        ";
                }
                // line 91
                echo "
                        ";
                // line 92
                if ($this->getAttribute($context["item"], "title", [])) {
                    // line 93
                    echo "                            <span class=\"g-carousel-title\">";
                    echo $this->getAttribute($context["item"], "title", []);
                    echo "</span>
                        ";
                }
                // line 95
                echo "
                        ";
                // line 96
                if ($this->getAttribute($context["item"], "description", [])) {
                    // line 97
                    echo "                            <span class=\"g-carousel-description\">";
                    echo $this->getAttribute($context["item"], "description", []);
                    echo "</span>
                        ";
                }
                // line 99
                echo "
                        ";
                // line 100
                if ($this->getAttribute($context["item"], "readmore", [])) {
                    // line 101
                    echo "                            <span class=\"g-carousel-readmore\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "readmore", []), "html", null, true);
                    echo "</span>
                        ";
                }
                // line 103
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 105
            echo "
            ";
        }
        // line 107
        echo "
        </div>
    </div>

";
    }

    // line 113
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 114
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/owlcarousel.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        jQuery(document).ready(function () {
            var owl";
        // line 117
        echo twig_escape_filter($this->env, twig_replace_filter(($context["id"] ?? null), ["-" => "_"]), "html", null, true);
        echo " = jQuery('#g-carousel-";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "');
            owl";
        // line 118
        echo twig_escape_filter($this->env, twig_replace_filter(($context["id"] ?? null), ["-" => "_"]), "html", null, true);
        echo ".owlCarousel({
                items: ";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "displayitems", []), "html", null, true);
        echo ",
                center: true,
                rtl: ";
        // line 121
        if (($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "page", []), "direction", []) == "rtl")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                ";
        // line 122
        if (($this->getAttribute(($context["particle"] ?? null), "animateOut", []) && ($this->getAttribute(($context["particle"] ?? null), "animateOut", []) != "default"))) {
            // line 123
            echo "                animateOut: '";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "animateOut", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "animateOut", []), "fadeOut")) : ("fadeOut")));
            echo "',
                ";
        }
        // line 125
        echo "                ";
        if (($this->getAttribute(($context["particle"] ?? null), "animateIn", []) && ($this->getAttribute(($context["particle"] ?? null), "animateIn", []) != "default"))) {
            // line 126
            echo "                animateIn: '";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "animateIn", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "animateIn", []), "fadeIn")) : ("fadeIn")));
            echo "',
                ";
        }
        // line 128
        echo "                ";
        if (($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled")) {
            // line 129
            echo "                nav: true,
                navText: ['";
            // line 130
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "prevText", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "prevText", []), "<i class=\"fa fa-chevron-left\"></i>")) : ("<i class=\"fa fa-chevron-left\"></i>")), "js"), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "nextText", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "nextText", []), "<i class=\"fa fa-chevron-right\"></i>")) : ("<i class=\"fa fa-chevron-right\"></i>")), "js"), "html", null, true);
            echo "'],
                navContainer: '#g-owlcarousel-carousel-controls-";
            // line 131
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "',
                ";
        } else {
            // line 133
            echo "                nav: false,
                ";
        }
        // line 135
        echo "                dots: false,
                loop: ";
        // line 136
        if (($this->getAttribute(($context["particle"] ?? null), "loop", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                ";
        // line 137
        if (($this->getAttribute(($context["particle"] ?? null), "autoplay", []) == "enabled")) {
            // line 138
            echo "                autoplay: true,
                autoplayTimeout: ";
            // line 139
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "autoplaySpeed", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "autoplaySpeed", []), "5000")) : ("5000")), "html", null, true);
            echo ",
                ";
            // line 140
            if (($this->getAttribute(($context["particle"] ?? null), "pauseOnHover", []) == "enabled")) {
                // line 141
                echo "                autoplayHoverPause: true,
                ";
            } else {
                // line 143
                echo "                autoplayHoverPause: false,
                ";
            }
            // line 145
            echo "                ";
        } else {
            // line 146
            echo "                autoplay: false,
                ";
        }
        // line 148
        echo "                responsive:{
                    0: {
                        items: 1,
                    },
                    650: {
                        items: 2,
                    },
                    800: {
                        items: 3,
                    },
                    1200: {
                        items: 4,
                    },
                    1400: {
                        items: ";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "displayitems", []), "html", null, true);
        echo ",
                    }
                },
            })
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "@particles/carousel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  422 => 162,  406 => 148,  402 => 146,  399 => 145,  395 => 143,  391 => 141,  389 => 140,  385 => 139,  382 => 138,  380 => 137,  372 => 136,  369 => 135,  365 => 133,  360 => 131,  354 => 130,  351 => 129,  348 => 128,  342 => 126,  339 => 125,  333 => 123,  331 => 122,  323 => 121,  318 => 119,  314 => 118,  308 => 117,  301 => 114,  298 => 113,  290 => 107,  286 => 105,  279 => 103,  273 => 101,  271 => 100,  268 => 99,  262 => 97,  260 => 96,  257 => 95,  251 => 93,  249 => 92,  246 => 91,  240 => 88,  237 => 87,  235 => 86,  232 => 85,  224 => 83,  222 => 82,  219 => 81,  214 => 80,  211 => 78,  207 => 76,  200 => 74,  194 => 72,  192 => 71,  189 => 70,  185 => 68,  179 => 66,  173 => 64,  171 => 63,  168 => 62,  165 => 61,  163 => 60,  160 => 59,  154 => 57,  152 => 56,  147 => 54,  144 => 53,  140 => 52,  137 => 51,  134 => 50,  131 => 49,  129 => 45,  128 => 44,  126 => 42,  123 => 41,  120 => 40,  117 => 39,  114 => 38,  111 => 37,  108 => 36,  105 => 35,  102 => 33,  100 => 32,  95 => 30,  92 => 29,  88 => 27,  82 => 24,  79 => 23,  77 => 22,  74 => 21,  70 => 19,  64 => 17,  62 => 16,  59 => 15,  53 => 12,  50 => 11,  48 => 10,  45 => 9,  43 => 8,  40 => 7,  38 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
    <div class=\"{{ particle.class|e }} g-owlcarousel-carousel\">

        {% if particle.title or particle.description or particle.nav == 'enabled' %}
        <div class=\"g-owlcarousel-carousel-header\">
            {% if particle.title or particle.nav == 'enabled' %}
                <div class=\"g-owlcarousel-carousel-title-container\">
                    {% if particle.title %}
                        <div class=\"g-owlcarousel-carousel-title\">
                            {{ particle.title|raw }}
                        </div>
                    {% endif %}

                    {% if particle.nav == 'enabled' %}
                        <div class=\"g-owlcarousel-carousel-controls\" id=\"g-owlcarousel-carousel-controls-{{ id }}\"></div>
                    {% endif %}
                </div>
            {% endif %}

            {% if particle.description %}
                <div class=\"g-owlcarousel-carousel-description\">
                    {{ particle.description|raw }}
                </div>
            {% endif %}
        </div>
        {% endif %}

        <div id=\"g-carousel-{{ id }}\" class=\"g-carousel owl-carousel\">

            {% if particle.source == 'grav' %}

                {# Load Content From Grav #}
                {% set attr_extra = particle.extra|attribute_array %}
                {% set article_settings = particle.article %}
                {% set filter = article_settings.filter %}
                {% set sort = article_settings.sort %}
                {% set limit = article_settings.limit %}
                {% set start = limit.start %}
                {% set display = article_settings.display %}
                {% set collection = grav.page.collection(
                    {
                        items: {'@taxonomy.category': filter.categories|split(' ')},
                        order: {by: sort.orderby, dir: sort.ordering},
                        url_taxonomy_filters: false
                    },
                    false) %}
                {% set total = collection.count() %}
                {% set pages = collection.slice(start, limit.total) %}

                {% for page in pages %}
                    <div class=\"g-carousel-item\">
                        <a class=\"g-carousel-link\" href=\"{{ page.url|e }}\"></a>

                        {% if display.title.enabled %}
                            <span class=\"g-carousel-title\">{{ (display.title.limit ? page.title|e|truncate_text(display.title.limit) : page.title|e)|raw }}</span>
                        {% endif %}

                        {% if display.text.type %}
                            {% set page_text = display.text.type == 'intro' ? page.summary : page.content %}
                            <span class=\"g-carousel-description\">
                                {% if display.text.formatting == 'text' %}
                                    {{ page_text|truncate_text(display.text.limit)|raw }}
                                {% else %}
                                    {{ page_text|truncate_html(display.text.limit)|raw }}
                                {% endif %}
                            </span>
                        {% endif %}

                        {% if display.readmore %}
                            <span class=\"g-carousel-readmore\">{{ display.readmore }}</span>
                        {% endif %}
                    </div>
                {% endfor %}

            {% else %}

                {# Particle Content #}
                {% for item in particle.items %}
                    <div class=\"g-carousel-item\">
                        {% if item.link is not empty %}
                            <a class=\"g-carousel-link\" href=\"{{ url(item.link)|e }}\" target=\"{{ item.linktarget|default('_self')|e }}\"></a>
                        {% endif %}

                        {% if item.icon %}
                            <span class=\"g-carousel-image-icon\">
                                <i class=\"{{ item.icon|e }}\"></i>
                            </span>
                        {% endif %}

                        {% if item.title %}
                            <span class=\"g-carousel-title\">{{ item.title|raw }}</span>
                        {% endif %}

                        {% if item.description %}
                            <span class=\"g-carousel-description\">{{ item.description|raw }}</span>
                        {% endif %}

                        {% if item.readmore %}
                            <span class=\"g-carousel-readmore\">{{ item.readmore }}</span>
                        {% endif %}
                    </div>
                {% endfor %}

            {% endif %}

        </div>
    </div>

{% endblock %}

{% block javascript_footer %}
    <script src=\"{{ url('gantry-theme://js/owlcarousel.js') }}\"></script>
    <script type=\"text/javascript\">
        jQuery(document).ready(function () {
            var owl{{ id|replace({'-':'_'}) }} = jQuery('#g-carousel-{{ id }}');
            owl{{ id|replace({'-':'_'}) }}.owlCarousel({
                items: {{ particle.displayitems }},
                center: true,
                rtl: {% if gantry.page.direction == 'rtl' %}true{% else %}false{% endif %},
                {% if particle.animateOut and particle.animateOut != 'default' %}
                animateOut: '{{ particle.animateOut|default('fadeOut')|e }}',
                {% endif %}
                {% if particle.animateIn and particle.animateIn != 'default' %}
                animateIn: '{{ particle.animateIn|default('fadeIn')|e }}',
                {% endif %}
                {% if particle.nav == 'enabled' %}
                nav: true,
                navText: ['{{ particle.prevText|default('<i class=\"fa fa-chevron-left\"></i>')|e('js') }}', '{{ particle.nextText|default('<i class=\"fa fa-chevron-right\"></i>')|e('js') }}'],
                navContainer: '#g-owlcarousel-carousel-controls-{{ id }}',
                {% else %}
                nav: false,
                {% endif %}
                dots: false,
                loop: {% if particle.loop == 'enabled' %}true{% else %}false{% endif %},
                {% if particle.autoplay == 'enabled' %}
                autoplay: true,
                autoplayTimeout: {{ particle.autoplaySpeed|default('5000') }},
                {% if particle.pauseOnHover == 'enabled' %}
                autoplayHoverPause: true,
                {% else %}
                autoplayHoverPause: false,
                {% endif %}
                {% else %}
                autoplay: false,
                {% endif %}
                responsive:{
                    0: {
                        items: 1,
                    },
                    650: {
                        items: 2,
                    },
                    800: {
                        items: 3,
                    },
                    1200: {
                        items: 4,
                    },
                    1400: {
                        items: {{ particle.displayitems }},
                    }
                },
            })
        });
    </script>
{% endblock %}
", "@particles/carousel.html.twig", "/app/web/user/themes/rt_aurora/particles/carousel.html.twig");
    }
}
