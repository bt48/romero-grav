<?php

/* @Page:/app/web/user/pages/05.contact */
class __TwigTemplate_552a05e6b153c5638170809c21793a6e1a1f5b083a40fdfd856aabd251097361 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<h2>Contact Form</h2>
";
        // line 2
        $this->loadTemplate("forms/form.html.twig", "@Page:/app/web/user/pages/05.contact", 2)->display(array_merge($context, ["form" => call_user_func_array($this->env->getFunction('forms')->getCallable(), [["route" => "/form/contact"]])]));
    }

    public function getTemplateName()
    {
        return "@Page:/app/web/user/pages/05.contact";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h2>Contact Form</h2>
{% include \"forms/form.html.twig\" with {form: forms( {route: '/form/contact'} )} %}", "@Page:/app/web/user/pages/05.contact", "");
    }
}
