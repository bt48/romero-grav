<?php

/* @particles/blockcontent.html.twig */
class __TwigTemplate_79e6ec285fe02cd12cc7a7277da2624b5a5eecada476f7c0bb8566f9fe9993ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/blockcontent.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "lightcase.init"], "method");
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_particle($context, array $blocks = [])
    {
        // line 6
        echo "
    <div class=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo "\">
        <div class=\"g-blockcontent\">
            ";
        // line 9
        if (((((($this->getAttribute(($context["particle"] ?? null), "icon", []) || $this->getAttribute(($context["particle"] ?? null), "image", [])) || $this->getAttribute(($context["particle"] ?? null), "headline", [])) || $this->getAttribute(($context["particle"] ?? null), "description", [])) || $this->getAttribute(($context["particle"] ?? null), "linktext", [])) || $this->getAttribute(($context["particle"] ?? null), "title", []))) {
            // line 10
            echo "                <div class=\"g-grid\">
                    <div class=\"g-block\">
                        <div class=\"g-content g-blockcontent-header\">
                            ";
            // line 13
            if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
                echo "<h2 class=\"g-title\">";
                echo $this->getAttribute(($context["particle"] ?? null), "title", []);
                echo "</h2>";
            }
            // line 14
            echo "                            ";
            if ($this->getAttribute(($context["particle"] ?? null), "image", [])) {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute(($context["particle"] ?? null), "image", [])), "html", null, true);
                echo "\" class=\"g-blockcontent-image\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "headline", []));
                echo "\" />";
            }
            // line 15
            echo "                            ";
            if ($this->getAttribute(($context["particle"] ?? null), "icon", [])) {
                echo "<span class=\"g-blockcontent-title-icon\"><i class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "icon", []), "html", null, true);
                echo "\"></i></span>";
            }
            // line 16
            echo "                            ";
            if ($this->getAttribute(($context["particle"] ?? null), "headline", [])) {
                echo "<h2 class=\"g-title g-blockcontent-headline\">";
                echo $this->getAttribute(($context["particle"] ?? null), "headline", []);
                echo "</h2>";
            }
            // line 17
            echo "                            ";
            if ($this->getAttribute(($context["particle"] ?? null), "description", [])) {
                echo "<div class=\"g-blockcontent-description\">";
                echo $this->getAttribute(($context["particle"] ?? null), "description", []);
                echo "</div>";
            }
            // line 18
            echo "                            ";
            if ($this->getAttribute(($context["particle"] ?? null), "linktext", [])) {
                // line 19
                echo "                                <p class=\"g-blockcontent-buttons\">
                                    <a target=\"";
                // line 20
                echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "linktarget", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "linktarget", []), "_self")) : ("_self")));
                echo "\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "link", []));
                echo "\" class=\"button ";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "linkclass", []));
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "linktext", []));
                echo "</a>
                                </p>
                            ";
            }
            // line 23
            echo "                        </div>
                    </div>
                </div>
            ";
        }
        // line 27
        echo "
            ";
        // line 28
        if ( !twig_test_empty($this->getAttribute(($context["particle"] ?? null), "subcontents", []))) {
            // line 29
            echo "                <div class=\"g-grid g-blockcontent-subcontent\">
                    ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "subcontents", []));
            foreach ($context['_seq'] as $context["_key"] => $context["subcontent"]) {
                // line 31
                echo "                        <div class=\"g-block g-blockcontent-subcontent-block ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "class", []), "html", null, true);
                echo " ";
                if ($this->getAttribute($context["subcontent"], "accent", [])) {
                    echo "g-blockcontent-subcontent-block-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "accent", []), "html", null, true);
                }
                echo "\">
                            <div class=\"g-content g-blockcontent-subcontent-block-content\">
                                ";
                // line 33
                if ($this->getAttribute($context["subcontent"], "rokboximage", [])) {
                    // line 34
                    echo "                                    <a data-rel=\"lightcase\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "rokboxcaption", []));
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["subcontent"], "rokboximage", [])), "html", null, true);
                    echo "\">
                                ";
                }
                // line 36
                echo "                                ";
                if ($this->getAttribute($context["subcontent"], "img", [])) {
                    echo "<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["subcontent"], "img", [])), "html", null, true);
                    echo "\" class=\"g-blockcontent-subcontent-img\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "subtitle", []));
                    echo "\" />";
                }
                // line 37
                echo "                                ";
                if ($this->getAttribute($context["subcontent"], "rokboximage", [])) {
                    // line 38
                    echo "                                    </a>
                                ";
                }
                // line 40
                echo "                                ";
                if ($this->getAttribute($context["subcontent"], "name", [])) {
                    // line 41
                    echo "                                    <h4 class=\"g-blockcontent-subcontent-title\">
                                        ";
                    // line 42
                    if ($this->getAttribute($context["subcontent"], "icon", [])) {
                        echo "<span class=\"g-blockcontent-subcontent-title-icon\"><i class=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "icon", []), "html", null, true);
                        echo "\"></i></span>";
                    }
                    // line 43
                    echo "                                        <span class=\"g-blockcontent-subcontent-title-text\">";
                    echo $this->getAttribute($context["subcontent"], "name", []);
                    echo "</span>
                                        ";
                    // line 44
                    if ($this->getAttribute($context["subcontent"], "subtitle", [])) {
                        echo " <span class=\"g-blockcontent-subcontent-subtitle\">";
                        echo $this->getAttribute($context["subcontent"], "subtitle", []);
                        echo "</span> ";
                    }
                    // line 45
                    echo "                                    </h4>
                                ";
                }
                // line 47
                echo "                                ";
                if ($this->getAttribute($context["subcontent"], "description", [])) {
                    echo "<div class=\"g-blockcontent-subcontent-desc\">";
                    echo $this->getAttribute($context["subcontent"], "description", []);
                    echo "</div>";
                }
                // line 48
                echo "                                ";
                if ($this->getAttribute($context["subcontent"], "button", [])) {
                    // line 49
                    echo "                                    <p class=\"g-blockcontent-buttons\">
                                        <a target=\"";
                    // line 50
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["subcontent"], "buttontarget", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["subcontent"], "buttontarget", []), "_self")) : ("_self")));
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "buttonlink", []));
                    echo "\" class=\"button ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["subcontent"], "buttonclass", []));
                    echo "\">";
                    echo $this->getAttribute($context["subcontent"], "button", []);
                    echo "</a>
                                    </p>
                                ";
                }
                // line 53
                echo "                            </div>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcontent'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "                </div>
            ";
        }
        // line 58
        echo "        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "@particles/blockcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 58,  218 => 56,  210 => 53,  198 => 50,  195 => 49,  192 => 48,  185 => 47,  181 => 45,  175 => 44,  170 => 43,  164 => 42,  161 => 41,  158 => 40,  154 => 38,  151 => 37,  142 => 36,  134 => 34,  132 => 33,  121 => 31,  117 => 30,  114 => 29,  112 => 28,  109 => 27,  103 => 23,  91 => 20,  88 => 19,  85 => 18,  78 => 17,  71 => 16,  64 => 15,  55 => 14,  49 => 13,  44 => 10,  42 => 9,  37 => 7,  34 => 6,  31 => 5,  27 => 1,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% do gantry.load('lightcase.init') %}

{% block particle %}

    <div class=\"{{ particle.class|e }}\">
        <div class=\"g-blockcontent\">
            {% if particle.icon or particle.image or particle.headline or particle.description or particle.linktext or particle.title %}
                <div class=\"g-grid\">
                    <div class=\"g-block\">
                        <div class=\"g-content g-blockcontent-header\">
                            {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}
                            {% if particle.image %}<img src=\"{{ url(particle.image) }}\" class=\"g-blockcontent-image\" alt=\"{{ particle.headline|e }}\" />{% endif %}
                            {% if particle.icon %}<span class=\"g-blockcontent-title-icon\"><i class=\"{{ particle.icon }}\"></i></span>{% endif %}
                            {% if particle.headline %}<h2 class=\"g-title g-blockcontent-headline\">{{ particle.headline|raw }}</h2>{% endif %}
                            {% if particle.description %}<div class=\"g-blockcontent-description\">{{ particle.description|raw }}</div>{% endif %}
                            {% if particle.linktext %}
                                <p class=\"g-blockcontent-buttons\">
                                    <a target=\"{{ particle.linktarget|default('_self')|e }}\" href=\"{{ particle.link|e }}\" class=\"button {{ particle.linkclass|e }}\">{{ particle.linktext|e }}</a>
                                </p>
                            {% endif %}
                        </div>
                    </div>
                </div>
            {% endif %}

            {% if particle.subcontents is not empty %}
                <div class=\"g-grid g-blockcontent-subcontent\">
                    {% for subcontent in particle.subcontents %}
                        <div class=\"g-block g-blockcontent-subcontent-block {{ subcontent.class }} {% if subcontent.accent %}g-blockcontent-subcontent-block-{{ subcontent.accent }}{% endif %}\">
                            <div class=\"g-content g-blockcontent-subcontent-block-content\">
                                {% if subcontent.rokboximage %}
                                    <a data-rel=\"lightcase\" title=\"{{ subcontent.rokboxcaption|e }}\" href=\"{{ url(subcontent.rokboximage) }}\">
                                {% endif %}
                                {% if subcontent.img %}<img src=\"{{ url(subcontent.img) }}\" class=\"g-blockcontent-subcontent-img\" alt=\"{{ subcontent.subtitle|e }}\" />{% endif %}
                                {% if subcontent.rokboximage %}
                                    </a>
                                {% endif %}
                                {% if subcontent.name %}
                                    <h4 class=\"g-blockcontent-subcontent-title\">
                                        {% if subcontent.icon %}<span class=\"g-blockcontent-subcontent-title-icon\"><i class=\"{{ subcontent.icon }}\"></i></span>{% endif %}
                                        <span class=\"g-blockcontent-subcontent-title-text\">{{ subcontent.name|raw }}</span>
                                        {% if subcontent.subtitle %} <span class=\"g-blockcontent-subcontent-subtitle\">{{ subcontent.subtitle|raw }}</span> {% endif %}
                                    </h4>
                                {% endif %}
                                {% if subcontent.description %}<div class=\"g-blockcontent-subcontent-desc\">{{ subcontent.description|raw }}</div>{% endif %}
                                {% if subcontent.button %}
                                    <p class=\"g-blockcontent-buttons\">
                                        <a target=\"{{ subcontent.buttontarget|default('_self')|e }}\" href=\"{{ subcontent.buttonlink|e }}\" class=\"button {{ subcontent.buttonclass|e }}\">{{ subcontent.button|raw }}</a>
                                    </p>
                                {% endif %}
                            </div>
                        </div>
                    {% endfor %}
                </div>
            {% endif %}
        </div>
    </div>

{% endblock %}
", "@particles/blockcontent.html.twig", "/app/web/user/themes/rt_aurora/particles/blockcontent.html.twig");
    }
}
