<?php

/* @particles/testimonials.html.twig */
class __TwigTemplate_2b072e96bfdbbcff95a97368a95e15909c4baee331a796696804bfd737bd56bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/testimonials.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "<div class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo " g-owlcarousel-testimonials\">

    ";
        // line 6
        if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
            echo "<h2 class=\"g-title\">";
            echo $this->getAttribute(($context["particle"] ?? null), "title", []);
            echo "</h2>";
        }
        // line 7
        echo "
    <div id=\"g-testimonials-";
        // line 8
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"g-testimonials owl-carousel\" data-testimonials-carousel-nav=\"";
        echo ((($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled")) ? ("true") : ("false"));
        echo "\">

        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "items", []));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 11
            echo "        <div class=\"g-testimonial\">

            ";
            // line 13
            if ($this->getAttribute($context["item"], "testimonial", [])) {
                // line 14
                echo "            <div class=\"g-testimonial-text\">";
                echo $this->getAttribute($context["item"], "testimonial", []);
                echo "</div>
            ";
            }
            // line 16
            echo "
            <div class=\"g-testimonial-info\">
                ";
            // line 18
            if ($this->getAttribute($context["item"], "image", [])) {
                // line 19
                echo "                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["item"], "image", [])));
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "author", []));
                echo "\" />
                ";
            }
            // line 21
            echo "
                ";
            // line 22
            if ($this->getAttribute($context["item"], "author", [])) {
                // line 23
                echo "                <div class=\"g-testimonial-author\">
                    ";
                // line 24
                echo $this->getAttribute($context["item"], "author", []);
                echo "

                    ";
                // line 26
                if ($this->getAttribute($context["item"], "position", [])) {
                    // line 27
                    echo "                    <div class=\"g-testimonial-position\">";
                    echo $this->getAttribute($context["item"], "position", []);
                    echo "</div>
                    ";
                }
                // line 29
                echo "                </div>
                ";
            }
            // line 31
            echo "            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
    </div>
</div>

";
    }

    // line 40
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 41
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/owlcarousel.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
jQuery(document).ready(function () {
    var owl";
        // line 44
        echo twig_escape_filter($this->env, twig_replace_filter(($context["id"] ?? null), ["-" => "_"]), "html", null, true);
        echo " = jQuery('#g-testimonials-";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "');
    owl";
        // line 45
        echo twig_escape_filter($this->env, twig_replace_filter(($context["id"] ?? null), ["-" => "_"]), "html", null, true);
        echo ".owlCarousel({
        items: 1,
        rtl: ";
        // line 47
        if (($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "page", []), "direction", []) == "rtl")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
        ";
        // line 48
        if (($this->getAttribute(($context["particle"] ?? null), "animateOut", []) && ($this->getAttribute(($context["particle"] ?? null), "animateOut", []) != "default"))) {
            // line 49
            echo "        animateOut: '";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "animateOut", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "animateOut", []), "fadeOut")) : ("fadeOut")));
            echo "',
        ";
        }
        // line 51
        echo "        ";
        if (($this->getAttribute(($context["particle"] ?? null), "animateIn", []) && ($this->getAttribute(($context["particle"] ?? null), "animateIn", []) != "default"))) {
            // line 52
            echo "        animateIn: '";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "animateIn", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "animateIn", []), "fadeIn")) : ("fadeIn")));
            echo "',
        ";
        }
        // line 54
        echo "        ";
        if (($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled")) {
            // line 55
            echo "        nav: true,
        navText: ['";
            // line 56
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "prevText", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "prevText", []), "<i class=\"fa fa-chevron-left\"></i>")) : ("<i class=\"fa fa-chevron-left\"></i>")), "js"), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "nextText", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "nextText", []), "<i class=\"fa fa-chevron-right\"></i>")) : ("<i class=\"fa fa-chevron-right\"></i>")), "js"), "html", null, true);
            echo "'],
        ";
        } else {
            // line 58
            echo "        nav: false,
        ";
        }
        // line 60
        echo "        ";
        if (($this->getAttribute(($context["particle"] ?? null), "dots", []) == "enabled")) {
            // line 61
            echo "        dots: true,
        ";
        } else {
            // line 63
            echo "        dots: false,
        ";
        }
        // line 65
        echo "        loop: ";
        if (($this->getAttribute(($context["particle"] ?? null), "loop", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
        ";
        // line 66
        if (($this->getAttribute(($context["particle"] ?? null), "autoplay", []) == "enabled")) {
            // line 67
            echo "        autoplay: true,
        autoplayTimeout: ";
            // line 68
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "autoplaySpeed", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "autoplaySpeed", []), "5000")) : ("5000")), "html", null, true);
            echo ",
        ";
            // line 69
            if (($this->getAttribute(($context["particle"] ?? null), "pauseOnHover", []) == "enabled")) {
                // line 70
                echo "        autoplayHoverPause: true,
        ";
            } else {
                // line 72
                echo "        autoplayHoverPause: false,
        ";
            }
            // line 74
            echo "        ";
        } else {
            // line 75
            echo "        autoplay: false,
        ";
        }
        // line 77
        echo "    })
});
</script>

";
    }

    public function getTemplateName()
    {
        return "@particles/testimonials.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 77,  230 => 75,  227 => 74,  223 => 72,  219 => 70,  217 => 69,  213 => 68,  210 => 67,  208 => 66,  199 => 65,  195 => 63,  191 => 61,  188 => 60,  184 => 58,  177 => 56,  174 => 55,  171 => 54,  165 => 52,  162 => 51,  156 => 49,  154 => 48,  146 => 47,  141 => 45,  135 => 44,  128 => 41,  125 => 40,  117 => 34,  109 => 31,  105 => 29,  99 => 27,  97 => 26,  92 => 24,  89 => 23,  87 => 22,  84 => 21,  76 => 19,  74 => 18,  70 => 16,  64 => 14,  62 => 13,  58 => 11,  54 => 10,  47 => 8,  44 => 7,  38 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
<div class=\"{{ particle.class|e }} g-owlcarousel-testimonials\">

    {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}

    <div id=\"g-testimonials-{{ id }}\" class=\"g-testimonials owl-carousel\" data-testimonials-carousel-nav=\"{{ particle.nav == 'enabled' ? 'true' : 'false' }}\">

        {% for item in particle.items %}
        <div class=\"g-testimonial\">

            {% if item.testimonial %}
            <div class=\"g-testimonial-text\">{{ item.testimonial|raw }}</div>
            {% endif %}

            <div class=\"g-testimonial-info\">
                {% if item.image %}
                <img src=\"{{ url(item.image)|e }}\" alt=\"{{ item.author|e }}\" />
                {% endif %}

                {% if item.author %}
                <div class=\"g-testimonial-author\">
                    {{ item.author|raw }}

                    {% if item.position %}
                    <div class=\"g-testimonial-position\">{{ item.position|raw }}</div>
                    {% endif %}
                </div>
                {% endif %}
            </div>
        </div>
        {% endfor %}

    </div>
</div>

{% endblock %}

{% block javascript_footer %}
<script src=\"{{ url('gantry-theme://js/owlcarousel.js') }}\"></script>
<script type=\"text/javascript\">
jQuery(document).ready(function () {
    var owl{{ id|replace({'-':'_'}) }} = jQuery('#g-testimonials-{{ id }}');
    owl{{ id|replace({'-':'_'}) }}.owlCarousel({
        items: 1,
        rtl: {% if gantry.page.direction == 'rtl' %}true{% else %}false{% endif %},
        {% if particle.animateOut and particle.animateOut != 'default' %}
        animateOut: '{{ particle.animateOut|default('fadeOut')|e }}',
        {% endif %}
        {% if particle.animateIn and particle.animateIn != 'default' %}
        animateIn: '{{ particle.animateIn|default('fadeIn')|e }}',
        {% endif %}
        {% if particle.nav == 'enabled' %}
        nav: true,
        navText: ['{{ particle.prevText|default('<i class=\"fa fa-chevron-left\"></i>')|e('js') }}', '{{ particle.nextText|default('<i class=\"fa fa-chevron-right\"></i>')|e('js') }}'],
        {% else %}
        nav: false,
        {% endif %}
        {% if particle.dots == 'enabled' %}
        dots: true,
        {% else %}
        dots: false,
        {% endif %}
        loop: {% if particle.loop == 'enabled' %}true{% else %}false{% endif %},
        {% if particle.autoplay == 'enabled' %}
        autoplay: true,
        autoplayTimeout: {{ particle.autoplaySpeed|default('5000') }},
        {% if particle.pauseOnHover == 'enabled' %}
        autoplayHoverPause: true,
        {% else %}
        autoplayHoverPause: false,
        {% endif %}
        {% else %}
        autoplay: false,
        {% endif %}
    })
});
</script>

{% endblock %}
", "@particles/testimonials.html.twig", "/app/web/user/themes/rt_aurora/particles/testimonials.html.twig");
    }
}
