<?php

/* @particles/verticalslider.html.twig */
class __TwigTemplate_d100afa44f16bf367ab93becc09bd78122597f9c6272c3bfd3feabe37f597a9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/verticalslider.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "    <div id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"g-verticalslider ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo "\" data-verticalslider-id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" ";
        if (($this->getAttribute(($context["particle"] ?? null), "presets", []) == "enabled")) {
            echo "data-verticalslider-presets=\"";
            echo twig_escape_filter($this->env, (twig_replace_filter((($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "theme", [], "any", false, true), "preset", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "theme", [], "any", false, true), "preset", []), "preset1")) : ("preset1")), ["preset" => ""]) - 1), "html", null, true);
            echo "\"";
        }
        echo "  data-verticalslider-height=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "height", []), "html", null, true);
        echo "\" data-verticalslider-mobileheight=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "mobileheight", []), "html", null, true);
        echo "\" data-verticalslider-speed=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "speed", []), "html", null, true);
        echo "\" data-verticalslider-rtl=\"";
        if (($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "page", []), "direction", []) == "rtl")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\" data-verticalslider-auto=\"";
        if (($this->getAttribute(($context["particle"] ?? null), "auto", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\" data-verticalslider-pause=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "pause", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "pause", []), "2000")) : ("2000")), "html", null, true);
        echo "\" data-verticalslider-loop=\"";
        if (($this->getAttribute(($context["particle"] ?? null), "loop", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\" data-verticalslider-controls=\"";
        if (($this->getAttribute(($context["particle"] ?? null), "controls", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\" data-verticalslider-pager=\"";
        if (($this->getAttribute(($context["particle"] ?? null), "pager", []) == "enabled")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\" data-verticalslider-mobile=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "config", [], "any", false, true), "get", [0 => "styles.breakpoints.mobile-menu-breakpoint"], "method", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "config", [], "any", false, true), "get", [0 => "styles.breakpoints.mobile-menu-breakpoint"], "method"), "51rem")) : ("51rem")), "html", null, true);
        echo "\">
        ";
        // line 5
        if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
            echo "<h2 class=\"g-title\">";
            echo $this->getAttribute(($context["particle"] ?? null), "title", []);
            echo "</h2>";
        }
        // line 6
        echo "
        <ul>
            ";
        // line 8
        if (($this->getAttribute(($context["particle"] ?? null), "source", []) == "grav")) {
            // line 9
            echo "
                ";
            // line 11
            echo "                ";
            $context["attr_extra"] = $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->attributeArrayFilter($this->getAttribute(($context["particle"] ?? null), "extra", []));
            // line 12
            echo "                ";
            $context["article_settings"] = $this->getAttribute(($context["particle"] ?? null), "article", []);
            // line 13
            echo "                ";
            $context["filter"] = $this->getAttribute(($context["article_settings"] ?? null), "filter", []);
            // line 14
            echo "                ";
            $context["sort"] = $this->getAttribute(($context["article_settings"] ?? null), "sort", []);
            // line 15
            echo "                ";
            $context["limit"] = $this->getAttribute(($context["article_settings"] ?? null), "limit", []);
            // line 16
            echo "                ";
            $context["start"] = $this->getAttribute(($context["limit"] ?? null), "start", []);
            // line 17
            echo "                ";
            $context["display"] = $this->getAttribute(($context["article_settings"] ?? null), "display", []);
            // line 18
            echo "                ";
            $context["collection"] = $this->getAttribute($this->getAttribute(($context["grav"] ?? null), "page", []), "collection", [0 => ["items" => ["@taxonomy.category" => twig_split_filter($this->env, $this->getAttribute(            // line 20
($context["filter"] ?? null), "categories", []), " ")], "order" => ["by" => $this->getAttribute(            // line 21
($context["sort"] ?? null), "orderby", []), "dir" => $this->getAttribute(($context["sort"] ?? null), "ordering", [])], "url_taxonomy_filters" => false], 1 => false], "method");
            // line 25
            echo "                ";
            $context["total"] = $this->getAttribute(($context["collection"] ?? null), "count", [], "method");
            // line 26
            echo "                ";
            $context["pages"] = $this->getAttribute(($context["collection"] ?? null), "slice", [0 => ($context["start"] ?? null), 1 => $this->getAttribute(($context["limit"] ?? null), "total", [])], "method");
            // line 27
            echo "
                ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["pages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 29
                echo "                    ";
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", [])) {
                    // line 30
                    echo "                        ";
                    if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", []) == "intro")) {
                        // line 31
                        echo "                            ";
                        $context["file"] = (($this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "enabled", []), false)) ? ((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "file", [])) ? ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "file", [])) : (true))) : (""));
                        // line 32
                        echo "                        ";
                    } elseif (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", []) == "full")) {
                        // line 33
                        echo "                            ";
                        $context["file"] = (($this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "enabled", []), false)) ? ((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "file", [])) ? ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "file", [])) : (true))) : (""));
                        // line 34
                        echo "                        ";
                    }
                    // line 35
                    echo "
                        ";
                    // line 36
                    $context["image"] = (((($context["file"] ?? null) === true)) ? (twig_first($this->env, $this->getAttribute($this->getAttribute($context["page"], "media", []), "images", []))) : (((($context["file"] ?? null)) ? ($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "media", []), "images", []), ($context["file"] ?? null), [], "array")) : (""))));
                    // line 37
                    echo "                    ";
                }
                // line 38
                echo "
                    <li data-thumb=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute(($context["image"] ?? null), "url", []), "html", null, true);
                echo "\" class=\"g-verticalslider-item\" style=\"background-image: url(";
                echo twig_escape_filter($this->env, $this->getAttribute(($context["image"] ?? null), "url", []), "html", null, true);
                echo ");\">
                        <div class=\"g-verticalslider-content\">
                            ";
                // line 41
                if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "enabled", []) && $this->getAttribute(($context["post"] ?? null), "title", []))) {
                    echo "<h1>";
                    echo (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", [])) ? ($this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", [])), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", []))) : (twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []))));
                    echo "</h1>";
                }
                // line 42
                echo "                            ";
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", [])) {
                    // line 43
                    echo "                                ";
                    $context["page_text"] = ((($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", []) == "intro")) ? ($this->getAttribute($context["page"], "summary", [])) : ($this->getAttribute($context["page"], "content", [])));
                    // line 44
                    echo "                                <p>
                                    ";
                    // line 45
                    if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "formatting", []) == "text")) {
                        // line 46
                        echo "                                        ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                    ";
                    } else {
                        // line 48
                        echo "                                        ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateHtml(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                    ";
                    }
                    // line 50
                    echo "                                </p>
                            ";
                }
                // line 52
                echo "                            ";
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", []), "enabled", [])) {
                    // line 53
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "url", []), "html", null, true);
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []));
                    echo "\" class=\"button ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", []), "css", []), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", [], "any", false, true), "label", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", [], "any", false, true), "label", []), "Read More...")) : ("Read More...")), "html", null, true);
                    echo "</a>
                            ";
                }
                // line 55
                echo "                        </div>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "
            ";
        } else {
            // line 60
            echo "
                ";
            // line 62
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "items", []));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 63
                echo "                    <li data-thumb=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["item"], "image", [])), "html", null, true);
                echo "\" class=\"g-verticalslider-item\" style=\"background-image: url(";
                echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["item"], "image", [])));
                echo ");\">
                        <div class=\"g-verticalslider-content\">
                            ";
                // line 65
                if ($this->getAttribute($context["item"], "small_title", [])) {
                    echo "<span>";
                    echo $this->getAttribute($context["item"], "small_title", []);
                    echo "</span>";
                }
                // line 66
                echo "
                            ";
                // line 67
                if ($this->getAttribute($context["item"], "title", [])) {
                    echo "<h1>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", []), "html", null, true);
                    echo "</h1>";
                }
                // line 68
                echo "                            ";
                if ($this->getAttribute($context["item"], "desc", [])) {
                    echo "<p>";
                    echo $this->getAttribute($context["item"], "desc", []);
                    echo "</p>";
                }
                // line 69
                echo "                            ";
                if ($this->getAttribute($context["item"], "buttontext", [])) {
                    // line 70
                    echo "                                <a target=\"";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "buttontarget", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "buttontarget", []), "_self")) : ("_self")));
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "buttonlink", []));
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "buttontext", []));
                    echo "\" class=\"button ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "buttonclass", []));
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "buttontext", []));
                    echo "</a>
                            ";
                }
                // line 72
                echo "                        </div>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "
            ";
        }
        // line 77
        echo "        </ul>
    </div>
";
    }

    // line 81
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 82
        $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery"], "method");
        // line 83
        $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "document", []), "addScript", [0 => $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/length.min.js"), 1 => 10, 2 => "footer"], "method");
        // line 84
        $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "document", []), "addScript", [0 => $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/lightslider.js"), 1 => 10, 2 => "footer"], "method");
        // line 85
        $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "document", []), "addScript", [0 => $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/tooltips.js"), 1 => 10, 2 => "footer"], "method");
        // line 86
        $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "document", []), "addScript", [0 => $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/verticalslider.init.js"), 1 => 9, 2 => "footer"], "method");
    }

    public function getTemplateName()
    {
        return "@particles/verticalslider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  321 => 86,  319 => 85,  317 => 84,  315 => 83,  313 => 82,  310 => 81,  304 => 77,  300 => 75,  292 => 72,  278 => 70,  275 => 69,  268 => 68,  262 => 67,  259 => 66,  253 => 65,  245 => 63,  240 => 62,  237 => 60,  233 => 58,  225 => 55,  213 => 53,  210 => 52,  206 => 50,  200 => 48,  194 => 46,  192 => 45,  189 => 44,  186 => 43,  183 => 42,  177 => 41,  170 => 39,  167 => 38,  164 => 37,  162 => 36,  159 => 35,  156 => 34,  153 => 33,  150 => 32,  147 => 31,  144 => 30,  141 => 29,  137 => 28,  134 => 27,  131 => 26,  128 => 25,  126 => 21,  125 => 20,  123 => 18,  120 => 17,  117 => 16,  114 => 15,  111 => 14,  108 => 13,  105 => 12,  102 => 11,  99 => 9,  97 => 8,  93 => 6,  87 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
    <div id=\"{{ id }}\" class=\"g-verticalslider {{ particle.class|e }}\" data-verticalslider-id=\"{{ id }}\" {% if particle.presets == 'enabled' %}data-verticalslider-presets=\"{{ gantry.theme.preset|default('preset1')|replace({'preset':''}) - 1 }}\"{% endif %}  data-verticalslider-height=\"{{ particle.height }}\" data-verticalslider-mobileheight=\"{{ particle.mobileheight }}\" data-verticalslider-speed=\"{{ particle.speed }}\" data-verticalslider-rtl=\"{% if gantry.page.direction == 'rtl' %}true{% else %}false{% endif %}\" data-verticalslider-auto=\"{% if particle.auto == 'enabled' %}true{% else %}false{% endif %}\" data-verticalslider-pause=\"{{ particle.pause|default('2000') }}\" data-verticalslider-loop=\"{% if particle.loop == 'enabled' %}true{% else %}false{% endif %}\" data-verticalslider-controls=\"{% if particle.controls == 'enabled' %}true{% else %}false{% endif %}\" data-verticalslider-pager=\"{% if particle.pager == 'enabled' %}true{% else %}false{% endif %}\" data-verticalslider-mobile=\"{{ gantry.config.get('styles.breakpoints.mobile-menu-breakpoint')|default('51rem') }}\">
        {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}

        <ul>
            {% if particle.source == 'grav' %}

                {# Load Content From Grav #}
                {% set attr_extra = particle.extra|attribute_array %}
                {% set article_settings = particle.article %}
                {% set filter = article_settings.filter %}
                {% set sort = article_settings.sort %}
                {% set limit = article_settings.limit %}
                {% set start = limit.start %}
                {% set display = article_settings.display %}
                {% set collection = grav.page.collection(
                    {
                        items: {'@taxonomy.category': filter.categories|split(' ')},
                        order: {by: sort.orderby, dir: sort.ordering},
                        url_taxonomy_filters: false
                    },
                    false) %}
                {% set total = collection.count() %}
                {% set pages = collection.slice(start, limit.total) %}

                {% for page in pages %}
                    {% if display.image.enabled %}
                        {% if display.image.enabled == 'intro' %}
                            {% set file = page.header.image.summary.enabled|defined(false) ? (page.header.image.summary.file ?: true) %}
                        {% elseif display.image.enabled == 'full' %}
                            {% set file = page.header.image.text.enabled|defined(false) ? (page.header.image.text.file ?: true) %}
                        {% endif %}

                        {% set image = file is same as(true) ? page.media.images|first : (file ? page.media.images[file]) %}
                    {% endif %}

                    <li data-thumb=\"{{ image.url }}\" class=\"g-verticalslider-item\" style=\"background-image: url({{ image.url }});\">
                        <div class=\"g-verticalslider-content\">
                            {% if display.title.enabled and post.title %}<h1>{{ (display.title.limit ? page.title|e|truncate_text(display.title.limit) : page.title|e)|raw }}</h1>{% endif %}
                            {% if display.text.type %}
                                {% set page_text = display.text.type == 'intro' ? page.summary : page.content %}
                                <p>
                                    {% if display.text.formatting == 'text' %}
                                        {{ page_text|truncate_text(display.text.limit)|raw }}
                                    {% else %}
                                        {{ page_text|truncate_html(display.text.limit)|raw }}
                                    {% endif %}
                                </p>
                            {% endif %}
                            {% if display.read_more.enabled %}
                                <a href=\"{{ page.url }}\" title=\"{{ page.title|e }}\" class=\"button {{ display.read_more.css }}\">{{ display.read_more.label|default('Read More...') }}</a>
                            {% endif %}
                        </div>
                    </li>
                {% endfor %}

            {% else %}

                {# Particle Content #}
                {% for item in particle.items %}
                    <li data-thumb=\"{{ url(item.image) }}\" class=\"g-verticalslider-item\" style=\"background-image: url({{ url(item.image)|e }});\">
                        <div class=\"g-verticalslider-content\">
                            {% if item.small_title %}<span>{{ item.small_title|raw }}</span>{% endif %}

                            {% if item.title %}<h1>{{ item.title }}</h1>{% endif %}
                            {% if item.desc %}<p>{{ item.desc|raw }}</p>{% endif %}
                            {% if item.buttontext %}
                                <a target=\"{{ item.buttontarget|default('_self')|e }}\" href=\"{{ item.buttonlink|e }}\" title=\"{{ item.buttontext|e }}\" class=\"button {{ item.buttonclass|e }}\">{{ item.buttontext|e }}</a>
                            {% endif %}
                        </div>
                    </li>
                {% endfor %}

            {% endif %}
        </ul>
    </div>
{% endblock %}

{% block javascript_footer %}
{% do gantry.load('jquery') %}
{% do gantry.document.addScript(url('gantry-theme://js/length.min.js'), 10, 'footer') %}
{% do gantry.document.addScript(url('gantry-theme://js/lightslider.js'), 10, 'footer') %}
{% do gantry.document.addScript(url('gantry-theme://js/tooltips.js'), 10, 'footer') %}
{% do gantry.document.addScript(url('gantry-theme://js/verticalslider.init.js'), 9, 'footer') %}
{% endblock %}
", "@particles/verticalslider.html.twig", "/app/web/user/themes/rt_aurora/particles/verticalslider.html.twig");
    }
}
