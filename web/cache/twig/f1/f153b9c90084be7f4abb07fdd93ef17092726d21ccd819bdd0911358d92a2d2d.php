<?php

/* @particles/panelslider.html.twig */
class __TwigTemplate_15d6a25536a2394b45d6f738aec2f82ef60a2df04a36670d7954ab0fab2d6ef0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/panelslider.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "    <div id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"g-panelslider ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo "\" data-panelslider-id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" data-panelslider-items=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "displayitems", []), "html", null, true);
        echo "\" data-panelslider-nav=\"";
        echo ((($this->getAttribute(($context["particle"] ?? null), "nav", []) == "enabled")) ? ("true") : ("false"));
        echo "\" data-panelslider-rtl=\"";
        if (($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "page", []), "direction", []) == "rtl")) {
            echo "true";
        } else {
            echo "false";
        }
        echo "\">
        ";
        // line 5
        if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
            echo "<h2 class=\"g-title\">";
            echo $this->getAttribute(($context["particle"] ?? null), "title", []);
            echo "</h2>";
        }
        // line 6
        echo "
        <div class=\"g-panelslider-slides g-owlcarousel owl-carousel\" data-panelslider-slides-id=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\">
            ";
        // line 8
        if (($this->getAttribute(($context["particle"] ?? null), "source", []) == "grav")) {
            // line 9
            echo "
                ";
            // line 11
            echo "                ";
            $context["attr_extra"] = $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->attributeArrayFilter($this->getAttribute(($context["particle"] ?? null), "extra", []));
            // line 12
            echo "                ";
            $context["article_settings"] = $this->getAttribute(($context["particle"] ?? null), "article", []);
            // line 13
            echo "                ";
            $context["filter"] = $this->getAttribute(($context["article_settings"] ?? null), "filter", []);
            // line 14
            echo "                ";
            $context["sort"] = $this->getAttribute(($context["article_settings"] ?? null), "sort", []);
            // line 15
            echo "                ";
            $context["limit"] = $this->getAttribute(($context["article_settings"] ?? null), "limit", []);
            // line 16
            echo "                ";
            $context["start"] = $this->getAttribute(($context["limit"] ?? null), "start", []);
            // line 17
            echo "                ";
            $context["display"] = $this->getAttribute(($context["article_settings"] ?? null), "display", []);
            // line 18
            echo "                ";
            $context["collection"] = $this->getAttribute($this->getAttribute(($context["grav"] ?? null), "page", []), "collection", [0 => ["items" => ["@taxonomy.category" => twig_split_filter($this->env, $this->getAttribute(            // line 20
($context["filter"] ?? null), "categories", []), " ")], "order" => ["by" => $this->getAttribute(            // line 21
($context["sort"] ?? null), "orderby", []), "dir" => $this->getAttribute(($context["sort"] ?? null), "ordering", [])], "url_taxonomy_filters" => false], 1 => false], "method");
            // line 25
            echo "                ";
            $context["total"] = $this->getAttribute(($context["collection"] ?? null), "count", [], "method");
            // line 26
            echo "                ";
            $context["pages"] = $this->getAttribute(($context["collection"] ?? null), "slice", [0 => ($context["start"] ?? null), 1 => $this->getAttribute(($context["limit"] ?? null), "total", [])], "method");
            // line 27
            echo "
                ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["pages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 29
                echo "                    <div class=\"panel\">
                        <div class=\"g-panelslider-slides-content-wrapper\">
                            ";
                // line 31
                if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "enabled", []) || $this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "enabled", []))) {
                    // line 32
                    echo "                                <div class=\"g-panelslider-slides-title-wrapper\">
                                    ";
                    // line 33
                    if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "enabled", [])) {
                        // line 34
                        echo "                                        <div class=\"g-panelslider-slides-title-topline\">
                                            ";
                        // line 35
                        if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "enabled", []) == "published")) {
                            // line 36
                            echo "                                                ";
                            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["page"], "publishDate", [], "method"), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "format", [])), "html", null, true);
                            echo "
                                            ";
                        } elseif (($this->getAttribute($this->getAttribute(                        // line 37
($context["display"] ?? null), "date", []), "enabled", []) == "modified")) {
                            // line 38
                            echo "                                                ";
                            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["page"], "modified", []), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "format", [])), "html", null, true);
                            echo "
                                            ";
                        } else {
                            // line 40
                            echo "                                                ";
                            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["page"], "date", []), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "date", []), "format", [])), "html", null, true);
                            echo "
                                            ";
                        }
                        // line 42
                        echo "                                        </div>
                                    ";
                    }
                    // line 44
                    echo "
                                    ";
                    // line 45
                    if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "enabled", [])) {
                        // line 46
                        echo "                                        <div class=\"g-panelslider-slides-title\">
                                            ";
                        // line 47
                        echo (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", [])) ? ($this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", [])), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", []))) : (twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []))));
                        echo "
                                        </div>
                                    ";
                    }
                    // line 50
                    echo "                                </div>
                            ";
                }
                // line 52
                echo "
                            ";
                // line 53
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", [])) {
                    // line 54
                    echo "                                ";
                    $context["page_text"] = ((($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "type", []) == "intro")) ? ($this->getAttribute($context["page"], "summary", [])) : ($this->getAttribute($context["page"], "content", [])));
                    // line 55
                    echo "                                <div class=\"g-panelslider-slides-description\">
                                    <div class=\"g-panelslider-slides-description-content\">
                                        ";
                    // line 57
                    if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "formatting", []) == "text")) {
                        // line 58
                        echo "                                            ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                        ";
                    } else {
                        // line 60
                        echo "                                            ";
                        echo $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateHtml(($context["page_text"] ?? null), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "text", []), "limit", []));
                        echo "
                                        ";
                    }
                    // line 62
                    echo "                                    </div>
                                </div>
                            ";
                }
                // line 65
                echo "                        </div>

                        ";
                // line 67
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", []), "enabled", [])) {
                    // line 68
                    echo "                            <div class=\"g-panelslider-panel-link\">
                                <a class=\"button button-3 button-white ";
                    // line 69
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", []), "css", []), "html", null, true);
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "url", []), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", [], "any", false, true), "label", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["display"] ?? null), "read_more", [], "any", false, true), "label", []), "Read More...")) : ("Read More...")), "html", null, true);
                    echo "</a>
                            </div>
                        ";
                }
                // line 72
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "
            ";
        } else {
            // line 76
            echo "
                ";
            // line 77
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "items", []));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 78
                echo "                    <div class=\"panel\">
                        <div class=\"g-panelslider-slides-content-wrapper\">
                            ";
                // line 80
                if (($this->getAttribute($context["item"], "topline", []) || $this->getAttribute($context["item"], "title", []))) {
                    // line 81
                    echo "                            <div class=\"g-panelslider-slides-title-wrapper\">
                                ";
                    // line 82
                    if ($this->getAttribute($context["item"], "topline", [])) {
                        // line 83
                        echo "                                    <div class=\"g-panelslider-slides-title-topline\">
                                        ";
                        // line 84
                        echo $this->getAttribute($context["item"], "topline", []);
                        echo "
                                    </div>
                                ";
                    }
                    // line 87
                    echo "
                                ";
                    // line 88
                    if ($this->getAttribute($context["item"], "title", [])) {
                        // line 89
                        echo "                                    <div class=\"g-panelslider-slides-title\">
                                        ";
                        // line 90
                        echo $this->getAttribute($context["item"], "title", []);
                        echo "
                                    </div>
                                ";
                    }
                    // line 93
                    echo "                            </div>
                            ";
                }
                // line 95
                echo "
                            <div class=\"g-panelslider-slides-description\">
                                <div class=\"g-panelslider-slides-description-content\">
                                    ";
                // line 98
                echo $this->getAttribute($context["item"], "description", []);
                echo "
                                </div>
                            </div>
                        </div>

                        ";
                // line 103
                if ( !twig_test_empty($this->getAttribute($context["item"], "link", []))) {
                    // line 104
                    echo "                            <div class=\"g-panelslider-panel-link\">
                                <a class=\"button button-3 button-white\" href=\"";
                    // line 105
                    echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["item"], "link", [])));
                    echo "\" target=\"";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "linktarget", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "linktarget", []), "_self")) : ("_self")));
                    echo "\">";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "linklabel", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "linklabel", []), "Read more")) : ("Read more")));
                    echo "</a>
                            </div>
                        ";
                }
                // line 108
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "
            ";
        }
        // line 112
        echo "        </div>

        <div class=\"g-panelslider-carousel g-owlcarousel owl-carousel\" data-panelslider-carousel-id=\"";
        // line 114
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\">
            ";
        // line 115
        if (($this->getAttribute(($context["particle"] ?? null), "source", []) == "grav")) {
            // line 116
            echo "
                ";
            // line 117
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["pages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 118
                echo "                    <div class=\"tab\">
                        ";
                // line 119
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", [])) {
                    // line 120
                    echo "                            ";
                    if (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", []) == "intro")) {
                        // line 121
                        echo "                                ";
                        $context["file"] = (($this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "enabled", []), false)) ? ((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "file", [])) ? ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "summary", []), "file", [])) : (true))) : (""));
                        // line 122
                        echo "                            ";
                    } elseif (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "image", []), "enabled", []) == "full")) {
                        // line 123
                        echo "                                ";
                        $context["file"] = (($this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "enabled", []), false)) ? ((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "file", [])) ? ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "header", []), "image", []), "text", []), "file", [])) : (true))) : (""));
                        // line 124
                        echo "                            ";
                    }
                    // line 125
                    echo "
                            ";
                    // line 126
                    $context["image"] = (((($context["file"] ?? null) === true)) ? (twig_first($this->env, $this->getAttribute($this->getAttribute($context["page"], "media", []), "images", []))) : (((($context["file"] ?? null)) ? ($this->getAttribute($this->getAttribute($this->getAttribute($context["page"], "media", []), "images", []), ($context["file"] ?? null), [], "array")) : (""))));
                    // line 127
                    echo "
                            <img src=\"";
                    // line 128
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["image"] ?? null), "url", []), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []));
                    echo "\">
                        ";
                }
                // line 130
                echo "
                        ";
                // line 131
                if ($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "enabled", [])) {
                    // line 132
                    echo "                            <div class=\"g-panelslider-carousel-title\">
                                ";
                    // line 133
                    echo (($this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", [])) ? ($this->env->getExtension('Gantry\Component\Twig\TwigExtension')->truncateText(twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", [])), $this->getAttribute($this->getAttribute(($context["display"] ?? null), "title", []), "limit", []))) : (twig_escape_filter($this->env, $this->getAttribute($context["page"], "title", []))));
                    echo "
                            </div>
                        ";
                }
                // line 136
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 138
            echo "
            ";
        } else {
            // line 140
            echo "
                ";
            // line 141
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "items", []));
            foreach ($context['_seq'] as $context["_key"] => $context["panel"]) {
                // line 142
                echo "                    <div class=\"tab\">
                        ";
                // line 143
                if ($this->getAttribute($context["panel"], "image", [])) {
                    // line 144
                    echo "                            <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute($context["panel"], "image", [])));
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["panel"], "title", []));
                    echo "\">
                        ";
                }
                // line 146
                echo "
                        ";
                // line 147
                if ($this->getAttribute($context["panel"], "title", [])) {
                    // line 148
                    echo "                            <div class=\"g-panelslider-carousel-title\">
                                ";
                    // line 149
                    echo $this->getAttribute($context["panel"], "title", []);
                    echo "
                            </div>
                        ";
                }
                // line 152
                echo "                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['panel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 154
            echo "
            ";
        }
        // line 156
        echo "        </div>
    </div>
";
    }

    // line 161
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 162
        $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery"], "method");
        // line 163
        $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "document", []), "addScript", [0 => $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/owlcarousel.js"), 1 => 10, 2 => "footer"], "method");
        // line 164
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/panelslider.init.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "@particles/panelslider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  445 => 164,  443 => 163,  441 => 162,  438 => 161,  432 => 156,  428 => 154,  421 => 152,  415 => 149,  412 => 148,  410 => 147,  407 => 146,  399 => 144,  397 => 143,  394 => 142,  390 => 141,  387 => 140,  383 => 138,  376 => 136,  370 => 133,  367 => 132,  365 => 131,  362 => 130,  355 => 128,  352 => 127,  350 => 126,  347 => 125,  344 => 124,  341 => 123,  338 => 122,  335 => 121,  332 => 120,  330 => 119,  327 => 118,  323 => 117,  320 => 116,  318 => 115,  314 => 114,  310 => 112,  306 => 110,  299 => 108,  289 => 105,  286 => 104,  284 => 103,  276 => 98,  271 => 95,  267 => 93,  261 => 90,  258 => 89,  256 => 88,  253 => 87,  247 => 84,  244 => 83,  242 => 82,  239 => 81,  237 => 80,  233 => 78,  229 => 77,  226 => 76,  222 => 74,  215 => 72,  205 => 69,  202 => 68,  200 => 67,  196 => 65,  191 => 62,  185 => 60,  179 => 58,  177 => 57,  173 => 55,  170 => 54,  168 => 53,  165 => 52,  161 => 50,  155 => 47,  152 => 46,  150 => 45,  147 => 44,  143 => 42,  137 => 40,  131 => 38,  129 => 37,  124 => 36,  122 => 35,  119 => 34,  117 => 33,  114 => 32,  112 => 31,  108 => 29,  104 => 28,  101 => 27,  98 => 26,  95 => 25,  93 => 21,  92 => 20,  90 => 18,  87 => 17,  84 => 16,  81 => 15,  78 => 14,  75 => 13,  72 => 12,  69 => 11,  66 => 9,  64 => 8,  60 => 7,  57 => 6,  51 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
    <div id=\"{{ id }}\" class=\"g-panelslider {{ particle.class|e }}\" data-panelslider-id=\"{{ id }}\" data-panelslider-items=\"{{ particle.displayitems }}\" data-panelslider-nav=\"{{ particle.nav == 'enabled' ? 'true' : 'false' }}\" data-panelslider-rtl=\"{% if gantry.page.direction == 'rtl' %}true{% else %}false{% endif %}\">
        {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}

        <div class=\"g-panelslider-slides g-owlcarousel owl-carousel\" data-panelslider-slides-id=\"{{ id }}\">
            {% if particle.source == 'grav' %}

                {# Load Content From Grav #}
                {% set attr_extra = particle.extra|attribute_array %}
                {% set article_settings = particle.article %}
                {% set filter = article_settings.filter %}
                {% set sort = article_settings.sort %}
                {% set limit = article_settings.limit %}
                {% set start = limit.start %}
                {% set display = article_settings.display %}
                {% set collection = grav.page.collection(
                    {
                        items: {'@taxonomy.category': filter.categories|split(' ')},
                        order: {by: sort.orderby, dir: sort.ordering},
                        url_taxonomy_filters: false
                    },
                false) %}
                {% set total = collection.count() %}
                {% set pages = collection.slice(start, limit.total) %}

                {% for page in pages %}
                    <div class=\"panel\">
                        <div class=\"g-panelslider-slides-content-wrapper\">
                            {% if display.date.enabled or display.title.enabled %}
                                <div class=\"g-panelslider-slides-title-wrapper\">
                                    {% if display.date.enabled %}
                                        <div class=\"g-panelslider-slides-title-topline\">
                                            {% if display.date.enabled == 'published' %}
                                                {{ page.publishDate()|date(display.date.format) }}
                                            {% elseif display.date.enabled == 'modified' %}
                                                {{ page.modified|date(display.date.format) }}
                                            {% else %}
                                                {{ page.date|date(display.date.format) }}
                                            {% endif %}
                                        </div>
                                    {% endif %}

                                    {% if display.title.enabled %}
                                        <div class=\"g-panelslider-slides-title\">
                                            {{ (display.title.limit ? page.title|e|truncate_text(display.title.limit) : page.title|e)|raw }}
                                        </div>
                                    {% endif %}
                                </div>
                            {% endif %}

                            {% if display.text.type %}
                                {% set page_text = display.text.type == 'intro' ? page.summary : page.content %}
                                <div class=\"g-panelslider-slides-description\">
                                    <div class=\"g-panelslider-slides-description-content\">
                                        {% if display.text.formatting == 'text' %}
                                            {{ page_text|truncate_text(display.text.limit)|raw }}
                                        {% else %}
                                            {{ page_text|truncate_html(display.text.limit)|raw }}
                                        {% endif %}
                                    </div>
                                </div>
                            {% endif %}
                        </div>

                        {% if display.read_more.enabled %}
                            <div class=\"g-panelslider-panel-link\">
                                <a class=\"button button-3 button-white {{ display.read_more.css }}\" href=\"{{ page.url }}\">{{ display.read_more.label|default('Read More...') }}</a>
                            </div>
                        {% endif %}
                    </div>
                {% endfor %}

            {% else %}

                {% for item in particle.items %}
                    <div class=\"panel\">
                        <div class=\"g-panelslider-slides-content-wrapper\">
                            {% if item.topline or item.title %}
                            <div class=\"g-panelslider-slides-title-wrapper\">
                                {% if item.topline %}
                                    <div class=\"g-panelslider-slides-title-topline\">
                                        {{ item.topline|raw }}
                                    </div>
                                {% endif %}

                                {% if item.title %}
                                    <div class=\"g-panelslider-slides-title\">
                                        {{ item.title|raw }}
                                    </div>
                                {% endif %}
                            </div>
                            {% endif %}

                            <div class=\"g-panelslider-slides-description\">
                                <div class=\"g-panelslider-slides-description-content\">
                                    {{ item.description|raw }}
                                </div>
                            </div>
                        </div>

                        {% if item.link is not empty %}
                            <div class=\"g-panelslider-panel-link\">
                                <a class=\"button button-3 button-white\" href=\"{{ url(item.link)|e }}\" target=\"{{ item.linktarget|default('_self')|e }}\">{{ item.linklabel|default('Read more')|e }}</a>
                            </div>
                        {% endif %}
                    </div>
                {% endfor %}

            {% endif %}
        </div>

        <div class=\"g-panelslider-carousel g-owlcarousel owl-carousel\" data-panelslider-carousel-id=\"{{ id }}\">
            {% if particle.source == 'grav' %}

                {% for page in pages %}
                    <div class=\"tab\">
                        {% if display.image.enabled %}
                            {% if display.image.enabled == 'intro' %}
                                {% set file = page.header.image.summary.enabled|defined(false) ? (page.header.image.summary.file ?: true) %}
                            {% elseif display.image.enabled == 'full' %}
                                {% set file = page.header.image.text.enabled|defined(false) ? (page.header.image.text.file ?: true) %}
                            {% endif %}

                            {% set image = file is same as(true) ? page.media.images|first : (file ? page.media.images[file]) %}

                            <img src=\"{{ image.url }}\" alt=\"{{ page.title|e }}\">
                        {% endif %}

                        {% if display.title.enabled %}
                            <div class=\"g-panelslider-carousel-title\">
                                {{ (display.title.limit ? page.title|e|truncate_text(display.title.limit) : page.title|e)|raw }}
                            </div>
                        {% endif %}
                    </div>
                {% endfor %}

            {% else %}

                {% for panel in particle.items %}
                    <div class=\"tab\">
                        {% if panel.image %}
                            <img src=\"{{ url(panel.image)|e }}\" alt=\"{{ panel.title|e }}\">
                        {% endif %}

                        {% if panel.title %}
                            <div class=\"g-panelslider-carousel-title\">
                                {{ panel.title|raw }}
                            </div>
                        {% endif %}
                    </div>
                {% endfor %}

            {% endif %}
        </div>
    </div>
{% endblock %}


{% block javascript_footer %}
{% do gantry.load('jquery') %}
{% do gantry.document.addScript(url('gantry-theme://js/owlcarousel.js'), 10, 'footer') %}
<script src=\"{{ url('gantry-theme://js/panelslider.init.js') }}\"></script>
{% endblock %}
", "@particles/panelslider.html.twig", "/app/web/user/themes/rt_aurora/particles/panelslider.html.twig");
    }
}
