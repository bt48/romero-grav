<?php

/* @particles/logo.html.twig */
class __TwigTemplate_0b4b0d78ab545704dd47056fcb2b6bcadf95b8ccb41e8289024816f3f80ab88d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/logo.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "    ";
        $context["url"] = _twig_default_filter($this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute(($context["particle"] ?? null), "url", [])), $this->getAttribute(($context["gantry"] ?? null), "siteUrl", [], "method"));
        // line 5
        echo "    ";
        if ((($context["url"] ?? null) == $this->getAttribute(($context["gantry"] ?? null), "siteUrl", [], "method"))) {
            $context["rel"] = "rel=\"home\"";
        }
        // line 6
        echo "    ";
        $context["class"] = (($this->getAttribute(($context["particle"] ?? null), "class", [])) ? ((("class=\"" . $this->getAttribute(($context["particle"] ?? null), "class", [])) . "\"")) : (""));
        // line 7
        echo "    ";
        $context["image"] = $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute(($context["particle"] ?? null), "image", []));
        // line 8
        echo "    
    ";
        // line 9
        if (($this->getAttribute(($context["particle"] ?? null), "link", []) == true)) {
            // line 10
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" target=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "target", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "target", []), "_self")) : ("_self")), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "text", []), "html", null, true);
            echo "\" ";
            echo (((isset($context["rel"]) || array_key_exists("rel", $context))) ? (_twig_default_filter(($context["rel"] ?? null), "")) : (""));
            echo " ";
            echo (((isset($context["class"]) || array_key_exists("class", $context))) ? (_twig_default_filter(($context["class"] ?? null), "")) : (""));
            echo ">
    ";
        } else {
            // line 11
            echo "<div ";
            echo (((isset($context["class"]) || array_key_exists("class", $context))) ? (_twig_default_filter(($context["class"] ?? null), "")) : (""));
            echo ">";
        }
        // line 12
        echo "        ";
        if ( !twig_test_empty($this->getAttribute(($context["particle"] ?? null), "svg", []))) {
            // line 13
            echo "            ";
            echo $this->getAttribute(($context["particle"] ?? null), "svg", []);
            echo "
        ";
        } elseif (        // line 14
($context["image"] ?? null)) {
            // line 15
            echo "            <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc($this->getAttribute(($context["particle"] ?? null), "image", [])), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "text", []), "html", null, true);
            echo "\" />
        ";
        } else {
            // line 17
            echo "            ";
            echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "text", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "text", []), "Logo")) : ("Logo")), "html", null, true);
            echo "
        ";
        }
        // line 19
        echo "    ";
        if (($this->getAttribute(($context["particle"] ?? null), "link", []) == true)) {
            echo "</a>";
        } else {
            echo "</div>";
        }
    }

    public function getTemplateName()
    {
        return "@particles/logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 19,  87 => 17,  79 => 15,  77 => 14,  72 => 13,  69 => 12,  64 => 11,  50 => 10,  48 => 9,  45 => 8,  42 => 7,  39 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
    {% set url = url(particle.url)|default(gantry.siteUrl()) %}
    {% if (url == gantry.siteUrl()) %}{% set rel='rel=\"home\"' %}{% endif %}
    {% set class=(particle.class ? 'class=\"'~ particle.class ~'\"') %}
    {% set image = url(particle.image) %}
    
    {% if particle.link == true %}
        <a href=\"{{ url }}\" target=\"{{ particle.target|default('_self') }}\" title=\"{{ particle.text }}\" {{ rel|default('')|raw }} {{ class|default('')|raw }}>
    {% else %}<div {{ class|default('')|raw }}>{% endif %}
        {% if particle.svg is not empty %}
            {{ particle.svg|raw }}
        {% elseif image %}
            <img src=\"{{ url(particle.image) }}\" alt=\"{{ particle.text }}\" />
        {% else %}
            {{ particle.text|default('Logo') }}
        {% endif %}
    {% if particle.link == true %}</a>{% else %}</div>{% endif %}
{% endblock %}
", "@particles/logo.html.twig", "/app/web/user/plugins/gantry5/engines/nucleus/particles/logo.html.twig");
    }
}
