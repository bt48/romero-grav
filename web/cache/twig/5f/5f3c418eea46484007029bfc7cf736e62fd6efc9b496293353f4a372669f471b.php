<?php

/* @Page:/app/web/user/pages/03.pages/08.offline */
class __TwigTemplate_96ce52cad541859c0be2e160557b90d603014da5e716ce02eb41166de5cf9080 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p style=\"text-align: center;\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-media://rocketlauncher/pages/offline/img-01.jpg"), "html", null, true);
        echo "\" alt=\"image\" /></p>";
    }

    public function getTemplateName()
    {
        return "@Page:/app/web/user/pages/03.pages/08.offline";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<p style=\"text-align: center;\"><img src=\"{{ url('gantry-media://rocketlauncher/pages/offline/img-01.jpg') }}\" alt=\"image\" /></p>", "@Page:/app/web/user/pages/03.pages/08.offline", "");
    }
}
