<?php

/* @particles/simplecounter.html.twig */
class __TwigTemplate_9e1406665d1addfe910dd393d01606d14fb0e1b292f4baf6f104413253c719e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/simplecounter.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_particle($context, array $blocks = [])
    {
        // line 4
        echo "<div class=\"g-simplecounter ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo "\">

    ";
        // line 6
        if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
            echo "<h2 class=\"g-title\">";
            echo $this->getAttribute(($context["particle"] ?? null), "title", []);
            echo "</h2>";
        }
        // line 7
        echo "
    <div class=\"g-simplecounter-content\">
        ";
        // line 9
        echo $this->getAttribute(($context["particle"] ?? null), "desc", []);
        echo "
    </div>

    <div class=\"g-simplecounter-block\">
        <div class=\"g-simplecounter-calendar\" id=\"g-";
        // line 13
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" data-simplecounter-id=\"g-";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" data-countdown=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "year", []), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, ($this->getAttribute(($context["particle"] ?? null), "month", []) + 1), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "date", []), "html", null, true);
        echo "\"  data-simplecounter-daytext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "daytext", []), "html", null, true);
        echo "\" data-simplecounter-daystext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "daystext", []), "html", null, true);
        echo "\" data-simplecounter-hourtext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "hourtext", []), "html", null, true);
        echo "\" data-simplecounter-hourstext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "hourstext", []), "html", null, true);
        echo "\" data-simplecounter-minutetext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "minutetext", []), "html", null, true);
        echo "\" data-simplecounter-minutestext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "minutestext", []), "html", null, true);
        echo "\" data-simplecounter-secondtext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "secondtext", []), "html", null, true);
        echo "\" data-simplecounter-secondstext=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "secondstext", []), "html", null, true);
        echo "\"></div>
    </div>
</div>

";
    }

    // line 20
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 21
        $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery"], "method");
        // line 22
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/simplecounter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/simplecounter.init.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "@particles/simplecounter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 23,  93 => 22,  91 => 21,  88 => 20,  55 => 13,  48 => 9,  44 => 7,  38 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% block particle %}
<div class=\"g-simplecounter {{ particle.class|e }}\">

    {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}

    <div class=\"g-simplecounter-content\">
        {{ particle.desc|raw }}
    </div>

    <div class=\"g-simplecounter-block\">
        <div class=\"g-simplecounter-calendar\" id=\"g-{{ id }}\" data-simplecounter-id=\"g-{{ id }}\" data-countdown=\"{{ particle.year }}/{{ particle.month + 1 }}/{{ particle.date }}\"  data-simplecounter-daytext=\"{{ particle.daytext }}\" data-simplecounter-daystext=\"{{ particle.daystext }}\" data-simplecounter-hourtext=\"{{ particle.hourtext }}\" data-simplecounter-hourstext=\"{{ particle.hourstext }}\" data-simplecounter-minutetext=\"{{ particle.minutetext }}\" data-simplecounter-minutestext=\"{{ particle.minutestext }}\" data-simplecounter-secondtext=\"{{ particle.secondtext }}\" data-simplecounter-secondstext=\"{{ particle.secondstext }}\"></div>
    </div>
</div>

{% endblock %}


{% block javascript_footer %}
{% do gantry.load('jquery') %}
<script src=\"{{ url('gantry-theme://js/simplecounter.js') }}\"></script>
<script src=\"{{ url('gantry-theme://js/simplecounter.init.js') }}\"></script>
{% endblock %}
", "@particles/simplecounter.html.twig", "/app/web/user/themes/rt_aurora/particles/simplecounter.html.twig");
    }
}
