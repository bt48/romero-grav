<?php

/* @particles/calendar.html.twig */
class __TwigTemplate_b0a88884cc036888154b2cbc5080938119fe1355d351045077d0be0524962ab9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/partials/particle.html.twig", "@particles/calendar.html.twig", 1);
        $this->blocks = [
            'particle' => [$this, 'block_particle'],
            'javascript_footer' => [$this, 'block_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/partials/particle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 3
        $context["language"] = $this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "page", []), "language", []);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_particle($context, array $blocks = [])
    {
        // line 6
        echo "    <div class=\"g-calendar-particle ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["particle"] ?? null), "class", []));
        echo "\">

        ";
        // line 8
        if ($this->getAttribute(($context["particle"] ?? null), "title", [])) {
            echo "<h2 class=\"g-title\">";
            echo $this->getAttribute(($context["particle"] ?? null), "title", []);
            echo "</h2>";
        }
        // line 9
        echo "
        <div id=\"g-";
        // line 10
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"g-calendar\">
            <script id=\"g-";
        // line 11
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-flux-clndr-template\" type=\"text/template\">

                <div class=\"controls\">
                    <div class=\"clndr-previous-button\"><i class=\"fa fa-fw fa-arrow-circle-left\"></i></div><div class=\"month-year\"><%= month %> <%= year %></div><div class=\"clndr-next-button\"><i class=\"fa fa-fw fa-arrow-circle-right\"></i></div>
                </div>

                <div class=\"days-container\">
                    <div class=\"days\">
                        <div class=\"headers\">
                            <% _.each(daysOfTheWeek, function(day) { %><div class=\"day-header\"><%= day %></div><% }); %>
                        </div>
                        <% _.each(days, function(day) { %><div class=\"<%= day.classes %>\" id=\"<%= day.id %>\"><%= day.day %></div><% }); %>
                    </div>
                    <div class=\"events\">
                        <div class=\"headers\">
                            <div class=\"x-button\"><i class=\"fa fa-fw fa-close\"></i></div>
                            <div class=\"event-header\">";
        // line 27
        echo twig_escape_filter($this->env, (($this->getAttribute(($context["particle"] ?? null), "eventsheader", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["particle"] ?? null), "eventsheader", []), "Events")) : ("Events")), "html", null, true);
        echo "</div>
                        </div>
                        <div class=\"events-list\">
                            <% _.each(eventsThisMonth, function(event) { %>
                            <div id=\"<%= event.id %>\" class=\"event hidden\">
                                <%
                                    var date_format = 'MMMM Do';
                                    var event_dates = moment(event.startDate).format(date_format);
                                    if (event.endDate !== '' && event.endDate !== event.startDate) {
                                        event_dates += ' - ' + moment(event.endDate).format(date_format);
                                    }
                                %>
                                <% if (event.url !== '') { %>
                                <a target=\"<%= event.target %>\" class=\"event-link\" href=\"<%= event.url %>\"><%= event_dates %>: <%= event.title %></a>
                                <% } else { %>
                                <%= event_dates %>: <%= event.title %>
                                <% } %>
                                <span class=\"event-desc\"><%= event.desc %></span>
                            </div>
                            <% }); %>
                        </div>
                    </div>
                </div>

            </script>
        </div>
    </div>
";
    }

    // line 56
    public function block_javascript_footer($context, array $blocks = [])
    {
        // line 57
        echo "    ";
        $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery"], "method");
        // line 58
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/moment-with-locales.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Gantry\Component\Twig\TwigExtension')->urlFunc("gantry-theme://js/clndr.js"), "html", null, true);
        echo "\"></script>
    <script>
        jQuery(document).ready(function () {
            moment.locale('";
        // line 62
        echo twig_escape_filter($this->env, ($context["language"] ?? null), "html", null, true);
        echo "');
            var weekdayNames = Array.apply(null, Array(7)).map(
                function (_, i) {
                    return moment(i, 'e').format('ddd');
                });

            var events = [
                ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["particle"] ?? null), "events", []));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 70
            echo "                    { id: 'g-";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "-event-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", []), "html", null, true);
            echo "', startDate: '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "begin", []), "html", null, true);
            echo "', endDate: '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "end", []), "html", null, true);
            echo "', title: '";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute($context["event"], "title", []), "js"), "html", null, true);
            echo "', url: '";
            echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "link", []));
            echo "', target: '";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["event"], "target", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["event"], "target", []), "_blank")) : ("_blank")), "html", null, true);
            echo "', desc: '";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute($context["event"], "description", []), "js"), "html", null, true);
            echo "' },
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "            ];

            jQuery(\"#g-";
        // line 74
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").clndr({
                template: jQuery('#g-";
        // line 75
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-flux-clndr-template').html(),
                moment: moment,
                daysOfTheWeek: weekdayNames,
                adjacentDaysChangeMonth: true,
                events: events,
                multiDayEvents: {
                    singleDay: 'startDate',
                    endDate: 'endDate',
                    startDate: 'startDate'
                },
                clickEvents: {
                    click: function(target) {
                        if(target.events.length) {
                            var eventContainer;
                            var daysContainer = jQuery(\"#g-";
        // line 89
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").find('.days-container');
                            daysContainer.toggleClass('show-events', true);

                            target.events.forEach(function(event) {
                                eventContainer = jQuery(\"#g-";
        // line 93
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").find('#' + event.id);
                                eventContainer.removeClass('hidden', false);
                            });

                            jQuery(\"#g-";
        // line 97
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").find('.x-button').click( function() {
                                daysContainer.toggleClass('show-events', false);

                                target.events.forEach(function(event) {
                                    eventContainer = jQuery(\"#g-";
        // line 101
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").find('#' + event.id);
                                    eventContainer.addClass('hidden', false);
                                });
                            });
                        }
                    }
                },
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "@particles/calendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 101,  219 => 97,  212 => 93,  205 => 89,  188 => 75,  184 => 74,  180 => 72,  149 => 70,  132 => 69,  122 => 62,  116 => 59,  111 => 58,  108 => 57,  105 => 56,  73 => 27,  54 => 11,  50 => 10,  47 => 9,  41 => 8,  35 => 6,  32 => 5,  28 => 1,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@nucleus/partials/particle.html.twig' %}

{% set language = gantry.page.language %}

{% block particle %}
    <div class=\"g-calendar-particle {{ particle.class|e }}\">

        {% if particle.title %}<h2 class=\"g-title\">{{ particle.title|raw }}</h2>{% endif %}

        <div id=\"g-{{ id }}\" class=\"g-calendar\">
            <script id=\"g-{{ id }}-flux-clndr-template\" type=\"text/template\">

                <div class=\"controls\">
                    <div class=\"clndr-previous-button\"><i class=\"fa fa-fw fa-arrow-circle-left\"></i></div><div class=\"month-year\"><%= month %> <%= year %></div><div class=\"clndr-next-button\"><i class=\"fa fa-fw fa-arrow-circle-right\"></i></div>
                </div>

                <div class=\"days-container\">
                    <div class=\"days\">
                        <div class=\"headers\">
                            <% _.each(daysOfTheWeek, function(day) { %><div class=\"day-header\"><%= day %></div><% }); %>
                        </div>
                        <% _.each(days, function(day) { %><div class=\"<%= day.classes %>\" id=\"<%= day.id %>\"><%= day.day %></div><% }); %>
                    </div>
                    <div class=\"events\">
                        <div class=\"headers\">
                            <div class=\"x-button\"><i class=\"fa fa-fw fa-close\"></i></div>
                            <div class=\"event-header\">{{ particle.eventsheader|default('Events') }}</div>
                        </div>
                        <div class=\"events-list\">
                            <% _.each(eventsThisMonth, function(event) { %>
                            <div id=\"<%= event.id %>\" class=\"event hidden\">
                                <%
                                    var date_format = 'MMMM Do';
                                    var event_dates = moment(event.startDate).format(date_format);
                                    if (event.endDate !== '' && event.endDate !== event.startDate) {
                                        event_dates += ' - ' + moment(event.endDate).format(date_format);
                                    }
                                %>
                                <% if (event.url !== '') { %>
                                <a target=\"<%= event.target %>\" class=\"event-link\" href=\"<%= event.url %>\"><%= event_dates %>: <%= event.title %></a>
                                <% } else { %>
                                <%= event_dates %>: <%= event.title %>
                                <% } %>
                                <span class=\"event-desc\"><%= event.desc %></span>
                            </div>
                            <% }); %>
                        </div>
                    </div>
                </div>

            </script>
        </div>
    </div>
{% endblock %}

{% block javascript_footer %}
    {% do gantry.load('jquery') %}
    <script src=\"{{ url('gantry-theme://js/moment-with-locales.min.js') }}\"></script>
    <script src=\"{{ url('gantry-theme://js/clndr.js') }}\"></script>
    <script>
        jQuery(document).ready(function () {
            moment.locale('{{ language }}');
            var weekdayNames = Array.apply(null, Array(7)).map(
                function (_, i) {
                    return moment(i, 'e').format('ddd');
                });

            var events = [
                {% for event in particle.events %}
                    { id: 'g-{{ id }}-event-{{ loop.index }}', startDate: '{{ event.begin }}', endDate: '{{ event.end }}', title: '{{ event.title|e('js') }}', url: '{{ event.link|e }}', target: '{{ event.target|default('_blank') }}', desc: '{{ event.description|e('js') }}' },
                {% endfor %}
            ];

            jQuery(\"#g-{{ id }}\").clndr({
                template: jQuery('#g-{{ id }}-flux-clndr-template').html(),
                moment: moment,
                daysOfTheWeek: weekdayNames,
                adjacentDaysChangeMonth: true,
                events: events,
                multiDayEvents: {
                    singleDay: 'startDate',
                    endDate: 'endDate',
                    startDate: 'startDate'
                },
                clickEvents: {
                    click: function(target) {
                        if(target.events.length) {
                            var eventContainer;
                            var daysContainer = jQuery(\"#g-{{ id }}\").find('.days-container');
                            daysContainer.toggleClass('show-events', true);

                            target.events.forEach(function(event) {
                                eventContainer = jQuery(\"#g-{{ id }}\").find('#' + event.id);
                                eventContainer.removeClass('hidden', false);
                            });

                            jQuery(\"#g-{{ id }}\").find('.x-button').click( function() {
                                daysContainer.toggleClass('show-events', false);

                                target.events.forEach(function(event) {
                                    eventContainer = jQuery(\"#g-{{ id }}\").find('#' + event.id);
                                    eventContainer.addClass('hidden', false);
                                });
                            });
                        }
                    }
                },
            });
        });
    </script>
{% endblock %}
", "@particles/calendar.html.twig", "/app/web/user/themes/rt_aurora/particles/calendar.html.twig");
    }
}
