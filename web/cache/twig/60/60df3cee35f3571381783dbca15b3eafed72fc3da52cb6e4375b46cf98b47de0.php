<?php

/* @particles/frameworks.html.twig */
class __TwigTemplate_9bd18e5c525eace5c1e7ec6c74d5725b5929dd3d8782c0eb5b65e11d3e8870b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "jquery", []), "enabled", [])) {
            // line 2
            echo "    ";
            $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery"], "method");
            // line 3
            echo "    ";
            if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "jquery", []), "ui_core", [])) {
                // line 4
                echo "        ";
                $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery.ui.core"], "method");
                // line 5
                echo "    ";
            }
            // line 6
            echo "    ";
            if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "jquery", []), "ui_sortable", [])) {
                // line 7
                echo "        ";
                $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "jquery.ui.sortable"], "method");
                // line 8
                echo "    ";
            }
        }
        // line 10
        echo "
";
        // line 11
        if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "bootstrap3", []), "enabled", [])) {
            // line 12
            echo "    ";
            $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "bootstrap.3"], "method");
        } elseif ($this->getAttribute($this->getAttribute(        // line 13
($context["particle"] ?? null), "bootstrap2", []), "enabled", [])) {
            // line 14
            echo "    ";
            $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "bootstrap.2"], "method");
        }
        // line 16
        echo "
";
        // line 17
        if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "mootools", []), "enabled", [])) {
            // line 18
            echo "    ";
            $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "mootools"], "method");
            // line 19
            echo "    ";
            if ($this->getAttribute($this->getAttribute(($context["particle"] ?? null), "mootools", []), "more", [])) {
                // line 20
                echo "        ";
                $this->getAttribute(($context["gantry"] ?? null), "load", [0 => "mootools.more"], "method");
                // line 21
                echo "    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@particles/frameworks.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 21,  68 => 20,  65 => 19,  62 => 18,  60 => 17,  57 => 16,  53 => 14,  51 => 13,  48 => 12,  46 => 11,  43 => 10,  39 => 8,  36 => 7,  33 => 6,  30 => 5,  27 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if particle.jquery.enabled %}
    {% do gantry.load('jquery') %}
    {% if particle.jquery.ui_core %}
        {% do gantry.load('jquery.ui.core') %}
    {% endif %}
    {% if particle.jquery.ui_sortable %}
        {% do gantry.load('jquery.ui.sortable') %}
    {% endif %}
{% endif %}

{% if particle.bootstrap3.enabled %}
    {% do gantry.load('bootstrap.3') %}
{% elseif particle.bootstrap2.enabled %}
    {% do gantry.load('bootstrap.2') %}
{% endif %}

{% if particle.mootools.enabled %}
    {% do gantry.load('mootools') %}
    {% if particle.mootools.more %}
        {% do gantry.load('mootools.more') %}
    {% endif %}
{% endif %}
", "@particles/frameworks.html.twig", "/app/web/user/plugins/gantry5/engines/nucleus/particles/frameworks.html.twig");
    }
}
