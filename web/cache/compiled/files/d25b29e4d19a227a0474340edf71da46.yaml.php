<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/web/system/blueprints/config/streams.yaml',
    'modified' => 1548416594,
    'data' => [
        'title' => 'PLUGIN_ADMIN.FILE_STREAMS',
        'form' => [
            'validation' => 'loose',
            'hidden' => true,
            'fields' => [
                'schemes.xxx' => [
                    'type' => 'array'
                ]
            ]
        ]
    ]
];
