<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://pagination/pagination.yaml',
    'modified' => 1548871668,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'delta' => 0
    ]
];
