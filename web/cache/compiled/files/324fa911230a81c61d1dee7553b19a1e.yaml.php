<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/accounts/feargalokane.yaml',
    'modified' => 1552943165,
    'data' => [
        'email' => 'feargal@bt48.com',
        'fullname' => 'Feargal O\'Kane',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$tJZ/Saq1bDr8vTqkl7j.SeV3HvqXl2Ms.HJ41Eh05qe.Lb2vsjGwm'
    ]
];
