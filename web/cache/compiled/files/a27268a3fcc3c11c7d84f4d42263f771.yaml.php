<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/plugins/admin/languages/tlh.yaml',
    'modified' => 1548871664,
    'data' => [
        'PLUGIN_ADMIN' => [
            'LOGIN_BTN_FORGOT' => 'lIj',
            'BACK' => 'chap',
            'NORMAL' => 'motlh',
            'YES' => 'HIja\'',
            'NO' => 'Qo\'',
            'DISABLED' => 'Qotlh'
        ]
    ]
];
