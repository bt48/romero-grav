<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/blueprints.yaml',
    'modified' => 1548871658,
    'data' => [
        'name' => 'Aurora',
        'version' => '1.1.2',
        'description' => 'Aurora theme',
        'icon' => 'tint',
        'author' => [
            'name' => 'RocketTheme',
            'email' => 'support@rockettheme.com',
            'url' => 'http://www.rockettheme.com'
        ],
        'homepage' => 'http://www.rockettheme.com/grav/themes/aurora',
        'keywords' => 'rockettheme, gantry5, theme',
        'bugs' => 'http://www.rockettheme.com/forum/grav-theme-aurora',
        'license' => 'RocketTheme License',
        'form' => [
            'validation' => 'loose',
            'fields' => NULL
        ]
    ]
];
