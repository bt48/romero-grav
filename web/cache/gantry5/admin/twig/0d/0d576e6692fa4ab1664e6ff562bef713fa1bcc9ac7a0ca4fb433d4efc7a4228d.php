<?php

/* forms/fields/input/section-variations.html.twig */
class __TwigTemplate_479b56e9b9df021307772e573e5b466cab58c887d960caac865e50c6962797b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("forms/fields/input/selectize.html.twig", "forms/fields/input/section-variations.html.twig", 1);
        $this->blocks = [
            'global_attributes' => [$this, 'block_global_attributes'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "forms/fields/input/selectize.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_global_attributes($context, array $blocks = [])
    {
        // line 4
        echo "    ";
        $context["variations"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["gantry"] ?? null), "theme", []), "details", []), "configuration", []), "section-variations", [], "array");
        // line 5
        echo "    ";
        $context["Options"] = $this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", []), "Options", []);
        // line 6
        echo "    ";
        $context["Optgroups"] = $this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", []), "Optgroups", []);
        // line 7
        echo "    ";
        $context["options"] = [];
        // line 8
        echo "    ";
        $context["optgroups"] = [];
        // line 9
        echo "    ";
        if (($context["variations"] ?? null)) {
            // line 10
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["variations"] ?? null));
            foreach ($context['_seq'] as $context["key"] => $context["text"]) {
                // line 11
                echo "            ";
                if (twig_test_iterable($context["text"])) {
                    // line 12
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["text"]);
                    foreach ($context['_seq'] as $context["item_key"] => $context["item_text"]) {
                        // line 13
                        echo "                    ";
                        $context["options"] = twig_array_merge(($context["options"] ?? null), [0 => ["optgroup" => $context["key"], "value" => $context["item_key"], "text" => $context["item_text"]]]);
                        // line 14
                        echo "                    ";
                        if (!twig_in_filter($context["key"], ($context["optgroups"] ?? null))) {
                            $context["optgroups"] = twig_array_merge(($context["optgroups"] ?? null), [0 => ["value" => $context["key"], "label" => $context["key"]]]);
                        }
                        // line 15
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['item_key'], $context['item_text'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 16
                    echo "            ";
                } else {
                    // line 17
                    echo "                ";
                    $context["options"] = twig_array_merge(($context["options"] ?? null), [0 => ["value" => $context["key"], "text" => $context["text"]]]);
                    // line 18
                    echo "            ";
                }
                // line 19
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['text'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "

        ";
            // line 22
            $context["field"] = twig_array_merge(twig_array_merge(twig_array_merge(($context["field"] ?? null), (($this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", [], "any", false, true), "Options", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", [], "any", false, true), "Options", []), [])) : ([]))), (($this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", [], "any", false, true), "Optgroups", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["field"] ?? null), "selectize", [], "any", false, true), "Optgroups", []), [])) : ([]))), ["selectize" => ["Options" => ($context["options"] ?? null), "Optgroups" => ($context["optgroups"] ?? null), "Subtitles" => true]]);
            // line 23
            echo "    ";
        }
        // line 24
        echo "
    data-selectize=\"";
        // line 25
        echo (($this->getAttribute(($context["field"] ?? null), "selectize", [], "any", true, true)) ? (twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute(($context["field"] ?? null), "selectize", [])), "html_attr")) : (""));
        echo "\"
    ";
        // line 26
        $this->displayParentBlock("global_attributes", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "forms/fields/input/section-variations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 26,  103 => 25,  100 => 24,  97 => 23,  95 => 22,  91 => 20,  85 => 19,  82 => 18,  79 => 17,  76 => 16,  70 => 15,  65 => 14,  62 => 13,  57 => 12,  54 => 11,  49 => 10,  46 => 9,  43 => 8,  40 => 7,  37 => 6,  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "forms/fields/input/section-variations.html.twig", "/app/web/user/plugins/gantry5/admin/templates/forms/fields/input/section-variations.html.twig");
    }
}
