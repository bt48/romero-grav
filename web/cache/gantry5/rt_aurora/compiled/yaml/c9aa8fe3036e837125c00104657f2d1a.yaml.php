<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'default',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1509009075
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'sidebar' => 'Sidebar',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'mainbar' => 'Main',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9828' => 'System Messages'
            ],
            'logo' => [
                'logo-5992' => 'Logo',
                'logo-3146' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-2350' => 'Menu'
            ],
            'position' => [
                'position-position-9414' => 'Sidebar',
                'position-position-8807' => 'Aside'
            ],
            'simplecontent' => [
                'simplecontent-5244' => 'contact module',
                'simplecontent-8236' => 'Simple Content',
                'simplecontent-4905' => 'Simple Content'
            ],
            'branding' => [
                'branding-1506' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-7951' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
