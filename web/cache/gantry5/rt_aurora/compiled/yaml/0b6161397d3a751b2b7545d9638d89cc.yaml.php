<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'class' => '',
        'title' => '',
        'animateOut' => 'fadeOut',
        'animateIn' => 'fadeIn',
        'nav' => 'disabled',
        'dots' => 'enabled',
        'loop' => 'enabled',
        'autoplay' => 'disabled',
        'autoplaySpeed' => '',
        'pauseOnHover' => 'enabled',
        'items' => [
            
        ]
    ]
];
