<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
    'modified' => 1548871662,
    'data' => [
        'enabled' => '1',
        'link' => '1',
        'format' => 'j F Y',
        'prefix' => 'Published:'
    ]
];
