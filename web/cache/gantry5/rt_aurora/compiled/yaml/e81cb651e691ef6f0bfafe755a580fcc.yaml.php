<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/plugins/gantry5/engines/nucleus/admin/blueprints/layout/inheritance/messages/inherited.yaml',
    'modified' => 1544785254,
    'data' => [
        'name' => 'Inheritance',
        'description' => 'Inherited tab',
        'type' => 'inherited.inheritance',
        'form' => [
            'fields' => [
                '_note' => [
                    'type' => 'separator.note',
                    'class' => 'alert alert-success blocksize-note',
                    'content' => 'This %s has been inherited by the following Outlines: %s'
                ]
            ]
        ]
    ]
];
