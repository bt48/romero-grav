<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/gantry/theme.yaml',
    'modified' => 1548871658,
    'data' => [
        'details' => [
            'name' => 'Aurora',
            'version' => '1.1.2',
            'icon' => 'paper-plane',
            'date' => 'January 30, 2019',
            'author' => [
                'name' => 'RocketTheme, LLC',
                'email' => 'support@rockettheme.com',
                'link' => 'http://www.rockettheme.com'
            ],
            'documentation' => [
                'link' => 'http://docs.gantry.org/gantry5'
            ],
            'support' => [
                'link' => 'https://gitter.im/gantry/gantry5'
            ],
            'updates' => [
                'link' => 'http://updates.rockettheme.com/themes/aurora.yaml'
            ],
            'copyright' => '(C) 2007 - 2018 RocketTheme, LLC. All rights reserved.',
            'license' => 'GPLv2',
            'description' => 'Aurora Theme',
            'images' => [
                'thumbnail' => 'admin/images/preset1.png',
                'preview' => 'admin/images/preset1.png'
            ]
        ],
        'configuration' => [
            'gantry' => [
                'platform' => 'grav',
                'engine' => 'nucleus'
            ],
            'theme' => [
                'parent' => 'rt_aurora',
                'base' => 'gantry-theme://common',
                'file' => 'gantry-theme://include/theme.php',
                'class' => '\\Gantry\\Framework\\Theme'
            ],
            'fonts' => [
                'muli' => [
                    400 => 'gantry-theme://fonts/muli/muli-regular/muli-regular-webfont',
                    '400italic' => 'gantry-theme://fonts/muli/muli-italic/muli-italic-webfont',
                    300 => 'gantry-theme://fonts/muli/muli-light/muli-light-webfont',
                    '300italic' => 'gantry-theme://fonts/muli/muli-lightitalic/muli-lightitalic-webfont'
                ]
            ],
            'css' => [
                'compiler' => '\\Gantry\\Component\\Stylesheet\\ScssCompiler',
                'target' => 'gantry-theme://css-compiled',
                'paths' => [
                    0 => 'gantry-theme://scss',
                    1 => 'gantry-engine://scss'
                ],
                'files' => [
                    0 => 'aurora',
                    1 => 'aurora-grav',
                    2 => 'custom'
                ],
                'persistent' => [
                    0 => 'aurora'
                ],
                'overrides' => [
                    0 => 'aurora-grav',
                    1 => 'custom'
                ]
            ],
            'dependencies' => [
                'gantry' => '5.4.*'
            ],
            'section-variations' => [
                'Padding Variations' => [
                    'section-vertical-paddings' => 'Section Vertical Paddings',
                    'section-horizontal-paddings' => 'Section Horizontal Paddings',
                    'section-vertical-paddings-large' => 'Large Vertical Paddings',
                    'section-horizontal-paddings-large' => 'Large Horizontal Paddings',
                    'section-vertical-paddings-small' => 'Small Vertical Paddings',
                    'section-horizontal-paddings-small' => 'Small Horizontal Paddings',
                    'largemarginall' => 'Large Margin All',
                    'largepaddingall' => 'Large Padding All',
                    'largemargintop' => 'Large Margin Top',
                    'largepaddingtop' => 'Large Padding Top',
                    'largemarginbottom' => 'Large Margin Bottom',
                    'largepaddingbottom' => 'Large Padding Bottom',
                    'largemarginleft' => 'Large Margin Left',
                    'largepaddingleft' => 'Large Padding Left',
                    'largemarginright' => 'Large Margin Right',
                    'largepaddingright' => 'Large Padding Right',
                    'medmarginall' => 'Medium Margin All',
                    'medpaddingall' => 'Medium Padding All',
                    'medmargintop' => 'Medium Margin Top',
                    'medpaddingtop' => 'Medium Padding Top',
                    'medmarginbottom' => 'Medium Margin Bottom',
                    'medpaddingbottom' => 'Medium Padding Bottom',
                    'medmarginleft' => 'Medium Margin Left',
                    'medpaddingleft' => 'Medium Padding Left',
                    'medmarginright' => 'Medium Margin Right',
                    'medpaddingright' => 'Medium Padding Right',
                    'smallmarginall' => 'Small Margin All',
                    'smallpaddingall' => 'Small Padding All',
                    'smallmargintop' => 'Small Margin Top',
                    'smallpaddingtop' => 'Small Padding Top',
                    'smallmarginbottom' => 'Small Margin Bottom',
                    'smallpaddingbottom' => 'Small Padding Bottom',
                    'smallmarginleft' => 'Small Margin Left',
                    'smallpaddingleft' => 'Small Padding Left',
                    'smallmarginright' => 'Small Margin Right',
                    'smallpaddingright' => 'Small Padding Right'
                ],
                'Remove Padding / Margin' => [
                    'nomarginall' => 'No Margin All',
                    'nopaddingall' => 'No Padding All',
                    'nomargintop' => 'No Margin Top',
                    'nopaddingtop' => 'No Padding Top',
                    'nomarginbottom' => 'No Margin Bottom',
                    'nopaddingbottom' => 'No Padding Bottom',
                    'nomarginleft' => 'No Margin Left',
                    'nopaddingleft' => 'No Padding Left',
                    'nomarginright' => 'No Margin Right',
                    'nopaddingright' => 'No Padding Right'
                ]
            ],
            'block-variations' => [
                'Title Variations' => [
                    'title1' => 'Title 1',
                    'title2' => 'Title 2',
                    'title3' => 'Title 3',
                    'title4' => 'Title 4',
                    'title5' => 'Title 5',
                    'title6' => 'Title 6',
                    'title-grey' => 'Title Grey',
                    'title-pink' => 'Title Pink',
                    'title-red' => 'Title Red',
                    'title-purple' => 'Title Purple',
                    'title-orange' => 'Title Orange',
                    'title-blue' => 'Title Blue',
                    'title-underline' => 'Title Underline',
                    'title-rounded' => 'Title Rounded'
                ],
                'Box Variations' => [
                    'box1' => 'Box 1',
                    'box2' => 'Box 2',
                    'box3' => 'Box 3',
                    'box4' => 'Box 4',
                    'box5' => 'Box 5',
                    'box6' => 'Box 6',
                    'box-white' => 'Box White',
                    'box-grey' => 'Box Grey',
                    'box-pink' => 'Box Pink',
                    'box-red' => 'Box Red',
                    'box-purple' => 'Box Purple',
                    'box-orange' => 'Box Orange',
                    'box-blue' => 'Box Blue'
                ],
                'Effects' => [
                    'spaced' => 'Spaced',
                    'bordered' => 'Bordered',
                    'shadow' => 'Shadow 1',
                    'shadow2' => 'Shadow 2',
                    'rounded' => 'Rounded',
                    'square' => 'Square'
                ],
                'Utility' => [
                    'equal-height' => 'Equal Height',
                    'g-outer-box' => 'Outer Box',
                    'disabled' => 'Disabled',
                    'align-right' => 'Align Right',
                    'align-left' => 'Align Left',
                    'title-center' => 'Centered Title',
                    'center' => 'Center',
                    'largemarginall' => 'Large Margin All',
                    'largepaddingall' => 'Large Padding All',
                    'largemargintop' => 'Large Margin Top',
                    'largepaddingtop' => 'Large Padding Top',
                    'largemarginbottom' => 'Large Margin Bottom',
                    'largepaddingbottom' => 'Large Padding Bottom',
                    'largemarginleft' => 'Large Margin Left',
                    'largepaddingleft' => 'Large Padding Left',
                    'largemarginright' => 'Large Margin Right',
                    'largepaddingright' => 'Large Padding Right',
                    'medmarginall' => 'Medium Margin All',
                    'medpaddingall' => 'Medium Padding All',
                    'medmargintop' => 'Medium Margin Top',
                    'medpaddingtop' => 'Medium Padding Top',
                    'medmarginbottom' => 'Medium Margin Bottom',
                    'medpaddingbottom' => 'Medium Padding Bottom',
                    'medmarginleft' => 'Medium Margin Left',
                    'medpaddingleft' => 'Medium Padding Left',
                    'medmarginright' => 'Medium Margin Right',
                    'medpaddingright' => 'Medium Padding Right',
                    'smallmarginall' => 'Small Margin All',
                    'smallpaddingall' => 'Small Padding All',
                    'smallmargintop' => 'Small Margin Top',
                    'smallpaddingtop' => 'Small Padding Top',
                    'smallmarginbottom' => 'Small Margin Bottom',
                    'smallpaddingbottom' => 'Small Padding Bottom',
                    'smallmarginleft' => 'Small Margin Left',
                    'smallpaddingleft' => 'Small Padding Left',
                    'smallmarginright' => 'Small Margin Right',
                    'smallpaddingright' => 'Small Padding Right'
                ],
                'Remove Padding / Margin' => [
                    'nomarginall' => 'No Margin All',
                    'nopaddingall' => 'No Padding All',
                    'nomargintop' => 'No Margin Top',
                    'nopaddingtop' => 'No Padding Top',
                    'nomarginbottom' => 'No Margin Bottom',
                    'nopaddingbottom' => 'No Padding Bottom',
                    'nomarginleft' => 'No Margin Left',
                    'nopaddingleft' => 'No Padding Left',
                    'nomarginright' => 'No Margin Right',
                    'nopaddingright' => 'No Padding Right'
                ]
            ]
        ],
        'admin' => [
            'styles' => [
                'core' => [
                    0 => 'base',
                    1 => 'accent',
                    2 => 'font'
                ],
                'section' => [
                    0 => 'top',
                    1 => 'navigation',
                    2 => 'header',
                    3 => 'above',
                    4 => 'main',
                    5 => 'showcase',
                    6 => 'slideshow',
                    7 => 'utility',
                    8 => 'feature',
                    9 => 'expanded',
                    10 => 'extension',
                    11 => 'bottom',
                    12 => 'footer',
                    13 => 'copyright',
                    14 => 'offcanvas'
                ],
                'configuration' => [
                    0 => 'breakpoints'
                ]
            ]
        ]
    ]
];
