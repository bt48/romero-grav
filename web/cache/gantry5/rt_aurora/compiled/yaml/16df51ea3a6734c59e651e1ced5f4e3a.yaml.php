<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/particles/carousel.yaml',
    'modified' => 1548871658,
    'data' => [
        'name' => 'Carousel',
        'description' => 'Display images in a carousel form.',
        'type' => 'particle',
        'icon' => 'fa-window-close',
        'configuration' => [
            'caching' => [
                'type' => 'static'
            ]
        ],
        'form' => [
            'fields' => [
                'enabled' => [
                    'type' => 'input.checkbox',
                    'label' => 'Enabled',
                    'description' => 'Globally enable particles.',
                    'default' => true
                ],
                '_tabs' => [
                    'type' => 'container.tabs',
                    'fields' => [
                        '_tab_settings' => [
                            'label' => 'Settings',
                            'overridable' => false,
                            'fields' => [
                                'source' => [
                                    'type' => 'select.select',
                                    'label' => 'Content Source',
                                    'description' => 'Choose if the content should be loaded from the WordPress posts or particle itself.',
                                    'default' => 'particle',
                                    'options' => [
                                        'particle' => 'Particle',
                                        'grav' => 'Grav'
                                    ]
                                ],
                                'class' => [
                                    'type' => 'input.selectize',
                                    'label' => 'CSS Classes',
                                    'description' => 'CSS class name for the particle.'
                                ],
                                'title' => [
                                    'type' => 'input.text',
                                    'label' => 'Title',
                                    'description' => 'Customize the title text.',
                                    'placeholder' => 'Enter title'
                                ],
                                'description' => [
                                    'type' => 'textarea.textarea',
                                    'label' => 'Description',
                                    'description' => 'Customize the description text.'
                                ],
                                'animateOut' => [
                                    'type' => 'select.select',
                                    'label' => 'Out Animation',
                                    'description' => 'Customize the Out Animation from animate css class.',
                                    'default' => 'fadeOut',
                                    'options' => [
                                        'default' => 'default',
                                        'bounce' => 'bounce',
                                        'flash' => 'flash',
                                        'pulse' => 'pulse',
                                        'rubberBand' => 'rubberBand',
                                        'shake' => 'shake',
                                        'swing' => 'swing',
                                        'tada' => 'tada',
                                        'wobble' => 'wobble',
                                        'jello' => 'jello',
                                        'bounceIn' => 'bounceIn',
                                        'bounceInDown' => 'bounceInDown',
                                        'bounceInLeft' => 'bounceInLeft',
                                        'bounceInRight' => 'bounceInRight',
                                        'bounceInUp' => 'bounceInUp',
                                        'bounceOut' => 'bounceOut',
                                        'bounceOutDown' => 'bounceOutDown',
                                        'bounceOutLeft' => 'bounceOutLeft',
                                        'bounceOutRight' => 'bounceOutRight',
                                        'bounceOutUp' => 'bounceOutUp',
                                        'fadeIn' => 'fadeIn',
                                        'fadeInDown' => 'fadeInDown',
                                        'fadeInDownBig' => 'fadeInDownBig',
                                        'fadeInLeft' => 'fadeInLeft',
                                        'fadeInLeftBig' => 'fadeInLeftBig',
                                        'fadeInRight' => 'fadeInRight',
                                        'fadeInRightBig' => 'fadeInRightBig',
                                        'fadeInUp' => 'fadeInUp',
                                        'fadeInUpBig' => 'fadeInUpBig',
                                        'fadeOut' => 'fadeOut',
                                        'fadeOutDown' => 'fadeOutDown',
                                        'fadeOutDownBig' => 'fadeOutDownBig',
                                        'fadeOutLeft' => 'fadeOutLeft',
                                        'fadeOutLeftBig' => 'fadeOutLeftBig',
                                        'fadeOutRight' => 'fadeOutRight',
                                        'fadeOutRightBig' => 'fadeOutRightBig',
                                        'fadeOutUp' => 'fadeOutUp',
                                        'fadeOutUpBig' => 'fadeOutUpBig',
                                        'flip' => 'flip',
                                        'flipInX' => 'flipInX',
                                        'flipInY' => 'flipInY',
                                        'flipOutX' => 'flipOutX',
                                        'flipOutY' => 'flipOutY',
                                        'lightSpeedIn' => 'lightSpeedIn',
                                        'lightSpeedOut' => 'lightSpeedOut',
                                        'rotateIn' => 'rotateIn',
                                        'rotateInDownLeft' => 'rotateInDownLeft',
                                        'rotateInDownRight' => 'rotateInDownRight',
                                        'rotateInUpLeft' => 'rotateInUpLeft',
                                        'rotateInUpRight' => 'rotateInUpRight',
                                        'rotateOut' => 'rotateOut',
                                        'rotateOutDownLeft' => 'rotateOutDownLeft',
                                        'rotateOutDownRight' => 'rotateOutDownRight',
                                        'rotateOutUpLeft' => 'rotateOutUpLeft',
                                        'rotateOutUpRight' => 'rotateOutUpRight',
                                        'slideInUp' => 'slideInUp',
                                        'slideInDown' => 'slideInDown',
                                        'slideInLeft' => 'slideInLeft',
                                        'slideInRight' => 'slideInRight',
                                        'slideOutUp' => 'slideOutUp',
                                        'slideOutDown' => 'slideOutDown',
                                        'slideOutLeft' => 'slideOutLeft',
                                        'slideOutRight' => 'slideOutRight',
                                        'zoomIn' => 'zoomIn',
                                        'zoomInDown' => 'zoomInDown',
                                        'zoomInLeft' => 'zoomInLeft',
                                        'zoomInRight' => 'zoomInRight',
                                        'zoomInUp' => 'zoomInUp',
                                        'zoomOut' => 'zoomOut',
                                        'zoomOutDown' => 'zoomOutDown',
                                        'zoomOutLeft' => 'zoomOutLeft',
                                        'zoomOutRight' => 'zoomOutRight',
                                        'zoomOutUp' => 'zoomOutUp',
                                        'hinge' => 'hinge',
                                        'rollIn' => 'rollIn',
                                        'rollOut' => 'rollOut'
                                    ]
                                ],
                                'animateIn' => [
                                    'type' => 'select.select',
                                    'label' => 'In Animation',
                                    'description' => 'Customize the In Animation from animate css class.',
                                    'default' => 'fadeIn',
                                    'options' => [
                                        'default' => 'default',
                                        'bounce' => 'bounce',
                                        'flash' => 'flash',
                                        'pulse' => 'pulse',
                                        'rubberBand' => 'rubberBand',
                                        'shake' => 'shake',
                                        'swing' => 'swing',
                                        'tada' => 'tada',
                                        'wobble' => 'wobble',
                                        'jello' => 'jello',
                                        'bounceIn' => 'bounceIn',
                                        'bounceInDown' => 'bounceInDown',
                                        'bounceInLeft' => 'bounceInLeft',
                                        'bounceInRight' => 'bounceInRight',
                                        'bounceInUp' => 'bounceInUp',
                                        'bounceOut' => 'bounceOut',
                                        'bounceOutDown' => 'bounceOutDown',
                                        'bounceOutLeft' => 'bounceOutLeft',
                                        'bounceOutRight' => 'bounceOutRight',
                                        'bounceOutUp' => 'bounceOutUp',
                                        'fadeIn' => 'fadeIn',
                                        'fadeInDown' => 'fadeInDown',
                                        'fadeInDownBig' => 'fadeInDownBig',
                                        'fadeInLeft' => 'fadeInLeft',
                                        'fadeInLeftBig' => 'fadeInLeftBig',
                                        'fadeInRight' => 'fadeInRight',
                                        'fadeInRightBig' => 'fadeInRightBig',
                                        'fadeInUp' => 'fadeInUp',
                                        'fadeInUpBig' => 'fadeInUpBig',
                                        'fadeOut' => 'fadeOut',
                                        'fadeOutDown' => 'fadeOutDown',
                                        'fadeOutDownBig' => 'fadeOutDownBig',
                                        'fadeOutLeft' => 'fadeOutLeft',
                                        'fadeOutLeftBig' => 'fadeOutLeftBig',
                                        'fadeOutRight' => 'fadeOutRight',
                                        'fadeOutRightBig' => 'fadeOutRightBig',
                                        'fadeOutUp' => 'fadeOutUp',
                                        'fadeOutUpBig' => 'fadeOutUpBig',
                                        'flip' => 'flip',
                                        'flipInX' => 'flipInX',
                                        'flipInY' => 'flipInY',
                                        'flipOutX' => 'flipOutX',
                                        'flipOutY' => 'flipOutY',
                                        'lightSpeedIn' => 'lightSpeedIn',
                                        'lightSpeedOut' => 'lightSpeedOut',
                                        'rotateIn' => 'rotateIn',
                                        'rotateInDownLeft' => 'rotateInDownLeft',
                                        'rotateInDownRight' => 'rotateInDownRight',
                                        'rotateInUpLeft' => 'rotateInUpLeft',
                                        'rotateInUpRight' => 'rotateInUpRight',
                                        'rotateOut' => 'rotateOut',
                                        'rotateOutDownLeft' => 'rotateOutDownLeft',
                                        'rotateOutDownRight' => 'rotateOutDownRight',
                                        'rotateOutUpLeft' => 'rotateOutUpLeft',
                                        'rotateOutUpRight' => 'rotateOutUpRight',
                                        'slideInUp' => 'slideInUp',
                                        'slideInDown' => 'slideInDown',
                                        'slideInLeft' => 'slideInLeft',
                                        'slideInRight' => 'slideInRight',
                                        'slideOutUp' => 'slideOutUp',
                                        'slideOutDown' => 'slideOutDown',
                                        'slideOutLeft' => 'slideOutLeft',
                                        'slideOutRight' => 'slideOutRight',
                                        'zoomIn' => 'zoomIn',
                                        'zoomInDown' => 'zoomInDown',
                                        'zoomInLeft' => 'zoomInLeft',
                                        'zoomInRight' => 'zoomInRight',
                                        'zoomInUp' => 'zoomInUp',
                                        'zoomOut' => 'zoomOut',
                                        'zoomOutDown' => 'zoomOutDown',
                                        'zoomOutLeft' => 'zoomOutLeft',
                                        'zoomOutRight' => 'zoomOutRight',
                                        'zoomOutUp' => 'zoomOutUp',
                                        'hinge' => 'hinge',
                                        'rollIn' => 'rollIn',
                                        'rollOut' => 'rollOut'
                                    ]
                                ],
                                'nav' => [
                                    'type' => 'select.select',
                                    'label' => 'Prev / Next',
                                    'description' => 'Enable or disable the Prev / Next navigation.',
                                    'default' => 'enabled',
                                    'options' => [
                                        'enabled' => 'Enable',
                                        'disabled' => 'Disable'
                                    ]
                                ],
                                'loop' => [
                                    'type' => 'select.select',
                                    'label' => 'Loop',
                                    'description' => 'Enable or disable the Inifnity loop. Duplicate last and first items to get loop illusion. This option <strong>won\'t work</strong> in Showcase mode.',
                                    'default' => 'enabled',
                                    'options' => [
                                        'enabled' => 'Enable',
                                        'disabled' => 'Disable'
                                    ]
                                ],
                                'autoplay' => [
                                    'type' => 'select.select',
                                    'label' => 'Autoplay',
                                    'description' => 'Enable or disable the Autoplay.',
                                    'default' => 'disabled',
                                    'options' => [
                                        'enabled' => 'Enable',
                                        'disabled' => 'Disable'
                                    ]
                                ],
                                'autoplaySpeed' => [
                                    'type' => 'input.text',
                                    'label' => 'Autoplay Speed',
                                    'description' => 'Set the speed of the Autoplay, in milliseconds.',
                                    'placeholder' => 5000
                                ],
                                'pauseOnHover' => [
                                    'type' => 'select.select',
                                    'label' => 'Pause on Hover',
                                    'description' => 'Pause the slideshow when hovering over slider, then resume when no longer hovering.',
                                    'default' => 'enabled',
                                    'options' => [
                                        'enabled' => 'Enable',
                                        'disabled' => 'Disable'
                                    ]
                                ],
                                'displayitems' => [
                                    'type' => 'input.number',
                                    'label' => 'Display Items',
                                    'description' => 'Amount of items to display.',
                                    'default' => 5,
                                    'min' => 1
                                ]
                            ]
                        ],
                        '_tab_collection' => [
                            'label' => 'Particle Items',
                            'overridable' => false,
                            'fields' => [
                                'items' => [
                                    'type' => 'collection.list',
                                    'array' => true,
                                    'label' => 'Items',
                                    'description' => 'Carousel items.',
                                    'value' => 'name',
                                    'ajax' => true,
                                    'fields' => [
                                        '.icon' => [
                                            'type' => 'input.icon',
                                            'label' => 'Icon',
                                            'description' => 'Select the icon.',
                                            'default' => 'fa fa-search'
                                        ],
                                        '.title' => [
                                            'type' => 'input.text',
                                            'label' => 'Title',
                                            'placeholder' => 'Enter title'
                                        ],
                                        '.description' => [
                                            'type' => 'textarea.textarea',
                                            'label' => 'Caption',
                                            'description' => 'Customize the image caption.',
                                            'placeholder' => 'Enter caption'
                                        ],
                                        '.readmore' => [
                                            'type' => 'input.text',
                                            'label' => 'Read More',
                                            'description' => 'Enable or disable read more text.',
                                            'placeholder' => 'Enter text'
                                        ],
                                        '.link' => [
                                            'type' => 'input.text',
                                            'label' => 'Link'
                                        ],
                                        '.linktarget' => [
                                            'type' => 'select.selectize',
                                            'label' => 'Target',
                                            'description' => 'Target browser window when item is clicked.',
                                            'placeholder' => 'Select...',
                                            'default' => '_self',
                                            'options' => [
                                                '_self' => 'Self',
                                                '_blank' => 'New Window'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        '_tab_articles' => [
                            'label' => 'Pages',
                            'overridable' => false,
                            'fields' => [
                                'article.filter.categories' => [
                                    'type' => 'input.selectize',
                                    'label' => 'Categories',
                                    'description' => 'Select the categories the content should be taken from.',
                                    'overridable' => false
                                ],
                                'article.limit.total' => [
                                    'type' => 'input.text',
                                    'label' => 'Number of Pages',
                                    'description' => 'Enter the maximum number of content to display.',
                                    'default' => 5,
                                    'pattern' => '\\d{1,2}',
                                    'overridable' => false
                                ],
                                'article.limit.start' => [
                                    'type' => 'input.text',
                                    'label' => 'Start From',
                                    'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first content item).',
                                    'default' => 0,
                                    'pattern' => '\\d{1,2}',
                                    'overridable' => false
                                ],
                                'article.sort.orderby' => [
                                    'type' => 'select.select',
                                    'label' => 'Order By',
                                    'description' => 'Select how the content should be ordered by.',
                                    'default' => 'default',
                                    'options' => [
                                        'default' => 'Default Ordering',
                                        'date' => 'Date',
                                        'publish_date' => 'Publish Date',
                                        'unpublish_date' => 'Unpublish Date',
                                        'modified' => 'Last Modified Date',
                                        'title' => 'Title',
                                        'slug' => 'Slug'
                                    ],
                                    'overridable' => false
                                ],
                                'article.sort.ordering' => [
                                    'type' => 'select.select',
                                    'label' => 'Ordering Direction',
                                    'description' => 'Select the direction the content should be ordered by.',
                                    'default' => 'asc',
                                    'options' => [
                                        'asc' => 'Ascending',
                                        'desc' => 'Descending'
                                    ],
                                    'overridable' => false
                                ]
                            ]
                        ],
                        '_tab_display' => [
                            'label' => 'Pages Display',
                            'fields' => [
                                'article.display.text.type' => [
                                    'type' => 'select.select',
                                    'label' => 'Article Text',
                                    'description' => 'Select if and how the content text should be shown.',
                                    'default' => 'intro',
                                    'options' => [
                                        'intro' => 'Introduction',
                                        'full' => 'Full Article',
                                        '' => 'Hide'
                                    ]
                                ],
                                'article.display.text.limit' => [
                                    'type' => 'input.text',
                                    'label' => 'Text Limit',
                                    'description' => 'Type in the number of characters the content text should be limited to.',
                                    'default' => '',
                                    'pattern' => '\\d+'
                                ],
                                'article.display.text.formatting' => [
                                    'type' => 'select.select',
                                    'label' => 'Text Formatting',
                                    'description' => 'Select the formatting you want to use to display the content text.',
                                    'default' => 'text',
                                    'options' => [
                                        'text' => 'Plain Text',
                                        'html' => 'HTML'
                                    ]
                                ],
                                'article.display.title.enabled' => [
                                    'type' => 'select.select',
                                    'label' => 'Title',
                                    'description' => 'Select if the content title should be shown.',
                                    'default' => 'show',
                                    'options' => [
                                        'show' => 'Show',
                                        '' => 'Hide'
                                    ]
                                ],
                                'article.display.title.limit' => [
                                    'type' => 'input.text',
                                    'label' => 'Title Limit',
                                    'description' => 'Enter the maximum number of characters the content title should be limited to.',
                                    'pattern' => '\\d+(\\.\\d+){0,1}'
                                ],
                                'article.display.readmore' => [
                                    'type' => 'input.text',
                                    'label' => 'Read More',
                                    'description' => 'Enable or disable read more text.',
                                    'placeholder' => 'Enter text'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
