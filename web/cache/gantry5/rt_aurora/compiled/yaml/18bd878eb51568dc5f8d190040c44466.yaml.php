<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_about_us/layout.yaml',
    'modified' => 1552956792,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1508514391
        ],
        'layout' => [
            'top' => [
                
            ],
            'navigation' => [
                
            ],
            '/slideshow/' => [
                0 => [
                    0 => 'simplecontent-9033'
                ]
            ],
            '/header/' => [
                
            ],
            '/above/' => [
                0 => [
                    0 => 'simplecontent-9044'
                ]
            ],
            '/showcase/' => [
                
            ],
            '/utility/' => [
                
            ],
            '/feature/' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 26' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 49' => [
                            0 => [
                                0 => 'logo-3428'
                            ],
                            1 => [
                                0 => 'system-content-6590'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 25' => [
                            
                        ]
                    ]
                ]
            ],
            '/expanded/' => [
                
            ],
            '/extension/' => [
                
            ],
            '/bottom/' => [
                
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        'sidebar-footer 30' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar-footer 40' => [
                            
                        ]
                    ],
                    2 => [
                        'aside-footer 30' => [
                            
                        ]
                    ]
                ]
            ],
            'copyright' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'top' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-slideshow section-horizontal-paddings'
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-header section-horizontal-paddings section-vertical-paddings-small'
                ]
            ],
            'above' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-above section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'utility' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-utility section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'feature' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-feature section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'subtype' => 'aside',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'subtype' => 'main',
                'title' => 'Main',
                'attributes' => [
                    'class' => 'section-horizontal-paddings'
                ]
            ],
            'aside' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-expanded section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-extension section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-bottom section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'aside-footer' => [
                'title' => 'Footer Aside',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'children',
                        1 => 'attributes'
                    ]
                ]
            ],
            'offcanvas' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'simplecontent-9033' => [
                'title' => 'Header - About Us',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => '<strong>About</strong> Us',
                            'author' => '',
                            'leading_content' => 'Deep-rooted Gospel values are very much at the heart of the Romero Trust’s vision.',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'About Us'
                        ]
                    ]
                ]
            ],
            'simplecontent-9044' => [
                'title' => 'Simple Content',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => '',
                            'author' => '',
                            'leading_content' => '',
                            'main_content' => '<p>Deep-rooted Gospel values are very much at the heart of the Trust’s vision, which serves the Catholic and wider communities of South-West Durham, aiming to provide the best education and opportunities for young people of the area. The Trust’s aim is to develop and grow strong partnerships so that every child is valued, supported and encouraged to develop their God-given talents in order to reach their full potential.</p>
<p>By working in partnership, we are stronger together and are committed to prioritising the welfare and well-being of our children. With outstanding pastoral care and greater access to specialist support, guidance and SEND expertise, we aim to provide a caring environment where children are nurtured and encouraged to flourish.</p>
<p>As a Trust we will continue to work with other schools and educational organisations on local and national initiatives, which will provide greater access to further opportunities and support for the benefit of children, staff and the individual schools involved.</p>
<p>The Romero Catholic Education Trust was established in December 2016 by St John’s and the Trust welcomed St Joseph’s Primary School, Newton Aycliffe, into the Trust on 1 April 2018. Currently, the Romero Catholic Education Trust has overarching accountability and governance of:<p>
<ul>
<li><a href="/st-johns">St John’s School & Sixth Form College – A Catholic Academy</a></li>
<li>St Joseph’s Primary School – A Catholic Academy
</ul>
<p>Being members of the Trust enhances our strong commitment to work collaboratively to remove barriers to learning, share expertise, resources and specialist services for the educational benefit of all children and staff. </p>',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'About Us'
                        ]
                    ]
                ]
            ],
            'logo-3428' => [
                'title' => 'SVG Pattern',
                'attributes' => [
                    'url' => '',
                    'link' => '0',
                    'svg' => '<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 2560 420">
<path
 d="M2141.000,240.000 L2560.533,240.000 L2561.000,212.001 L2561.000,300.000 L959.000,300.000 L216.000,300.000 L213.000,300.000 C196.432,300.000 183.000,286.569 183.000,270.000 C183.000,253.432 196.432,240.000 213.000,240.000 L216.000,240.000 L466.000,240.000 C482.569,240.000 496.000,226.568 496.000,210.000 C496.000,193.431 482.569,180.000 466.000,180.000 L1.000,180.000 L1.000,0.000 L407.000,0.000 L1502.000,0.000 L1931.000,0.000 C1947.569,0.000 1961.000,13.431 1961.000,30.000 C1961.000,46.568 1947.569,60.000 1931.000,60.000 L1541.000,60.000 C1524.431,60.000 1511.000,73.432 1511.000,90.000 C1511.000,106.569 1524.431,120.000 1541.000,120.000 L2561.000,120.000 L2561.000,180.000 L2141.000,180.000 C2124.431,180.000 2111.000,193.431 2111.000,210.000 C2111.000,226.568 2124.431,240.000 2141.000,240.000 ZM602.000,60.000 L407.000,60.000 L82.000,60.000 C65.431,60.000 52.000,73.432 52.000,90.000 C52.000,106.569 65.431,120.000 82.000,120.000 L602.000,120.000 C618.569,120.000 632.000,106.569 632.000,90.000 C632.000,73.432 618.569,60.000 602.000,60.000 ZM1139.000,120.000 L760.000,120.000 C743.431,120.000 730.000,133.431 730.000,150.000 C730.000,166.569 743.431,180.000 760.000,180.000 L1139.000,180.000 C1155.568,180.000 1169.000,166.569 1169.000,150.000 C1169.000,133.431 1155.568,120.000 1139.000,120.000 ZM1906.000,180.000 L1353.000,180.000 C1336.432,180.000 1323.000,193.431 1323.000,210.000 C1323.000,226.568 1336.432,240.000 1353.000,240.000 L1906.000,240.000 C1922.569,240.000 1936.000,226.568 1936.000,210.000 C1936.000,193.431 1922.569,180.000 1906.000,180.000 ZM2561.000,60.000 L2561.000,60.004 L2561.000,60.000 L2561.000,60.000 ZM115.000,390.000 C115.000,406.569 101.569,420.000 85.000,420.000 L0.000,420.000 L0.000,360.000 L85.000,360.000 C101.569,360.000 115.000,373.432 115.000,390.000 ZM1401.000,360.000 L1569.000,360.000 C1585.568,360.000 1599.000,373.432 1599.000,390.000 C1599.000,406.569 1585.568,420.000 1569.000,420.000 L1401.000,420.000 C1384.431,420.000 1371.000,406.569 1371.000,390.000 C1371.000,373.432 1384.431,360.000 1401.000,360.000 ZM2339.000,360.000 L2377.000,360.000 C2393.569,360.000 2407.000,373.432 2407.000,390.000 C2407.000,406.569 2393.569,420.000 2377.000,420.000 L2339.000,420.000 C2322.431,420.000 2309.000,406.569 2309.000,390.000 C2309.000,373.432 2322.431,360.000 2339.000,360.000 Z"></path>
</svg>',
                    'text' => ''
                ],
                'block' => [
                    'class' => 'svg-pattern-mainbar'
                ]
            ]
        ]
    ]
];
