<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/_body_only/index.yaml',
    'modified' => 1549585225,
    'data' => [
        'name' => '_body_only',
        'timestamp' => 1548871664,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/body-only.png',
            'name' => '_body_only',
            'timestamp' => 1509009075
        ],
        'positions' => [
            
        ],
        'sections' => [
            'body' => 'Body'
        ],
        'particles' => [
            'messages' => [
                'system-messages-8195' => 'System Messages'
            ],
            'content' => [
                'system-content-2391' => 'Page Content'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
