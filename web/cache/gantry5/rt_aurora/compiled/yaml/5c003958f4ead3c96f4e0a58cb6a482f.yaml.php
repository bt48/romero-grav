<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_contact/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'pages_-_contact',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1508514391
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'sidebar' => 'Sidebar',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'mainbar' => 'Main',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-9318' => 'Header - Contact us',
                'simplecontent-5458' => 'contact module',
                'simplecontent-5267' => 'Simple Content',
                'simplecontent-2333' => 'Simple Content'
            ],
            'logo' => [
                'logo-3428' => 'SVG Pattern',
                'logo-3771' => 'Logo',
                'logo-6190' => 'SVG Pattern'
            ],
            'content' => [
                'system-content-5115' => 'Page Content'
            ],
            'messages' => [
                'system-messages-5687' => 'System Messages'
            ],
            'menu' => [
                'menu-1216' => 'Menu'
            ],
            'position' => [
                'position-position-9232' => 'Sidebar',
                'position-position-6232' => 'Aside'
            ],
            'branding' => [
                'branding-4147' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-2720' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'sidebar' => 'sidebar',
                'aside' => 'aside',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-5687' => 'system-messages-9828',
                'logo-3771' => 'logo-5992',
                'menu-1216' => 'menu-2350',
                'logo-6190' => 'logo-3146',
                'position-position-9232' => 'position-position-9414',
                'position-position-6232' => 'position-position-8807',
                'simplecontent-5458' => 'simplecontent-5244',
                'simplecontent-5267' => 'simplecontent-8236',
                'simplecontent-2333' => 'simplecontent-4905',
                'branding-4147' => 'branding-1506',
                'mobile-menu-2720' => 'mobile-menu-7951'
            ]
        ]
    ]
];
