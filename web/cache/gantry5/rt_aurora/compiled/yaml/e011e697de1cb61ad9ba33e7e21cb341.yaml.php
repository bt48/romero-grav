<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_contact/styles.yaml',
    'modified' => 1552956700,
    'data' => [
        'preset' => 'preset1',
        'accent' => [
            'color-1' => '#8a7224'
        ],
        'slideshow' => [
            'background' => '#8a7224',
            'background-image' => 'gantry-media://RCET_Logo.jpg',
            'text-color' => '#ffffff'
        ],
        'footer' => [
            'background' => '#f5f3e7',
            'text-color' => '#4a3309'
        ],
        'copyright' => [
            'background' => '#f5f3e7',
            'text-color' => '#4a3309'
        ]
    ]
];
