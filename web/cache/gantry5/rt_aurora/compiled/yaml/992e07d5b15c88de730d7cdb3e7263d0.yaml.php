<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'menu' => 'mainmenu',
        'base' => '/',
        'startLevel' => '1',
        'maxLevels' => '0',
        'renderTitles' => '0',
        'hoverExpand' => '1',
        'mobileTarget' => '0',
        'forceTarget' => '0'
    ]
];
