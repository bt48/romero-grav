<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/layouts_-_left_sidebar/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'layouts_-_left_sidebar',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'left_sidebar',
            'timestamp' => 1508428073
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-7561' => 'Left Sidebar Example',
                'simplecontent-1384' => 'contact module',
                'simplecontent-5256' => 'Simple Content',
                'simplecontent-6302' => 'Simple Content'
            ],
            'custom' => [
                'custom-7406' => 'Left Sidebar',
                'custom-9224' => 'Flexible Layouts',
                'custom-8235' => 'H2 Heading'
            ],
            'messages' => [
                'system-messages-2015' => 'System Messages'
            ],
            'logo' => [
                'logo-8767' => 'Logo',
                'logo-3955' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-5861' => 'Menu'
            ],
            'branding' => [
                'branding-1012' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-8827' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'header' => 'header',
                'slideshow' => 'slideshow',
                'above' => 'above',
                'showcase' => 'showcase',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-2015' => 'system-messages-9828',
                'logo-8767' => 'logo-5992',
                'menu-5861' => 'menu-2350',
                'logo-3955' => 'logo-3146',
                'simplecontent-1384' => 'simplecontent-5244',
                'simplecontent-5256' => 'simplecontent-8236',
                'simplecontent-6302' => 'simplecontent-4905',
                'branding-1012' => 'branding-1506',
                'mobile-menu-8827' => 'mobile-menu-7951'
            ]
        ]
    ]
];
