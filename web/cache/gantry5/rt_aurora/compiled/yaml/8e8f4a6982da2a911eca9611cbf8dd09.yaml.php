<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'date' => [
            'start' => '2007',
            'end' => 'now'
        ],
        'owner' => 'Romero Trust'
    ]
];
