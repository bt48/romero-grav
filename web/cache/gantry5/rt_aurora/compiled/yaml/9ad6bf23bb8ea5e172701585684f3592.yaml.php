<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/index.yaml',
    'modified' => 1553165229,
    'data' => [
        'name' => 'page_-_st_josephs',
        'timestamp' => 1553165229,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'fullwidth',
            'timestamp' => 1508428073
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9021' => 'System Messages'
            ],
            'logo' => [
                'logo-1317' => 'Logo',
                'logo-9369' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-5165' => 'Menu'
            ],
            'simplecontent' => [
                'simplecontent-7561' => 'Intro Text',
                'simplecontent-2480' => 'Simple Content',
                'simplecontent-5057' => 'contact module',
                'simplecontent-2127' => 'Simple Content',
                'simplecontent-8879' => 'Simple Content'
            ],
            'branding' => [
                'branding-6761' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-9370' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'system-messages-9021' => 'system-messages-9828',
                'navigation' => 'navigation',
                'logo-1317' => 'logo-5992',
                'menu-5165' => 'menu-2350',
                'logo-9369' => 'logo-3146',
                'header' => 'header',
                'above' => 'above',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'simplecontent-5057' => 'simplecontent-5244',
                'mainbar-footer' => 'mainbar-footer',
                'simplecontent-2127' => 'simplecontent-8236',
                'aside-footer' => 'aside-footer',
                'simplecontent-8879' => 'simplecontent-4905',
                'copyright' => 'copyright',
                'branding-6761' => 'branding-1506',
                'offcanvas' => 'offcanvas',
                'mobile-menu-9370' => 'mobile-menu-7951'
            ],
            'layouts_-_full_width' => [
                'sidebar' => 'sidebar',
                'aside' => 'aside'
            ]
        ]
    ]
];
