<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/home/index.yaml',
    'modified' => 1552958110,
    'data' => [
        'name' => 'home',
        'timestamp' => 1552958110,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'home_-_particles',
            'timestamp' => 1508851818
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'feature' => 'Feature',
            'showcase' => 'Showcase',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'utility' => 'Utility',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-1675' => 'System Messages'
            ],
            'logo' => [
                'logo-4214' => 'Logo'
            ],
            'menu' => [
                'menu-7163' => 'Menu'
            ],
            'verticalslider' => [
                'verticalslider-5007' => 'Vertical Slider'
            ],
            'simplecontent' => [
                'simplecontent-1596' => 'Intro Text',
                'simplecontent-4215' => 'Intro Lead',
                'simplecontent-1208' => 'contact module',
                'simplecontent-5935' => 'Simple Content',
                'simplecontent-4318' => 'Simple Content'
            ],
            'branding' => [
                'branding-8906' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-9619' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'system-messages-1675' => 'system-messages-9828',
                'menu-7163' => 'menu-2350',
                'sidebar-footer' => 'sidebar-footer',
                'simplecontent-1208' => 'simplecontent-5244',
                'simplecontent-5935' => 'simplecontent-8236',
                'aside-footer' => 'aside-footer',
                'simplecontent-4318' => 'simplecontent-4905',
                'copyright' => 'copyright',
                'branding-8906' => 'branding-1506',
                'offcanvas' => 'offcanvas',
                'mobile-menu-9619' => 'mobile-menu-7951'
            ]
        ]
    ]
];
