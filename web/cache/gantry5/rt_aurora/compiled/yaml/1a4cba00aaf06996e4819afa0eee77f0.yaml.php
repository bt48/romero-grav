<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/menu/mainmenu.yaml',
    'modified' => 1552956094,
    'data' => [
        'settings' => [
            'title' => 'Mainmenu'
        ],
        'ordering' => [
            'home' => '',
            'about-us' => '',
            'st-josephs' => '',
            'st-johns' => '',
            'contact' => ''
        ],
        'items' => [
            'home' => [
                'object_id' => 31,
                'enabled' => true,
                'anchor_class' => ''
            ],
            'about-us' => [
                'enabled' => true,
                'anchor_class' => ''
            ],
            'pages/blog' => [
                'object_id' => 22,
                'enabled' => true,
                'anchor_class' => ''
            ],
            'pages/error' => [
                'object_id' => 5,
                'enabled' => true,
                'anchor_class' => ''
            ],
            'pages/offline' => [
                'object_id' => 51,
                'enabled' => true,
                'anchor_class' => ''
            ],
            'st-johns' => [
                'enabled' => '1',
                'dropdown_dir' => 'right',
                'dropdown_hide' => '0',
                'width' => 'auto',
                'anchor_class' => ''
            ],
            'contact' => [
                'enabled' => true,
                'anchor_class' => ''
            ],
            'st-josephs' => [
                'enabled' => true,
                'anchor_class' => ''
            ],
            'styles/__particle-NIhhN' => [
                'type' => 'particle',
                'particle' => 'custom',
                'title' => 'Custom HTML',
                'options' => [
                    'particle' => [
                        'enabled' => '1',
                        'html' => '<div class="g-grid g-preset-thumbnails center">
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset1">
                <img src="gantry-media://rocketlauncher/styles/preset1.jpg" alt="Preset 1">
                <br />
                <span>Preset 1</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset2">
                <img src="gantry-media://rocketlauncher/styles/preset2.jpg" alt="Preset 2">
                <br />
                <span>Preset 2</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset3">
                <img src="gantry-media://rocketlauncher/styles/preset3.jpg" alt="Preset 3">
                <br />
                <span>Preset 3</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset4">
                <img src="gantry-media://rocketlauncher/styles/preset4.jpg" alt="Preset 4">
                <br />
                <span>Preset 4</span>
            </a>
        </div>
    </div>
    <div class="g-block">
        <div class="g-content">
            <a href="?presets=preset5">
                <img src="gantry-media://rocketlauncher/styles/preset5.jpg" alt="Preset 5">
                <br />
                <span>Preset 5</span>
            </a>
        </div>
    </div>
</div> ',
                        'twig' => '0',
                        'filter' => '0'
                    ],
                    'block' => [
                        'extra' => [
                            
                        ]
                    ]
                ],
                'enabled' => true,
                'anchor_class' => ''
            ]
        ]
    ]
];
