<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/styles.yaml',
    'modified' => 1552958916,
    'data' => [
        'preset' => 'preset1',
        'menustyle' => [
            'background-active' => 'rgba(255,255,255, 0)'
        ],
        'slideshow' => [
            'background' => '#8a7224',
            'background-image' => 'gantry-media://pupils.jpg',
            'text-color' => '#fffff7'
        ]
    ]
];
