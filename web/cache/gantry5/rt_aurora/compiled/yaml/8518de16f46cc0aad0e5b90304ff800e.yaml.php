<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/layout.yaml',
    'modified' => 1553165229,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'fullwidth',
            'timestamp' => 1508428073
        ],
        'layout' => [
            'top' => [
                
            ],
            'navigation' => [
                
            ],
            'header' => [
                
            ],
            '/slideshow/' => [
                0 => [
                    0 => 'simplecontent-7561'
                ]
            ],
            'above' => [
                
            ],
            '/showcase/' => [
                
            ],
            'utility' => [
                
            ],
            'feature' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 13' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 74' => [
                            0 => [
                                0 => 'simplecontent-2480'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 13' => [
                            
                        ]
                    ]
                ]
            ],
            'expanded' => [
                
            ],
            'extension' => [
                
            ],
            'bottom' => [
                
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        'sidebar-footer 30' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar-footer 40' => [
                            
                        ]
                    ],
                    2 => [
                        'aside-footer 30' => [
                            
                        ]
                    ]
                ]
            ],
            'copyright' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'top' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'header' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-slideshow section-horizontal-paddings',
                    'variations' => ''
                ]
            ],
            'above' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings',
                    'variations' => ''
                ]
            ],
            'utility' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'feature' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'layouts_-_full_width',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => '',
                    'variations' => ''
                ],
                'block' => [
                    'class' => 'equal-height',
                    'fixed' => '1'
                ]
            ],
            'aside' => [
                'inherit' => [
                    'outline' => 'layouts_-_full_width',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'extension' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'aside-footer' => [
                'title' => 'Footer Aside',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'offcanvas' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'simplecontent-7561' => [
                'title' => 'Intro Text',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => 'St Joseph\'s Primary School – A Catholic Academy ',
                            'author' => '',
                            'leading_content' => 'Deep-rooted Gospel values are very much at the heart of the Trust’s vision.',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'Aurora combines elegance and simplicity in one professional template.'
                        ]
                    ]
                ]
            ],
            'simplecontent-2480' => [
                'title' => 'Simple Content',
                'attributes' => [
                    'enabled' => 0,
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => '',
                            'author' => '',
                            'leading_content' => '',
                            'main_content' => '<h2>We believe that each person is unique and created in God’s image.  In our school, we provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.</h2>
<p>At St Joseph’s we hold Christ at the centre of everything we do and our children know that they are unique and created in God’s image. We celebrate each individual, supporting their journey through school and developing their gifts and talents. Our staff provide excellent opportunities for our children to enjoy and achieve in a happy, safe, and caring environment.</p>
<p>We know that your child will be happy at St Joseph’s and we hope that you as parents and carers will be fully involved in your child’s education and the life of the school in general.</p>
<p>If you need any further information please do not hesitate to look at our school brochure or contact the office and we will do our very best to help.</p>
<ul>
<li> w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a></li>
<li>e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a></li>
<li>t: +44 (0)1325 300 337</li>
</ul>',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'Basic Block'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
