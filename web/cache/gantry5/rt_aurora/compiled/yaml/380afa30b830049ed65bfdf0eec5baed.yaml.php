<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_offline/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'pages_-_offline',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1508514391
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'sidebar' => 'Sidebar',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'mainbar' => 'Main',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-3589' => 'Header - Offline',
                'simplecontent-2822' => 'contact module',
                'simplecontent-8122' => 'Simple Content',
                'simplecontent-7380' => 'Simple Content'
            ],
            'logo' => [
                'logo-3428' => 'SVG Pattern',
                'logo-9269' => 'Logo',
                'logo-7948' => 'SVG Pattern'
            ],
            'content' => [
                'system-content-1120' => 'Page Content'
            ],
            'custom' => [
                'custom-3437' => 'Make your Grav Website offline'
            ],
            'messages' => [
                'system-messages-5848' => 'System Messages'
            ],
            'menu' => [
                'menu-6335' => 'Menu'
            ],
            'position' => [
                'position-position-7203' => 'Sidebar',
                'position-position-3250' => 'Aside'
            ],
            'branding' => [
                'branding-7747' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-2846' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'sidebar' => 'sidebar',
                'aside' => 'aside',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-5848' => 'system-messages-9828',
                'logo-9269' => 'logo-5992',
                'menu-6335' => 'menu-2350',
                'logo-7948' => 'logo-3146',
                'position-position-7203' => 'position-position-9414',
                'position-position-3250' => 'position-position-8807',
                'simplecontent-2822' => 'simplecontent-5244',
                'simplecontent-8122' => 'simplecontent-8236',
                'simplecontent-7380' => 'simplecontent-4905',
                'branding-7747' => 'branding-1506',
                'mobile-menu-2846' => 'mobile-menu-7951'
            ]
        ]
    ]
];
