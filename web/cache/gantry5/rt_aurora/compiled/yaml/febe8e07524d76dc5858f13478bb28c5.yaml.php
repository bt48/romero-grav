<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'nowplaying' => 'Now Playing',
        'scrollbar' => 'noscrollbar',
        'overflow' => '350'
    ]
];
