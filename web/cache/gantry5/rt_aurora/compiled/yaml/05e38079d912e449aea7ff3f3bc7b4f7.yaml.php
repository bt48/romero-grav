<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/layouts_-_right_sidebar/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'layouts_-_right_sidebar',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'right_sidebar',
            'timestamp' => 1508428073
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-7561' => 'Right Sidebar Example',
                'simplecontent-8129' => 'contact module',
                'simplecontent-9803' => 'Simple Content',
                'simplecontent-1991' => 'Simple Content'
            ],
            'custom' => [
                'custom-8235' => 'H2 Heading',
                'custom-7406' => 'Right Sidebar',
                'custom-9224' => 'Flexible Layouts'
            ],
            'messages' => [
                'system-messages-7897' => 'System Messages'
            ],
            'logo' => [
                'logo-1668' => 'Logo',
                'logo-4371' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-1534' => 'Menu'
            ],
            'branding' => [
                'branding-6917' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-4651' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'header' => 'header',
                'slideshow' => 'slideshow',
                'above' => 'above',
                'showcase' => 'showcase',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-7897' => 'system-messages-9828',
                'logo-1668' => 'logo-5992',
                'menu-1534' => 'menu-2350',
                'logo-4371' => 'logo-3146',
                'simplecontent-8129' => 'simplecontent-5244',
                'simplecontent-9803' => 'simplecontent-8236',
                'simplecontent-1991' => 'simplecontent-4905',
                'branding-6917' => 'branding-1506',
                'mobile-menu-4651' => 'mobile-menu-7951'
            ]
        ]
    ]
];
