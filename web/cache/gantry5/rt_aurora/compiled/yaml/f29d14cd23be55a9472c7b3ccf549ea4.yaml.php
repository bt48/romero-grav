<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'article' => [
            'display' => [
                'text' => [
                    'type' => 'intro',
                    'limit' => '',
                    'formatting' => 'text'
                ],
                'title' => [
                    'enabled' => 'show',
                    'limit' => ''
                ],
                'readmore' => ''
            ]
        ]
    ]
];
