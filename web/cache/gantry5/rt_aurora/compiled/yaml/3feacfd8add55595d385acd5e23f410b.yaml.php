<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/layout.yaml',
    'modified' => 1552957980,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'fullwidth',
            'timestamp' => 1508428073
        ],
        'layout' => [
            'top' => [
                
            ],
            'navigation' => [
                
            ],
            'header' => [
                
            ],
            'slideshow' => [
                0 => [
                    0 => 'simplecontent-7561'
                ]
            ],
            'above' => [
                
            ],
            'showcase' => [
                
            ],
            'utility' => [
                
            ],
            'feature' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 13' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 74' => [
                            0 => [
                                0 => 'simplecontent-5107'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 13' => [
                            
                        ]
                    ]
                ]
            ],
            'expanded' => [
                
            ],
            'extension' => [
                
            ],
            'bottom' => [
                
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        'sidebar-footer 30' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar-footer 40' => [
                            
                        ]
                    ],
                    2 => [
                        'aside-footer 30' => [
                            
                        ]
                    ]
                ]
            ],
            'copyright' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'top' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'header' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'above' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'utility' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'feature' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'layouts_-_full_width',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => '',
                    'variations' => ''
                ],
                'block' => [
                    'class' => 'equal-height',
                    'fixed' => '1'
                ]
            ],
            'aside' => [
                'inherit' => [
                    'outline' => 'layouts_-_full_width',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'extension' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'aside-footer' => [
                'title' => 'Footer Aside',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'offcanvas' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'simplecontent-7561' => [
                'title' => 'Intro Text',
                'inherit' => [
                    'outline' => 'layouts_-_full_width',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block'
                    ]
                ]
            ],
            'simplecontent-5107' => [
                'title' => 'Simple Content',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => '',
                            'author' => '',
                            'leading_content' => '',
                            'main_content' => '<h2>At St John’s we pride ourselves on being A Learning Community Guided by Gospel Values, where each individual is valued and nurtured to grow in confidence and to embrace their God-given talents.  As a supportive and inclusive school, we respond to the changing needs of all our students across the age and ability range and we are committed to the highest standards of teaching and learning.</h2>
<p>By providing a wealth of exciting experiences and opportunities, we encourage our young people to be aspirational, fulfilling their potential and realising their dreams with determination and enthusiasm.</p>
<p>From sporting events at regional, national and international levels to Arts projects, foreign exchanges and residential trips, we strive to provide many opportunities for our young people to grow in confidence and to develop their skills.  We believe that our community is a very special place to learn, work and grow, supporting individuals to become independent and resilient learners who enjoy their education and are well-equipped to be the leaders of tomorrow.</p>
<p>Whilst our students participate in many activities that enrich their education and life experiences, we encourage them to recognise their role in helping those in need, both within and beyond our community. We support our young people to respect one another, to embrace diversity and to respond with compassion to those less fortunate than ourselves.</p>
<p>By working in partnership with parents/carers, parishes, feeder schools and the wider community, we create a positive, safe and stimulating environment in which each individual can succeed.</p>
<p>Our dedicated and highly-motivated staff are always available to offer help, support and guidance for our young people and their families, so please come and see our learning community in action.  Meet our students, who are our best ambassadors, and please take the opportunity to speak with them about their experiences when you visit our school.  In addition, spend time exploring our excellent facilities including our floodlit 3G pitch and recently refurbished swimming pool, our professional recording studio, and much, much more.</p>
<p>Please contact us at any time if you require any additional information:</p>
<h3>St John’s School & Sixth Form College – A Catholic Academy</h3>
<ul>
<li>w: <a href="https://www.stjohnsrc.org.uk">www.stjohns.org.uk</a></li>
<li>e: <a href="mailto:staff@stjohnsrc.org.uk">staff@stjohns.org.uk</a></li>
<li>t: 01388 603246</li>
</ul>',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'Simple Block'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
