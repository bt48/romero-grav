<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
    'modified' => 1552956792,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1509009075
        ],
        'layout' => [
            '/top/' => [
                0 => [
                    0 => 'system-messages-9828'
                ]
            ],
            '/navigation/' => [
                0 => [
                    0 => 'logo-5992 25',
                    1 => 'menu-2350 75'
                ],
                1 => [
                    0 => 'logo-3146'
                ]
            ],
            '/slideshow/' => [
                
            ],
            '/header/' => [
                
            ],
            '/above/' => [
                
            ],
            '/showcase/' => [
                
            ],
            '/utility/' => [
                
            ],
            '/feature/' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 26' => [
                            0 => [
                                0 => 'position-position-9414'
                            ]
                        ]
                    ],
                    1 => [
                        'mainbar 49' => [
                            
                        ]
                    ],
                    2 => [
                        'aside 25' => [
                            0 => [
                                0 => 'position-position-8807'
                            ]
                        ]
                    ]
                ]
            ],
            '/expanded/' => [
                
            ],
            '/extension/' => [
                
            ],
            '/bottom/' => [
                
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        '/sidebar-footer/ 40' => [
                            0 => [
                                0 => 'simplecontent-5244'
                            ]
                        ]
                    ],
                    1 => [
                        'mainbar-footer 30' => [
                            0 => [
                                0 => 'simplecontent-8236'
                            ]
                        ]
                    ],
                    2 => [
                        '/aside-footer/ 30' => [
                            0 => [
                                0 => 'simplecontent-4905'
                            ]
                        ]
                    ]
                ]
            ],
            '/copyright/' => [
                0 => [
                    0 => 'branding-1506'
                ]
            ],
            'offcanvas' => [
                0 => [
                    0 => 'mobile-menu-7951'
                ]
            ]
        ],
        'structure' => [
            'top' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-top section-horizontal-paddings'
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-fluid-navigation'
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-slideshow section-horizontal-paddings'
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-header section-horizontal-paddings section-vertical-paddings-small'
                ]
            ],
            'above' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-above section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'utility' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-utility section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'feature' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-feature section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'subtype' => 'aside',
                'attributes' => [
                    'class' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'subtype' => 'main',
                'title' => 'Main',
                'attributes' => [
                    'class' => 'section-horizontal-paddings'
                ]
            ],
            'aside' => [
                'attributes' => [
                    'class' => ''
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-expanded section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'extension' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-extension section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-bottom section-horizontal-paddings section-vertical-paddings'
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar',
                'attributes' => [
                    'boxed' => '3',
                    'class' => ''
                ]
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main'
            ],
            'aside-footer' => [
                'title' => 'Footer Aside',
                'attributes' => [
                    'boxed' => '3',
                    'class' => ''
                ]
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'offcanvas' => [
                'attributes' => [
                    'swipe' => '0'
                ]
            ]
        ],
        'content' => [
            'logo-5992' => [
                'attributes' => [
                    'image' => 'gantry-media://RCET_Logo.jpg'
                ]
            ],
            'logo-3146' => [
                'title' => 'SVG Pattern',
                'attributes' => [
                    'link' => '0',
                    'svg' => '<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 2560 431">
<path
 d="M2141.000,1190.000 L2561.000,1190.000 L2561.000,1250.000 L959.000,1250.000 L216.000,1250.000 L213.000,1250.000 C196.432,1250.000 183.000,1236.569 183.000,1220.000 C183.000,1203.432 196.432,1190.000 213.000,1190.000 L216.000,1190.000 L466.000,1190.000 C482.569,1190.000 496.000,1176.568 496.000,1160.000 C496.000,1143.431 482.569,1130.000 466.000,1130.000 L1.134,1130.000 L1.134,1190.000 L1.134,1250.000 L1.000,1250.000 L1.000,950.000 L407.000,950.000 L1502.000,950.000 L1931.000,950.000 C1947.569,950.000 1961.000,963.431 1961.000,980.000 C1961.000,996.568 1947.569,1010.000 1931.000,1010.000 L1541.000,1010.000 C1524.431,1010.000 1511.000,1023.431 1511.000,1040.000 C1511.000,1056.568 1524.431,1070.000 1541.000,1070.000 L2561.000,1070.000 L2561.000,1130.000 L2141.000,1130.000 C2124.431,1130.000 2111.000,1143.431 2111.000,1160.000 C2111.000,1176.568 2124.431,1190.000 2141.000,1190.000 ZM602.000,1010.000 L407.000,1010.000 L82.000,1010.000 C65.431,1010.000 52.000,1023.431 52.000,1040.000 C52.000,1056.568 65.431,1070.000 82.000,1070.000 L602.000,1070.000 C618.569,1070.000 632.000,1056.568 632.000,1040.000 C632.000,1023.431 618.569,1010.000 602.000,1010.000 ZM1139.000,1070.000 L760.000,1070.000 C743.431,1070.000 730.000,1083.432 730.000,1100.000 C730.000,1116.569 743.431,1130.000 760.000,1130.000 L1139.000,1130.000 C1155.568,1130.000 1169.000,1116.569 1169.000,1100.000 C1169.000,1083.432 1155.568,1070.000 1139.000,1070.000 ZM1906.000,1130.000 L1353.000,1130.000 C1336.432,1130.000 1323.000,1143.431 1323.000,1160.000 C1323.000,1176.568 1336.432,1190.000 1353.000,1190.000 L1906.000,1190.000 C1922.569,1190.000 1936.000,1176.568 1936.000,1160.000 C1936.000,1143.431 1922.569,1130.000 1906.000,1130.000 ZM2560.381,950.000 L2561.000,950.000 L2561.000,1010.000 L2560.381,1010.000 L2560.381,950.000 ZM2560.491,360.000 L2560.491,420.000 L2477.000,420.000 C2460.431,420.000 2447.000,406.569 2447.000,390.000 C2447.000,373.432 2460.431,360.000 2477.000,360.000 L2560.491,360.000 ZM1161.000,360.000 C1177.569,360.000 1191.000,373.432 1191.000,390.000 C1191.000,406.569 1177.569,420.000 1161.000,420.000 L993.000,420.000 C976.432,420.000 963.000,406.569 963.000,390.000 C963.000,373.432 976.432,360.000 993.000,360.000 L1161.000,360.000 ZM223.000,360.000 C239.569,360.000 253.000,373.432 253.000,390.000 C253.000,406.569 239.569,420.000 223.000,420.000 L185.000,420.000 C168.431,420.000 155.000,406.569 155.000,390.000 C155.000,373.432 168.431,360.000 185.000,360.000 L223.000,360.000 ZM2560.585,240.000 L2560.873,240.000 L2560.873,180.000 L2096.000,180.000 C2079.431,180.000 2066.000,193.431 2066.000,210.000 C2066.000,226.568 2079.431,240.000 2096.000,240.000 L2346.000,240.000 L2349.000,240.000 C2365.568,240.000 2379.000,253.432 2379.000,270.000 C2379.000,286.569 2365.568,300.000 2349.000,300.000 L2346.000,300.000 L1603.000,300.000 L1.000,300.000 L1.000,60.000 L1.000,60.000 L1.000,120.000 L1021.000,120.000 C1037.569,120.000 1051.000,106.569 1051.000,90.000 C1051.000,73.432 1037.569,60.000 1021.000,60.000 L631.000,60.000 C614.431,60.000 601.000,46.568 601.000,30.000 C601.000,13.431 614.431,-0.000 631.000,-0.000 L1060.000,-0.000 L2155.000,-0.000 L2561.000,-0.000 L2561.000,300.000 L2560.585,300.000 L2560.585,240.000 ZM421.000,180.000 L1.465,180.000 L1.465,240.000 L421.000,240.000 C437.569,240.000 451.000,226.568 451.000,210.000 C451.000,193.431 437.569,180.000 421.000,180.000 ZM1209.000,180.000 L656.000,180.000 C639.431,180.000 626.000,193.431 626.000,210.000 C626.000,226.568 639.431,240.000 656.000,240.000 L1209.000,240.000 C1225.568,240.000 1239.000,226.568 1239.000,210.000 C1239.000,193.431 1225.568,180.000 1209.000,180.000 ZM1802.000,120.000 L1423.000,120.000 C1406.432,120.000 1393.000,133.431 1393.000,150.000 C1393.000,166.569 1406.432,180.000 1423.000,180.000 L1802.000,180.000 C1818.569,180.000 1832.000,166.569 1832.000,150.000 C1832.000,133.431 1818.569,120.000 1802.000,120.000 ZM2480.000,60.000 L2155.000,60.000 L1960.000,60.000 C1943.431,60.000 1930.000,73.432 1930.000,90.000 C1930.000,106.569 1943.431,120.000 1960.000,120.000 L2480.000,120.000 C2496.569,120.000 2510.000,106.569 2510.000,90.000 C2510.000,73.432 2496.569,60.000 2480.000,60.000 ZM115.000,1340.000 C115.000,1356.568 101.569,1370.000 85.000,1370.000 L0.401,1370.000 L0.401,1310.000 L85.000,1310.000 C101.569,1310.000 115.000,1323.431 115.000,1340.000 ZM1401.000,1310.000 L1569.000,1310.000 C1585.568,1310.000 1599.000,1323.431 1599.000,1340.000 C1599.000,1356.568 1585.568,1370.000 1569.000,1370.000 L1401.000,1370.000 C1384.431,1370.000 1371.000,1356.568 1371.000,1340.000 C1371.000,1323.431 1384.431,1310.000 1401.000,1310.000 ZM2339.000,1310.000 L2377.000,1310.000 C2393.569,1310.000 2407.000,1323.431 2407.000,1340.000 C2407.000,1356.568 2393.569,1370.000 2377.000,1370.000 L2339.000,1370.000 C2322.431,1370.000 2309.000,1356.568 2309.000,1340.000 C2309.000,1323.431 2322.431,1310.000 2339.000,1310.000 Z"/></path>
</svg>',
                    'text' => '',
                    'class' => ''
                ],
                'block' => [
                    'class' => 'svg-pattern-default'
                ]
            ],
            'position-position-9414' => [
                'title' => 'Sidebar',
                'attributes' => [
                    'key' => 'sidebar'
                ]
            ],
            'position-position-8807' => [
                'title' => 'Aside',
                'attributes' => [
                    'key' => 'aside'
                ]
            ],
            'simplecontent-5244' => [
                'title' => 'contact module',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => 'contact information',
                            'author' => '',
                            'leading_content' => 'You can find out more about the Romero Trust by contacting either of our schools:',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'contact information'
                        ]
                    ]
                ],
                'block' => [
                    'class' => 'nopaddingbottom'
                ]
            ],
            'simplecontent-8236' => [
                'title' => 'Simple Content',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => 'St John’s School & Sixth Form College – A Catholic Academy ',
                            'author' => '',
                            'leading_content' => 'w: <a href="https://www.stjohnsrc.org.uk">www.stjohnsrc.org.uk</a><br />
e: <a href="mailto:staff@stjohnsrc.org.uk">staff@stjohnsrc.org.uk</a></br />
t: 01388 603246',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'St John’s School & Sixth Form College – A Catholic Academy'
                        ]
                    ]
                ]
            ],
            'simplecontent-4905' => [
                'title' => 'Simple Content',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'standard',
                            'created_date' => '',
                            'content_title' => 'St Joseph’s Primary School – A Catholic Academy',
                            'author' => '',
                            'leading_content' => 'w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a><br />
t: +44 (0)1325 300 337<br />
e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a>',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'St Josephs\'s'
                        ]
                    ]
                ]
            ],
            'branding-1506' => [
                'attributes' => [
                    'content' => '&copy; 2019 by Romero Trust. All rights reserved.'
                ],
                'block' => [
                    'variations' => 'center'
                ]
            ]
        ]
    ]
];
