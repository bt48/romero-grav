<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/config/default/particles/copyright.yaml',
    'modified' => 1548871658,
    'data' => [
        'enabled' => '1',
        'date' => [
            'start' => '2007',
            'end' => 'now'
        ],
        'owner' => 'RocketTheme LLC',
        'link' => '',
        'target' => '_blank',
        'css' => [
            'class' => ''
        ]
    ]
];
