<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_about_us/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'pages_-_about_us',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1508514391
        ],
        'positions' => [
            'sidebar' => 'Sidebar',
            'aside' => 'Aside'
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'sidebar' => 'Sidebar',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'mainbar' => 'Main',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-9033' => 'Header - About Us',
                'simplecontent-9044' => 'Simple Content',
                'simplecontent-3298' => 'contact module',
                'simplecontent-5888' => 'Simple Content',
                'simplecontent-7104' => 'Simple Content'
            ],
            'logo' => [
                'logo-3428' => 'SVG Pattern',
                'logo-2707' => 'Logo',
                'logo-3317' => 'SVG Pattern'
            ],
            'content' => [
                'system-content-6590' => 'Page Content'
            ],
            'messages' => [
                'system-messages-5582' => 'System Messages'
            ],
            'menu' => [
                'menu-9174' => 'Menu'
            ],
            'position' => [
                'position-position-5406' => 'Sidebar',
                'position-position-8018' => 'Aside'
            ],
            'branding' => [
                'branding-9719' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-7692' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'sidebar' => 'sidebar',
                'aside' => 'aside',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-5582' => 'system-messages-9828',
                'logo-2707' => 'logo-5992',
                'menu-9174' => 'menu-2350',
                'logo-3317' => 'logo-3146',
                'position-position-5406' => 'position-position-9414',
                'position-position-8018' => 'position-position-8807',
                'simplecontent-3298' => 'simplecontent-5244',
                'simplecontent-5888' => 'simplecontent-8236',
                'simplecontent-7104' => 'simplecontent-4905',
                'branding-9719' => 'branding-1506',
                'mobile-menu-7692' => 'mobile-menu-7951'
            ]
        ]
    ]
];
