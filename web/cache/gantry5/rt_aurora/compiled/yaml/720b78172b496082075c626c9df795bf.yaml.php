<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/blueprints/styles/copyright.yaml',
    'modified' => 1548871658,
    'data' => [
        'name' => 'Copyright Styles',
        'description' => 'Copyright styles for the Aurora theme',
        'type' => 'section',
        'form' => [
            'fields' => [
                'background' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Background',
                    'default' => '#eaf6f4'
                ],
                'text-color' => [
                    'type' => 'input.colorpicker',
                    'label' => 'Text',
                    'default' => '#50818b'
                ]
            ]
        ]
    ]
];
