<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/_body_only/layout.yaml',
    'modified' => 1548871664,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/body-only.png',
            'name' => '_body_only',
            'timestamp' => 1509009075
        ],
        'layout' => [
            '/body/' => [
                0 => [
                    0 => 'system-messages-8195'
                ],
                1 => [
                    0 => 'system-content-2391'
                ]
            ]
        ],
        'structure' => [
            'body' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'system-messages-8195' => [
                'block' => [
                    'variations' => 'nomarginall nopaddingall'
                ]
            ]
        ]
    ]
];
