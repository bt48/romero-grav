<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => 'layouts_-_full_width',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'fullwidth',
            'timestamp' => 1508428073
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'simplecontent' => [
                'simplecontent-7561' => 'Intro Text',
                'simplecontent-7366' => 'contact module',
                'simplecontent-9754' => 'Simple Content',
                'simplecontent-9809' => 'Simple Content'
            ],
            'content' => [
                'system-content-6621' => 'Page Content'
            ],
            'messages' => [
                'system-messages-8361' => 'System Messages'
            ],
            'logo' => [
                'logo-5211' => 'Logo',
                'logo-3323' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-4340' => 'Menu'
            ],
            'branding' => [
                'branding-5506' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-6627' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'navigation' => 'navigation',
                'header' => 'header',
                'slideshow' => 'slideshow',
                'above' => 'above',
                'showcase' => 'showcase',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'system-messages-8361' => 'system-messages-9828',
                'logo-5211' => 'logo-5992',
                'menu-4340' => 'menu-2350',
                'logo-3323' => 'logo-3146',
                'simplecontent-7366' => 'simplecontent-5244',
                'simplecontent-9754' => 'simplecontent-8236',
                'simplecontent-9809' => 'simplecontent-4905',
                'branding-5506' => 'branding-1506',
                'mobile-menu-6627' => 'mobile-menu-7951'
            ]
        ]
    ]
];
