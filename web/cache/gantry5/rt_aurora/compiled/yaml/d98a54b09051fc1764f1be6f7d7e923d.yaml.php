<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
    'modified' => 1553164097,
    'data' => [
        'preset' => 'preset1',
        'base' => [
            'background' => '#ffffff',
            'text-color' => '#666666',
            'text-active-color' => '#31a594'
        ],
        'accent' => [
            'color-1' => '#8a7224',
            'color-2' => '#c7b31c',
            'color-3' => '#f7f3d2'
        ],
        'font' => [
            'family-default' => 'muli'
        ],
        'menustyle' => [
            'text-color' => '#2b2323',
            'text-color-alt' => '#8e9dab',
            'text-color-active' => '#8a7224',
            'background-active' => 'rgba(255,255,255, 0)',
            'sublevel-text-color' => '#000000',
            'sublevel-text-color-active' => '#31a594',
            'sublevel-background' => '#ffffff',
            'sublevel-background-active' => '#31a594'
        ],
        'top' => [
            'background' => '#ffffff',
            'text-color' => '#2b2323'
        ],
        'navigation' => [
            'background' => '#ffffff',
            'text-color' => '#2b2323'
        ],
        'header' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'above' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'main' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'showcase' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'slideshow' => [
            'background' => '#0d0d0d',
            'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg',
            'text-color' => '#ffffff'
        ],
        'utility' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'feature' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'expanded' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'extension' => [
            'background' => '#ffffff',
            'text-color' => '#666666'
        ],
        'bottom' => [
            'background' => '#0d0d0d',
            'text-color' => '#ffffff'
        ],
        'footer' => [
            'background' => '#f4f5e7',
            'text-color' => '#333333'
        ],
        'copyright' => [
            'background' => '#f4f5e7',
            'text-color' => '#333333'
        ],
        'offcanvas' => [
            'background' => '#ff4b64',
            'text-color' => '#ffffff',
            'toggle-color' => '#ffffff',
            'width' => '10rem',
            'toggle-visibility' => '1'
        ],
        'breakpoints' => [
            'large-desktop-container' => '75rem',
            'desktop-container' => '60rem',
            'tablet-container' => '51rem',
            'large-mobile-container' => '30rem',
            'mobile-menu-breakpoint' => '51rem'
        ],
        'menu' => [
            'animation' => 'g-fade'
        ]
    ]
];
