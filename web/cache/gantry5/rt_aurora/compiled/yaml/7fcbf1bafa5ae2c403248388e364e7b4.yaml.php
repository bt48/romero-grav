<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/index.yaml',
    'modified' => 1552957980,
    'data' => [
        'name' => 'page_-_st_johns',
        'timestamp' => 1552957980,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'fullwidth',
            'timestamp' => 1508428073
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9138' => 'System Messages'
            ],
            'logo' => [
                'logo-6511' => 'Logo',
                'logo-4091' => 'SVG Pattern'
            ],
            'menu' => [
                'menu-2983' => 'Menu'
            ],
            'simplecontent' => [
                'simplecontent-7561' => 'Intro Text',
                'simplecontent-5107' => 'Simple Content',
                'simplecontent-6722' => 'contact module',
                'simplecontent-9181' => 'Simple Content',
                'simplecontent-5675' => 'Simple Content'
            ],
            'branding' => [
                'branding-5737' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-7330' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'top' => 'top',
                'system-messages-9138' => 'system-messages-9828',
                'navigation' => 'navigation',
                'logo-6511' => 'logo-5992',
                'menu-2983' => 'menu-2350',
                'logo-4091' => 'logo-3146',
                'header' => 'header',
                'slideshow' => 'slideshow',
                'above' => 'above',
                'showcase' => 'showcase',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'simplecontent-6722' => 'simplecontent-5244',
                'mainbar-footer' => 'mainbar-footer',
                'simplecontent-9181' => 'simplecontent-8236',
                'aside-footer' => 'aside-footer',
                'simplecontent-5675' => 'simplecontent-4905',
                'copyright' => 'copyright',
                'branding-5737' => 'branding-1506',
                'offcanvas' => 'offcanvas',
                'mobile-menu-7330' => 'mobile-menu-7951'
            ],
            'layouts_-_full_width' => [
                'simplecontent-7561' => 'simplecontent-7561',
                'sidebar' => 'sidebar',
                'aside' => 'aside'
            ]
        ]
    ]
];
