<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/pages_-_about_us/styles.yaml',
    'modified' => 1552955464,
    'data' => [
        'preset' => 'preset1',
        'slideshow' => [
            'background' => '#8a7224',
            'background-image' => 'gantry-media://RCET_Logo.jpg',
            'text-color' => '#ffffff'
        ],
        'utility' => [
            'background' => '#333333',
            'text-color' => '#cccccc'
        ]
    ]
];
