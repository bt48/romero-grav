<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
    'modified' => 1552954183,
    'data' => [
        'enabled' => '1',
        'class' => '',
        'width' => 'g-newsletter-fullwidth',
        'layout' => 'g-newsletter-aside-compact',
        'style' => 'g-newsletter-rounded',
        'title' => '',
        'headtext' => '',
        'sidetext' => '',
        'inputboxtext' => 'Your email address...',
        'buttonicon' => '',
        'buttontext' => 'Subscribe',
        'uri' => '',
        'buttonclass' => 'button-xlarge button-block'
    ]
];
