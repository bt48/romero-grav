<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/_error/index.yaml',
    'modified' => 1552956792,
    'data' => [
        'name' => '_error',
        'timestamp' => 1552956792,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => '_error',
            'timestamp' => 1509009075
        ],
        'positions' => [
            
        ],
        'sections' => [
            'top' => 'Top',
            'navigation' => 'Navigation',
            'slideshow' => 'Slideshow',
            'above' => 'Above',
            'showcase' => 'Showcase',
            'utility' => 'Utility',
            'feature' => 'Feature',
            'sidebar' => 'Sidebar',
            'mainbar' => 'Mainbar',
            'expanded' => 'Expanded',
            'extension' => 'Extension',
            'bottom' => 'Bottom',
            'sidebar-footer' => 'Footer Sidebar',
            'mainbar-footer' => 'Footer Main',
            'copyright' => 'Copyright',
            'header' => 'Header',
            'aside' => 'Aside',
            'aside-footer' => 'Footer Aside',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'messages' => [
                'system-messages-9828' => 'System Messages'
            ],
            'logo' => [
                'logo-6939' => 'Logo'
            ],
            'menu' => [
                'menu-5286' => 'Menu'
            ],
            'simplecontent' => [
                'simplecontent-7561' => 'We are sorry!',
                'simplecontent-5027' => 'contact module',
                'simplecontent-8166' => 'Simple Content',
                'simplecontent-8175' => 'Simple Content'
            ],
            'content' => [
                'system-content-3192' => 'Page Content'
            ],
            'branding' => [
                'branding-1298' => 'Branding'
            ],
            'mobile-menu' => [
                'mobile-menu-4522' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            'default' => [
                'navigation' => 'navigation',
                'logo-6939' => 'logo-5992',
                'menu-5286' => 'menu-2350',
                'header' => 'header',
                'slideshow' => 'slideshow',
                'above' => 'above',
                'showcase' => 'showcase',
                'utility' => 'utility',
                'feature' => 'feature',
                'expanded' => 'expanded',
                'extension' => 'extension',
                'bottom' => 'bottom',
                'sidebar-footer' => 'sidebar-footer',
                'mainbar-footer' => 'mainbar-footer',
                'aside-footer' => 'aside-footer',
                'copyright' => 'copyright',
                'offcanvas' => 'offcanvas',
                'simplecontent-5027' => 'simplecontent-5244',
                'simplecontent-8166' => 'simplecontent-8236',
                'simplecontent-8175' => 'simplecontent-4905',
                'branding-1298' => 'branding-1506',
                'mobile-menu-4522' => 'mobile-menu-7951'
            ]
        ]
    ]
];
