<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/styles.yaml',
    'modified' => 1552955449,
    'data' => [
        'preset' => 'preset1',
        'menustyle' => [
            'background-active' => 'rgba(255,255,255, 0)'
        ],
        'slideshow' => [
            'background' => '#8a7224',
            'background-image' => 'gantry-media://pupils.jpg',
            'text-color' => '#ffffff'
        ]
    ]
];
