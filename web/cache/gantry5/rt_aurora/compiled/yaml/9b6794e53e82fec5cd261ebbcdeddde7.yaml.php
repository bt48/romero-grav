<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/config/default/particles/branding.yaml',
    'modified' => 1548871658,
    'data' => [
        'enabled' => '1',
        'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
        'css' => [
            'class' => ''
        ]
    ]
];
