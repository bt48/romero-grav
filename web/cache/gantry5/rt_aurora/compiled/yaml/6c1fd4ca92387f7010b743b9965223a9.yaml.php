<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/_offline/layout.yaml',
    'modified' => 1552956792,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => '_offline',
            'timestamp' => 1509009075
        ],
        'layout' => [
            'top' => [
                0 => [
                    0 => 'system-messages-9828'
                ]
            ],
            'navigation' => [
                0 => [
                    0 => 'logo-5992'
                ]
            ],
            'slideshow' => [
                0 => [
                    0 => 'simplecontent-5551'
                ]
            ],
            '/header/' => [
                
            ],
            '/above/' => [
                
            ],
            '/showcase/' => [
                
            ],
            '/utility/' => [
                
            ],
            '/feature/' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 20' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 60' => [
                            0 => [
                                0 => 'system-content-7661'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 20' => [
                            
                        ]
                    ]
                ]
            ],
            '/expanded/' => [
                
            ],
            'extension' => [
                
            ],
            'bottom' => [
                0 => [
                    0 => 'blockcontent-4574'
                ]
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        'sidebar-footer 30' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar-footer 40' => [
                            
                        ]
                    ],
                    2 => [
                        'aside-footer 30' => [
                            
                        ]
                    ]
                ]
            ],
            'copyright' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'top' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ],
                'type' => 'section'
            ],
            'navigation' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-header'
                ]
            ],
            'above' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-above'
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-showcase'
                ]
            ],
            'utility' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-utility'
                ]
            ],
            'feature' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-feature'
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => ''
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => ''
                ],
                'block' => [
                    'class' => 'equal-height',
                    'variations' => 'center'
                ]
            ],
            'aside' => [
                'attributes' => [
                    'class' => ''
                ],
                'block' => [
                    'fixed' => '1'
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '0',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '1',
                    'class' => 'g-default-expanded'
                ]
            ],
            'extension' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar'
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main'
            ],
            'aside-footer' => [
                'title' => 'Footer Aside'
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'logo-5992' => [
                'block' => [
                    'variations' => 'nomarginall center'
                ]
            ],
            'simplecontent-5551' => [
                'title' => 'Offline Description',
                'attributes' => [
                    'class' => '',
                    'title' => 'Offline',
                    'items' => [
                        0 => [
                            'layout' => 'header',
                            'created_date' => '',
                            'content_title' => 'Site offline',
                            'author' => '',
                            'leading_content' => 'We are working on new and exciting stuff.',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'Offline'
                        ]
                    ]
                ],
                'block' => [
                    'variations' => 'align-left'
                ]
            ],
            'system-content-7661' => [
                'block' => [
                    'variations' => 'title6'
                ]
            ],
            'blockcontent-4574' => [
                'title' => 'Maintenance Mode',
                'attributes' => [
                    'class' => '',
                    'headline' => 'Maintenance Mode',
                    'description' => 'Sorry, our Website is Temporarily Down for Maintenance. Please Check Back Again Soon.'
                ],
                'block' => [
                    'class' => 'g-title-large',
                    'variations' => 'center'
                ]
            ]
        ]
    ]
];
