<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/data/gantry5/themes/rt_aurora/config/_error/layout.yaml',
    'modified' => 1552956792,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => '_error',
            'timestamp' => 1509009075
        ],
        'layout' => [
            '/top/' => [
                0 => [
                    0 => 'system-messages-9828'
                ]
            ],
            'navigation' => [
                0 => [
                    0 => 'logo-6939 20',
                    1 => 'menu-5286 80'
                ]
            ],
            'header' => [
                
            ],
            'slideshow' => [
                0 => [
                    0 => 'simplecontent-7561'
                ]
            ],
            'above' => [
                
            ],
            'showcase' => [
                
            ],
            'utility' => [
                
            ],
            'feature' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'sidebar 20' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar 60' => [
                            0 => [
                                0 => 'system-content-3192'
                            ]
                        ]
                    ],
                    2 => [
                        'aside 20' => [
                            
                        ]
                    ]
                ]
            ],
            'expanded' => [
                
            ],
            'extension' => [
                
            ],
            'bottom' => [
                
            ],
            '/container-footer/' => [
                0 => [
                    0 => [
                        'sidebar-footer 30' => [
                            
                        ]
                    ],
                    1 => [
                        'mainbar-footer 40' => [
                            
                        ]
                    ],
                    2 => [
                        'aside-footer 30' => [
                            
                        ]
                    ]
                ]
            ],
            'copyright' => [
                
            ],
            'offcanvas' => [
                
            ]
        ],
        'structure' => [
            'top' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '3'
                ]
            ],
            'navigation' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'header' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'slideshow' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'above' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'showcase' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'utility' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'feature' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'sidebar' => [
                'type' => 'section',
                'block' => [
                    'class' => 'equal-height'
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'attributes' => [
                    'class' => ''
                ],
                'block' => [
                    'class' => 'equal-height',
                    'variations' => 'center'
                ]
            ],
            'aside' => [
                'block' => [
                    'class' => 'equal-height'
                ]
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'g-default-main',
                    'extra' => [
                        
                    ]
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'extension' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'bottom' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes'
                    ]
                ]
            ],
            'sidebar-footer' => [
                'type' => 'section',
                'title' => 'Footer Sidebar',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'mainbar-footer' => [
                'type' => 'section',
                'title' => 'Footer Main',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'aside-footer' => [
                'title' => 'Footer Aside',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block',
                        2 => 'children'
                    ]
                ]
            ],
            'container-footer' => [
                'attributes' => [
                    'boxed' => '3',
                    'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                    'extra' => [
                        
                    ]
                ]
            ],
            'copyright' => [
                'type' => 'section',
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ],
            'offcanvas' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'children'
                    ]
                ]
            ]
        ],
        'content' => [
            'logo-6939' => [
                'inherit' => [
                    'outline' => 'default',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block'
                    ],
                    'particle' => 'logo-5992'
                ]
            ],
            'menu-5286' => [
                'inherit' => [
                    'outline' => 'default',
                    'particle' => 'menu-2350',
                    'include' => [
                        0 => 'attributes',
                        1 => 'block'
                    ]
                ]
            ],
            'simplecontent-7561' => [
                'title' => 'We are sorry!',
                'attributes' => [
                    'class' => '',
                    'title' => '',
                    'items' => [
                        0 => [
                            'layout' => 'header',
                            'created_date' => '',
                            'content_title' => '<strong>We</strong> are sorry!',
                            'author' => '',
                            'leading_content' => 'Gantry 5 provides a custom error page for you to configure with either positions, particles or a combination of both, as well as styling.',
                            'main_content' => '',
                            'readmore_label' => '',
                            'readmore_link' => '',
                            'readmore_class' => '',
                            'readmore_target' => '_self',
                            'title' => 'We are sorry!'
                        ]
                    ]
                ]
            ],
            'system-content-3192' => [
                'block' => [
                    'class' => 'g-error'
                ]
            ]
        ]
    ]
];
