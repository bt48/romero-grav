<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => '/app/web/user/themes/rt_aurora/config/default/particles/totop.yaml',
    'modified' => 1548871658,
    'data' => [
        'enabled' => '1',
        'css' => [
            'class' => 'g-totop'
        ],
        'icon' => '',
        'content' => 'Top'
    ]
];
