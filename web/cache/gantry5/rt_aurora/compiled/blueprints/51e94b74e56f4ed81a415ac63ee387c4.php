<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledBlueprints',
    'timestamp' => 1552956984,
    'checksum' => '9ad00ee932d0327753cc31e6dbcf60cc',
    'files' => [
        'user/themes/rt_aurora/particles' => [
            'particles/aos' => [
                'file' => 'user/themes/rt_aurora/particles/aos.yaml',
                'modified' => 1548871658
            ],
            'particles/audioplayer' => [
                'file' => 'user/themes/rt_aurora/particles/audioplayer.yaml',
                'modified' => 1548871658
            ],
            'particles/blockcontent' => [
                'file' => 'user/themes/rt_aurora/particles/blockcontent.yaml',
                'modified' => 1548871658
            ],
            'particles/calendar' => [
                'file' => 'user/themes/rt_aurora/particles/calendar.yaml',
                'modified' => 1548871658
            ],
            'particles/carousel' => [
                'file' => 'user/themes/rt_aurora/particles/carousel.yaml',
                'modified' => 1548871658
            ],
            'particles/casestudies' => [
                'file' => 'user/themes/rt_aurora/particles/casestudies.yaml',
                'modified' => 1548871658
            ],
            'particles/fixedheader' => [
                'file' => 'user/themes/rt_aurora/particles/fixedheader.yaml',
                'modified' => 1548871658
            ],
            'particles/gridcontent' => [
                'file' => 'user/themes/rt_aurora/particles/gridcontent.yaml',
                'modified' => 1548871658
            ],
            'particles/gridstatistic' => [
                'file' => 'user/themes/rt_aurora/particles/gridstatistic.yaml',
                'modified' => 1548871658
            ],
            'particles/imagegrid' => [
                'file' => 'user/themes/rt_aurora/particles/imagegrid.yaml',
                'modified' => 1548871658
            ],
            'particles/infolist' => [
                'file' => 'user/themes/rt_aurora/particles/infolist.yaml',
                'modified' => 1548871658
            ],
            'particles/newsletter' => [
                'file' => 'user/themes/rt_aurora/particles/newsletter.yaml',
                'modified' => 1548871658
            ],
            'particles/panelslider' => [
                'file' => 'user/themes/rt_aurora/particles/panelslider.yaml',
                'modified' => 1548871658
            ],
            'particles/pricingtable' => [
                'file' => 'user/themes/rt_aurora/particles/pricingtable.yaml',
                'modified' => 1548871658
            ],
            'particles/search' => [
                'file' => 'user/themes/rt_aurora/particles/search.yaml',
                'modified' => 1548871658
            ],
            'particles/simplecontent' => [
                'file' => 'user/themes/rt_aurora/particles/simplecontent.yaml',
                'modified' => 1548871658
            ],
            'particles/simplecounter' => [
                'file' => 'user/themes/rt_aurora/particles/simplecounter.yaml',
                'modified' => 1548871658
            ],
            'particles/simplemenu' => [
                'file' => 'user/themes/rt_aurora/particles/simplemenu.yaml',
                'modified' => 1548871658
            ],
            'particles/testimonials' => [
                'file' => 'user/themes/rt_aurora/particles/testimonials.yaml',
                'modified' => 1548871658
            ],
            'particles/verticalslider' => [
                'file' => 'user/themes/rt_aurora/particles/verticalslider.yaml',
                'modified' => 1548871658
            ],
            'particles/video' => [
                'file' => 'user/themes/rt_aurora/particles/video.yaml',
                'modified' => 1548871658
            ]
        ],
        'user/plugins/gantry5/engines/nucleus/particles' => [
            'particles/analytics' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/analytics.yaml',
                'modified' => 1544785254
            ],
            'particles/assets' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/assets.yaml',
                'modified' => 1544785254
            ],
            'particles/branding' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/branding.yaml',
                'modified' => 1544785254
            ],
            'particles/breadcrumbs' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/breadcrumbs.yaml',
                'modified' => 1544785254
            ],
            'particles/content' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/content.yaml',
                'modified' => 1544785254
            ],
            'particles/contentarray' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/contentarray.yaml',
                'modified' => 1544785254
            ],
            'particles/copyright' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/copyright.yaml',
                'modified' => 1544785254
            ],
            'particles/custom' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/custom.yaml',
                'modified' => 1544785254
            ],
            'particles/date' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/date.yaml',
                'modified' => 1544785254
            ],
            'particles/feed' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/feed.yaml',
                'modified' => 1544785254
            ],
            'particles/frameworks' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/frameworks.yaml',
                'modified' => 1544785254
            ],
            'particles/langswitcher' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/langswitcher.yaml',
                'modified' => 1544785254
            ],
            'particles/lightcase' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/lightcase.yaml',
                'modified' => 1544785254
            ],
            'particles/login' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/login.yaml',
                'modified' => 1544785254
            ],
            'particles/logo' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/logo.yaml',
                'modified' => 1544785254
            ],
            'particles/menu' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/menu.yaml',
                'modified' => 1544785254
            ],
            'particles/messages' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/messages.yaml',
                'modified' => 1544785254
            ],
            'particles/mobile-menu' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/mobile-menu.yaml',
                'modified' => 1544785254
            ],
            'particles/position' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/position.yaml',
                'modified' => 1544785254
            ],
            'particles/search' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/search.yaml',
                'modified' => 1544785254
            ],
            'particles/social' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/social.yaml',
                'modified' => 1544785254
            ],
            'particles/spacer' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/spacer.yaml',
                'modified' => 1544785254
            ],
            'particles/totop' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/totop.yaml',
                'modified' => 1544785254
            ]
        ],
        'user/themes/rt_aurora/blueprints' => [
            'styles/above' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/above.yaml',
                'modified' => 1548871658
            ],
            'styles/accent' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/accent.yaml',
                'modified' => 1548871658
            ],
            'styles/base' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/base.yaml',
                'modified' => 1548871658
            ],
            'styles/bottom' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/bottom.yaml',
                'modified' => 1548871658
            ],
            'styles/breakpoints' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/breakpoints.yaml',
                'modified' => 1548871658
            ],
            'styles/copyright' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/copyright.yaml',
                'modified' => 1548871658
            ],
            'styles/expanded' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/expanded.yaml',
                'modified' => 1548871658
            ],
            'styles/extension' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/extension.yaml',
                'modified' => 1548871658
            ],
            'styles/feature' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/feature.yaml',
                'modified' => 1548871658
            ],
            'styles/font' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/font.yaml',
                'modified' => 1548871658
            ],
            'styles/footer' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/footer.yaml',
                'modified' => 1548871658
            ],
            'styles/header' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/header.yaml',
                'modified' => 1548871658
            ],
            'styles/main' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/main.yaml',
                'modified' => 1548871658
            ],
            'styles/menu' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/menu.yaml',
                'modified' => 1548871658
            ],
            'styles/menustyle' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/menustyle.yaml',
                'modified' => 1548871658
            ],
            'styles/navigation' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/navigation.yaml',
                'modified' => 1548871658
            ],
            'styles/offcanvas' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/offcanvas.yaml',
                'modified' => 1548871658
            ],
            'styles/showcase' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/showcase.yaml',
                'modified' => 1548871658
            ],
            'styles/slideshow' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/slideshow.yaml',
                'modified' => 1548871658
            ],
            'styles/top' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/top.yaml',
                'modified' => 1548871658
            ],
            'styles/utility' => [
                'file' => 'user/themes/rt_aurora/blueprints/styles/utility.yaml',
                'modified' => 1548871658
            ]
        ],
        'user/plugins/gantry5/engines/nucleus/blueprints' => [
            'page/assets' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/assets.yaml',
                'modified' => 1544785254
            ],
            'page/body' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/body.yaml',
                'modified' => 1544785254
            ],
            'page/head' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/head.yaml',
                'modified' => 1544785254
            ],
            'pages/blog_item' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/blog_item.yaml',
                'modified' => 1544785254
            ],
            'pages/blog_list' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/blog_list.yaml',
                'modified' => 1544785254
            ],
            'pages/form' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/form.yaml',
                'modified' => 1544785254
            ],
            'pages/modular/features' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/modular/features.yaml',
                'modified' => 1544785254
            ],
            'pages/modular/showcase' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/modular/showcase.yaml',
                'modified' => 1544785254
            ],
            'pages/modular/text' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/modular/text.yaml',
                'modified' => 1544785254
            ]
        ]
    ],
    'data' => [
        'items' => [
            'particles.aos' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles' => [
                'type' => '_parent',
                'name' => 'particles',
                'form_field' => false
            ],
            'particles.aos.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.aos.enabled'
            ],
            'particles.aos.duration' => [
                'type' => 'input.text',
                'label' => 'Duration',
                'description' => 'Values from 0 to 3000, with step 50ms',
                'default' => '1000',
                'name' => 'particles.aos.duration'
            ],
            'particles.aos.once' => [
                'type' => 'select.select',
                'label' => 'Once',
                'description' => 'Whether animation should happen only once - while scrolling down',
                'default' => true,
                'options' => [
                    1 => 'true',
                    0 => 'false'
                ],
                'name' => 'particles.aos.once'
            ],
            'particles.aos.delay' => [
                'type' => 'input.text',
                'label' => 'Delay',
                'description' => 'Values from 0 to 3000, with step 50ms',
                'default' => '0',
                'name' => 'particles.aos.delay'
            ],
            'particles.aos.easing' => [
                'type' => 'input.text',
                'label' => 'Easing',
                'description' => 'Default easing for AOS animations',
                'default' => 'ease',
                'name' => 'particles.aos.easing'
            ],
            'particles.aos.offset' => [
                'type' => 'input.text',
                'label' => 'Offset',
                'description' => 'Offset (in px) from the original trigger point',
                'default' => '120',
                'name' => 'particles.aos.offset'
            ],
            'particles.audioplayer' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.audioplayer.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.audioplayer.enabled'
            ],
            'particles.audioplayer.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.audioplayer.class'
            ],
            'particles.audioplayer.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.audioplayer.title'
            ],
            'particles.audioplayer.nowplaying' => [
                'type' => 'input.text',
                'label' => 'Now Playing Label',
                'description' => 'Customize the now playing text.',
                'default' => 'Now Playing',
                'name' => 'particles.audioplayer.nowplaying'
            ],
            'particles.audioplayer.scrollbar' => [
                'type' => 'select.selectize',
                'label' => 'Playlist Layout',
                'description' => 'Decide if you want playlist with scrollbar and overflow or without.',
                'default' => 'noscrollbar',
                'options' => [
                    'scrollbar' => 'With Scrollbar',
                    'noscrollbar' => 'Without Scrollbar'
                ],
                'name' => 'particles.audioplayer.scrollbar'
            ],
            'particles.audioplayer.overflow' => [
                'type' => 'input.number',
                'label' => 'Overflow Height',
                'description' => 'Specify overflow size for playlist with scrollbar.',
                'default' => 350,
                'name' => 'particles.audioplayer.overflow'
            ],
            'particles.audioplayer.soundcloudclientid' => [
                'type' => 'input.text',
                'label' => 'SoundCloud Client ID',
                'description' => 'Provide client id.',
                'name' => 'particles.audioplayer.soundcloudclientid'
            ],
            'particles.audioplayer.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Files',
                'description' => 'Add audio player items.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.audioplayer.items'
            ],
            'particles.audioplayer.items.*' => [
                'type' => '_parent',
                'name' => 'particles.audioplayer.items.*',
                'form_field' => false
            ],
            'particles.audioplayer.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Item Title',
                'description' => 'Customize item title.',
                'name' => 'particles.audioplayer.items.*.title'
            ],
            'particles.audioplayer.items.*.artist' => [
                'type' => 'input.text',
                'label' => 'Artist',
                'description' => 'Customize artist name.',
                'name' => 'particles.audioplayer.items.*.artist'
            ],
            'particles.audioplayer.items.*.tracktitle' => [
                'type' => 'input.text',
                'label' => 'Track Title',
                'description' => 'Customize track title.',
                'name' => 'particles.audioplayer.items.*.tracktitle'
            ],
            'particles.audioplayer.items.*.cover' => [
                'type' => 'input.imagepicker',
                'label' => 'Album Cover',
                'description' => 'Select album cover.',
                'name' => 'particles.audioplayer.items.*.cover'
            ],
            'particles.audioplayer.items.*.source' => [
                'type' => 'select.selectize',
                'label' => 'Audio Source',
                'description' => 'Audio source. Local, external url or SoundCloud.',
                'default' => 'local',
                'options' => [
                    'local' => 'Local File',
                    'external' => 'External Url',
                    'soundcloud' => 'SoundCloud Track'
                ],
                'name' => 'particles.audioplayer.items.*.source'
            ],
            'particles.audioplayer.items.*.externalurl' => [
                'type' => 'input.text',
                'label' => 'External Url',
                'description' => 'URL for external audio file',
                'name' => 'particles.audioplayer.items.*.externalurl'
            ],
            'particles.audioplayer.items.*.localurl' => [
                'type' => 'input.filepicker',
                'label' => 'Local Audio',
                'description' => 'Select desired audio file.',
                'name' => 'particles.audioplayer.items.*.localurl'
            ],
            'particles.audioplayer.items.*.soundcloudurl' => [
                'type' => 'input.text',
                'label' => 'SoundCloud Track',
                'description' => 'Soundcloud track number. Grab it from embed code.',
                'name' => 'particles.audioplayer.items.*.soundcloudurl'
            ],
            'particles.audioplayer.items.*.soundcloudartist' => [
                'type' => 'select.select',
                'label' => 'SoundCloud Artist',
                'description' => 'Enable or disable SoundCloud artist display in track title.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.audioplayer.items.*.soundcloudartist'
            ],
            'particles.audioplayer.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'description' => 'Input the item link.',
                'name' => 'particles.audioplayer.items.*.link'
            ],
            'particles.audioplayer.items.*.linktext' => [
                'type' => 'input.text',
                'label' => 'Link Text',
                'description' => 'Input the text for the item link.',
                'name' => 'particles.audioplayer.items.*.linktext'
            ],
            'particles.blockcontent' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.blockcontent.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.blockcontent.enabled'
            ],
            'particles.blockcontent.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.blockcontent.class'
            ],
            'particles.blockcontent.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.blockcontent.title'
            ],
            'particles.blockcontent.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Select the icon.',
                'name' => 'particles.blockcontent.icon'
            ],
            'particles.blockcontent.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select the main image.',
                'name' => 'particles.blockcontent.image'
            ],
            'particles.blockcontent.headline' => [
                'type' => 'input.text',
                'label' => 'Headline',
                'description' => 'Customize the headline text.',
                'name' => 'particles.blockcontent.headline'
            ],
            'particles.blockcontent.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.blockcontent.description'
            ],
            'particles.blockcontent.linktext' => [
                'type' => 'input.text',
                'label' => 'Button Label',
                'description' => 'Customize the button label.',
                'name' => 'particles.blockcontent.linktext'
            ],
            'particles.blockcontent.link' => [
                'type' => 'input.text',
                'label' => 'Button Link',
                'description' => 'Specify the button link address.',
                'name' => 'particles.blockcontent.link'
            ],
            'particles.blockcontent.linkclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class names for the button.',
                'name' => 'particles.blockcontent.linkclass'
            ],
            'particles.blockcontent.linktarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.blockcontent.linktarget'
            ],
            'particles.blockcontent.subcontents' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Content Items',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.blockcontent.subcontents'
            ],
            'particles.blockcontent.subcontents.*' => [
                'type' => '_parent',
                'name' => 'particles.blockcontent.subcontents.*',
                'form_field' => false
            ],
            'particles.blockcontent.subcontents.*.name' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter title.',
                'name' => 'particles.blockcontent.subcontents.*.name'
            ],
            'particles.blockcontent.subcontents.*.accent' => [
                'type' => 'select.selectize',
                'label' => 'Accent',
                'description' => 'Select preferred block accent color.',
                'default' => 'none',
                'options' => [
                    'none' => 'None',
                    'accent1' => 'Accent Color 1',
                    'accent2' => 'Accent Color 2',
                    'accent3' => 'Accent Color 3'
                ],
                'name' => 'particles.blockcontent.subcontents.*.accent'
            ],
            'particles.blockcontent.subcontents.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Select the icon.',
                'name' => 'particles.blockcontent.subcontents.*.icon'
            ],
            'particles.blockcontent.subcontents.*.img' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select the image.',
                'name' => 'particles.blockcontent.subcontents.*.img'
            ],
            'particles.blockcontent.subcontents.*.rokboximage' => [
                'type' => 'input.imagepicker',
                'label' => 'Lightcase Image',
                'description' => 'Select the Lightcase image.',
                'name' => 'particles.blockcontent.subcontents.*.rokboximage'
            ],
            'particles.blockcontent.subcontents.*.rokboxcaption' => [
                'type' => 'input.text',
                'label' => 'Caption',
                'description' => 'Customize the Lightcase caption.',
                'name' => 'particles.blockcontent.subcontents.*.rokboxcaption'
            ],
            'particles.blockcontent.subcontents.*.subtitle' => [
                'type' => 'input.text',
                'label' => 'Subtitle',
                'description' => 'Enter subtitle.',
                'name' => 'particles.blockcontent.subcontents.*.subtitle'
            ],
            'particles.blockcontent.subcontents.*.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Enter the descriptiopn.',
                'name' => 'particles.blockcontent.subcontents.*.description'
            ],
            'particles.blockcontent.subcontents.*.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.blockcontent.subcontents.*.class'
            ],
            'particles.blockcontent.subcontents.*.button' => [
                'type' => 'input.text',
                'label' => 'Button Label',
                'description' => 'Specify the button label.',
                'name' => 'particles.blockcontent.subcontents.*.button'
            ],
            'particles.blockcontent.subcontents.*.buttonlink' => [
                'type' => 'input.text',
                'label' => 'Button Link',
                'description' => 'Specify the button link.',
                'name' => 'particles.blockcontent.subcontents.*.buttonlink'
            ],
            'particles.blockcontent.subcontents.*.buttonclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class names for the button.',
                'name' => 'particles.blockcontent.subcontents.*.buttonclass'
            ],
            'particles.blockcontent.subcontents.*.buttontarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.blockcontent.subcontents.*.buttontarget'
            ],
            'particles.calendar' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.calendar.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.calendar.enabled'
            ],
            'particles.calendar.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.calendar.class'
            ],
            'particles.calendar.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.calendar.title'
            ],
            'particles.calendar.eventsheader' => [
                'type' => 'input.text',
                'label' => 'Events Header',
                'description' => 'Customize the events header.',
                'name' => 'particles.calendar.eventsheader'
            ],
            'particles.calendar.events' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Events',
                'description' => 'Create each set of images to appear in column.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.calendar.events'
            ],
            'particles.calendar.events.*' => [
                'type' => '_parent',
                'name' => 'particles.calendar.events.*',
                'form_field' => false
            ],
            'particles.calendar.events.*.title' => [
                'type' => 'input.text',
                'label' => 'Event Title',
                'name' => 'particles.calendar.events.*.title'
            ],
            'particles.calendar.events.*.begin' => [
                'type' => 'input.date',
                'label' => 'Begin Date',
                'name' => 'particles.calendar.events.*.begin'
            ],
            'particles.calendar.events.*.end' => [
                'type' => 'input.date',
                'label' => 'End Date',
                'name' => 'particles.calendar.events.*.end'
            ],
            'particles.calendar.events.*.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'name' => 'particles.calendar.events.*.description'
            ],
            'particles.calendar.events.*.link' => [
                'type' => 'input.text',
                'label' => 'Event Link',
                'name' => 'particles.calendar.events.*.link'
            ],
            'particles.calendar.events.*.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.calendar.events.*.target'
            ],
            'particles.carousel' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.carousel.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particles.',
                'default' => true,
                'name' => 'particles.carousel.enabled'
            ],
            'particles.carousel.source' => [
                'type' => 'select.select',
                'label' => 'Content Source',
                'description' => 'Choose if the content should be loaded from the WordPress posts or particle itself.',
                'default' => 'particle',
                'options' => [
                    'particle' => 'Particle',
                    'grav' => 'Grav'
                ],
                'name' => 'particles.carousel.source'
            ],
            'particles.carousel.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.carousel.class'
            ],
            'particles.carousel.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.carousel.title'
            ],
            'particles.carousel.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description text.',
                'name' => 'particles.carousel.description'
            ],
            'particles.carousel.animateOut' => [
                'type' => 'select.select',
                'label' => 'Out Animation',
                'description' => 'Customize the Out Animation from animate css class.',
                'default' => 'fadeOut',
                'options' => [
                    'default' => 'default',
                    'bounce' => 'bounce',
                    'flash' => 'flash',
                    'pulse' => 'pulse',
                    'rubberBand' => 'rubberBand',
                    'shake' => 'shake',
                    'swing' => 'swing',
                    'tada' => 'tada',
                    'wobble' => 'wobble',
                    'jello' => 'jello',
                    'bounceIn' => 'bounceIn',
                    'bounceInDown' => 'bounceInDown',
                    'bounceInLeft' => 'bounceInLeft',
                    'bounceInRight' => 'bounceInRight',
                    'bounceInUp' => 'bounceInUp',
                    'bounceOut' => 'bounceOut',
                    'bounceOutDown' => 'bounceOutDown',
                    'bounceOutLeft' => 'bounceOutLeft',
                    'bounceOutRight' => 'bounceOutRight',
                    'bounceOutUp' => 'bounceOutUp',
                    'fadeIn' => 'fadeIn',
                    'fadeInDown' => 'fadeInDown',
                    'fadeInDownBig' => 'fadeInDownBig',
                    'fadeInLeft' => 'fadeInLeft',
                    'fadeInLeftBig' => 'fadeInLeftBig',
                    'fadeInRight' => 'fadeInRight',
                    'fadeInRightBig' => 'fadeInRightBig',
                    'fadeInUp' => 'fadeInUp',
                    'fadeInUpBig' => 'fadeInUpBig',
                    'fadeOut' => 'fadeOut',
                    'fadeOutDown' => 'fadeOutDown',
                    'fadeOutDownBig' => 'fadeOutDownBig',
                    'fadeOutLeft' => 'fadeOutLeft',
                    'fadeOutLeftBig' => 'fadeOutLeftBig',
                    'fadeOutRight' => 'fadeOutRight',
                    'fadeOutRightBig' => 'fadeOutRightBig',
                    'fadeOutUp' => 'fadeOutUp',
                    'fadeOutUpBig' => 'fadeOutUpBig',
                    'flip' => 'flip',
                    'flipInX' => 'flipInX',
                    'flipInY' => 'flipInY',
                    'flipOutX' => 'flipOutX',
                    'flipOutY' => 'flipOutY',
                    'lightSpeedIn' => 'lightSpeedIn',
                    'lightSpeedOut' => 'lightSpeedOut',
                    'rotateIn' => 'rotateIn',
                    'rotateInDownLeft' => 'rotateInDownLeft',
                    'rotateInDownRight' => 'rotateInDownRight',
                    'rotateInUpLeft' => 'rotateInUpLeft',
                    'rotateInUpRight' => 'rotateInUpRight',
                    'rotateOut' => 'rotateOut',
                    'rotateOutDownLeft' => 'rotateOutDownLeft',
                    'rotateOutDownRight' => 'rotateOutDownRight',
                    'rotateOutUpLeft' => 'rotateOutUpLeft',
                    'rotateOutUpRight' => 'rotateOutUpRight',
                    'slideInUp' => 'slideInUp',
                    'slideInDown' => 'slideInDown',
                    'slideInLeft' => 'slideInLeft',
                    'slideInRight' => 'slideInRight',
                    'slideOutUp' => 'slideOutUp',
                    'slideOutDown' => 'slideOutDown',
                    'slideOutLeft' => 'slideOutLeft',
                    'slideOutRight' => 'slideOutRight',
                    'zoomIn' => 'zoomIn',
                    'zoomInDown' => 'zoomInDown',
                    'zoomInLeft' => 'zoomInLeft',
                    'zoomInRight' => 'zoomInRight',
                    'zoomInUp' => 'zoomInUp',
                    'zoomOut' => 'zoomOut',
                    'zoomOutDown' => 'zoomOutDown',
                    'zoomOutLeft' => 'zoomOutLeft',
                    'zoomOutRight' => 'zoomOutRight',
                    'zoomOutUp' => 'zoomOutUp',
                    'hinge' => 'hinge',
                    'rollIn' => 'rollIn',
                    'rollOut' => 'rollOut'
                ],
                'name' => 'particles.carousel.animateOut'
            ],
            'particles.carousel.animateIn' => [
                'type' => 'select.select',
                'label' => 'In Animation',
                'description' => 'Customize the In Animation from animate css class.',
                'default' => 'fadeIn',
                'options' => [
                    'default' => 'default',
                    'bounce' => 'bounce',
                    'flash' => 'flash',
                    'pulse' => 'pulse',
                    'rubberBand' => 'rubberBand',
                    'shake' => 'shake',
                    'swing' => 'swing',
                    'tada' => 'tada',
                    'wobble' => 'wobble',
                    'jello' => 'jello',
                    'bounceIn' => 'bounceIn',
                    'bounceInDown' => 'bounceInDown',
                    'bounceInLeft' => 'bounceInLeft',
                    'bounceInRight' => 'bounceInRight',
                    'bounceInUp' => 'bounceInUp',
                    'bounceOut' => 'bounceOut',
                    'bounceOutDown' => 'bounceOutDown',
                    'bounceOutLeft' => 'bounceOutLeft',
                    'bounceOutRight' => 'bounceOutRight',
                    'bounceOutUp' => 'bounceOutUp',
                    'fadeIn' => 'fadeIn',
                    'fadeInDown' => 'fadeInDown',
                    'fadeInDownBig' => 'fadeInDownBig',
                    'fadeInLeft' => 'fadeInLeft',
                    'fadeInLeftBig' => 'fadeInLeftBig',
                    'fadeInRight' => 'fadeInRight',
                    'fadeInRightBig' => 'fadeInRightBig',
                    'fadeInUp' => 'fadeInUp',
                    'fadeInUpBig' => 'fadeInUpBig',
                    'fadeOut' => 'fadeOut',
                    'fadeOutDown' => 'fadeOutDown',
                    'fadeOutDownBig' => 'fadeOutDownBig',
                    'fadeOutLeft' => 'fadeOutLeft',
                    'fadeOutLeftBig' => 'fadeOutLeftBig',
                    'fadeOutRight' => 'fadeOutRight',
                    'fadeOutRightBig' => 'fadeOutRightBig',
                    'fadeOutUp' => 'fadeOutUp',
                    'fadeOutUpBig' => 'fadeOutUpBig',
                    'flip' => 'flip',
                    'flipInX' => 'flipInX',
                    'flipInY' => 'flipInY',
                    'flipOutX' => 'flipOutX',
                    'flipOutY' => 'flipOutY',
                    'lightSpeedIn' => 'lightSpeedIn',
                    'lightSpeedOut' => 'lightSpeedOut',
                    'rotateIn' => 'rotateIn',
                    'rotateInDownLeft' => 'rotateInDownLeft',
                    'rotateInDownRight' => 'rotateInDownRight',
                    'rotateInUpLeft' => 'rotateInUpLeft',
                    'rotateInUpRight' => 'rotateInUpRight',
                    'rotateOut' => 'rotateOut',
                    'rotateOutDownLeft' => 'rotateOutDownLeft',
                    'rotateOutDownRight' => 'rotateOutDownRight',
                    'rotateOutUpLeft' => 'rotateOutUpLeft',
                    'rotateOutUpRight' => 'rotateOutUpRight',
                    'slideInUp' => 'slideInUp',
                    'slideInDown' => 'slideInDown',
                    'slideInLeft' => 'slideInLeft',
                    'slideInRight' => 'slideInRight',
                    'slideOutUp' => 'slideOutUp',
                    'slideOutDown' => 'slideOutDown',
                    'slideOutLeft' => 'slideOutLeft',
                    'slideOutRight' => 'slideOutRight',
                    'zoomIn' => 'zoomIn',
                    'zoomInDown' => 'zoomInDown',
                    'zoomInLeft' => 'zoomInLeft',
                    'zoomInRight' => 'zoomInRight',
                    'zoomInUp' => 'zoomInUp',
                    'zoomOut' => 'zoomOut',
                    'zoomOutDown' => 'zoomOutDown',
                    'zoomOutLeft' => 'zoomOutLeft',
                    'zoomOutRight' => 'zoomOutRight',
                    'zoomOutUp' => 'zoomOutUp',
                    'hinge' => 'hinge',
                    'rollIn' => 'rollIn',
                    'rollOut' => 'rollOut'
                ],
                'name' => 'particles.carousel.animateIn'
            ],
            'particles.carousel.nav' => [
                'type' => 'select.select',
                'label' => 'Prev / Next',
                'description' => 'Enable or disable the Prev / Next navigation.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.carousel.nav'
            ],
            'particles.carousel.loop' => [
                'type' => 'select.select',
                'label' => 'Loop',
                'description' => 'Enable or disable the Inifnity loop. Duplicate last and first items to get loop illusion. This option <strong>won\'t work</strong> in Showcase mode.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.carousel.loop'
            ],
            'particles.carousel.autoplay' => [
                'type' => 'select.select',
                'label' => 'Autoplay',
                'description' => 'Enable or disable the Autoplay.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.carousel.autoplay'
            ],
            'particles.carousel.autoplaySpeed' => [
                'type' => 'input.text',
                'label' => 'Autoplay Speed',
                'description' => 'Set the speed of the Autoplay, in milliseconds.',
                'name' => 'particles.carousel.autoplaySpeed'
            ],
            'particles.carousel.pauseOnHover' => [
                'type' => 'select.select',
                'label' => 'Pause on Hover',
                'description' => 'Pause the slideshow when hovering over slider, then resume when no longer hovering.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.carousel.pauseOnHover'
            ],
            'particles.carousel.displayitems' => [
                'type' => 'input.number',
                'label' => 'Display Items',
                'description' => 'Amount of items to display.',
                'default' => 5,
                'min' => 1,
                'name' => 'particles.carousel.displayitems'
            ],
            'particles.carousel._tab_settings' => [
                'label' => 'Settings',
                'overridable' => false,
                'name' => 'particles.carousel._tab_settings'
            ],
            'particles.carousel.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Carousel items.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.carousel.items'
            ],
            'particles.carousel.items.*' => [
                'type' => '_parent',
                'name' => 'particles.carousel.items.*',
                'form_field' => false
            ],
            'particles.carousel.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Select the icon.',
                'default' => 'fa fa-search',
                'name' => 'particles.carousel.items.*.icon'
            ],
            'particles.carousel.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'name' => 'particles.carousel.items.*.title'
            ],
            'particles.carousel.items.*.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Caption',
                'description' => 'Customize the image caption.',
                'name' => 'particles.carousel.items.*.description'
            ],
            'particles.carousel.items.*.readmore' => [
                'type' => 'input.text',
                'label' => 'Read More',
                'description' => 'Enable or disable read more text.',
                'name' => 'particles.carousel.items.*.readmore'
            ],
            'particles.carousel.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.carousel.items.*.link'
            ],
            'particles.carousel.items.*.linktarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.carousel.items.*.linktarget'
            ],
            'particles.carousel._tab_collection' => [
                'label' => 'Particle Items',
                'overridable' => false,
                'name' => 'particles.carousel._tab_collection'
            ],
            'particles.carousel.article' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article',
                'form_field' => false
            ],
            'particles.carousel.article.filter' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.filter',
                'form_field' => false
            ],
            'particles.carousel.article.filter.categories' => [
                'type' => 'input.selectize',
                'label' => 'Categories',
                'description' => 'Select the categories the content should be taken from.',
                'overridable' => false,
                'name' => 'particles.carousel.article.filter.categories'
            ],
            'particles.carousel.article.limit' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.limit',
                'form_field' => false
            ],
            'particles.carousel.article.limit.total' => [
                'type' => 'input.text',
                'label' => 'Number of Pages',
                'description' => 'Enter the maximum number of content to display.',
                'default' => 5,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.carousel.article.limit.total'
            ],
            'particles.carousel.article.limit.start' => [
                'type' => 'input.text',
                'label' => 'Start From',
                'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first content item).',
                'default' => 0,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.carousel.article.limit.start'
            ],
            'particles.carousel.article.sort' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.sort',
                'form_field' => false
            ],
            'particles.carousel.article.sort.orderby' => [
                'type' => 'select.select',
                'label' => 'Order By',
                'description' => 'Select how the content should be ordered by.',
                'default' => 'default',
                'options' => [
                    'default' => 'Default Ordering',
                    'date' => 'Date',
                    'publish_date' => 'Publish Date',
                    'unpublish_date' => 'Unpublish Date',
                    'modified' => 'Last Modified Date',
                    'title' => 'Title',
                    'slug' => 'Slug'
                ],
                'overridable' => false,
                'name' => 'particles.carousel.article.sort.orderby'
            ],
            'particles.carousel.article.sort.ordering' => [
                'type' => 'select.select',
                'label' => 'Ordering Direction',
                'description' => 'Select the direction the content should be ordered by.',
                'default' => 'asc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'overridable' => false,
                'name' => 'particles.carousel.article.sort.ordering'
            ],
            'particles.carousel._tab_articles' => [
                'label' => 'Pages',
                'overridable' => false,
                'name' => 'particles.carousel._tab_articles'
            ],
            'particles.carousel.article.display' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.display',
                'form_field' => false
            ],
            'particles.carousel.article.display.text' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.display.text',
                'form_field' => false
            ],
            'particles.carousel.article.display.text.type' => [
                'type' => 'select.select',
                'label' => 'Article Text',
                'description' => 'Select if and how the content text should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Introduction',
                    'full' => 'Full Article',
                    '' => 'Hide'
                ],
                'name' => 'particles.carousel.article.display.text.type'
            ],
            'particles.carousel.article.display.text.limit' => [
                'type' => 'input.text',
                'label' => 'Text Limit',
                'description' => 'Type in the number of characters the content text should be limited to.',
                'default' => '',
                'pattern' => '\\d+',
                'name' => 'particles.carousel.article.display.text.limit'
            ],
            'particles.carousel.article.display.text.formatting' => [
                'type' => 'select.select',
                'label' => 'Text Formatting',
                'description' => 'Select the formatting you want to use to display the content text.',
                'default' => 'text',
                'options' => [
                    'text' => 'Plain Text',
                    'html' => 'HTML'
                ],
                'name' => 'particles.carousel.article.display.text.formatting'
            ],
            'particles.carousel.article.display.title' => [
                'type' => '_parent',
                'name' => 'particles.carousel.article.display.title',
                'form_field' => false
            ],
            'particles.carousel.article.display.title.enabled' => [
                'type' => 'select.select',
                'label' => 'Title',
                'description' => 'Select if the content title should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.carousel.article.display.title.enabled'
            ],
            'particles.carousel.article.display.title.limit' => [
                'type' => 'input.text',
                'label' => 'Title Limit',
                'description' => 'Enter the maximum number of characters the content title should be limited to.',
                'pattern' => '\\d+(\\.\\d+){0,1}',
                'name' => 'particles.carousel.article.display.title.limit'
            ],
            'particles.carousel.article.display.readmore' => [
                'type' => 'input.text',
                'label' => 'Read More',
                'description' => 'Enable or disable read more text.',
                'name' => 'particles.carousel.article.display.readmore'
            ],
            'particles.carousel._tab_display' => [
                'label' => 'Pages Display',
                'name' => 'particles.carousel._tab_display'
            ],
            'particles.casestudies' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.casestudies.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particles.',
                'default' => true,
                'name' => 'particles.casestudies.enabled'
            ],
            'particles.casestudies.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.casestudies.class'
            ],
            'particles.casestudies.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.casestudies.title'
            ],
            'particles.casestudies.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount',
                'default' => '2cols',
                'options' => [
                    'cols-2' => '2 Columns',
                    'cols-3' => '3 Columns',
                    'cols-4' => '4 Columns',
                    'cols-5' => '5 Columns'
                ],
                'name' => 'particles.casestudies.cols'
            ],
            'particles.casestudies.cases' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Cases',
                'description' => 'Create case studies.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.casestudies.cases'
            ],
            'particles.casestudies.cases.*' => [
                'type' => '_parent',
                'name' => 'particles.casestudies.cases.*',
                'form_field' => false
            ],
            'particles.casestudies.cases.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter case title',
                'name' => 'particles.casestudies.cases.*.title'
            ],
            'particles.casestudies.cases.*.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Create case studies items.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.casestudies.cases.*.items'
            ],
            'particles.casestudies.cases.*.items.*' => [
                'type' => '_parent',
                'name' => 'particles.casestudies.cases.*.items.*',
                'form_field' => false
            ],
            'particles.casestudies.cases.*.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.casestudies.cases.*.items.*.image'
            ],
            'particles.casestudies.cases.*.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter case title',
                'name' => 'particles.casestudies.cases.*.items.*.title'
            ],
            'particles.casestudies.cases.*.items.*.subtitle' => [
                'type' => 'input.text',
                'label' => 'Subtitle',
                'description' => 'Enter case subtitle',
                'name' => 'particles.casestudies.cases.*.items.*.subtitle'
            ],
            'particles.casestudies.cases.*.items.*.url' => [
                'type' => 'input.text',
                'label' => 'Url',
                'description' => 'Customize case url.',
                'name' => 'particles.casestudies.cases.*.items.*.url'
            ],
            'particles.casestudies.cases.*.items.*.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.casestudies.cases.*.items.*.target'
            ],
            'particles.fixedheader' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.fixedheader.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.fixedheader.enabled'
            ],
            'particles.fixedheader.section' => [
                'type' => 'input.text',
                'label' => 'Section',
                'description' => 'Define the ID or class of the section that you want to set as fixed.',
                'default' => '#g-navigation',
                'name' => 'particles.fixedheader.section'
            ],
            'particles.fixedheader.pinnedbg' => [
                'type' => 'select.selectize',
                'label' => 'Background to Use',
                'description' => 'Choose if the Pinned background should be set below or if it should come from the section styling.',
                'default' => 'section',
                'options' => [
                    'section' => 'Section',
                    'custom' => 'Custom'
                ],
                'name' => 'particles.fixedheader.pinnedbg'
            ],
            'particles.fixedheader.custombg' => [
                'type' => 'input.colorpicker',
                'label' => 'Custom Background',
                'default' => '#ffffff',
                'name' => 'particles.fixedheader.custombg'
            ],
            'particles.fixedheader.autohide' => [
                'type' => 'select.selectize',
                'label' => 'Autohide',
                'description' => 'Choose if the fixed section should autohide on scroll.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'particles.fixedheader.autohide'
            ],
            'particles.fixedheader.mobile' => [
                'type' => 'select.selectize',
                'label' => 'Mobile',
                'description' => 'Enable or disable fixed header in mobile',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'particles.fixedheader.mobile'
            ],
            'particles.fixedheader.margin' => [
                'type' => 'input.number',
                'label' => 'Margin Top',
                'description' => 'Top margin in px for adjacent section / header',
                'min' => 0,
                'default' => 115,
                'name' => 'particles.fixedheader.margin'
            ],
            'particles.fixedheader.mobilemargin' => [
                'type' => 'input.number',
                'label' => 'Mobile Margin Top',
                'description' => 'Top margin in px for adjacent section / header in mobile',
                'min' => 0,
                'default' => 115,
                'name' => 'particles.fixedheader.mobilemargin'
            ],
            'particles.fixedheader.offset' => [
                'type' => 'input.number',
                'label' => 'Offset',
                'description' => 'Vertical offset in px before element is first unpinned',
                'min' => 0,
                'default' => 100,
                'name' => 'particles.fixedheader.offset'
            ],
            'particles.fixedheader.tolerance' => [
                'type' => 'input.number',
                'label' => 'Tolerance',
                'description' => 'Scroll tolerance in px before state changes',
                'min' => 0,
                'name' => 'particles.fixedheader.tolerance'
            ],
            'particles.gridcontent' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.gridcontent.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.gridcontent.enabled'
            ],
            'particles.gridcontent.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.gridcontent.class'
            ],
            'particles.gridcontent.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.gridcontent.title'
            ],
            'particles.gridcontent.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.gridcontent.desc'
            ],
            'particles.gridcontent.readmore' => [
                'type' => 'input.text',
                'label' => 'Readmore Text',
                'description' => 'Specify the readmore text.',
                'name' => 'particles.gridcontent.readmore'
            ],
            'particles.gridcontent.readmorelink' => [
                'type' => 'input.text',
                'label' => 'Readmore Link',
                'description' => 'Specify the readmore link address.',
                'name' => 'particles.gridcontent.readmorelink'
            ],
            'particles.gridcontent.readmoreclass' => [
                'type' => 'select.select',
                'label' => 'Readmore Style',
                'description' => 'Style for the readmore button.',
                'default' => 'button-3',
                'options' => [
                    'button-2' => 'Button 2',
                    'button-3' => 'Button 3',
                    'button-4' => 'Button 4'
                ],
                'name' => 'particles.gridcontent.readmoreclass'
            ],
            'particles.gridcontent.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount',
                'default' => 'g-gridcontent-2cols',
                'options' => [
                    'g-gridcontent-1cols' => '1 Column',
                    'g-gridcontent-2cols' => '2 Columns',
                    'g-gridcontent-3cols' => '3 Columns',
                    'g-gridcontent-4cols' => '4 Columns',
                    'g-gridcontent-5cols' => '5 Columns',
                    'g-gridcontent-6cols' => '6 Columns'
                ],
                'name' => 'particles.gridcontent.cols'
            ],
            'particles.gridcontent.gridcontentitems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.gridcontent.gridcontentitems'
            ],
            'particles.gridcontent.gridcontentitems.*' => [
                'type' => '_parent',
                'name' => 'particles.gridcontent.gridcontentitems.*',
                'form_field' => false
            ],
            'particles.gridcontent.gridcontentitems.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.gridcontent.gridcontentitems.*.icon'
            ],
            'particles.gridcontent.gridcontentitems.*.iconcolor' => [
                'type' => 'select.select',
                'label' => 'Icon Color',
                'description' => 'Choose color for your icon.',
                'default' => 'accent1',
                'options' => [
                    'accent1' => 'Accent Color 1',
                    'accent2' => 'Accent Color 2',
                    'accent3' => 'Black',
                    'accent4' => 'White'
                ],
                'name' => 'particles.gridcontent.gridcontentitems.*.iconcolor'
            ],
            'particles.gridcontent.gridcontentitems.*.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.gridcontent.gridcontentitems.*.desc'
            ],
            'particles.gridcontent.gridcontentitems.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.gridcontent.gridcontentitems.*.link'
            ],
            'particles.gridstatistic' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.gridstatistic.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.gridstatistic.enabled'
            ],
            'particles.gridstatistic.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.gridstatistic.class'
            ],
            'particles.gridstatistic.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.gridstatistic.title'
            ],
            'particles.gridstatistic.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.gridstatistic.desc'
            ],
            'particles.gridstatistic.readmore' => [
                'type' => 'input.text',
                'label' => 'Readmore Text',
                'description' => 'Specify the readmore text.',
                'name' => 'particles.gridstatistic.readmore'
            ],
            'particles.gridstatistic.readmorelink' => [
                'type' => 'input.text',
                'label' => 'Readmore Link',
                'description' => 'Specify the readmore link address.',
                'name' => 'particles.gridstatistic.readmorelink'
            ],
            'particles.gridstatistic.readmoreclass' => [
                'type' => 'select.select',
                'label' => 'Readmore Style',
                'description' => 'Style for the readmore button.',
                'default' => 'button-3',
                'options' => [
                    'button-2' => 'Button 2',
                    'button-3' => 'Button 3',
                    'button-4' => 'Button 4'
                ],
                'name' => 'particles.gridstatistic.readmoreclass'
            ],
            'particles.gridstatistic.footerdesc' => [
                'type' => 'textarea.textarea',
                'label' => 'Footer Description',
                'description' => 'Customize the footer description.',
                'name' => 'particles.gridstatistic.footerdesc'
            ],
            'particles.gridstatistic.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount',
                'default' => 'g-gridstatistic-2cols',
                'options' => [
                    'g-gridstatistic-1cols' => '1 Column',
                    'g-gridstatistic-2cols' => '2 Columns',
                    'g-gridstatistic-3cols' => '3 Columns',
                    'g-gridstatistic-4cols' => '4 Columns',
                    'g-gridstatistic-5cols' => '5 Columns',
                    'g-gridstatistic-6cols' => '6 Columns'
                ],
                'name' => 'particles.gridstatistic.cols'
            ],
            'particles.gridstatistic.gridstatisticitems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.gridstatistic.gridstatisticitems'
            ],
            'particles.gridstatistic.gridstatisticitems.*' => [
                'type' => '_parent',
                'name' => 'particles.gridstatistic.gridstatisticitems.*',
                'form_field' => false
            ],
            'particles.gridstatistic.gridstatisticitems.*.text1' => [
                'type' => 'input.text',
                'label' => 'Statictic Number',
                'name' => 'particles.gridstatistic.gridstatisticitems.*.text1'
            ],
            'particles.gridstatistic.gridstatisticitems.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.gridstatistic.gridstatisticitems.*.icon'
            ],
            'particles.gridstatistic.gridstatisticitems.*.text2' => [
                'type' => 'input.text',
                'label' => 'Statistic Text',
                'name' => 'particles.gridstatistic.gridstatisticitems.*.text2'
            ],
            'particles.imagegrid' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.imagegrid.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.imagegrid.enabled'
            ],
            'particles.imagegrid.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.imagegrid.class'
            ],
            'particles.imagegrid.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.imagegrid.title'
            ],
            'particles.imagegrid.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.imagegrid.desc'
            ],
            'particles.imagegrid.cols' => [
                'type' => 'select.select',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount',
                'default' => 'g-imagegrid-2cols',
                'options' => [
                    'g-imagegrid-2cols' => '2 Columns',
                    'g-imagegrid-3cols' => '3 Columns',
                    'g-imagegrid-4cols' => '4 Columns',
                    'g-imagegrid-5cols' => '5 Columns'
                ],
                'name' => 'particles.imagegrid.cols'
            ],
            'particles.imagegrid.layout' => [
                'type' => 'select.select',
                'label' => 'Layout',
                'description' => 'Select preferred layout',
                'default' => 'g-imagegrid-standard',
                'options' => [
                    'g-imagegrid-standard' => 'Standard',
                    'g-imagegrid-captions' => 'Visible captions'
                ],
                'name' => 'particles.imagegrid.layout'
            ],
            'particles.imagegrid.album' => [
                'type' => 'input.text',
                'label' => 'Album Name',
                'description' => 'Customize the album name.',
                'name' => 'particles.imagegrid.album'
            ],
            'particles.imagegrid.imagegriditems' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Images',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'caption',
                'ajax' => true,
                'name' => 'particles.imagegrid.imagegriditems'
            ],
            'particles.imagegrid.imagegriditems.*' => [
                'type' => '_parent',
                'name' => 'particles.imagegrid.imagegriditems.*',
                'form_field' => false
            ],
            'particles.imagegrid.imagegriditems.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Promo Image',
                'description' => 'Select desired promo image.',
                'name' => 'particles.imagegrid.imagegriditems.*.image'
            ],
            'particles.imagegrid.imagegriditems.*.caption' => [
                'type' => 'input.text',
                'label' => 'Caption',
                'description' => 'Customize the image caption.',
                'name' => 'particles.imagegrid.imagegriditems.*.caption'
            ],
            'particles.infolist' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.infolist.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.infolist.enabled'
            ],
            'particles.infolist.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.infolist.class'
            ],
            'particles.infolist.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.infolist.title'
            ],
            'particles.infolist.intro' => [
                'type' => 'textarea.textarea',
                'label' => 'Intro',
                'description' => 'Customize the intro text.',
                'name' => 'particles.infolist.intro'
            ],
            'particles.infolist.cols' => [
                'type' => 'select.selectize',
                'label' => 'Grid Column',
                'description' => 'Select the grid column amount for the list items',
                'default' => 'g-1cols',
                'options' => [
                    'g-1cols' => '1 Column',
                    'g-2cols' => '2 Columns',
                    'g-3cols' => '3 Columns',
                    'g-4cols' => '4 Columns',
                    'g-5cols' => '5 Columns'
                ],
                'name' => 'particles.infolist.cols'
            ],
            'particles.infolist.infolists' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Info Lists',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.infolist.infolists'
            ],
            'particles.infolist.infolists.*' => [
                'type' => '_parent',
                'name' => 'particles.infolist.infolists.*',
                'form_field' => false
            ],
            'particles.infolist.infolists.*.title' => [
                'type' => 'input.text',
                'label' => 'Item Title',
                'description' => 'Customize the item title text.',
                'name' => 'particles.infolist.infolists.*.title'
            ],
            'particles.infolist.infolists.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Select the icon.',
                'name' => 'particles.infolist.infolists.*.icon'
            ],
            'particles.infolist.infolists.*.iconloc' => [
                'type' => 'select.select',
                'label' => 'Icon Location',
                'description' => 'Select the location for the icon',
                'default' => 'left',
                'options' => [
                    'center' => 'Center',
                    'left' => 'Left',
                    'right' => 'Right',
                    'inline' => 'Inline'
                ],
                'name' => 'particles.infolist.infolists.*.iconloc'
            ],
            'particles.infolist.infolists.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.infolist.infolists.*.image'
            ],
            'particles.infolist.infolists.*.imageloc' => [
                'type' => 'select.select',
                'label' => 'Image Location',
                'description' => 'Select the location for the image',
                'default' => 'left',
                'options' => [
                    'center' => 'Center',
                    'left' => 'Left',
                    'right' => 'Right'
                ],
                'name' => 'particles.infolist.infolists.*.imageloc'
            ],
            'particles.infolist.infolists.*.textstyle' => [
                'type' => 'select.select',
                'label' => 'Text Style',
                'description' => 'Select the style for the text',
                'default' => 'compact',
                'options' => [
                    'compact' => 'Compact',
                    'large' => 'Large',
                    'bold' => 'Bold',
                    'header' => 'Header'
                ],
                'name' => 'particles.infolist.infolists.*.textstyle'
            ],
            'particles.infolist.infolists.*.imagestyle' => [
                'type' => 'select.select',
                'label' => 'Image Style',
                'description' => 'Select the style for the image',
                'default' => 'compact',
                'options' => [
                    'compact' => 'Compact',
                    'large' => 'Large',
                    'browser' => 'Browser',
                    'avatar' => 'Avatar'
                ],
                'name' => 'particles.infolist.infolists.*.imagestyle'
            ],
            'particles.infolist.infolists.*.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.infolist.infolists.*.desc'
            ],
            'particles.infolist.infolists.*.tag' => [
                'type' => 'input.text',
                'label' => 'Tag',
                'description' => 'Customize the tag',
                'name' => 'particles.infolist.infolists.*.tag'
            ],
            'particles.infolist.infolists.*.subtag' => [
                'type' => 'input.text',
                'label' => 'Sub Tag',
                'description' => 'Customize the sub tag',
                'name' => 'particles.infolist.infolists.*.subtag'
            ],
            'particles.infolist.infolists.*.label' => [
                'type' => 'input.text',
                'label' => 'Label',
                'description' => 'Customize the read more label',
                'name' => 'particles.infolist.infolists.*.label'
            ],
            'particles.infolist.infolists.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'description' => 'Specify the link address.',
                'name' => 'particles.infolist.infolists.*.link'
            ],
            'particles.infolist.infolists.*.buttonicon' => [
                'type' => 'input.icon',
                'label' => 'Button Icon',
                'description' => 'Specify the read more icon.',
                'name' => 'particles.infolist.infolists.*.buttonicon'
            ],
            'particles.infolist.infolists.*.readmoreclass' => [
                'type' => 'input.selectize',
                'label' => 'Read More Classes',
                'description' => 'CSS class name for the read more.',
                'name' => 'particles.infolist.infolists.*.readmoreclass'
            ],
            'particles.infolist.infolists.*.readmoretarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.infolist.infolists.*.readmoretarget'
            ],
            'particles.newsletter' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.newsletter.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.newsletter.enabled'
            ],
            'particles.newsletter.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.newsletter.class'
            ],
            'particles.newsletter.width' => [
                'type' => 'select.selectize',
                'label' => 'Width',
                'description' => 'Choose the width.',
                'default' => 'g-newsletter-fullwidth',
                'options' => [
                    'g-newsletter-fullwidth' => 'Full Width',
                    'g-newsletter-compact' => 'Compact'
                ],
                'name' => 'particles.newsletter.width'
            ],
            'particles.newsletter.layout' => [
                'type' => 'select.selectize',
                'label' => 'Layout',
                'description' => 'Choose the layout.',
                'default' => 'g-newsletter-aside-compact',
                'options' => [
                    'g-newsletter-stack-fullwidth' => 'Stack Fullwidth',
                    'g-newsletter-stack-compact' => 'Stack Compact',
                    'g-newsletter-aside-wrap' => 'Aside Wrap',
                    'g-newsletter-aside-compact' => 'Aside Compact'
                ],
                'name' => 'particles.newsletter.layout'
            ],
            'particles.newsletter.style' => [
                'type' => 'select.selectize',
                'label' => 'Style',
                'description' => 'Choose the style for the Input Box and Button.',
                'default' => 'g-newsletter-rounded',
                'options' => [
                    'g-newsletter-square' => 'Square',
                    'g-newsletter-rounded' => 'Rounded'
                ],
                'name' => 'particles.newsletter.style'
            ],
            'particles.newsletter.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.newsletter.title'
            ],
            'particles.newsletter.headtext' => [
                'type' => 'textarea.textarea',
                'label' => 'Heading Text',
                'description' => 'Customize the heading text.',
                'name' => 'particles.newsletter.headtext'
            ],
            'particles.newsletter.sidetext' => [
                'type' => 'input.text',
                'label' => 'Side Text',
                'description' => 'Customize the side text.',
                'name' => 'particles.newsletter.sidetext'
            ],
            'particles.newsletter.inputboxtext' => [
                'type' => 'input.text',
                'label' => 'InputBox Text',
                'description' => 'Customize the InputBox text.',
                'default' => 'Your email address...',
                'name' => 'particles.newsletter.inputboxtext'
            ],
            'particles.newsletter.buttonicon' => [
                'type' => 'input.icon',
                'label' => 'Button Icon',
                'description' => 'Select the icon.',
                'name' => 'particles.newsletter.buttonicon'
            ],
            'particles.newsletter.buttontext' => [
                'type' => 'input.text',
                'label' => 'Button Text',
                'description' => 'Customize the Button text.',
                'default' => 'Subscribe',
                'name' => 'particles.newsletter.buttontext'
            ],
            'particles.newsletter.uri' => [
                'type' => 'input.text',
                'label' => 'Feedburner URI',
                'description' => 'Please put your Feedburner Email Subscriptions URI here.',
                'name' => 'particles.newsletter.uri'
            ],
            'particles.newsletter.buttonclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class name for the join button.',
                'default' => 'button-xlarge button-block',
                'name' => 'particles.newsletter.buttonclass'
            ],
            'particles.panelslider' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.panelslider.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.panelslider.enabled'
            ],
            'particles.panelslider.source' => [
                'type' => 'select.select',
                'label' => 'Content Source',
                'description' => 'Choose if the content should be loaded from the WordPress posts or particle itself.',
                'default' => 'particle',
                'options' => [
                    'particle' => 'Particle',
                    'grav' => 'Grav'
                ],
                'name' => 'particles.panelslider.source'
            ],
            'particles.panelslider.displayitems' => [
                'type' => 'input.number',
                'label' => 'Display Items',
                'description' => 'Amount of items to display.',
                'default' => 5,
                'min' => 1,
                'name' => 'particles.panelslider.displayitems'
            ],
            'particles.panelslider.nav' => [
                'type' => 'select.select',
                'label' => 'Prev / Next',
                'description' => 'Enable or disable the Prev / Next navigation.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.panelslider.nav'
            ],
            'particles.panelslider._tab_settings' => [
                'label' => 'Settings',
                'overridable' => false,
                'name' => 'particles.panelslider._tab_settings'
            ],
            'particles.panelslider.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Create items.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.panelslider.items'
            ],
            'particles.panelslider.items.*' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.items.*',
                'form_field' => false
            ],
            'particles.panelslider.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Title',
                'name' => 'particles.panelslider.items.*.title'
            ],
            'particles.panelslider.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select image for slideshow',
                'name' => 'particles.panelslider.items.*.image'
            ],
            'particles.panelslider.items.*.topline' => [
                'type' => 'input.text',
                'label' => 'Top Line',
                'name' => 'particles.panelslider.items.*.topline'
            ],
            'particles.panelslider.items.*.description' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Provide template description',
                'name' => 'particles.panelslider.items.*.description'
            ],
            'particles.panelslider.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.panelslider.items.*.link'
            ],
            'particles.panelslider.items.*.linklabel' => [
                'type' => 'input.text',
                'label' => 'Link Label',
                'name' => 'particles.panelslider.items.*.linklabel'
            ],
            'particles.panelslider.items.*.linktarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.panelslider.items.*.linktarget'
            ],
            'particles.panelslider._tab_collection' => [
                'label' => 'Particle Items',
                'overridable' => false,
                'name' => 'particles.panelslider._tab_collection'
            ],
            'particles.panelslider.article' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article',
                'form_field' => false
            ],
            'particles.panelslider.article.filter' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.filter',
                'form_field' => false
            ],
            'particles.panelslider.article.filter.categories' => [
                'type' => 'input.selectize',
                'label' => 'Categories',
                'description' => 'Select the categories the content should be taken from.',
                'overridable' => false,
                'name' => 'particles.panelslider.article.filter.categories'
            ],
            'particles.panelslider.article.limit' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.limit',
                'form_field' => false
            ],
            'particles.panelslider.article.limit.total' => [
                'type' => 'input.text',
                'label' => 'Number of Pages',
                'description' => 'Enter the maximum number of content to display.',
                'default' => 5,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.panelslider.article.limit.total'
            ],
            'particles.panelslider.article.limit.start' => [
                'type' => 'input.text',
                'label' => 'Start From',
                'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first content item).',
                'default' => 0,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.panelslider.article.limit.start'
            ],
            'particles.panelslider.article.sort' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.sort',
                'form_field' => false
            ],
            'particles.panelslider.article.sort.orderby' => [
                'type' => 'select.select',
                'label' => 'Order By',
                'description' => 'Select how the content should be ordered by.',
                'default' => 'default',
                'options' => [
                    'default' => 'Default Ordering',
                    'date' => 'Date',
                    'publish_date' => 'Publish Date',
                    'unpublish_date' => 'Unpublish Date',
                    'modified' => 'Last Modified Date',
                    'title' => 'Title',
                    'slug' => 'Slug'
                ],
                'overridable' => false,
                'name' => 'particles.panelslider.article.sort.orderby'
            ],
            'particles.panelslider.article.sort.ordering' => [
                'type' => 'select.select',
                'label' => 'Ordering Direction',
                'description' => 'Select the direction the content should be ordered by.',
                'default' => 'asc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'overridable' => false,
                'name' => 'particles.panelslider.article.sort.ordering'
            ],
            'particles.panelslider._tab_articles' => [
                'label' => 'Pages',
                'overridable' => false,
                'name' => 'particles.panelslider._tab_articles'
            ],
            'particles.panelslider.article.display' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display',
                'form_field' => false
            ],
            'particles.panelslider.article.display.image' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display.image',
                'form_field' => false
            ],
            'particles.panelslider.article.display.image.enabled' => [
                'type' => 'select.select',
                'label' => 'Image',
                'description' => 'Select if and what image of the content should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Intro',
                    'full' => 'Full',
                    '' => 'None'
                ],
                'name' => 'particles.panelslider.article.display.image.enabled'
            ],
            'particles.panelslider.article.display.text' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display.text',
                'form_field' => false
            ],
            'particles.panelslider.article.display.text.type' => [
                'type' => 'select.select',
                'label' => 'Article Text',
                'description' => 'Select if and how the content text should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Introduction',
                    'full' => 'Full Article',
                    '' => 'Hide'
                ],
                'name' => 'particles.panelslider.article.display.text.type'
            ],
            'particles.panelslider.article.display.text.limit' => [
                'type' => 'input.text',
                'label' => 'Text Limit',
                'description' => 'Type in the number of characters the content text should be limited to.',
                'default' => '',
                'pattern' => '\\d+',
                'name' => 'particles.panelslider.article.display.text.limit'
            ],
            'particles.panelslider.article.display.text.formatting' => [
                'type' => 'select.select',
                'label' => 'Text Formatting',
                'description' => 'Select the formatting you want to use to display the content text.',
                'default' => 'text',
                'options' => [
                    'text' => 'Plain Text',
                    'html' => 'HTML'
                ],
                'name' => 'particles.panelslider.article.display.text.formatting'
            ],
            'particles.panelslider.article.display.title' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display.title',
                'form_field' => false
            ],
            'particles.panelslider.article.display.title.enabled' => [
                'type' => 'select.select',
                'label' => 'Title',
                'description' => 'Select if the content title should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.panelslider.article.display.title.enabled'
            ],
            'particles.panelslider.article.display.title.limit' => [
                'type' => 'input.text',
                'label' => 'Title Limit',
                'description' => 'Enter the maximum number of characters the content title should be limited to.',
                'pattern' => '\\d+(\\.\\d+){0,1}',
                'name' => 'particles.panelslider.article.display.title.limit'
            ],
            'particles.panelslider.article.display.date' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display.date',
                'form_field' => false
            ],
            'particles.panelslider.article.display.date.enabled' => [
                'type' => 'select.select',
                'label' => 'Date',
                'description' => 'Select if the content date should be shown.',
                'default' => 'published',
                'options' => [
                    'created' => 'Show Created Date',
                    'published' => 'Show Published Date',
                    'modified' => 'Show Modified Date',
                    '' => 'Hide'
                ],
                'name' => 'particles.panelslider.article.display.date.enabled'
            ],
            'particles.panelslider.article.display.date.format' => [
                'type' => 'select.date',
                'label' => 'Date Format',
                'description' => 'Select preferred date format. Leave empty not to display a date.',
                'default' => 'l, F d, Y',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11',
                    'F d, Y' => 'Date12'
                ],
                'name' => 'particles.panelslider.article.display.date.format'
            ],
            'particles.panelslider.article.display.read_more' => [
                'type' => '_parent',
                'name' => 'particles.panelslider.article.display.read_more',
                'form_field' => false
            ],
            'particles.panelslider.article.display.read_more.enabled' => [
                'type' => 'select.select',
                'label' => 'Read More',
                'description' => 'Select if the content \'Read More\' button should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.panelslider.article.display.read_more.enabled'
            ],
            'particles.panelslider.article.display.read_more.label' => [
                'type' => 'input.text',
                'label' => 'Read More Label',
                'description' => 'Type in the label for the \'Read More\' button.',
                'name' => 'particles.panelslider.article.display.read_more.label'
            ],
            'particles.panelslider.article.display.read_more.css' => [
                'type' => 'input.selectize',
                'label' => 'Button CSS Classes',
                'description' => 'CSS class name for the \'Read More\' button.',
                'name' => 'particles.panelslider.article.display.read_more.css'
            ],
            'particles.panelslider._tab_display' => [
                'label' => 'Pages Display',
                'name' => 'particles.panelslider._tab_display'
            ],
            'particles.pricingtable' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.pricingtable.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.pricingtable.enabled'
            ],
            'particles.pricingtable.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.pricingtable.class'
            ],
            'particles.pricingtable.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.pricingtable.title'
            ],
            'particles.pricingtable.headertext' => [
                'type' => 'textarea.textarea',
                'label' => 'Header Text',
                'description' => 'Customize the header text.',
                'name' => 'particles.pricingtable.headertext'
            ],
            'particles.pricingtable.footertext' => [
                'type' => 'textarea.textarea',
                'label' => 'Footer Text',
                'description' => 'Customize the footer text.',
                'name' => 'particles.pricingtable.footertext'
            ],
            'particles.pricingtable.columns' => [
                'type' => 'select.selectize',
                'label' => 'Grid Columns',
                'description' => 'Select the Grid columns amount.',
                'default' => 'g-pricingtable-4-col',
                'options' => [
                    'g-pricingtable-1-col' => '1 Column',
                    'g-pricingtable-2-col' => '2 Columns',
                    'g-pricingtable-3-col' => '3 Columns',
                    'g-pricingtable-4-col' => '4 Columns',
                    'g-pricingtable-5-col' => '5 Columns',
                    'g-pricingtable-6-col' => '6 Columns'
                ],
                'name' => 'particles.pricingtable.columns'
            ],
            'particles.pricingtable.tables' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Tables',
                'description' => 'Create each table to display.',
                'value' => 'plan',
                'ajax' => true,
                'name' => 'particles.pricingtable.tables'
            ],
            'particles.pricingtable.tables.*' => [
                'type' => '_parent',
                'name' => 'particles.pricingtable.tables.*',
                'form_field' => false
            ],
            'particles.pricingtable.tables.*.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.pricingtable.tables.*.class'
            ],
            'particles.pricingtable.tables.*.highlight' => [
                'type' => 'select.selectize',
                'label' => 'Highlight',
                'description' => 'Choose if the item should be highlighted.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.pricingtable.tables.*.highlight'
            ],
            'particles.pricingtable.tables.*.ribbon' => [
                'type' => 'input.text',
                'label' => 'Ribbon Text',
                'description' => 'Input the corner ribbon text.',
                'name' => 'particles.pricingtable.tables.*.ribbon'
            ],
            'particles.pricingtable.tables.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Input the icon.',
                'name' => 'particles.pricingtable.tables.*.icon'
            ],
            'particles.pricingtable.tables.*.plan' => [
                'type' => 'input.text',
                'label' => 'Plan Name',
                'description' => 'Customize the table plan name text.',
                'name' => 'particles.pricingtable.tables.*.plan'
            ],
            'particles.pricingtable.tables.*.price' => [
                'type' => 'input.text',
                'label' => 'Price',
                'description' => 'Customize the price.',
                'name' => 'particles.pricingtable.tables.*.price'
            ],
            'particles.pricingtable.tables.*.period' => [
                'type' => 'input.text',
                'label' => 'Period',
                'description' => 'Customize the period.',
                'name' => 'particles.pricingtable.tables.*.period'
            ],
            'particles.pricingtable.tables.*.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.pricingtable.tables.*.desc'
            ],
            'particles.pricingtable.tables.*.buttontext' => [
                'type' => 'input.text',
                'label' => 'Button Label',
                'description' => 'Specify the button label.',
                'name' => 'particles.pricingtable.tables.*.buttontext'
            ],
            'particles.pricingtable.tables.*.buttonlink' => [
                'type' => 'input.text',
                'label' => 'Button Link',
                'description' => 'Specify the button link.',
                'name' => 'particles.pricingtable.tables.*.buttonlink'
            ],
            'particles.pricingtable.tables.*.buttontarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.pricingtable.tables.*.buttontarget'
            ],
            'particles.pricingtable.tables.*.buttonclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class names for the button.',
                'name' => 'particles.pricingtable.tables.*.buttonclass'
            ],
            'particles.pricingtable.tables.*.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Items',
                'description' => 'Create the items for your table.',
                'value' => 'text',
                'ajax' => true,
                'name' => 'particles.pricingtable.tables.*.items'
            ],
            'particles.pricingtable.tables.*.items.*' => [
                'type' => '_parent',
                'name' => 'particles.pricingtable.tables.*.items.*',
                'form_field' => false
            ],
            'particles.pricingtable.tables.*.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'Create the items for your table.',
                'name' => 'particles.pricingtable.tables.*.items.*.text'
            ],
            'particles.pricingtable.tables.*.items.*.class' => [
                'type' => 'input.selectize',
                'label' => 'Item Classes',
                'description' => 'Enter the CSS class.',
                'name' => 'particles.pricingtable.tables.*.items.*.class'
            ],
            'particles.search' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.search.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.search.enabled'
            ],
            'particles.search.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'default' => 'Search Form',
                'name' => 'particles.search.title'
            ],
            'particles.search.placeholder' => [
                'type' => 'input.text',
                'label' => 'Placeholder',
                'description' => 'Input your custom placeholder',
                'name' => 'particles.search.placeholder'
            ],
            'particles.simplecontent' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.simplecontent.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.simplecontent.enabled'
            ],
            'particles.simplecontent.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.simplecontent.class'
            ],
            'particles.simplecontent.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.simplecontent.title'
            ],
            'particles.simplecontent.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Content Items',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.simplecontent.items'
            ],
            'particles.simplecontent.items.*' => [
                'type' => '_parent',
                'name' => 'particles.simplecontent.items.*',
                'form_field' => false
            ],
            'particles.simplecontent.items.*.layout' => [
                'type' => 'select.select',
                'label' => 'Layout Style',
                'description' => 'Choose the layout style.',
                'default' => 'standard',
                'options' => [
                    'standard' => 'Standard',
                    'header' => 'Header'
                ],
                'name' => 'particles.simplecontent.items.*.layout'
            ],
            'particles.simplecontent.items.*.created_date' => [
                'type' => 'input.text',
                'label' => 'Created Date',
                'description' => 'Customize the created date.',
                'name' => 'particles.simplecontent.items.*.created_date'
            ],
            'particles.simplecontent.items.*.content_title' => [
                'type' => 'input.text',
                'label' => 'Content Title',
                'description' => 'Customize the content title.',
                'name' => 'particles.simplecontent.items.*.content_title'
            ],
            'particles.simplecontent.items.*.author' => [
                'type' => 'input.text',
                'label' => 'Author',
                'description' => 'Customize the author.',
                'name' => 'particles.simplecontent.items.*.author'
            ],
            'particles.simplecontent.items.*.leading_content' => [
                'type' => 'textarea.textarea',
                'label' => 'Leading Content',
                'description' => 'Customize the leading content.',
                'name' => 'particles.simplecontent.items.*.leading_content'
            ],
            'particles.simplecontent.items.*.main_content' => [
                'type' => 'textarea.textarea',
                'label' => 'Main Content',
                'description' => 'Customize the main content.',
                'name' => 'particles.simplecontent.items.*.main_content'
            ],
            'particles.simplecontent.items.*.readmore_label' => [
                'type' => 'input.text',
                'label' => 'Read More Label',
                'description' => 'Customize the readmore label.',
                'name' => 'particles.simplecontent.items.*.readmore_label'
            ],
            'particles.simplecontent.items.*.readmore_link' => [
                'type' => 'input.text',
                'label' => 'Read More Link',
                'description' => 'Specify the readmore link address.',
                'name' => 'particles.simplecontent.items.*.readmore_link'
            ],
            'particles.simplecontent.items.*.readmore_class' => [
                'type' => 'input.selectize',
                'label' => 'Read More Classes',
                'description' => 'CSS class names for the readmore.',
                'name' => 'particles.simplecontent.items.*.readmore_class'
            ],
            'particles.simplecontent.items.*.readmore_target' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.simplecontent.items.*.readmore_target'
            ],
            'particles.simplecounter' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.simplecounter.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.simplecounter.enabled'
            ],
            'particles.simplecounter.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.simplecounter.class'
            ],
            'particles.simplecounter.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.simplecounter.title'
            ],
            'particles.simplecounter.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.simplecounter.desc'
            ],
            'particles.simplecounter.date' => [
                'type' => 'select.select',
                'label' => 'Day',
                'description' => 'Select the day',
                'default' => 1,
                'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6,
                    7 => 7,
                    8 => 8,
                    9 => 9,
                    10 => 10,
                    11 => 11,
                    12 => 12,
                    13 => 13,
                    14 => 14,
                    15 => 15,
                    16 => 16,
                    17 => 17,
                    18 => 18,
                    19 => 19,
                    20 => 20,
                    21 => 21,
                    22 => 22,
                    23 => 23,
                    24 => 24,
                    25 => 25,
                    26 => 26,
                    27 => 27,
                    28 => 28,
                    29 => 29,
                    30 => 30,
                    31 => 31
                ],
                'name' => 'particles.simplecounter.date'
            ],
            'particles.simplecounter.month' => [
                'type' => 'select.select',
                'label' => 'Month',
                'description' => 'Select the month',
                'default' => 0,
                'options' => [
                    0 => 'January',
                    1 => 'February',
                    2 => 'March',
                    3 => 'April',
                    4 => 'May',
                    5 => 'June',
                    6 => 'July',
                    7 => 'August',
                    8 => 'September',
                    9 => 'October',
                    10 => 'November',
                    11 => 'December'
                ],
                'name' => 'particles.simplecounter.month'
            ],
            'particles.simplecounter.year' => [
                'type' => 'input.text',
                'label' => 'Year',
                'description' => 'Set the year.',
                'name' => 'particles.simplecounter.year'
            ],
            'particles.simplecounter.daytext' => [
                'type' => 'input.text',
                'label' => 'Day Text',
                'description' => 'Set the text for Day.',
                'name' => 'particles.simplecounter.daytext'
            ],
            'particles.simplecounter.daystext' => [
                'type' => 'input.text',
                'label' => 'Days Text',
                'description' => 'Set the text for Days.',
                'name' => 'particles.simplecounter.daystext'
            ],
            'particles.simplecounter.hourtext' => [
                'type' => 'input.text',
                'label' => 'Hour Text',
                'description' => 'Set the text for Hour.',
                'name' => 'particles.simplecounter.hourtext'
            ],
            'particles.simplecounter.hourstext' => [
                'type' => 'input.text',
                'label' => 'Hours Text',
                'description' => 'Set the text for Hours.',
                'name' => 'particles.simplecounter.hourstext'
            ],
            'particles.simplecounter.minutetext' => [
                'type' => 'input.text',
                'label' => 'Minute Text',
                'description' => 'Set the text for Minute.',
                'name' => 'particles.simplecounter.minutetext'
            ],
            'particles.simplecounter.minutestext' => [
                'type' => 'input.text',
                'label' => 'Minutes Text',
                'description' => 'Set the text for Minutes.',
                'name' => 'particles.simplecounter.minutestext'
            ],
            'particles.simplecounter.secondtext' => [
                'type' => 'input.text',
                'label' => 'Second Text',
                'description' => 'Set the text for Second.',
                'name' => 'particles.simplecounter.secondtext'
            ],
            'particles.simplecounter.secondstext' => [
                'type' => 'input.text',
                'label' => 'Seconds Text',
                'description' => 'Set the text for Seconds.',
                'name' => 'particles.simplecounter.secondstext'
            ],
            'particles.simplemenu' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.simplemenu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.simplemenu.enabled'
            ],
            'particles.simplemenu.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.simplemenu.title'
            ],
            'particles.simplemenu.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.simplemenu.class'
            ],
            'particles.simplemenu.menus' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Menus',
                'description' => 'Create menu.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.simplemenu.menus'
            ],
            'particles.simplemenu.menus.*' => [
                'type' => '_parent',
                'name' => 'particles.simplemenu.menus.*',
                'form_field' => false
            ],
            'particles.simplemenu.menus.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'skip' => true,
                'name' => 'particles.simplemenu.menus.*.title'
            ],
            'particles.simplemenu.menus.*.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Menu Items',
                'description' => 'Create each menu item to display.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.simplemenu.menus.*.items'
            ],
            'particles.simplemenu.menus.*.items.*' => [
                'type' => '_parent',
                'name' => 'particles.simplemenu.menus.*.items.*',
                'form_field' => false
            ],
            'particles.simplemenu.menus.*.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'skip' => true,
                'name' => 'particles.simplemenu.menus.*.items.*.title'
            ],
            'particles.simplemenu.menus.*.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'Select the icon.',
                'name' => 'particles.simplemenu.menus.*.items.*.icon'
            ],
            'particles.simplemenu.menus.*.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.simplemenu.menus.*.items.*.link'
            ],
            'particles.simplemenu.menus.*.items.*.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.simplemenu.menus.*.items.*.target'
            ],
            'particles.testimonials' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.testimonials.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particles.',
                'default' => true,
                'name' => 'particles.testimonials.enabled'
            ],
            'particles.testimonials.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.testimonials.class'
            ],
            'particles.testimonials.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.testimonials.title'
            ],
            'particles.testimonials.animateOut' => [
                'type' => 'select.select',
                'label' => 'Out Animation',
                'description' => 'Customize the Out Animation from animate css class.',
                'default' => 'fadeOut',
                'options' => [
                    'default' => 'default',
                    'bounce' => 'bounce',
                    'flash' => 'flash',
                    'pulse' => 'pulse',
                    'rubberBand' => 'rubberBand',
                    'shake' => 'shake',
                    'swing' => 'swing',
                    'tada' => 'tada',
                    'wobble' => 'wobble',
                    'jello' => 'jello',
                    'bounceIn' => 'bounceIn',
                    'bounceInDown' => 'bounceInDown',
                    'bounceInLeft' => 'bounceInLeft',
                    'bounceInRight' => 'bounceInRight',
                    'bounceInUp' => 'bounceInUp',
                    'bounceOut' => 'bounceOut',
                    'bounceOutDown' => 'bounceOutDown',
                    'bounceOutLeft' => 'bounceOutLeft',
                    'bounceOutRight' => 'bounceOutRight',
                    'bounceOutUp' => 'bounceOutUp',
                    'fadeIn' => 'fadeIn',
                    'fadeInDown' => 'fadeInDown',
                    'fadeInDownBig' => 'fadeInDownBig',
                    'fadeInLeft' => 'fadeInLeft',
                    'fadeInLeftBig' => 'fadeInLeftBig',
                    'fadeInRight' => 'fadeInRight',
                    'fadeInRightBig' => 'fadeInRightBig',
                    'fadeInUp' => 'fadeInUp',
                    'fadeInUpBig' => 'fadeInUpBig',
                    'fadeOut' => 'fadeOut',
                    'fadeOutDown' => 'fadeOutDown',
                    'fadeOutDownBig' => 'fadeOutDownBig',
                    'fadeOutLeft' => 'fadeOutLeft',
                    'fadeOutLeftBig' => 'fadeOutLeftBig',
                    'fadeOutRight' => 'fadeOutRight',
                    'fadeOutRightBig' => 'fadeOutRightBig',
                    'fadeOutUp' => 'fadeOutUp',
                    'fadeOutUpBig' => 'fadeOutUpBig',
                    'flip' => 'flip',
                    'flipInX' => 'flipInX',
                    'flipInY' => 'flipInY',
                    'flipOutX' => 'flipOutX',
                    'flipOutY' => 'flipOutY',
                    'lightSpeedIn' => 'lightSpeedIn',
                    'lightSpeedOut' => 'lightSpeedOut',
                    'rotateIn' => 'rotateIn',
                    'rotateInDownLeft' => 'rotateInDownLeft',
                    'rotateInDownRight' => 'rotateInDownRight',
                    'rotateInUpLeft' => 'rotateInUpLeft',
                    'rotateInUpRight' => 'rotateInUpRight',
                    'rotateOut' => 'rotateOut',
                    'rotateOutDownLeft' => 'rotateOutDownLeft',
                    'rotateOutDownRight' => 'rotateOutDownRight',
                    'rotateOutUpLeft' => 'rotateOutUpLeft',
                    'rotateOutUpRight' => 'rotateOutUpRight',
                    'slideInUp' => 'slideInUp',
                    'slideInDown' => 'slideInDown',
                    'slideInLeft' => 'slideInLeft',
                    'slideInRight' => 'slideInRight',
                    'slideOutUp' => 'slideOutUp',
                    'slideOutDown' => 'slideOutDown',
                    'slideOutLeft' => 'slideOutLeft',
                    'slideOutRight' => 'slideOutRight',
                    'zoomIn' => 'zoomIn',
                    'zoomInDown' => 'zoomInDown',
                    'zoomInLeft' => 'zoomInLeft',
                    'zoomInRight' => 'zoomInRight',
                    'zoomInUp' => 'zoomInUp',
                    'zoomOut' => 'zoomOut',
                    'zoomOutDown' => 'zoomOutDown',
                    'zoomOutLeft' => 'zoomOutLeft',
                    'zoomOutRight' => 'zoomOutRight',
                    'zoomOutUp' => 'zoomOutUp',
                    'hinge' => 'hinge',
                    'rollIn' => 'rollIn',
                    'rollOut' => 'rollOut'
                ],
                'name' => 'particles.testimonials.animateOut'
            ],
            'particles.testimonials.animateIn' => [
                'type' => 'select.select',
                'label' => 'In Animation',
                'description' => 'Customize the In Animation from animate css class.',
                'default' => 'fadeIn',
                'options' => [
                    'default' => 'default',
                    'bounce' => 'bounce',
                    'flash' => 'flash',
                    'pulse' => 'pulse',
                    'rubberBand' => 'rubberBand',
                    'shake' => 'shake',
                    'swing' => 'swing',
                    'tada' => 'tada',
                    'wobble' => 'wobble',
                    'jello' => 'jello',
                    'bounceIn' => 'bounceIn',
                    'bounceInDown' => 'bounceInDown',
                    'bounceInLeft' => 'bounceInLeft',
                    'bounceInRight' => 'bounceInRight',
                    'bounceInUp' => 'bounceInUp',
                    'bounceOut' => 'bounceOut',
                    'bounceOutDown' => 'bounceOutDown',
                    'bounceOutLeft' => 'bounceOutLeft',
                    'bounceOutRight' => 'bounceOutRight',
                    'bounceOutUp' => 'bounceOutUp',
                    'fadeIn' => 'fadeIn',
                    'fadeInDown' => 'fadeInDown',
                    'fadeInDownBig' => 'fadeInDownBig',
                    'fadeInLeft' => 'fadeInLeft',
                    'fadeInLeftBig' => 'fadeInLeftBig',
                    'fadeInRight' => 'fadeInRight',
                    'fadeInRightBig' => 'fadeInRightBig',
                    'fadeInUp' => 'fadeInUp',
                    'fadeInUpBig' => 'fadeInUpBig',
                    'fadeOut' => 'fadeOut',
                    'fadeOutDown' => 'fadeOutDown',
                    'fadeOutDownBig' => 'fadeOutDownBig',
                    'fadeOutLeft' => 'fadeOutLeft',
                    'fadeOutLeftBig' => 'fadeOutLeftBig',
                    'fadeOutRight' => 'fadeOutRight',
                    'fadeOutRightBig' => 'fadeOutRightBig',
                    'fadeOutUp' => 'fadeOutUp',
                    'fadeOutUpBig' => 'fadeOutUpBig',
                    'flip' => 'flip',
                    'flipInX' => 'flipInX',
                    'flipInY' => 'flipInY',
                    'flipOutX' => 'flipOutX',
                    'flipOutY' => 'flipOutY',
                    'lightSpeedIn' => 'lightSpeedIn',
                    'lightSpeedOut' => 'lightSpeedOut',
                    'rotateIn' => 'rotateIn',
                    'rotateInDownLeft' => 'rotateInDownLeft',
                    'rotateInDownRight' => 'rotateInDownRight',
                    'rotateInUpLeft' => 'rotateInUpLeft',
                    'rotateInUpRight' => 'rotateInUpRight',
                    'rotateOut' => 'rotateOut',
                    'rotateOutDownLeft' => 'rotateOutDownLeft',
                    'rotateOutDownRight' => 'rotateOutDownRight',
                    'rotateOutUpLeft' => 'rotateOutUpLeft',
                    'rotateOutUpRight' => 'rotateOutUpRight',
                    'slideInUp' => 'slideInUp',
                    'slideInDown' => 'slideInDown',
                    'slideInLeft' => 'slideInLeft',
                    'slideInRight' => 'slideInRight',
                    'slideOutUp' => 'slideOutUp',
                    'slideOutDown' => 'slideOutDown',
                    'slideOutLeft' => 'slideOutLeft',
                    'slideOutRight' => 'slideOutRight',
                    'zoomIn' => 'zoomIn',
                    'zoomInDown' => 'zoomInDown',
                    'zoomInLeft' => 'zoomInLeft',
                    'zoomInRight' => 'zoomInRight',
                    'zoomInUp' => 'zoomInUp',
                    'zoomOut' => 'zoomOut',
                    'zoomOutDown' => 'zoomOutDown',
                    'zoomOutLeft' => 'zoomOutLeft',
                    'zoomOutRight' => 'zoomOutRight',
                    'zoomOutUp' => 'zoomOutUp',
                    'hinge' => 'hinge',
                    'rollIn' => 'rollIn',
                    'rollOut' => 'rollOut'
                ],
                'name' => 'particles.testimonials.animateIn'
            ],
            'particles.testimonials.nav' => [
                'type' => 'select.select',
                'label' => 'Prev / Next',
                'description' => 'Enable or disable the Prev / Next navigation.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.testimonials.nav'
            ],
            'particles.testimonials.dots' => [
                'type' => 'select.select',
                'label' => 'Dots',
                'description' => 'Enable or disable the Dots navigation.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.testimonials.dots'
            ],
            'particles.testimonials.loop' => [
                'type' => 'select.select',
                'label' => 'Loop',
                'description' => 'Enable or disable the Inifnity loop. Duplicate last and first items to get loop illusion. This option <strong>won\'t work</strong> in Showcase mode.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.testimonials.loop'
            ],
            'particles.testimonials.autoplay' => [
                'type' => 'select.select',
                'label' => 'Autoplay',
                'description' => 'Enable or disable the Autoplay.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.testimonials.autoplay'
            ],
            'particles.testimonials.autoplaySpeed' => [
                'type' => 'input.text',
                'label' => 'Autoplay Speed',
                'description' => 'Set the speed of the Autoplay, in milliseconds.',
                'name' => 'particles.testimonials.autoplaySpeed'
            ],
            'particles.testimonials.pauseOnHover' => [
                'type' => 'select.select',
                'label' => 'Pause on Hover',
                'description' => 'Pause the slideshow when hovering over slider, then resume when no longer hovering.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.testimonials.pauseOnHover'
            ],
            'particles.testimonials.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Testimonials',
                'description' => 'Create testimonials.',
                'value' => 'author',
                'ajax' => true,
                'name' => 'particles.testimonials.items'
            ],
            'particles.testimonials.items.*' => [
                'type' => '_parent',
                'name' => 'particles.testimonials.items.*',
                'form_field' => false
            ],
            'particles.testimonials.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.testimonials.items.*.image'
            ],
            'particles.testimonials.items.*.author' => [
                'type' => 'input.text',
                'label' => 'Author',
                'description' => 'Enter testimonial author name',
                'name' => 'particles.testimonials.items.*.author'
            ],
            'particles.testimonials.items.*.position' => [
                'type' => 'input.text',
                'label' => 'Position',
                'description' => 'Enter author position',
                'name' => 'particles.testimonials.items.*.position'
            ],
            'particles.testimonials.items.*.testimonial' => [
                'type' => 'textarea.textarea',
                'label' => 'Testimonial',
                'description' => 'Customize testimonial text.',
                'name' => 'particles.testimonials.items.*.testimonial'
            ],
            'particles.verticalslider' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.verticalslider.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.verticalslider.enabled'
            ],
            'particles.verticalslider.source' => [
                'type' => 'select.select',
                'label' => 'Content Source',
                'description' => 'Choose if the content should be loaded from the WordPress posts or particle itself.',
                'default' => 'particle',
                'options' => [
                    'particle' => 'Particle',
                    'grav' => 'Grav'
                ],
                'name' => 'particles.verticalslider.source'
            ],
            'particles.verticalslider.presets' => [
                'type' => 'select.select',
                'label' => 'Presets Sync',
                'description' => 'Enable or disable demo mode. Specifically used for presets on demo.rockettheme.com.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'particles.verticalslider.presets'
            ],
            'particles.verticalslider.speed' => [
                'type' => 'input.number',
                'label' => 'Speed',
                'description' => 'Transition duration (in ms).',
                'default' => 400,
                'name' => 'particles.verticalslider.speed'
            ],
            'particles.verticalslider.auto' => [
                'type' => 'select.select',
                'label' => 'Autoplay',
                'description' => 'If enabled, the Slider will automatically start to play.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.verticalslider.auto'
            ],
            'particles.verticalslider.pause' => [
                'type' => 'input.number',
                'label' => 'Pause Duration',
                'description' => 'The time (in ms) between each auto transition.',
                'default' => 2000,
                'min' => 0,
                'name' => 'particles.verticalslider.pause'
            ],
            'particles.verticalslider.loop' => [
                'type' => 'select.select',
                'label' => 'Loop',
                'description' => 'Enable or disable looping of the slides.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'particles.verticalslider.loop'
            ],
            'particles.verticalslider.controls' => [
                'type' => 'select.select',
                'label' => 'Controls',
                'description' => 'If disabled, prev/next buttons will not be displayed.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.verticalslider.controls'
            ],
            'particles.verticalslider.pager' => [
                'type' => 'select.select',
                'label' => 'Pager',
                'description' => 'Enable pager option',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.verticalslider.pager'
            ],
            'particles.verticalslider.height' => [
                'type' => 'input.number',
                'label' => 'Height',
                'description' => 'Slider height in px.',
                'default' => 800,
                'name' => 'particles.verticalslider.height'
            ],
            'particles.verticalslider.mobileheight' => [
                'type' => 'input.number',
                'label' => 'Mobile Height',
                'description' => 'Slider height in px for mobile view.',
                'default' => 600,
                'name' => 'particles.verticalslider.mobileheight'
            ],
            'particles.verticalslider._tab_settings' => [
                'label' => 'Settings',
                'overridable' => false,
                'name' => 'particles.verticalslider._tab_settings'
            ],
            'particles.verticalslider.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Images',
                'description' => 'Create slideshow items.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.verticalslider.items'
            ],
            'particles.verticalslider.items.*' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.items.*',
                'form_field' => false
            ],
            'particles.verticalslider.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select image for slideshow',
                'name' => 'particles.verticalslider.items.*.image'
            ],
            'particles.verticalslider.items.*.small_title' => [
                'type' => 'input.text',
                'label' => 'Small Title',
                'description' => 'Customize the small Title.',
                'name' => 'particles.verticalslider.items.*.small_title'
            ],
            'particles.verticalslider.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the Title.',
                'name' => 'particles.verticalslider.items.*.title'
            ],
            'particles.verticalslider.items.*.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'name' => 'particles.verticalslider.items.*.desc'
            ],
            'particles.verticalslider.items.*.buttontext' => [
                'type' => 'input.text',
                'label' => 'Button Label',
                'description' => 'Specify the button label.',
                'name' => 'particles.verticalslider.items.*.buttontext'
            ],
            'particles.verticalslider.items.*.buttonlink' => [
                'type' => 'input.text',
                'label' => 'Button Link',
                'description' => 'Specify the button link.',
                'name' => 'particles.verticalslider.items.*.buttonlink'
            ],
            'particles.verticalslider.items.*.buttontarget' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.verticalslider.items.*.buttontarget'
            ],
            'particles.verticalslider.items.*.buttonclass' => [
                'type' => 'input.selectize',
                'label' => 'Button Classes',
                'description' => 'CSS class names for the button.',
                'name' => 'particles.verticalslider.items.*.buttonclass'
            ],
            'particles.verticalslider._tab_collection' => [
                'label' => 'Particle Items',
                'overridable' => false,
                'name' => 'particles.verticalslider._tab_collection'
            ],
            'particles.verticalslider.article' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article',
                'form_field' => false
            ],
            'particles.verticalslider.article.filter' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.filter',
                'form_field' => false
            ],
            'particles.verticalslider.article.filter.categories' => [
                'type' => 'input.selectize',
                'label' => 'Categories',
                'description' => 'Select the categories the content should be taken from.',
                'overridable' => false,
                'name' => 'particles.verticalslider.article.filter.categories'
            ],
            'particles.verticalslider.article.limit' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.limit',
                'form_field' => false
            ],
            'particles.verticalslider.article.limit.total' => [
                'type' => 'input.text',
                'label' => 'Number of Pages',
                'description' => 'Enter the maximum number of content to display.',
                'default' => 5,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.verticalslider.article.limit.total'
            ],
            'particles.verticalslider.article.limit.start' => [
                'type' => 'input.text',
                'label' => 'Start From',
                'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first content item).',
                'default' => 0,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.verticalslider.article.limit.start'
            ],
            'particles.verticalslider.article.sort' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.sort',
                'form_field' => false
            ],
            'particles.verticalslider.article.sort.orderby' => [
                'type' => 'select.select',
                'label' => 'Order By',
                'description' => 'Select how the content should be ordered by.',
                'default' => 'default',
                'options' => [
                    'default' => 'Default Ordering',
                    'date' => 'Date',
                    'publish_date' => 'Publish Date',
                    'unpublish_date' => 'Unpublish Date',
                    'modified' => 'Last Modified Date',
                    'title' => 'Title',
                    'slug' => 'Slug'
                ],
                'overridable' => false,
                'name' => 'particles.verticalslider.article.sort.orderby'
            ],
            'particles.verticalslider.article.sort.ordering' => [
                'type' => 'select.select',
                'label' => 'Ordering Direction',
                'description' => 'Select the direction the content should be ordered by.',
                'default' => 'asc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'overridable' => false,
                'name' => 'particles.verticalslider.article.sort.ordering'
            ],
            'particles.verticalslider._tab_articles' => [
                'label' => 'Pages',
                'overridable' => false,
                'name' => 'particles.verticalslider._tab_articles'
            ],
            'particles.verticalslider.article.display' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.display',
                'form_field' => false
            ],
            'particles.verticalslider.article.display.image' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.display.image',
                'form_field' => false
            ],
            'particles.verticalslider.article.display.image.enabled' => [
                'type' => 'select.select',
                'label' => 'Image',
                'description' => 'Select if and what image of the content should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Intro',
                    'full' => 'Full',
                    '' => 'None'
                ],
                'name' => 'particles.verticalslider.article.display.image.enabled'
            ],
            'particles.verticalslider.article.display.text' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.display.text',
                'form_field' => false
            ],
            'particles.verticalslider.article.display.text.type' => [
                'type' => 'select.select',
                'label' => 'Article Text',
                'description' => 'Select if and how the content text should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Introduction',
                    'full' => 'Full Article',
                    '' => 'Hide'
                ],
                'name' => 'particles.verticalslider.article.display.text.type'
            ],
            'particles.verticalslider.article.display.text.limit' => [
                'type' => 'input.text',
                'label' => 'Text Limit',
                'description' => 'Type in the number of characters the content text should be limited to.',
                'default' => '',
                'pattern' => '\\d+',
                'name' => 'particles.verticalslider.article.display.text.limit'
            ],
            'particles.verticalslider.article.display.text.formatting' => [
                'type' => 'select.select',
                'label' => 'Text Formatting',
                'description' => 'Select the formatting you want to use to display the content text.',
                'default' => 'text',
                'options' => [
                    'text' => 'Plain Text',
                    'html' => 'HTML'
                ],
                'name' => 'particles.verticalslider.article.display.text.formatting'
            ],
            'particles.verticalslider.article.display.title' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.display.title',
                'form_field' => false
            ],
            'particles.verticalslider.article.display.title.enabled' => [
                'type' => 'select.select',
                'label' => 'Title',
                'description' => 'Select if the content title should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.verticalslider.article.display.title.enabled'
            ],
            'particles.verticalslider.article.display.title.limit' => [
                'type' => 'input.text',
                'label' => 'Title Limit',
                'description' => 'Enter the maximum number of characters the content title should be limited to.',
                'pattern' => '\\d+(\\.\\d+){0,1}',
                'name' => 'particles.verticalslider.article.display.title.limit'
            ],
            'particles.verticalslider.article.display.read_more' => [
                'type' => '_parent',
                'name' => 'particles.verticalslider.article.display.read_more',
                'form_field' => false
            ],
            'particles.verticalslider.article.display.read_more.enabled' => [
                'type' => 'select.select',
                'label' => 'Read More',
                'description' => 'Select if the content \'Read More\' button should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.verticalslider.article.display.read_more.enabled'
            ],
            'particles.verticalslider.article.display.read_more.label' => [
                'type' => 'input.text',
                'label' => 'Read More Label',
                'description' => 'Type in the label for the \'Read More\' button.',
                'name' => 'particles.verticalslider.article.display.read_more.label'
            ],
            'particles.verticalslider.article.display.read_more.css' => [
                'type' => 'input.selectize',
                'label' => 'Button CSS Classes',
                'description' => 'CSS class name for the \'Read More\' button.',
                'name' => 'particles.verticalslider.article.display.read_more.css'
            ],
            'particles.verticalslider._tab_display' => [
                'label' => 'Pages Display',
                'name' => 'particles.verticalslider._tab_display'
            ],
            'particles.video' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'overrideable' => false
                ]
            ],
            'particles.video.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable particle.',
                'default' => true,
                'name' => 'particles.video.enabled'
            ],
            'particles.video.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.video.class'
            ],
            'particles.video.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.video.title'
            ],
            'particles.video.columns' => [
                'type' => 'select.selectize',
                'label' => 'Columns',
                'description' => 'Select columns amount.',
                'default' => 1,
                'options' => [
                    1 => '1 column',
                    2 => '2 columns',
                    3 => '3 columns',
                    4 => '4 columns'
                ],
                'name' => 'particles.video.columns'
            ],
            'particles.video.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Blocks',
                'description' => 'Create each item to appear in the content row.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.video.items'
            ],
            'particles.video.items.*' => [
                'type' => '_parent',
                'name' => 'particles.video.items.*',
                'form_field' => false
            ],
            'particles.video.items.*.caption' => [
                'type' => 'input.text',
                'label' => 'Caption',
                'description' => 'Video caption',
                'name' => 'particles.video.items.*.caption'
            ],
            'particles.video.items.*.source' => [
                'type' => 'select.selectize',
                'label' => 'Source',
                'description' => 'Video Source',
                'default' => 'local',
                'options' => [
                    'preset' => 'Slideshow Preset',
                    'vimeo' => 'Vimeo Url',
                    'youtube' => 'YouTube Url',
                    'local' => 'Local Video',
                    'external' => 'External Url'
                ],
                'name' => 'particles.video.items.*.source'
            ],
            'particles.video.items.*.video' => [
                'type' => 'input.text',
                'label' => 'Url',
                'description' => 'URL from YouTube, Vimeo, or External',
                'name' => 'particles.video.items.*.video'
            ],
            'particles.video.items.*.local' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Local Video Sources',
                'description' => 'Serve your video in multiple formats.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.video.items.*.local'
            ],
            'particles.video.items.*.local.*' => [
                'type' => '_parent',
                'name' => 'particles.video.items.*.local.*',
                'form_field' => false
            ],
            'particles.video.items.*.local.*.file' => [
                'type' => 'input.videopicker',
                'label' => 'Local Video',
                'description' => 'Select desired video.',
                'name' => 'particles.video.items.*.local.*.file'
            ],
            'particles.video.items.*.posterimage' => [
                'type' => 'input.imagepicker',
                'label' => 'Poster Image',
                'description' => 'Select the poster image for Local or External Video.',
                'name' => 'particles.video.items.*.posterimage'
            ],
            'particles.video.items.*.loop' => [
                'type' => 'select.select',
                'label' => 'Loop',
                'description' => 'Enable or disable video looping.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.loop'
            ],
            'particles.video.items.*.autoplay' => [
                'type' => 'select.select',
                'label' => 'Autoplay',
                'description' => 'Enable or disable video autoplay.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.autoplay'
            ],
            'particles.video.items.*.controls' => [
                'type' => 'select.select',
                'label' => 'Show Controls (YouTube, Local, and External)',
                'description' => 'Enable or disable play bar (applies to YouTube, Local, and External videos).',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.controls'
            ],
            'particles.video.items.*.info' => [
                'type' => 'select.select',
                'label' => 'Show Info (YouTube)',
                'description' => 'Enable or disable top info bar (applies to YouTube only).',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.info'
            ],
            'particles.video.items.*.related' => [
                'type' => 'select.select',
                'label' => 'Related Videos (YouTube)',
                'description' => 'Enable or disable displaying related videos when Youtube video is paused or has ended.',
                'default' => 'disabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.related'
            ],
            'particles.video.items.*.muted' => [
                'type' => 'select.select',
                'label' => 'Muted',
                'description' => 'Videos must be Muted in order for Autoplay to work in Chrome.',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enable',
                    'disabled' => 'Disable'
                ],
                'name' => 'particles.video.items.*.muted'
            ],
            'particles.video.items.*.start' => [
                'type' => 'input.number',
                'label' => 'Start Time (YouTube)',
                'description' => 'Determine start time of YouTube video (in seconds).',
                'name' => 'particles.video.items.*.start'
            ],
            'particles.analytics' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.analytics.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable analytic particles.',
                'default' => true,
                'name' => 'particles.analytics.enabled'
            ],
            'particles.analytics.ua' => [
                'type' => '_parent',
                'name' => 'particles.analytics.ua',
                'form_field' => false
            ],
            'particles.analytics.ua.code' => [
                'type' => 'input.text',
                'description' => 'Enter the Google UA tracking code for analytics (UA-XXXXXXXX-X)',
                'label' => 'UA Code',
                'name' => 'particles.analytics.ua.code'
            ],
            'particles.analytics.ua.anonym' => [
                'type' => 'input.checkbox',
                'description' => 'Send only Anonymous IP Addresses (mandatory in Europe)',
                'label' => 'Anonym Statistics',
                'default' => false,
                'name' => 'particles.analytics.ua.anonym'
            ],
            'particles.analytics.ua.ssl' => [
                'type' => 'input.checkbox',
                'description' => 'Send all data using SSL, even from insecure (HTTP) pages.',
                'label' => 'Force SSL use',
                'default' => false,
                'name' => 'particles.analytics.ua.ssl'
            ],
            'particles.analytics.ua.debug' => [
                'type' => 'input.checkbox',
                'description' => 'Enable the debug version of analytics.js - DON\'T USE ON PRODUCTION!',
                'label' => 'Use Debug Mode',
                'default' => false,
                'name' => 'particles.analytics.ua.debug'
            ],
            'particles.assets' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.assets.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable CSS/JS particles.',
                'default' => true,
                'name' => 'particles.assets.enabled'
            ],
            'particles.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.css'
            ],
            'particles.assets.css.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.css.*',
                'form_field' => false
            ],
            'particles.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.css.*.name'
            ],
            'particles.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.css.*.location'
            ],
            'particles.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'particles.assets.css.*.inline'
            ],
            'particles.assets.css.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'rel',
                    1 => 'href',
                    2 => 'type'
                ],
                'name' => 'particles.assets.css.*.extra'
            ],
            'particles.assets.css.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'particles.assets.css.*.priority'
            ],
            'particles.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.javascript'
            ],
            'particles.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.javascript.*',
                'form_field' => false
            ],
            'particles.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.javascript.*.name'
            ],
            'particles.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.javascript.*.location'
            ],
            'particles.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'particles.assets.javascript.*.inline'
            ],
            'particles.assets.javascript.*.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'particles.assets.javascript.*.in_footer'
            ],
            'particles.assets.javascript.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'particles.assets.javascript.*.extra'
            ],
            'particles.assets.javascript.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'particles.assets.javascript.*.priority'
            ],
            'particles.branding' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.branding.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to the particles.',
                'default' => true,
                'name' => 'particles.branding.enabled'
            ],
            'particles.branding.content' => [
                'type' => 'textarea.textarea',
                'label' => 'Content',
                'description' => 'Create or modify custom branding content.',
                'default' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'name' => 'particles.branding.content'
            ],
            'particles.branding.css' => [
                'type' => '_parent',
                'name' => 'particles.branding.css',
                'form_field' => false
            ],
            'particles.branding.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'branding',
                'name' => 'particles.branding.css.class'
            ],
            'particles.breadcrumbs' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.breadcrumbs.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable breadcrumbs particles.',
                'default' => true,
                'name' => 'particles.breadcrumbs.enabled'
            ],
            'particles.content' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.content.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable page content.',
                'default' => true,
                'name' => 'particles.content.enabled'
            ],
            'particles.contentarray' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.contentarray.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Grav Content particles.',
                'default' => true,
                'name' => 'particles.contentarray.enabled'
            ],
            'particles.contentarray.article' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article',
                'form_field' => false
            ],
            'particles.contentarray.article.filter' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.filter',
                'form_field' => false
            ],
            'particles.contentarray.article.filter.categories' => [
                'type' => 'input.selectize',
                'label' => 'Categories',
                'description' => 'Select the categories the content should be taken from.',
                'overridable' => false,
                'name' => 'particles.contentarray.article.filter.categories'
            ],
            'particles.contentarray.article.limit' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.limit',
                'form_field' => false
            ],
            'particles.contentarray.article.limit.total' => [
                'type' => 'input.text',
                'label' => 'Number of Pages',
                'description' => 'Enter the maximum number of content to display.',
                'default' => 2,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.total'
            ],
            'particles.contentarray.article.display' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display',
                'form_field' => false
            ],
            'particles.contentarray.article.display.pagination_buttons' => [
                'type' => 'select.select',
                'label' => 'Pagination',
                'description' => 'Select if the pagination buttons should be shown to allow users to see more articles.',
                'default' => '',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.display.pagination_buttons'
            ],
            'particles.contentarray.article.limit.columns' => [
                'type' => 'select.select',
                'label' => 'Number of columns',
                'description' => 'Select the number of columns that you want content to appear in.',
                'default' => 2,
                'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.columns'
            ],
            'particles.contentarray.article.limit.start' => [
                'type' => 'input.text',
                'label' => 'Start From',
                'description' => 'Enter offset specifying the first article to return. The default is \'0\' (the first content item).',
                'default' => 0,
                'pattern' => '\\d{1,2}',
                'overridable' => false,
                'name' => 'particles.contentarray.article.limit.start'
            ],
            'particles.contentarray.article.sort' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.sort',
                'form_field' => false
            ],
            'particles.contentarray.article.sort.orderby' => [
                'type' => 'select.select',
                'label' => 'Order By',
                'description' => 'Select how the content should be ordered by.',
                'default' => 'default',
                'options' => [
                    'default' => 'Default Ordering',
                    'date' => 'Date',
                    'publish_date' => 'Publish Date',
                    'unpublish_date' => 'Unpublish Date',
                    'modified' => 'Last Modified Date',
                    'title' => 'Title',
                    'slug' => 'Slug'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.sort.orderby'
            ],
            'particles.contentarray.article.sort.ordering' => [
                'type' => 'select.select',
                'label' => 'Ordering Direction',
                'description' => 'Select the direction the content should be ordered by.',
                'default' => 'asc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'overridable' => false,
                'name' => 'particles.contentarray.article.sort.ordering'
            ],
            'particles.contentarray._tab_articles' => [
                'label' => 'Pages',
                'overridable' => false,
                'name' => 'particles.contentarray._tab_articles'
            ],
            'particles.contentarray.article.display.image' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.image',
                'form_field' => false
            ],
            'particles.contentarray.article.display.image.enabled' => [
                'type' => 'select.select',
                'label' => 'Image',
                'description' => 'Select if and what image of the content should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Intro',
                    'full' => 'Full',
                    '' => 'None'
                ],
                'name' => 'particles.contentarray.article.display.image.enabled'
            ],
            'particles.contentarray.article.display.text' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.text',
                'form_field' => false
            ],
            'particles.contentarray.article.display.text.type' => [
                'type' => 'select.select',
                'label' => 'Article Text',
                'description' => 'Select if and how the content text should be shown.',
                'default' => 'intro',
                'options' => [
                    'intro' => 'Introduction',
                    'full' => 'Full Article',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.text.type'
            ],
            'particles.contentarray.article.display.text.limit' => [
                'type' => 'input.text',
                'label' => 'Text Limit',
                'description' => 'Type in the number of characters the content text should be limited to.',
                'default' => '',
                'pattern' => '\\d+',
                'name' => 'particles.contentarray.article.display.text.limit'
            ],
            'particles.contentarray.article.display.text.formatting' => [
                'type' => 'select.select',
                'label' => 'Text Formatting',
                'description' => 'Select the formatting you want to use to display the content text.',
                'default' => 'text',
                'options' => [
                    'text' => 'Plain Text',
                    'html' => 'HTML'
                ],
                'name' => 'particles.contentarray.article.display.text.formatting'
            ],
            'particles.contentarray.article.display.title' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.title',
                'form_field' => false
            ],
            'particles.contentarray.article.display.title.enabled' => [
                'type' => 'select.select',
                'label' => 'Title',
                'description' => 'Select if the content title should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.title.enabled'
            ],
            'particles.contentarray.article.display.title.limit' => [
                'type' => 'input.text',
                'label' => 'Title Limit',
                'description' => 'Enter the maximum number of characters the content title should be limited to.',
                'pattern' => '\\d+(\\.\\d+){0,1}',
                'name' => 'particles.contentarray.article.display.title.limit'
            ],
            'particles.contentarray.article.display.date' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.date',
                'form_field' => false
            ],
            'particles.contentarray.article.display.date.enabled' => [
                'type' => 'select.select',
                'label' => 'Date',
                'description' => 'Select if the content date should be shown.',
                'default' => 'published',
                'options' => [
                    'created' => 'Show Created Date',
                    'published' => 'Show Published Date',
                    'modified' => 'Show Modified Date',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.date.enabled'
            ],
            'particles.contentarray.article.display.date.format' => [
                'type' => 'select.date',
                'label' => 'Date Format',
                'description' => 'Select preferred date format. Leave empty not to display a date.',
                'default' => 'l, F d, Y',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11',
                    'F d, Y' => 'Date12'
                ],
                'name' => 'particles.contentarray.article.display.date.format'
            ],
            'particles.contentarray._tab_display' => [
                'label' => 'Display',
                'name' => 'particles.contentarray._tab_display'
            ],
            'particles.contentarray.article.display.read_more' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.read_more',
                'form_field' => false
            ],
            'particles.contentarray.article.display.read_more.enabled' => [
                'type' => 'select.select',
                'label' => 'Read More',
                'description' => 'Select if the content \'Read More\' button should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.read_more.enabled'
            ],
            'particles.contentarray.article.display.read_more.label' => [
                'type' => 'input.text',
                'label' => 'Read More Label',
                'description' => 'Type in the label for the \'Read More\' button.',
                'name' => 'particles.contentarray.article.display.read_more.label'
            ],
            'particles.contentarray.article.display.read_more.css' => [
                'type' => 'input.selectize',
                'label' => 'Button CSS Classes',
                'description' => 'CSS class name for the \'Read More\' button.',
                'name' => 'particles.contentarray.article.display.read_more.css'
            ],
            'particles.contentarray._tab_readmore' => [
                'label' => 'Read More',
                'name' => 'particles.contentarray._tab_readmore'
            ],
            'particles.contentarray.article.display.author' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.author',
                'form_field' => false
            ],
            'particles.contentarray.article.display.author.enabled' => [
                'type' => 'select.select',
                'label' => 'Author',
                'description' => 'Select if the content author should be shown.',
                'default' => 'show',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.author.enabled'
            ],
            'particles.contentarray.article.display.category' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.article.display.category',
                'form_field' => false
            ],
            'particles.contentarray.article.display.category.enabled' => [
                'type' => 'select.select',
                'label' => 'Category',
                'description' => 'Select if and how the content category should be shown.',
                'default' => 'link',
                'options' => [
                    'show' => 'Show',
                    '' => 'Hide'
                ],
                'name' => 'particles.contentarray.article.display.category.enabled'
            ],
            'particles.contentarray.article.display.category.route' => [
                'type' => 'input.text',
                'label' => 'Category Route',
                'description' => 'Route to the category page',
                'name' => 'particles.contentarray.article.display.category.route'
            ],
            'particles.contentarray._tab_extras' => [
                'label' => 'Extras',
                'name' => 'particles.contentarray._tab_extras'
            ],
            'particles.contentarray.css' => [
                'type' => '_parent',
                'name' => 'particles.contentarray.css',
                'form_field' => false
            ],
            'particles.contentarray.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contentarray.css.class'
            ],
            'particles.contentarray.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag Attributes',
                'description' => 'Extra Tag attributes.',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'id',
                    1 => 'class'
                ],
                'name' => 'particles.contentarray.extra'
            ],
            'particles.copyright' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.copyright.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.copyright.enabled'
            ],
            'particles.copyright.date' => [
                'type' => '_parent',
                'name' => 'particles.copyright.date',
                'form_field' => false
            ],
            'particles.copyright.date.start' => [
                'type' => 'input.text',
                'label' => 'Start Year',
                'description' => 'Select the copyright start year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.start'
            ],
            'particles.copyright.date.end' => [
                'type' => 'input.text',
                'label' => 'End Year',
                'description' => 'Select the copyright end year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.end'
            ],
            'particles.copyright.owner' => [
                'type' => 'input.text',
                'label' => 'Copyright owner',
                'description' => 'Add copyright owner name.',
                'name' => 'particles.copyright.owner'
            ],
            'particles.custom' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.custom.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.custom.enabled'
            ],
            'particles.custom.html' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom HTML',
                'description' => 'Enter custom HTML into here.',
                'overridable' => false,
                'name' => 'particles.custom.html'
            ],
            'particles.custom.twig' => [
                'type' => 'input.checkbox',
                'label' => 'Process Twig',
                'description' => 'Enable Twig template processing in the content. Twig will be processed before shortcodes.',
                'default' => '0',
                'name' => 'particles.custom.twig'
            ],
            'particles.custom.filter' => [
                'type' => 'input.checkbox',
                'label' => 'Process shortcodes',
                'description' => 'Enable shortcode processing / filtering in the content.',
                'default' => '0',
                'name' => 'particles.custom.filter'
            ],
            'particles.date' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.date.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable date particles.',
                'default' => true,
                'name' => 'particles.date.enabled'
            ],
            'particles.date.css' => [
                'type' => '_parent',
                'name' => 'particles.date.css',
                'form_field' => false
            ],
            'particles.date.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'date',
                'name' => 'particles.date.css.class'
            ],
            'particles.date.date' => [
                'type' => '_parent',
                'name' => 'particles.date.date',
                'form_field' => false
            ],
            'particles.date.date.formats' => [
                'type' => 'select.date',
                'label' => 'Format',
                'description' => 'Select preferred date format.',
                'default' => 'l, F d, Y',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11',
                    'F d, Y' => 'Date12'
                ],
                'name' => 'particles.date.date.formats'
            ],
            'particles.feed' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.feed.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable feed particles.',
                'default' => true,
                'name' => 'particles.feed.enabled'
            ],
            'particles.frameworks' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.frameworks.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Frameworks atom.',
                'default' => true,
                'name' => 'particles.frameworks.enabled'
            ],
            'particles.frameworks.jquery' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.jquery',
                'form_field' => false
            ],
            'particles.frameworks.jquery.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.enabled'
            ],
            'particles.frameworks.jquery.ui_core' => [
                'type' => 'enable.enable',
                'label' => 'UI Core',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.ui_core'
            ],
            'particles.frameworks.jquery.ui_sortable' => [
                'type' => 'enable.enable',
                'label' => 'UI Sortable',
                'default' => 0,
                'name' => 'particles.frameworks.jquery.ui_sortable'
            ],
            'particles.frameworks.bootstrap2' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.bootstrap2',
                'form_field' => false
            ],
            'particles.frameworks.bootstrap2.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.bootstrap2.enabled'
            ],
            'particles.frameworks.bootstrap3' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.bootstrap3',
                'form_field' => false
            ],
            'particles.frameworks.bootstrap3.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.bootstrap3.enabled'
            ],
            'particles.frameworks.mootools' => [
                'type' => '_parent',
                'name' => 'particles.frameworks.mootools',
                'form_field' => false
            ],
            'particles.frameworks.mootools.enabled' => [
                'type' => 'enable.enable',
                'label' => 'Framework',
                'default' => 0,
                'name' => 'particles.frameworks.mootools.enabled'
            ],
            'particles.frameworks.mootools.more' => [
                'type' => 'enable.enable',
                'label' => 'Mootools More',
                'default' => 0,
                'name' => 'particles.frameworks.mootools.more'
            ],
            'particles.langswitcher' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.langswitcher.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Language Switcher particles.',
                'default' => true,
                'name' => 'particles.langswitcher.enabled'
            ],
            'particles.lightcase' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.lightcase.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Lightcase atom.',
                'default' => true,
                'name' => 'particles.lightcase.enabled'
            ],
            'particles.login' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.login.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable login particles.',
                'default' => true,
                'name' => 'particles.login.enabled'
            ],
            'particles.logo' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.logo.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable logo particles.',
                'default' => true,
                'name' => 'particles.logo.enabled'
            ],
            'particles.logo.url' => [
                'type' => 'input.text',
                'label' => 'Url',
                'description' => 'Url for the image. Leave empty to go to home page.',
                'name' => 'particles.logo.url'
            ],
            'particles.logo.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when logo is clicked.',
                'default' => '_self',
                'options' => [
                    '_self' => 'Same Frame (default)',
                    '_parent' => 'Parent Frame',
                    '_blank' => 'New Window or Tab'
                ],
                'name' => 'particles.logo.target'
            ],
            'particles.logo.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired logo image.',
                'name' => 'particles.logo.image'
            ],
            'particles.logo.link' => [
                'type' => 'input.checkbox',
                'label' => 'Link',
                'description' => 'Renders Logo/Image with a link.',
                'default' => true,
                'name' => 'particles.logo.link'
            ],
            'particles.logo.svg' => [
                'type' => 'textarea.textarea',
                'label' => 'SVG Code',
                'description' => 'Your SVG code that will be added inline to the site.',
                'name' => 'particles.logo.svg'
            ],
            'particles.logo.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'Input logo description text.',
                'name' => 'particles.logo.text'
            ],
            'particles.logo.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'Set a specific CSS class for custom styling.',
                'name' => 'particles.logo.class'
            ],
            'particles.menu' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the menu particle.',
                'default' => true,
                'name' => 'particles.menu.enabled'
            ],
            'particles.menu.menu' => [
                'type' => 'menu.list',
                'label' => 'Menu',
                'description' => 'Select menu to be used with the particle.',
                'default' => '',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    '' => 'Use Default Menu',
                    '-active-' => 'Use Active Menu'
                ],
                'name' => 'particles.menu.menu'
            ],
            'particles.menu.base' => [
                'type' => 'menu.item',
                'label' => 'Base Item',
                'description' => 'Select a menu item to always be used as the base for the menu display.',
                'default' => '/',
                'options' => [
                    '/' => 'Active'
                ],
                'name' => 'particles.menu.base'
            ],
            'particles.menu.startLevel' => [
                'type' => 'input.text',
                'label' => 'Start Level',
                'description' => 'Set the start level of the menu.',
                'default' => 1,
                'name' => 'particles.menu.startLevel'
            ],
            'particles.menu.maxLevels' => [
                'type' => 'input.text',
                'label' => 'Max Levels',
                'description' => 'Set the maximum number of menu levels to display.',
                'default' => 0,
                'name' => 'particles.menu.maxLevels'
            ],
            'particles.menu.renderTitles' => [
                'type' => 'input.checkbox',
                'label' => 'Render Titles',
                'description' => 'Renders the titles/tooltips of the Menu Items for accessibility.',
                'default' => 0,
                'name' => 'particles.menu.renderTitles'
            ],
            'particles.menu.hoverExpand' => [
                'type' => 'input.checkbox',
                'label' => 'Expand on Hover',
                'description' => 'Allows to enable / disable the ability to expand menu items by hover or click only',
                'default' => 1,
                'name' => 'particles.menu.hoverExpand'
            ],
            'particles.menu.mobileTarget' => [
                'type' => 'input.checkbox',
                'label' => 'Mobile Target',
                'description' => 'Check this field if you want this menu to become the target for Mobile Menu and to appear in Offcanvas',
                'default' => 0,
                'name' => 'particles.menu.mobileTarget'
            ],
            'particles.menu.forceTarget' => [
                'type' => 'input.checkbox',
                'label' => 'Force Target Attribute',
                'description' => 'Adds \'target=&quot;_self&quot;\' attribute to all menu links instead of omitting the default value. Fixes an issue with pinned tabs in Firefox where external links always open in a new tab.',
                'default' => 0,
                'name' => 'particles.menu.forceTarget'
            ],
            'particles.messages' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.messages.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable system messages.',
                'default' => true,
                'name' => 'particles.messages.enabled'
            ],
            'particles.mobile-menu' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.mobile-menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.mobile-menu.enabled'
            ],
            'particles.position' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.position.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable module positions.',
                'default' => true,
                'name' => 'particles.position.enabled'
            ],
            'particles.position.key' => [
                'type' => 'input.text',
                'label' => 'Key',
                'description' => 'Position name.',
                'pattern' => '[A-Za-z0-9-]+',
                'overridable' => false,
                'name' => 'particles.position.key'
            ],
            'particles.position.chrome' => [
                'type' => 'input.text',
                'label' => 'Chrome',
                'description' => 'Module chrome in this position.',
                'name' => 'particles.position.chrome'
            ],
            'particles.social' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.social.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable social particles.',
                'default' => true,
                'name' => 'particles.social.enabled'
            ],
            'particles.social.css' => [
                'type' => '_parent',
                'name' => 'particles.social.css',
                'form_field' => false
            ],
            'particles.social.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'social',
                'name' => 'particles.social.css.class'
            ],
            'particles.social.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'name' => 'particles.social.title'
            ],
            'particles.social.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.social.target'
            ],
            'particles.social.display' => [
                'type' => 'input.radios',
                'label' => 'Display',
                'description' => 'How to display the Social Icons',
                'default' => 'both',
                'options' => [
                    'icons_only' => 'Icons Only',
                    'text_only' => 'Text Only',
                    'both' => 'Both'
                ],
                'name' => 'particles.social.display'
            ],
            'particles.social.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Social Items',
                'description' => 'Create each social item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.social.items'
            ],
            'particles.social.items.*' => [
                'type' => '_parent',
                'name' => 'particles.social.items.*',
                'form_field' => false
            ],
            'particles.social.items.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.social.items.*.name'
            ],
            'particles.social.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.social.items.*.icon'
            ],
            'particles.social.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.social.items.*.text'
            ],
            'particles.social.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.social.items.*.link'
            ],
            'particles.spacer' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.spacer.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.spacer.enabled'
            ],
            'particles.totop' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'particles.totop.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to top particles.',
                'default' => true,
                'name' => 'particles.totop.enabled'
            ],
            'particles.totop.css' => [
                'type' => '_parent',
                'name' => 'particles.totop.css',
                'form_field' => false
            ],
            'particles.totop.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'totop',
                'name' => 'particles.totop.css.class'
            ],
            'particles.totop.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'A Font Awesome icon to be displayed for the link.',
                'name' => 'particles.totop.icon'
            ],
            'particles.totop.content' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'The text to be displayed for the link. HTML is allowed.',
                'name' => 'particles.totop.content'
            ],
            'styles.above' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles' => [
                'type' => '_parent',
                'name' => 'styles',
                'form_field' => false
            ],
            'styles.above.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.above.background'
            ],
            'styles.above.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.above.text-color'
            ],
            'styles.accent' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.accent.color-1' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 1',
                'default' => '#ff4b64',
                'name' => 'styles.accent.color-1'
            ],
            'styles.accent.color-2' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 2',
                'default' => '#31a594',
                'name' => 'styles.accent.color-2'
            ],
            'styles.accent.color-3' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 3',
                'default' => '#508b70',
                'name' => 'styles.accent.color-3'
            ],
            'styles.base' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.base.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Background',
                'default' => '#ffffff',
                'name' => 'styles.base.background'
            ],
            'styles.base.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Text Color',
                'default' => '#666666',
                'name' => 'styles.base.text-color'
            ],
            'styles.base.text-active-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Text Active Color',
                'default' => '#31a594',
                'name' => 'styles.base.text-active-color'
            ],
            'styles.bottom' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.bottom.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#0d0d0d',
                'name' => 'styles.bottom.background'
            ],
            'styles.bottom.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.bottom.text-color'
            ],
            'styles.breakpoints' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.breakpoints.large-desktop-container' => [
                'type' => 'input.text',
                'label' => 'Large Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '75rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-desktop-container'
            ],
            'styles.breakpoints.desktop-container' => [
                'type' => 'input.text',
                'label' => 'Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '60rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.desktop-container'
            ],
            'styles.breakpoints.tablet-container' => [
                'type' => 'input.text',
                'label' => 'Tablet',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '51rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.tablet-container'
            ],
            'styles.breakpoints.large-mobile-container' => [
                'type' => 'input.text',
                'label' => 'Mobile',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '30rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-mobile-container'
            ],
            'styles.breakpoints.mobile-menu-breakpoint' => [
                'type' => 'input.text',
                'label' => 'Mobile Menu',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '51rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.mobile-menu-breakpoint'
            ],
            'styles.copyright' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.copyright.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#eaf6f4',
                'name' => 'styles.copyright.background'
            ],
            'styles.copyright.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#50818b',
                'name' => 'styles.copyright.text-color'
            ],
            'styles.expanded' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.expanded.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.expanded.background'
            ],
            'styles.expanded.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.expanded.text-color'
            ],
            'styles.extension' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.extension.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.extension.background'
            ],
            'styles.extension.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.extension.text-color'
            ],
            'styles.feature' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.feature.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.feature.background'
            ],
            'styles.feature.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.feature.text-color'
            ],
            'styles.font' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.font.family-default' => [
                'type' => 'input.fonts',
                'label' => 'Body Font',
                'default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-default'
            ],
            'styles.footer' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.footer.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#eaf6f4',
                'name' => 'styles.footer.background'
            ],
            'styles.footer.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#50818b',
                'name' => 'styles.footer.text-color'
            ],
            'styles.header' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.header.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.header.background'
            ],
            'styles.header.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.header.text-color'
            ],
            'styles.main' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.main.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.main.background'
            ],
            'styles.main.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.main.text-color'
            ],
            'styles.menu' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.menu.animation' => [
                'type' => 'select.selectize',
                'label' => 'Dropdown Animation',
                'description' => 'Select the dropdown animation.',
                'default' => 'g-fade',
                'options' => [
                    'g-no-animation' => 'No Animation',
                    'g-fade' => 'Fade',
                    'g-zoom' => 'Zoom',
                    'g-fade-in-up' => 'Fade In Up',
                    'g-dropdown-bounce-in-down' => 'Bounce In Down',
                    'g-dropdown-bounce-in-left' => 'Bounce In Left',
                    'g-dropdown-bounce-in-right' => 'Bounce In Right'
                ],
                'name' => 'styles.menu.animation'
            ],
            'styles.menustyle' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.menustyle.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.menustyle.text-color'
            ],
            'styles.menustyle.text-color-alt' => [
                'type' => 'input.colorpicker',
                'label' => 'Text Alt',
                'default' => '#8e9dab',
                'name' => 'styles.menustyle.text-color-alt'
            ],
            'styles.menustyle.text-color-active' => [
                'type' => 'input.colorpicker',
                'label' => 'Hover & Active Text',
                'default' => '#ffffff',
                'name' => 'styles.menustyle.text-color-active'
            ],
            'styles.menustyle.background-active' => [
                'type' => 'input.colorpicker',
                'label' => 'Active Background',
                'default' => 'rgba(255,255,255, 0)',
                'name' => 'styles.menustyle.background-active'
            ],
            'styles.menustyle._tab_toplevel' => [
                'label' => 'Top Level',
                'name' => 'styles.menustyle._tab_toplevel'
            ],
            'styles.menustyle.sublevel-text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#000000',
                'name' => 'styles.menustyle.sublevel-text-color'
            ],
            'styles.menustyle.sublevel-text-color-active' => [
                'type' => 'input.colorpicker',
                'label' => 'Hover & Active Text',
                'default' => '#31a594',
                'name' => 'styles.menustyle.sublevel-text-color-active'
            ],
            'styles.menustyle.sublevel-background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.menustyle.sublevel-background'
            ],
            'styles.menustyle.sublevel-background-active' => [
                'type' => 'input.colorpicker',
                'label' => 'Hover & Active Bg',
                'default' => '#31a594',
                'name' => 'styles.menustyle.sublevel-background-active'
            ],
            'styles.menustyle._tab_sublevel' => [
                'label' => 'Sub Level',
                'name' => 'styles.menustyle._tab_sublevel'
            ],
            'styles.navigation' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.navigation.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#0d0d0d',
                'name' => 'styles.navigation.background'
            ],
            'styles.navigation.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.navigation.text-color'
            ],
            'styles.offcanvas' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.offcanvas.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ff4b64',
                'name' => 'styles.offcanvas.background'
            ],
            'styles.offcanvas.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.offcanvas.text-color'
            ],
            'styles.offcanvas.toggle-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Toggle Color',
                'default' => '#ffffff',
                'name' => 'styles.offcanvas.toggle-color'
            ],
            'styles.offcanvas.width' => [
                'type' => 'input.text',
                'label' => 'Panel Width',
                'description' => 'Set offcanvas size in rem, em, px, or percentage unit values',
                'default' => '10rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.offcanvas.width'
            ],
            'styles.offcanvas.toggle-visibility' => [
                'type' => 'select.selectize',
                'label' => 'Toggle Visibility',
                'description' => 'Choose the OffCanvas Toggle Visibility.',
                'default' => 1,
                'options' => [
                    1 => 'Mobile Menu',
                    2 => 'Always'
                ],
                'name' => 'styles.offcanvas.toggle-visibility'
            ],
            'styles.showcase' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.showcase.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.showcase.background'
            ],
            'styles.showcase.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.showcase.text-color'
            ],
            'styles.slideshow' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.slideshow.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#0d0d0d',
                'name' => 'styles.slideshow.background'
            ],
            'styles.slideshow.background-image' => [
                'type' => 'input.imagepicker',
                'label' => 'Background Image',
                'default' => 'gantry-media://backgrounds/slideshow/img-01.jpg',
                'name' => 'styles.slideshow.background-image'
            ],
            'styles.slideshow.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.slideshow.text-color'
            ],
            'styles.top' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.top.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#0d0d0d',
                'name' => 'styles.top.background'
            ],
            'styles.top.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.top.text-color'
            ],
            'styles.utility' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'styles.utility.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.utility.background'
            ],
            'styles.utility.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#666666',
                'name' => 'styles.utility.text-color'
            ],
            'page.assets' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'page' => [
                'type' => '_parent',
                'name' => 'page',
                'form_field' => false
            ],
            'page.assets.favicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Favicon',
                'filter' => '.(jpe?g|gif|png|svg|ico)$',
                'name' => 'page.assets.favicon'
            ],
            'page.assets.touchicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Touch Icon',
                'description' => 'A PNG only image that will be used as icon for Touch Devices. Recommended 180x180 or 192x192.',
                'filter' => '.png$',
                'name' => 'page.assets.touchicon'
            ],
            'page.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.css'
            ],
            'page.assets.css.*' => [
                'type' => '_parent',
                'name' => 'page.assets.css.*',
                'form_field' => false
            ],
            'page.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.css.*.name'
            ],
            'page.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.css.*.location'
            ],
            'page.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'page.assets.css.*.inline'
            ],
            'page.assets.css.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'rel',
                    1 => 'href',
                    2 => 'type'
                ],
                'name' => 'page.assets.css.*.extra'
            ],
            'page.assets.css.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'page.assets.css.*.priority'
            ],
            'page.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.javascript'
            ],
            'page.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'page.assets.javascript.*',
                'form_field' => false
            ],
            'page.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.javascript.*.name'
            ],
            'page.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.javascript.*.location'
            ],
            'page.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'page.assets.javascript.*.inline'
            ],
            'page.assets.javascript.*.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'page.assets.javascript.*.in_footer'
            ],
            'page.assets.javascript.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'page.assets.javascript.*.extra'
            ],
            'page.assets.javascript.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'page.assets.javascript.*.priority'
            ],
            'page.body' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'page.body.attribs' => [
                'type' => '_parent',
                'name' => 'page.body.attribs',
                'form_field' => false
            ],
            'page.body.attribs.id' => [
                'type' => 'input.text',
                'label' => 'Body Id',
                'default' => NULL,
                'name' => 'page.body.attribs.id'
            ],
            'page.body.attribs.class' => [
                'type' => 'input.selectize',
                'label' => 'Body Classes',
                'default' => 'gantry',
                'name' => 'page.body.attribs.class'
            ],
            'page.body.attribs.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag Attributes',
                'description' => 'Extra Tag attributes.',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'id',
                    1 => 'class'
                ],
                'name' => 'page.body.attribs.extra'
            ],
            'page.body.layout' => [
                'type' => '_parent',
                'name' => 'page.body.layout',
                'form_field' => false
            ],
            'page.body.layout.sections' => [
                'type' => 'select.selectize',
                'label' => 'Sections Layout',
                'description' => 'Default layout container behavior for Sections',
                'default' => 0,
                'options' => [
                    0 => 'Fullwidth (Boxed Content)',
                    2 => 'Fullwidth (Flushed Content)',
                    1 => 'Boxed',
                    3 => 'Remove Container'
                ],
                'name' => 'page.body.layout.sections'
            ],
            'page.body.body_top' => [
                'type' => 'textarea.textarea',
                'label' => 'After <body>',
                'description' => 'Anything in this field will be appended right after the opening body tag',
                'name' => 'page.body.body_top'
            ],
            'page.body.body_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Before </body>',
                'description' => 'Anything in this field will be appended right before the closing body tag',
                'name' => 'page.body.body_bottom'
            ],
            'page.head' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'page.head.meta' => [
                'type' => 'collection.keyvalue',
                'label' => 'Meta Tags',
                'description' => 'Meta Tags for extras such as Facebook and Twitter.',
                'key_placeholder' => 'og:title, og:site_name, twitter:site',
                'value_placeholder' => 'Value',
                'default' => NULL,
                'name' => 'page.head.meta'
            ],
            'page.head.head_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom Content',
                'description' => 'Anything in this field will be appended to the head tag',
                'name' => 'page.head.head_bottom'
            ],
            'page.head.atoms' => [
                'type' => 'input.hidden',
                'override_target' => '#atoms .atoms-list + input[type="checkbox"]',
                'array' => true,
                'name' => 'page.head.atoms'
            ],
            'pages.blog_item' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'validation' => 'loose'
                ]
            ],
            'pages' => [
                'type' => '_parent',
                'name' => 'pages',
                'form_field' => false
            ],
            'pages.blog_item.xss_check' => [
                'type' => 'xss',
                'name' => 'pages.blog_item.xss_check',
                'validation' => 'loose'
            ],
            'pages.blog_item.header' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header',
                'form_field' => false
            ],
            'pages.blog_item.header.title' => [
                'type' => 'text',
                'autofocus' => true,
                'style' => 'vertical',
                'label' => 'PLUGIN_ADMIN.TITLE',
                'name' => 'pages.blog_item.header.title',
                'validation' => 'loose'
            ],
            'pages.blog_item.content' => [
                'type' => 'tab',
                'name' => 'pages.blog_item.content',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.media_order' => [
                'type' => 'pagemedia',
                'label' => 'PLUGIN_ADMIN.PAGE_MEDIA',
                'name' => 'pages.blog_item.header.media_order',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.published' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.PUBLISHED',
                'highlight' => 1,
                'size' => 'medium',
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.published',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.DATE',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.date',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.publish_date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.PUBLISHED_DATE',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.publish_date',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.unpublish_date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.UNPUBLISHED_DATE',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.unpublish_date',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.metadata' => [
                'toggleable' => true,
                'type' => 'array',
                'label' => 'PLUGIN_ADMIN.METADATA',
                'placeholder_key' => 'PLUGIN_ADMIN.METADATA_KEY',
                'placeholder_value' => 'PLUGIN_ADMIN.METADATA_VALUE',
                'name' => 'pages.blog_item.header.metadata',
                'validation' => 'loose'
            ],
            'pages.blog_item.publishing' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.publishing',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.taxonomy' => [
                'type' => 'taxonomy',
                'label' => 'PLUGIN_ADMIN.TAXONOMY',
                'multiple' => true,
                'validate' => [
                    'type' => 'array'
                ],
                'name' => 'pages.blog_item.header.taxonomy',
                'validation' => 'loose'
            ],
            'pages.blog_item.taxonomies' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.taxonomies',
                'validation' => 'loose'
            ],
            'pages.blog_item.options' => [
                'type' => 'tab',
                'name' => 'pages.blog_item.options',
                'validation' => 'loose'
            ],
            'pages.blog_item.settings' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.settings',
                'validation' => 'loose'
            ],
            'pages.blog_item.folder' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.FOLDER_NAME',
                'validate' => [
                    'rule' => 'slug',
                    'pattern' => '[a-zA-Zа-яA-Я0-9_\\-]+',
                    'min' => 1,
                    'max' => 200
                ],
                'name' => 'pages.blog_item.folder',
                'validation' => 'loose'
            ],
            'pages.blog_item.route' => [
                'type' => 'parents',
                'label' => 'PLUGIN_ADMIN.PARENT',
                'classes' => 'fancy',
                'name' => 'pages.blog_item.route',
                'validation' => 'loose'
            ],
            'pages.blog_item.name' => [
                'type' => 'select',
                'classes' => 'fancy',
                'label' => 'PLUGIN_ADMIN.PAGE_FILE',
                'default' => 'default',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::pageTypes',
                'name' => 'pages.blog_item.name',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.body_classes' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.BODY_CLASSES',
                'name' => 'pages.blog_item.header.body_classes',
                'validation' => 'loose'
            ],
            'pages.blog_item.column1' => [
                'type' => 'column',
                'name' => 'pages.blog_item.column1',
                'validation' => 'loose'
            ],
            'pages.blog_item.order_title' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.order_title',
                'validation' => 'loose'
            ],
            'pages.blog_item.ordering' => [
                'type' => 'toggle',
                'label' => 'PLUGIN_ADMIN.FOLDER_NUMERIC_PREFIX',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.ordering',
                'validation' => 'loose'
            ],
            'pages.blog_item.order' => [
                'type' => 'order',
                'label' => 'PLUGIN_ADMIN.SORTABLE_PAGES',
                'sitemap' => NULL,
                'name' => 'pages.blog_item.order',
                'validation' => 'loose'
            ],
            'pages.blog_item.column2' => [
                'type' => 'column',
                'name' => 'pages.blog_item.column2',
                'validation' => 'loose'
            ],
            'pages.blog_item.columns' => [
                'type' => 'columns',
                'name' => 'pages.blog_item.columns',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.dateformat' => [
                'toggleable' => true,
                'type' => 'select',
                'size' => 'medium',
                'selectize' => [
                    'create' => true
                ],
                'label' => 'PLUGIN_ADMIN.DEFAULT_DATE_FORMAT',
                'data-options@' => '\\Grav\\Common\\Utils::dateFormats',
                'validate' => [
                    'type' => 'string'
                ],
                'name' => 'pages.blog_item.header.dateformat',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.menu' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.MENU',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.menu',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.slug' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.SLUG',
                'toggleable' => true,
                'validate' => [
                    'message' => 'PLUGIN_ADMIN.SLUG_VALIDATE_MESSAGE',
                    'rule' => 'slug',
                    'pattern' => '[a-zA-Zа-яA-Я0-9_\\-]+',
                    'min' => 1,
                    'max' => 200
                ],
                'name' => 'pages.blog_item.header.slug',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.redirect' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.REDIRECT',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.redirect',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.process' => [
                'type' => 'checkboxes',
                'label' => 'PLUGIN_ADMIN.PROCESS',
                'toggleable' => true,
                'config-default@' => 'system.pages.process',
                'default' => [
                    'markdown' => true,
                    'twig' => false
                ],
                'options' => [
                    'markdown' => 'Markdown',
                    'twig' => 'Twig'
                ],
                'use' => 'keys',
                'name' => 'pages.blog_item.header.process',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.twig_first' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.TWIG_FIRST',
                'highlight' => 0,
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.twig_first',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.never_cache_twig' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.NEVER_CACHE_TWIG',
                'highlight' => 0,
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.never_cache_twig',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.child_type' => [
                'type' => 'select',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DEFAULT_CHILD_TYPE',
                'default' => 'default',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::types',
                'name' => 'pages.blog_item.header.child_type',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.routable' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTABLE',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.routable',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.cache_enable' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.CACHING',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.cache_enable',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.visible' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.VISIBLE',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.visible',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.debugger' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DEBUGGER',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.debugger',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.template' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DISPLAY_TEMPLATE',
                'name' => 'pages.blog_item.header.template',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.append_url_extension' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.APPEND_URL_EXT',
                'toggleable' => true,
                'name' => 'pages.blog_item.header.append_url_extension',
                'validation' => 'loose'
            ],
            'pages.blog_item.overrides' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.overrides',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.routes' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.routes',
                'form_field' => false
            ],
            'pages.blog_item.header.routes.default' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTE_DEFAULT',
                'name' => 'pages.blog_item.header.routes.default',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.routes.canonical' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTE_CANONICAL',
                'name' => 'pages.blog_item.header.routes.canonical',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.routes.aliases' => [
                'type' => 'array',
                'toggleable' => true,
                'value_only' => true,
                'size' => 'large',
                'label' => 'PLUGIN_ADMIN.ROUTE_ALIASES',
                'name' => 'pages.blog_item.header.routes.aliases',
                'validation' => 'loose'
            ],
            'pages.blog_item.routes_only' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.routes_only',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.admin' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.admin',
                'form_field' => false
            ],
            'pages.blog_item.header.admin.children_display_order' => [
                'type' => 'select',
                'label' => 'PLUGIN_ADMIN.ADMIN_CHILDREN_DISPLAY_ORDER',
                'toggleable' => true,
                'classes' => 'fancy',
                'default' => 'collection',
                'options' => [
                    'default' => 'Ordered by Folder name (default)',
                    'collection' => 'Ordered by Collection definition'
                ],
                'name' => 'pages.blog_item.header.admin.children_display_order',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.order_by' => [
                'type' => 'hidden',
                'name' => 'pages.blog_item.header.order_by',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.order_manual' => [
                'type' => 'hidden',
                'validate' => [
                    'type' => 'commalist'
                ],
                'name' => 'pages.blog_item.header.order_manual',
                'validation' => 'loose'
            ],
            'pages.blog_item.blueprint' => [
                'type' => 'blueprint',
                'name' => 'pages.blog_item.blueprint',
                'validation' => 'loose'
            ],
            'pages.blog_item.admin_only' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.admin_only',
                'validation' => 'loose'
            ],
            'pages.blog_item.advanced' => [
                'type' => 'tab',
                'name' => 'pages.blog_item.advanced',
                'validation' => 'loose'
            ],
            'pages.blog_item.header_image' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.header_image',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.image',
                'form_field' => false
            ],
            'pages.blog_item.header.image.summary' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.image.summary',
                'form_field' => false
            ],
            'pages.blog_item.header.image.summary.enabled' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Display Summary Image',
                'highlight' => 1,
                'options' => [
                    0 => 'PLUGIN_ADMIN.DISABLED',
                    1 => 'PLUGIN_ADMIN.ENABLED'
                ],
                'name' => 'pages.blog_item.header.image.summary.enabled',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image.summary.file' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Summary Image File',
                'name' => 'pages.blog_item.header.image.summary.file',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image.text' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.image.text',
                'form_field' => false
            ],
            'pages.blog_item.header.image.text.enabled' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Display Blog Image',
                'highlight' => 1,
                'options' => [
                    0 => 'PLUGIN_ADMIN.DISABLED',
                    1 => 'PLUGIN_ADMIN.ENABLED'
                ],
                'name' => 'pages.blog_item.header.image.text.enabled',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image.text.file' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Blog Image File',
                'name' => 'pages.blog_item.header.image.text.file',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image.width' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Width',
                'size' => 'small',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'pages.blog_item.header.image.width',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.image.height' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Height',
                'size' => 'small',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'pages.blog_item.header.image.height',
                'validation' => 'loose'
            ],
            'pages.blog_item.summary' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.summary',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.summary' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.summary',
                'form_field' => false
            ],
            'pages.blog_item.header.summary.enabled' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Summary',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'name' => 'pages.blog_item.header.summary.enabled',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.summary.format' => [
                'type' => 'select',
                'toggleable' => true,
                'label' => 'Format',
                'classes' => 'fancy',
                'options' => [
                    'short' => 'Use the first occurence of delimter or size',
                    'long' => 'Summary delimiter will be ignored'
                ],
                'name' => 'pages.blog_item.header.summary.format',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.summary.size' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Size',
                'classes' => 'large',
                'validate' => [
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'pages.blog_item.header.summary.size',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.summary.delimiter' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Summary delimiter',
                'classes' => 'large',
                'name' => 'pages.blog_item.header.summary.delimiter',
                'validation' => 'loose'
            ],
            'pages.blog_item.readmore' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_item.readmore',
                'validation' => 'loose'
            ],
            'pages.blog_item.header.continue_link' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Read More',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_item.header.continue_link',
                'validation' => 'loose'
            ],
            'pages.blog_item.blog' => [
                'type' => 'tab',
                'name' => 'pages.blog_item.blog',
                'validation' => 'loose'
            ],
            'pages.blog_item.tabs' => [
                'type' => 'tabs',
                'active' => 1,
                'name' => 'pages.blog_item.tabs',
                'validation' => 'loose'
            ],
            'pages.blog_list' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    'validation' => 'loose'
                ]
            ],
            'pages.blog_list.xss_check' => [
                'type' => 'xss',
                'name' => 'pages.blog_list.xss_check',
                'validation' => 'loose'
            ],
            'pages.blog_list.header' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header',
                'form_field' => false
            ],
            'pages.blog_list.header.title' => [
                'type' => 'text',
                'autofocus' => true,
                'style' => 'vertical',
                'label' => 'PLUGIN_ADMIN.TITLE',
                'name' => 'pages.blog_list.header.title',
                'validation' => 'loose'
            ],
            'pages.blog_list.content' => [
                'type' => 'tab',
                'name' => 'pages.blog_list.content',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.media_order' => [
                'type' => 'pagemedia',
                'label' => 'PLUGIN_ADMIN.PAGE_MEDIA',
                'name' => 'pages.blog_list.header.media_order',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.published' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.PUBLISHED',
                'highlight' => 1,
                'size' => 'medium',
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.published',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.DATE',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.date',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.publish_date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.PUBLISHED_DATE',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.publish_date',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.unpublish_date' => [
                'type' => 'datetime',
                'label' => 'PLUGIN_ADMIN.UNPUBLISHED_DATE',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.unpublish_date',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.metadata' => [
                'toggleable' => true,
                'type' => 'array',
                'label' => 'PLUGIN_ADMIN.METADATA',
                'placeholder_key' => 'PLUGIN_ADMIN.METADATA_KEY',
                'placeholder_value' => 'PLUGIN_ADMIN.METADATA_VALUE',
                'name' => 'pages.blog_list.header.metadata',
                'validation' => 'loose'
            ],
            'pages.blog_list.publishing' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.publishing',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.taxonomy' => [
                'type' => 'taxonomy',
                'label' => 'PLUGIN_ADMIN.TAXONOMY',
                'multiple' => true,
                'validate' => [
                    'type' => 'array'
                ],
                'name' => 'pages.blog_list.header.taxonomy',
                'validation' => 'loose'
            ],
            'pages.blog_list.taxonomies' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.taxonomies',
                'validation' => 'loose'
            ],
            'pages.blog_list.options' => [
                'type' => 'tab',
                'name' => 'pages.blog_list.options',
                'validation' => 'loose'
            ],
            'pages.blog_list.settings' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.settings',
                'validation' => 'loose'
            ],
            'pages.blog_list.folder' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.FOLDER_NAME',
                'validate' => [
                    'rule' => 'slug',
                    'pattern' => '[a-z][a-z0-9_\\-]+',
                    'min' => 2,
                    'max' => 80
                ],
                'name' => 'pages.blog_list.folder',
                'validation' => 'loose'
            ],
            'pages.blog_list.route' => [
                'type' => 'parents',
                'label' => 'PLUGIN_ADMIN.PARENT',
                'classes' => 'fancy',
                'name' => 'pages.blog_list.route',
                'validation' => 'loose'
            ],
            'pages.blog_list.name' => [
                'type' => 'select',
                'classes' => 'fancy',
                'label' => 'PLUGIN_ADMIN.PAGE_FILE',
                'default' => 'default',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::pageTypes',
                'name' => 'pages.blog_list.name',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.body_classes' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.BODY_CLASSES',
                'name' => 'pages.blog_list.header.body_classes',
                'validation' => 'loose'
            ],
            'pages.blog_list.column1' => [
                'type' => 'column',
                'name' => 'pages.blog_list.column1',
                'validation' => 'loose'
            ],
            'pages.blog_list.order_title' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.order_title',
                'validation' => 'loose'
            ],
            'pages.blog_list.ordering' => [
                'type' => 'toggle',
                'label' => 'PLUGIN_ADMIN.FOLDER_NUMERIC_PREFIX',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.ordering',
                'validation' => 'loose'
            ],
            'pages.blog_list.order' => [
                'type' => 'order',
                'label' => 'PLUGIN_ADMIN.SORTABLE_PAGES',
                'sitemap' => NULL,
                'name' => 'pages.blog_list.order',
                'validation' => 'loose'
            ],
            'pages.blog_list.column2' => [
                'type' => 'column',
                'name' => 'pages.blog_list.column2',
                'validation' => 'loose'
            ],
            'pages.blog_list.columns' => [
                'type' => 'columns',
                'name' => 'pages.blog_list.columns',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.dateformat' => [
                'toggleable' => true,
                'type' => 'select',
                'size' => 'medium',
                'selectize' => [
                    'create' => true
                ],
                'label' => 'PLUGIN_ADMIN.DEFAULT_DATE_FORMAT',
                'data-options@' => '\\Grav\\Common\\Utils::dateFormats',
                'validate' => [
                    'type' => 'string'
                ],
                'name' => 'pages.blog_list.header.dateformat',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.menu' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.MENU',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.menu',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.slug' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.SLUG',
                'toggleable' => true,
                'validate' => [
                    'message' => 'PLUGIN_ADMIN.SLUG_VALIDATE_MESSAGE',
                    'rule' => 'slug',
                    'pattern' => '[a-z][a-z0-9_\\-]+',
                    'min' => 2,
                    'max' => 80
                ],
                'name' => 'pages.blog_list.header.slug',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.redirect' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.REDIRECT',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.redirect',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.process' => [
                'type' => 'checkboxes',
                'label' => 'PLUGIN_ADMIN.PROCESS',
                'toggleable' => true,
                'config-default@' => 'system.pages.process',
                'default' => [
                    'markdown' => true,
                    'twig' => false
                ],
                'options' => [
                    'markdown' => 'Markdown',
                    'twig' => 'Twig'
                ],
                'use' => 'keys',
                'name' => 'pages.blog_list.header.process',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.twig_first' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.TWIG_FIRST',
                'highlight' => 0,
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.twig_first',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.never_cache_twig' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.NEVER_CACHE_TWIG',
                'highlight' => 0,
                'options' => [
                    1 => 'PLUGIN_ADMIN.YES',
                    0 => 'PLUGIN_ADMIN.NO'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.never_cache_twig',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.child_type' => [
                'type' => 'select',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DEFAULT_CHILD_TYPE',
                'default' => 'blog_item',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::types',
                'name' => 'pages.blog_list.header.child_type',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.routable' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTABLE',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.routable',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.cache_enable' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.CACHING',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.cache_enable',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.visible' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.VISIBLE',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.visible',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.debugger' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DEBUGGER',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.debugger',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.template' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.DISPLAY_TEMPLATE',
                'name' => 'pages.blog_list.header.template',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.append_url_extension' => [
                'type' => 'text',
                'label' => 'PLUGIN_ADMIN.APPEND_URL_EXT',
                'toggleable' => true,
                'name' => 'pages.blog_list.header.append_url_extension',
                'validation' => 'loose'
            ],
            'pages.blog_list.overrides' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.overrides',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.routes' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.routes',
                'form_field' => false
            ],
            'pages.blog_list.header.routes.default' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTE_DEFAULT',
                'name' => 'pages.blog_list.header.routes.default',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.routes.canonical' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'PLUGIN_ADMIN.ROUTE_CANONICAL',
                'name' => 'pages.blog_list.header.routes.canonical',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.routes.aliases' => [
                'type' => 'array',
                'toggleable' => true,
                'value_only' => true,
                'size' => 'large',
                'label' => 'PLUGIN_ADMIN.ROUTE_ALIASES',
                'name' => 'pages.blog_list.header.routes.aliases',
                'validation' => 'loose'
            ],
            'pages.blog_list.routes_only' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.routes_only',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.admin' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.admin',
                'form_field' => false
            ],
            'pages.blog_list.header.admin.children_display_order' => [
                'type' => 'select',
                'label' => 'PLUGIN_ADMIN.ADMIN_CHILDREN_DISPLAY_ORDER',
                'toggleable' => true,
                'classes' => 'fancy',
                'default' => 'collection',
                'options' => [
                    'default' => 'Ordered by Folder name (default)',
                    'collection' => 'Ordered by Collection definition'
                ],
                'name' => 'pages.blog_list.header.admin.children_display_order',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.order_by' => [
                'type' => 'hidden',
                'name' => 'pages.blog_list.header.order_by',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.order_manual' => [
                'type' => 'hidden',
                'validate' => [
                    'type' => 'commalist'
                ],
                'name' => 'pages.blog_list.header.order_manual',
                'validation' => 'loose'
            ],
            'pages.blog_list.blueprint' => [
                'type' => 'blueprint',
                'name' => 'pages.blog_list.blueprint',
                'validation' => 'loose'
            ],
            'pages.blog_list.admin_only' => [
                'type' => 'section',
                'underline' => true,
                'name' => 'pages.blog_list.admin_only',
                'validation' => 'loose'
            ],
            'pages.blog_list.advanced' => [
                'type' => 'tab',
                'name' => 'pages.blog_list.advanced',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.content',
                'form_field' => false
            ],
            'pages.blog_list.header.content.items' => [
                'type' => 'textarea',
                'yaml' => true,
                'label' => 'Items',
                'default' => '@self.children',
                'name' => 'pages.blog_list.header.content.items',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.leading' => [
                'type' => 'text',
                'label' => 'Leading Articles',
                'default' => 0,
                'validate' => [
                    'type' => 'int',
                    'min' => 0
                ],
                'name' => 'pages.blog_list.header.content.leading',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.columns' => [
                'type' => 'text',
                'label' => 'Columns',
                'default' => 2,
                'validate' => [
                    'type' => 'int',
                    'min' => 0
                ],
                'name' => 'pages.blog_list.header.content.columns',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.limit' => [
                'type' => 'text',
                'label' => 'Max Item Count',
                'default' => 5,
                'validate' => [
                    'required' => true,
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'pages.blog_list.header.content.limit',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.order' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.content.order',
                'form_field' => false
            ],
            'pages.blog_list.header.content.order.by' => [
                'type' => 'select',
                'label' => 'Order By',
                'default' => 'date',
                'options' => [
                    'folder' => 'Folder',
                    'title' => 'Title',
                    'date' => 'Date',
                    'default' => 'Default'
                ],
                'name' => 'pages.blog_list.header.content.order.by',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.order.dir' => [
                'type' => 'select',
                'label' => 'Order',
                'default' => 'desc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'name' => 'pages.blog_list.header.content.order.dir',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.show_date' => [
                'type' => 'toggle',
                'label' => 'Show Date',
                'highlight' => 0,
                'default' => 0,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.content.show_date',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.pagination' => [
                'type' => 'toggle',
                'label' => 'Pagination',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.content.pagination',
                'validation' => 'loose'
            ],
            'pages.blog_list.header.content.url_taxonomy_filters' => [
                'type' => 'toggle',
                'label' => 'URL Taxonomy Filters',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.content.url_taxonomy_filters',
                'validation' => 'loose'
            ],
            'pages.blog_list.blog' => [
                'type' => 'tab',
                'name' => 'pages.blog_list.blog',
                'validation' => 'loose'
            ],
            'pages.blog_list.tabs' => [
                'type' => 'tabs',
                'active' => 1,
                'name' => 'pages.blog_list.tabs',
                'validation' => 'loose'
            ],
            'pages.form' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'pages.modular.features' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'pages.modular' => [
                'type' => '_parent',
                'name' => 'pages.modular',
                'form_field' => false
            ],
            'pages.modular.features.name' => [
                'default' => 'modular/features',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.features.name'
            ],
            'pages.modular.features.column1' => [
                'name' => 'pages.modular.features.column1'
            ],
            'pages.modular.features.columns' => [
                'name' => 'pages.modular.features.columns'
            ],
            'pages.modular.features.header' => [
                'type' => '_parent',
                'name' => 'pages.modular.features.header',
                'form_field' => false
            ],
            'pages.modular.features.header.template' => [
                'default' => 'modular/features',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.features.header.template'
            ],
            'pages.modular.features.overrides' => [
                'name' => 'pages.modular.features.overrides'
            ],
            'pages.modular.features.advanced' => [
                'name' => 'pages.modular.features.advanced'
            ],
            'pages.modular.features.header.features' => [
                'name' => 'pages.modular.features.header.features',
                'type' => 'list',
                'label' => 'Features'
            ],
            'pages.modular.features.header.features.icon' => [
                'type' => 'text',
                'label' => 'Icon',
                'name' => 'pages.modular.features.header.features.icon'
            ],
            'pages.modular.features.header.features.header' => [
                'type' => 'text',
                'label' => 'Header',
                'name' => 'pages.modular.features.header.features.header'
            ],
            'pages.modular.features.header.features.text' => [
                'type' => 'text',
                'label' => 'Text',
                'name' => 'pages.modular.features.header.features.text'
            ],
            'pages.modular.features.features' => [
                'type' => 'tab',
                'name' => 'pages.modular.features.features'
            ],
            'pages.modular.features.tabs' => [
                'name' => 'pages.modular.features.tabs'
            ],
            'pages.modular.showcase' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'pages.modular.showcase.name' => [
                'default' => 'modular/showcase',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.showcase.name'
            ],
            'pages.modular.showcase.column1' => [
                'name' => 'pages.modular.showcase.column1'
            ],
            'pages.modular.showcase.columns' => [
                'name' => 'pages.modular.showcase.columns'
            ],
            'pages.modular.showcase.header' => [
                'type' => '_parent',
                'name' => 'pages.modular.showcase.header',
                'form_field' => false
            ],
            'pages.modular.showcase.header.template' => [
                'default' => 'modular/showcase',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.showcase.header.template'
            ],
            'pages.modular.showcase.overrides' => [
                'name' => 'pages.modular.showcase.overrides'
            ],
            'pages.modular.showcase.advanced' => [
                'name' => 'pages.modular.showcase.advanced'
            ],
            'pages.modular.showcase.header.buttons' => [
                'name' => 'pages.modular.showcase.header.buttons',
                'type' => 'list',
                'label' => 'Buttons'
            ],
            'pages.modular.showcase.header.buttons.text' => [
                'type' => 'text',
                'label' => 'Text',
                'name' => 'pages.modular.showcase.header.buttons.text'
            ],
            'pages.modular.showcase.header.buttons.url' => [
                'type' => 'text',
                'label' => 'URL',
                'name' => 'pages.modular.showcase.header.buttons.url'
            ],
            'pages.modular.showcase.header.buttons.primary' => [
                'type' => 'toggle',
                'label' => 'Primary',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'Yes',
                    0 => 'No'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.modular.showcase.header.buttons.primary'
            ],
            'pages.modular.showcase.buttons' => [
                'type' => 'tab',
                'name' => 'pages.modular.showcase.buttons'
            ],
            'pages.modular.showcase.tabs' => [
                'name' => 'pages.modular.showcase.tabs'
            ],
            'pages.modular.text' => [
                'type' => '_root',
                'form_field' => false,
                'form' => [
                    
                ]
            ],
            'pages.modular.text.name' => [
                'default' => 'modular/text',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.text.name'
            ],
            'pages.modular.text.column1' => [
                'name' => 'pages.modular.text.column1'
            ],
            'pages.modular.text.columns' => [
                'name' => 'pages.modular.text.columns'
            ],
            'pages.modular.text.header' => [
                'type' => '_parent',
                'name' => 'pages.modular.text.header',
                'form_field' => false
            ],
            'pages.modular.text.header.template' => [
                'default' => 'modular/text',
                'data-options@' => '\\Grav\\Common\\Page\\Pages::modularTypes',
                'name' => 'pages.modular.text.header.template'
            ],
            'pages.modular.text.overrides' => [
                'name' => 'pages.modular.text.overrides'
            ],
            'pages.modular.text.advanced' => [
                'name' => 'pages.modular.text.advanced'
            ],
            'pages.modular.text.uploads' => [
                'label' => 'Page Media (first one will be displayed next to your content)',
                'name' => 'pages.modular.text.uploads'
            ],
            'pages.modular.text.header.image_align' => [
                'type' => 'select',
                'label' => 'Image position',
                'classes' => 'fancy',
                'default' => 'left',
                'options' => [
                    'left' => 'Left',
                    'right' => 'Right'
                ],
                'name' => 'pages.modular.text.header.image_align'
            ],
            'pages.modular.text.content' => [
                'name' => 'pages.modular.text.content'
            ],
            'pages.modular.text.tabs' => [
                'name' => 'pages.modular.text.tabs'
            ]
        ],
        'rules' => [
            'slug' => [
                'pattern' => '[a-z][a-z0-9_\\-]+',
                'min' => 2,
                'max' => 80
            ]
        ],
        'nested' => [
            'particles' => [
                'aos' => [
                    'enabled' => 'particles.aos.enabled',
                    'duration' => 'particles.aos.duration',
                    'once' => 'particles.aos.once',
                    'delay' => 'particles.aos.delay',
                    'easing' => 'particles.aos.easing',
                    'offset' => 'particles.aos.offset'
                ],
                'audioplayer' => [
                    'enabled' => 'particles.audioplayer.enabled',
                    'class' => 'particles.audioplayer.class',
                    'title' => 'particles.audioplayer.title',
                    'nowplaying' => 'particles.audioplayer.nowplaying',
                    'scrollbar' => 'particles.audioplayer.scrollbar',
                    'overflow' => 'particles.audioplayer.overflow',
                    'soundcloudclientid' => 'particles.audioplayer.soundcloudclientid',
                    'items' => [
                        '*' => [
                            'title' => 'particles.audioplayer.items.*.title',
                            'artist' => 'particles.audioplayer.items.*.artist',
                            'tracktitle' => 'particles.audioplayer.items.*.tracktitle',
                            'cover' => 'particles.audioplayer.items.*.cover',
                            'source' => 'particles.audioplayer.items.*.source',
                            'externalurl' => 'particles.audioplayer.items.*.externalurl',
                            'localurl' => 'particles.audioplayer.items.*.localurl',
                            'soundcloudurl' => 'particles.audioplayer.items.*.soundcloudurl',
                            'soundcloudartist' => 'particles.audioplayer.items.*.soundcloudartist',
                            'link' => 'particles.audioplayer.items.*.link',
                            'linktext' => 'particles.audioplayer.items.*.linktext'
                        ]
                    ]
                ],
                'blockcontent' => [
                    'enabled' => 'particles.blockcontent.enabled',
                    'class' => 'particles.blockcontent.class',
                    'title' => 'particles.blockcontent.title',
                    'icon' => 'particles.blockcontent.icon',
                    'image' => 'particles.blockcontent.image',
                    'headline' => 'particles.blockcontent.headline',
                    'description' => 'particles.blockcontent.description',
                    'linktext' => 'particles.blockcontent.linktext',
                    'link' => 'particles.blockcontent.link',
                    'linkclass' => 'particles.blockcontent.linkclass',
                    'linktarget' => 'particles.blockcontent.linktarget',
                    'subcontents' => [
                        '*' => [
                            'name' => 'particles.blockcontent.subcontents.*.name',
                            'accent' => 'particles.blockcontent.subcontents.*.accent',
                            'icon' => 'particles.blockcontent.subcontents.*.icon',
                            'img' => 'particles.blockcontent.subcontents.*.img',
                            'rokboximage' => 'particles.blockcontent.subcontents.*.rokboximage',
                            'rokboxcaption' => 'particles.blockcontent.subcontents.*.rokboxcaption',
                            'subtitle' => 'particles.blockcontent.subcontents.*.subtitle',
                            'description' => 'particles.blockcontent.subcontents.*.description',
                            'class' => 'particles.blockcontent.subcontents.*.class',
                            'button' => 'particles.blockcontent.subcontents.*.button',
                            'buttonlink' => 'particles.blockcontent.subcontents.*.buttonlink',
                            'buttonclass' => 'particles.blockcontent.subcontents.*.buttonclass',
                            'buttontarget' => 'particles.blockcontent.subcontents.*.buttontarget'
                        ]
                    ]
                ],
                'calendar' => [
                    'enabled' => 'particles.calendar.enabled',
                    'class' => 'particles.calendar.class',
                    'title' => 'particles.calendar.title',
                    'eventsheader' => 'particles.calendar.eventsheader',
                    'events' => [
                        '*' => [
                            'title' => 'particles.calendar.events.*.title',
                            'begin' => 'particles.calendar.events.*.begin',
                            'end' => 'particles.calendar.events.*.end',
                            'description' => 'particles.calendar.events.*.description',
                            'link' => 'particles.calendar.events.*.link',
                            'target' => 'particles.calendar.events.*.target'
                        ]
                    ]
                ],
                'carousel' => [
                    'enabled' => 'particles.carousel.enabled',
                    '_tab_settings' => 'particles.carousel._tab_settings',
                    'source' => 'particles.carousel.source',
                    'class' => 'particles.carousel.class',
                    'title' => 'particles.carousel.title',
                    'description' => 'particles.carousel.description',
                    'animateOut' => 'particles.carousel.animateOut',
                    'animateIn' => 'particles.carousel.animateIn',
                    'nav' => 'particles.carousel.nav',
                    'loop' => 'particles.carousel.loop',
                    'autoplay' => 'particles.carousel.autoplay',
                    'autoplaySpeed' => 'particles.carousel.autoplaySpeed',
                    'pauseOnHover' => 'particles.carousel.pauseOnHover',
                    'displayitems' => 'particles.carousel.displayitems',
                    '_tab_collection' => 'particles.carousel._tab_collection',
                    'items' => [
                        '*' => [
                            'icon' => 'particles.carousel.items.*.icon',
                            'title' => 'particles.carousel.items.*.title',
                            'description' => 'particles.carousel.items.*.description',
                            'readmore' => 'particles.carousel.items.*.readmore',
                            'link' => 'particles.carousel.items.*.link',
                            'linktarget' => 'particles.carousel.items.*.linktarget'
                        ]
                    ],
                    '_tab_articles' => 'particles.carousel._tab_articles',
                    'article' => [
                        'filter' => [
                            'categories' => 'particles.carousel.article.filter.categories'
                        ],
                        'limit' => [
                            'total' => 'particles.carousel.article.limit.total',
                            'start' => 'particles.carousel.article.limit.start'
                        ],
                        'sort' => [
                            'orderby' => 'particles.carousel.article.sort.orderby',
                            'ordering' => 'particles.carousel.article.sort.ordering'
                        ],
                        'display' => [
                            'text' => [
                                'type' => 'particles.carousel.article.display.text.type',
                                'limit' => 'particles.carousel.article.display.text.limit',
                                'formatting' => 'particles.carousel.article.display.text.formatting'
                            ],
                            'title' => [
                                'enabled' => 'particles.carousel.article.display.title.enabled',
                                'limit' => 'particles.carousel.article.display.title.limit'
                            ],
                            'readmore' => 'particles.carousel.article.display.readmore'
                        ]
                    ],
                    '_tab_display' => 'particles.carousel._tab_display'
                ],
                'casestudies' => [
                    'enabled' => 'particles.casestudies.enabled',
                    'class' => 'particles.casestudies.class',
                    'title' => 'particles.casestudies.title',
                    'cols' => 'particles.casestudies.cols',
                    'cases' => [
                        '*' => [
                            'title' => 'particles.casestudies.cases.*.title',
                            'items' => [
                                '*' => [
                                    'image' => 'particles.casestudies.cases.*.items.*.image',
                                    'title' => 'particles.casestudies.cases.*.items.*.title',
                                    'subtitle' => 'particles.casestudies.cases.*.items.*.subtitle',
                                    'url' => 'particles.casestudies.cases.*.items.*.url',
                                    'target' => 'particles.casestudies.cases.*.items.*.target'
                                ]
                            ]
                        ]
                    ]
                ],
                'fixedheader' => [
                    'enabled' => 'particles.fixedheader.enabled',
                    'section' => 'particles.fixedheader.section',
                    'pinnedbg' => 'particles.fixedheader.pinnedbg',
                    'custombg' => 'particles.fixedheader.custombg',
                    'autohide' => 'particles.fixedheader.autohide',
                    'mobile' => 'particles.fixedheader.mobile',
                    'margin' => 'particles.fixedheader.margin',
                    'mobilemargin' => 'particles.fixedheader.mobilemargin',
                    'offset' => 'particles.fixedheader.offset',
                    'tolerance' => 'particles.fixedheader.tolerance'
                ],
                'gridcontent' => [
                    'enabled' => 'particles.gridcontent.enabled',
                    'class' => 'particles.gridcontent.class',
                    'title' => 'particles.gridcontent.title',
                    'desc' => 'particles.gridcontent.desc',
                    'readmore' => 'particles.gridcontent.readmore',
                    'readmorelink' => 'particles.gridcontent.readmorelink',
                    'readmoreclass' => 'particles.gridcontent.readmoreclass',
                    'cols' => 'particles.gridcontent.cols',
                    'gridcontentitems' => [
                        '*' => [
                            'icon' => 'particles.gridcontent.gridcontentitems.*.icon',
                            'iconcolor' => 'particles.gridcontent.gridcontentitems.*.iconcolor',
                            'desc' => 'particles.gridcontent.gridcontentitems.*.desc',
                            'link' => 'particles.gridcontent.gridcontentitems.*.link'
                        ]
                    ]
                ],
                'gridstatistic' => [
                    'enabled' => 'particles.gridstatistic.enabled',
                    'class' => 'particles.gridstatistic.class',
                    'title' => 'particles.gridstatistic.title',
                    'desc' => 'particles.gridstatistic.desc',
                    'readmore' => 'particles.gridstatistic.readmore',
                    'readmorelink' => 'particles.gridstatistic.readmorelink',
                    'readmoreclass' => 'particles.gridstatistic.readmoreclass',
                    'footerdesc' => 'particles.gridstatistic.footerdesc',
                    'cols' => 'particles.gridstatistic.cols',
                    'gridstatisticitems' => [
                        '*' => [
                            'text1' => 'particles.gridstatistic.gridstatisticitems.*.text1',
                            'icon' => 'particles.gridstatistic.gridstatisticitems.*.icon',
                            'text2' => 'particles.gridstatistic.gridstatisticitems.*.text2'
                        ]
                    ]
                ],
                'imagegrid' => [
                    'enabled' => 'particles.imagegrid.enabled',
                    'class' => 'particles.imagegrid.class',
                    'title' => 'particles.imagegrid.title',
                    'desc' => 'particles.imagegrid.desc',
                    'cols' => 'particles.imagegrid.cols',
                    'layout' => 'particles.imagegrid.layout',
                    'album' => 'particles.imagegrid.album',
                    'imagegriditems' => [
                        '*' => [
                            'image' => 'particles.imagegrid.imagegriditems.*.image',
                            'caption' => 'particles.imagegrid.imagegriditems.*.caption'
                        ]
                    ]
                ],
                'infolist' => [
                    'enabled' => 'particles.infolist.enabled',
                    'class' => 'particles.infolist.class',
                    'title' => 'particles.infolist.title',
                    'intro' => 'particles.infolist.intro',
                    'cols' => 'particles.infolist.cols',
                    'infolists' => [
                        '*' => [
                            'title' => 'particles.infolist.infolists.*.title',
                            'icon' => 'particles.infolist.infolists.*.icon',
                            'iconloc' => 'particles.infolist.infolists.*.iconloc',
                            'image' => 'particles.infolist.infolists.*.image',
                            'imageloc' => 'particles.infolist.infolists.*.imageloc',
                            'textstyle' => 'particles.infolist.infolists.*.textstyle',
                            'imagestyle' => 'particles.infolist.infolists.*.imagestyle',
                            'desc' => 'particles.infolist.infolists.*.desc',
                            'tag' => 'particles.infolist.infolists.*.tag',
                            'subtag' => 'particles.infolist.infolists.*.subtag',
                            'label' => 'particles.infolist.infolists.*.label',
                            'link' => 'particles.infolist.infolists.*.link',
                            'buttonicon' => 'particles.infolist.infolists.*.buttonicon',
                            'readmoreclass' => 'particles.infolist.infolists.*.readmoreclass',
                            'readmoretarget' => 'particles.infolist.infolists.*.readmoretarget'
                        ]
                    ]
                ],
                'newsletter' => [
                    'enabled' => 'particles.newsletter.enabled',
                    'class' => 'particles.newsletter.class',
                    'width' => 'particles.newsletter.width',
                    'layout' => 'particles.newsletter.layout',
                    'style' => 'particles.newsletter.style',
                    'title' => 'particles.newsletter.title',
                    'headtext' => 'particles.newsletter.headtext',
                    'sidetext' => 'particles.newsletter.sidetext',
                    'inputboxtext' => 'particles.newsletter.inputboxtext',
                    'buttonicon' => 'particles.newsletter.buttonicon',
                    'buttontext' => 'particles.newsletter.buttontext',
                    'uri' => 'particles.newsletter.uri',
                    'buttonclass' => 'particles.newsletter.buttonclass'
                ],
                'panelslider' => [
                    'enabled' => 'particles.panelslider.enabled',
                    '_tab_settings' => 'particles.panelslider._tab_settings',
                    'source' => 'particles.panelslider.source',
                    'displayitems' => 'particles.panelslider.displayitems',
                    'nav' => 'particles.panelslider.nav',
                    '_tab_collection' => 'particles.panelslider._tab_collection',
                    'items' => [
                        '*' => [
                            'title' => 'particles.panelslider.items.*.title',
                            'image' => 'particles.panelslider.items.*.image',
                            'topline' => 'particles.panelslider.items.*.topline',
                            'description' => 'particles.panelslider.items.*.description',
                            'link' => 'particles.panelslider.items.*.link',
                            'linklabel' => 'particles.panelslider.items.*.linklabel',
                            'linktarget' => 'particles.panelslider.items.*.linktarget'
                        ]
                    ],
                    '_tab_articles' => 'particles.panelslider._tab_articles',
                    'article' => [
                        'filter' => [
                            'categories' => 'particles.panelslider.article.filter.categories'
                        ],
                        'limit' => [
                            'total' => 'particles.panelslider.article.limit.total',
                            'start' => 'particles.panelslider.article.limit.start'
                        ],
                        'sort' => [
                            'orderby' => 'particles.panelslider.article.sort.orderby',
                            'ordering' => 'particles.panelslider.article.sort.ordering'
                        ],
                        'display' => [
                            'image' => [
                                'enabled' => 'particles.panelslider.article.display.image.enabled'
                            ],
                            'text' => [
                                'type' => 'particles.panelslider.article.display.text.type',
                                'limit' => 'particles.panelslider.article.display.text.limit',
                                'formatting' => 'particles.panelslider.article.display.text.formatting'
                            ],
                            'title' => [
                                'enabled' => 'particles.panelslider.article.display.title.enabled',
                                'limit' => 'particles.panelslider.article.display.title.limit'
                            ],
                            'date' => [
                                'enabled' => 'particles.panelslider.article.display.date.enabled',
                                'format' => 'particles.panelslider.article.display.date.format'
                            ],
                            'read_more' => [
                                'enabled' => 'particles.panelslider.article.display.read_more.enabled',
                                'label' => 'particles.panelslider.article.display.read_more.label',
                                'css' => 'particles.panelslider.article.display.read_more.css'
                            ]
                        ]
                    ],
                    '_tab_display' => 'particles.panelslider._tab_display'
                ],
                'pricingtable' => [
                    'enabled' => 'particles.pricingtable.enabled',
                    'class' => 'particles.pricingtable.class',
                    'title' => 'particles.pricingtable.title',
                    'headertext' => 'particles.pricingtable.headertext',
                    'footertext' => 'particles.pricingtable.footertext',
                    'columns' => 'particles.pricingtable.columns',
                    'tables' => [
                        '*' => [
                            'class' => 'particles.pricingtable.tables.*.class',
                            'highlight' => 'particles.pricingtable.tables.*.highlight',
                            'ribbon' => 'particles.pricingtable.tables.*.ribbon',
                            'icon' => 'particles.pricingtable.tables.*.icon',
                            'plan' => 'particles.pricingtable.tables.*.plan',
                            'price' => 'particles.pricingtable.tables.*.price',
                            'period' => 'particles.pricingtable.tables.*.period',
                            'desc' => 'particles.pricingtable.tables.*.desc',
                            'buttontext' => 'particles.pricingtable.tables.*.buttontext',
                            'buttonlink' => 'particles.pricingtable.tables.*.buttonlink',
                            'buttontarget' => 'particles.pricingtable.tables.*.buttontarget',
                            'buttonclass' => 'particles.pricingtable.tables.*.buttonclass',
                            'items' => [
                                '*' => [
                                    'text' => 'particles.pricingtable.tables.*.items.*.text',
                                    'class' => 'particles.pricingtable.tables.*.items.*.class'
                                ]
                            ]
                        ]
                    ]
                ],
                'search' => [
                    'enabled' => 'particles.search.enabled',
                    'title' => 'particles.search.title',
                    'placeholder' => 'particles.search.placeholder'
                ],
                'simplecontent' => [
                    'enabled' => 'particles.simplecontent.enabled',
                    'class' => 'particles.simplecontent.class',
                    'title' => 'particles.simplecontent.title',
                    'items' => [
                        '*' => [
                            'layout' => 'particles.simplecontent.items.*.layout',
                            'created_date' => 'particles.simplecontent.items.*.created_date',
                            'content_title' => 'particles.simplecontent.items.*.content_title',
                            'author' => 'particles.simplecontent.items.*.author',
                            'leading_content' => 'particles.simplecontent.items.*.leading_content',
                            'main_content' => 'particles.simplecontent.items.*.main_content',
                            'readmore_label' => 'particles.simplecontent.items.*.readmore_label',
                            'readmore_link' => 'particles.simplecontent.items.*.readmore_link',
                            'readmore_class' => 'particles.simplecontent.items.*.readmore_class',
                            'readmore_target' => 'particles.simplecontent.items.*.readmore_target'
                        ]
                    ]
                ],
                'simplecounter' => [
                    'enabled' => 'particles.simplecounter.enabled',
                    'class' => 'particles.simplecounter.class',
                    'title' => 'particles.simplecounter.title',
                    'desc' => 'particles.simplecounter.desc',
                    'date' => 'particles.simplecounter.date',
                    'month' => 'particles.simplecounter.month',
                    'year' => 'particles.simplecounter.year',
                    'daytext' => 'particles.simplecounter.daytext',
                    'daystext' => 'particles.simplecounter.daystext',
                    'hourtext' => 'particles.simplecounter.hourtext',
                    'hourstext' => 'particles.simplecounter.hourstext',
                    'minutetext' => 'particles.simplecounter.minutetext',
                    'minutestext' => 'particles.simplecounter.minutestext',
                    'secondtext' => 'particles.simplecounter.secondtext',
                    'secondstext' => 'particles.simplecounter.secondstext'
                ],
                'simplemenu' => [
                    'enabled' => 'particles.simplemenu.enabled',
                    'title' => 'particles.simplemenu.title',
                    'class' => 'particles.simplemenu.class',
                    'menus' => [
                        '*' => [
                            'title' => 'particles.simplemenu.menus.*.title',
                            'items' => [
                                '*' => [
                                    'title' => 'particles.simplemenu.menus.*.items.*.title',
                                    'icon' => 'particles.simplemenu.menus.*.items.*.icon',
                                    'link' => 'particles.simplemenu.menus.*.items.*.link',
                                    'target' => 'particles.simplemenu.menus.*.items.*.target'
                                ]
                            ]
                        ]
                    ]
                ],
                'testimonials' => [
                    'enabled' => 'particles.testimonials.enabled',
                    'class' => 'particles.testimonials.class',
                    'title' => 'particles.testimonials.title',
                    'animateOut' => 'particles.testimonials.animateOut',
                    'animateIn' => 'particles.testimonials.animateIn',
                    'nav' => 'particles.testimonials.nav',
                    'dots' => 'particles.testimonials.dots',
                    'loop' => 'particles.testimonials.loop',
                    'autoplay' => 'particles.testimonials.autoplay',
                    'autoplaySpeed' => 'particles.testimonials.autoplaySpeed',
                    'pauseOnHover' => 'particles.testimonials.pauseOnHover',
                    'items' => [
                        '*' => [
                            'image' => 'particles.testimonials.items.*.image',
                            'author' => 'particles.testimonials.items.*.author',
                            'position' => 'particles.testimonials.items.*.position',
                            'testimonial' => 'particles.testimonials.items.*.testimonial'
                        ]
                    ]
                ],
                'verticalslider' => [
                    'enabled' => 'particles.verticalslider.enabled',
                    '_tab_settings' => 'particles.verticalslider._tab_settings',
                    'source' => 'particles.verticalslider.source',
                    'presets' => 'particles.verticalslider.presets',
                    'speed' => 'particles.verticalslider.speed',
                    'auto' => 'particles.verticalslider.auto',
                    'pause' => 'particles.verticalslider.pause',
                    'loop' => 'particles.verticalslider.loop',
                    'controls' => 'particles.verticalslider.controls',
                    'pager' => 'particles.verticalslider.pager',
                    'height' => 'particles.verticalslider.height',
                    'mobileheight' => 'particles.verticalslider.mobileheight',
                    '_tab_collection' => 'particles.verticalslider._tab_collection',
                    'items' => [
                        '*' => [
                            'image' => 'particles.verticalslider.items.*.image',
                            'small_title' => 'particles.verticalslider.items.*.small_title',
                            'title' => 'particles.verticalslider.items.*.title',
                            'desc' => 'particles.verticalslider.items.*.desc',
                            'buttontext' => 'particles.verticalslider.items.*.buttontext',
                            'buttonlink' => 'particles.verticalslider.items.*.buttonlink',
                            'buttontarget' => 'particles.verticalslider.items.*.buttontarget',
                            'buttonclass' => 'particles.verticalslider.items.*.buttonclass'
                        ]
                    ],
                    '_tab_articles' => 'particles.verticalslider._tab_articles',
                    'article' => [
                        'filter' => [
                            'categories' => 'particles.verticalslider.article.filter.categories'
                        ],
                        'limit' => [
                            'total' => 'particles.verticalslider.article.limit.total',
                            'start' => 'particles.verticalslider.article.limit.start'
                        ],
                        'sort' => [
                            'orderby' => 'particles.verticalslider.article.sort.orderby',
                            'ordering' => 'particles.verticalslider.article.sort.ordering'
                        ],
                        'display' => [
                            'image' => [
                                'enabled' => 'particles.verticalslider.article.display.image.enabled'
                            ],
                            'text' => [
                                'type' => 'particles.verticalslider.article.display.text.type',
                                'limit' => 'particles.verticalslider.article.display.text.limit',
                                'formatting' => 'particles.verticalslider.article.display.text.formatting'
                            ],
                            'title' => [
                                'enabled' => 'particles.verticalslider.article.display.title.enabled',
                                'limit' => 'particles.verticalslider.article.display.title.limit'
                            ],
                            'read_more' => [
                                'enabled' => 'particles.verticalslider.article.display.read_more.enabled',
                                'label' => 'particles.verticalslider.article.display.read_more.label',
                                'css' => 'particles.verticalslider.article.display.read_more.css'
                            ]
                        ]
                    ],
                    '_tab_display' => 'particles.verticalslider._tab_display'
                ],
                'video' => [
                    'enabled' => 'particles.video.enabled',
                    'class' => 'particles.video.class',
                    'title' => 'particles.video.title',
                    'columns' => 'particles.video.columns',
                    'items' => [
                        '*' => [
                            'caption' => 'particles.video.items.*.caption',
                            'source' => 'particles.video.items.*.source',
                            'video' => 'particles.video.items.*.video',
                            'local' => [
                                '*' => [
                                    'file' => 'particles.video.items.*.local.*.file'
                                ]
                            ],
                            'posterimage' => 'particles.video.items.*.posterimage',
                            'loop' => 'particles.video.items.*.loop',
                            'autoplay' => 'particles.video.items.*.autoplay',
                            'controls' => 'particles.video.items.*.controls',
                            'info' => 'particles.video.items.*.info',
                            'related' => 'particles.video.items.*.related',
                            'muted' => 'particles.video.items.*.muted',
                            'start' => 'particles.video.items.*.start'
                        ]
                    ]
                ],
                'analytics' => [
                    'enabled' => 'particles.analytics.enabled',
                    'ua' => [
                        'code' => 'particles.analytics.ua.code',
                        'anonym' => 'particles.analytics.ua.anonym',
                        'ssl' => 'particles.analytics.ua.ssl',
                        'debug' => 'particles.analytics.ua.debug'
                    ]
                ],
                'assets' => [
                    'enabled' => 'particles.assets.enabled',
                    'css' => [
                        '*' => [
                            'name' => 'particles.assets.css.*.name',
                            'location' => 'particles.assets.css.*.location',
                            'inline' => 'particles.assets.css.*.inline',
                            'extra' => 'particles.assets.css.*.extra',
                            'priority' => 'particles.assets.css.*.priority'
                        ]
                    ],
                    'javascript' => [
                        '*' => [
                            'name' => 'particles.assets.javascript.*.name',
                            'location' => 'particles.assets.javascript.*.location',
                            'inline' => 'particles.assets.javascript.*.inline',
                            'in_footer' => 'particles.assets.javascript.*.in_footer',
                            'extra' => 'particles.assets.javascript.*.extra',
                            'priority' => 'particles.assets.javascript.*.priority'
                        ]
                    ]
                ],
                'branding' => [
                    'enabled' => 'particles.branding.enabled',
                    'content' => 'particles.branding.content',
                    'css' => [
                        'class' => 'particles.branding.css.class'
                    ]
                ],
                'breadcrumbs' => [
                    'enabled' => 'particles.breadcrumbs.enabled'
                ],
                'content' => [
                    'enabled' => 'particles.content.enabled'
                ],
                'contentarray' => [
                    'enabled' => 'particles.contentarray.enabled',
                    '_tab_articles' => 'particles.contentarray._tab_articles',
                    'article' => [
                        'filter' => [
                            'categories' => 'particles.contentarray.article.filter.categories'
                        ],
                        'limit' => [
                            'total' => 'particles.contentarray.article.limit.total',
                            'columns' => 'particles.contentarray.article.limit.columns',
                            'start' => 'particles.contentarray.article.limit.start'
                        ],
                        'display' => [
                            'pagination_buttons' => 'particles.contentarray.article.display.pagination_buttons',
                            'image' => [
                                'enabled' => 'particles.contentarray.article.display.image.enabled'
                            ],
                            'text' => [
                                'type' => 'particles.contentarray.article.display.text.type',
                                'limit' => 'particles.contentarray.article.display.text.limit',
                                'formatting' => 'particles.contentarray.article.display.text.formatting'
                            ],
                            'title' => [
                                'enabled' => 'particles.contentarray.article.display.title.enabled',
                                'limit' => 'particles.contentarray.article.display.title.limit'
                            ],
                            'date' => [
                                'enabled' => 'particles.contentarray.article.display.date.enabled',
                                'format' => 'particles.contentarray.article.display.date.format'
                            ],
                            'read_more' => [
                                'enabled' => 'particles.contentarray.article.display.read_more.enabled',
                                'label' => 'particles.contentarray.article.display.read_more.label',
                                'css' => 'particles.contentarray.article.display.read_more.css'
                            ],
                            'author' => [
                                'enabled' => 'particles.contentarray.article.display.author.enabled'
                            ],
                            'category' => [
                                'enabled' => 'particles.contentarray.article.display.category.enabled',
                                'route' => 'particles.contentarray.article.display.category.route'
                            ]
                        ],
                        'sort' => [
                            'orderby' => 'particles.contentarray.article.sort.orderby',
                            'ordering' => 'particles.contentarray.article.sort.ordering'
                        ]
                    ],
                    '_tab_display' => 'particles.contentarray._tab_display',
                    '_tab_readmore' => 'particles.contentarray._tab_readmore',
                    '_tab_extras' => 'particles.contentarray._tab_extras',
                    'css' => [
                        'class' => 'particles.contentarray.css.class'
                    ],
                    'extra' => 'particles.contentarray.extra'
                ],
                'copyright' => [
                    'enabled' => 'particles.copyright.enabled',
                    'date' => [
                        'start' => 'particles.copyright.date.start',
                        'end' => 'particles.copyright.date.end'
                    ],
                    'owner' => 'particles.copyright.owner'
                ],
                'custom' => [
                    'enabled' => 'particles.custom.enabled',
                    'html' => 'particles.custom.html',
                    'twig' => 'particles.custom.twig',
                    'filter' => 'particles.custom.filter'
                ],
                'date' => [
                    'enabled' => 'particles.date.enabled',
                    'css' => [
                        'class' => 'particles.date.css.class'
                    ],
                    'date' => [
                        'formats' => 'particles.date.date.formats'
                    ]
                ],
                'feed' => [
                    'enabled' => 'particles.feed.enabled'
                ],
                'frameworks' => [
                    'enabled' => 'particles.frameworks.enabled',
                    'jquery' => [
                        'enabled' => 'particles.frameworks.jquery.enabled',
                        'ui_core' => 'particles.frameworks.jquery.ui_core',
                        'ui_sortable' => 'particles.frameworks.jquery.ui_sortable'
                    ],
                    'bootstrap2' => [
                        'enabled' => 'particles.frameworks.bootstrap2.enabled'
                    ],
                    'bootstrap3' => [
                        'enabled' => 'particles.frameworks.bootstrap3.enabled'
                    ],
                    'mootools' => [
                        'enabled' => 'particles.frameworks.mootools.enabled',
                        'more' => 'particles.frameworks.mootools.more'
                    ]
                ],
                'langswitcher' => [
                    'enabled' => 'particles.langswitcher.enabled'
                ],
                'lightcase' => [
                    'enabled' => 'particles.lightcase.enabled'
                ],
                'login' => [
                    'enabled' => 'particles.login.enabled'
                ],
                'logo' => [
                    'enabled' => 'particles.logo.enabled',
                    'url' => 'particles.logo.url',
                    'target' => 'particles.logo.target',
                    'image' => 'particles.logo.image',
                    'link' => 'particles.logo.link',
                    'svg' => 'particles.logo.svg',
                    'text' => 'particles.logo.text',
                    'class' => 'particles.logo.class'
                ],
                'menu' => [
                    'enabled' => 'particles.menu.enabled',
                    'menu' => 'particles.menu.menu',
                    'base' => 'particles.menu.base',
                    'startLevel' => 'particles.menu.startLevel',
                    'maxLevels' => 'particles.menu.maxLevels',
                    'renderTitles' => 'particles.menu.renderTitles',
                    'hoverExpand' => 'particles.menu.hoverExpand',
                    'mobileTarget' => 'particles.menu.mobileTarget',
                    'forceTarget' => 'particles.menu.forceTarget'
                ],
                'messages' => [
                    'enabled' => 'particles.messages.enabled'
                ],
                'mobile-menu' => [
                    'enabled' => 'particles.mobile-menu.enabled'
                ],
                'position' => [
                    'enabled' => 'particles.position.enabled',
                    'key' => 'particles.position.key',
                    'chrome' => 'particles.position.chrome'
                ],
                'social' => [
                    'enabled' => 'particles.social.enabled',
                    'css' => [
                        'class' => 'particles.social.css.class'
                    ],
                    'title' => 'particles.social.title',
                    'target' => 'particles.social.target',
                    'display' => 'particles.social.display',
                    'items' => [
                        '*' => [
                            'name' => 'particles.social.items.*.name',
                            'icon' => 'particles.social.items.*.icon',
                            'text' => 'particles.social.items.*.text',
                            'link' => 'particles.social.items.*.link'
                        ]
                    ]
                ],
                'spacer' => [
                    'enabled' => 'particles.spacer.enabled'
                ],
                'totop' => [
                    'enabled' => 'particles.totop.enabled',
                    'css' => [
                        'class' => 'particles.totop.css.class'
                    ],
                    'icon' => 'particles.totop.icon',
                    'content' => 'particles.totop.content'
                ]
            ],
            'styles' => [
                'above' => [
                    'background' => 'styles.above.background',
                    'text-color' => 'styles.above.text-color'
                ],
                'accent' => [
                    'color-1' => 'styles.accent.color-1',
                    'color-2' => 'styles.accent.color-2',
                    'color-3' => 'styles.accent.color-3'
                ],
                'base' => [
                    'background' => 'styles.base.background',
                    'text-color' => 'styles.base.text-color',
                    'text-active-color' => 'styles.base.text-active-color'
                ],
                'bottom' => [
                    'background' => 'styles.bottom.background',
                    'text-color' => 'styles.bottom.text-color'
                ],
                'breakpoints' => [
                    'large-desktop-container' => 'styles.breakpoints.large-desktop-container',
                    'desktop-container' => 'styles.breakpoints.desktop-container',
                    'tablet-container' => 'styles.breakpoints.tablet-container',
                    'large-mobile-container' => 'styles.breakpoints.large-mobile-container',
                    'mobile-menu-breakpoint' => 'styles.breakpoints.mobile-menu-breakpoint'
                ],
                'copyright' => [
                    'background' => 'styles.copyright.background',
                    'text-color' => 'styles.copyright.text-color'
                ],
                'expanded' => [
                    'background' => 'styles.expanded.background',
                    'text-color' => 'styles.expanded.text-color'
                ],
                'extension' => [
                    'background' => 'styles.extension.background',
                    'text-color' => 'styles.extension.text-color'
                ],
                'feature' => [
                    'background' => 'styles.feature.background',
                    'text-color' => 'styles.feature.text-color'
                ],
                'font' => [
                    'family-default' => 'styles.font.family-default'
                ],
                'footer' => [
                    'background' => 'styles.footer.background',
                    'text-color' => 'styles.footer.text-color'
                ],
                'header' => [
                    'background' => 'styles.header.background',
                    'text-color' => 'styles.header.text-color'
                ],
                'main' => [
                    'background' => 'styles.main.background',
                    'text-color' => 'styles.main.text-color'
                ],
                'menu' => [
                    'animation' => 'styles.menu.animation'
                ],
                'menustyle' => [
                    '_tab_toplevel' => 'styles.menustyle._tab_toplevel',
                    'text-color' => 'styles.menustyle.text-color',
                    'text-color-alt' => 'styles.menustyle.text-color-alt',
                    'text-color-active' => 'styles.menustyle.text-color-active',
                    'background-active' => 'styles.menustyle.background-active',
                    '_tab_sublevel' => 'styles.menustyle._tab_sublevel',
                    'sublevel-text-color' => 'styles.menustyle.sublevel-text-color',
                    'sublevel-text-color-active' => 'styles.menustyle.sublevel-text-color-active',
                    'sublevel-background' => 'styles.menustyle.sublevel-background',
                    'sublevel-background-active' => 'styles.menustyle.sublevel-background-active'
                ],
                'navigation' => [
                    'background' => 'styles.navigation.background',
                    'text-color' => 'styles.navigation.text-color'
                ],
                'offcanvas' => [
                    'background' => 'styles.offcanvas.background',
                    'text-color' => 'styles.offcanvas.text-color',
                    'toggle-color' => 'styles.offcanvas.toggle-color',
                    'width' => 'styles.offcanvas.width',
                    'toggle-visibility' => 'styles.offcanvas.toggle-visibility'
                ],
                'showcase' => [
                    'background' => 'styles.showcase.background',
                    'text-color' => 'styles.showcase.text-color'
                ],
                'slideshow' => [
                    'background' => 'styles.slideshow.background',
                    'background-image' => 'styles.slideshow.background-image',
                    'text-color' => 'styles.slideshow.text-color'
                ],
                'top' => [
                    'background' => 'styles.top.background',
                    'text-color' => 'styles.top.text-color'
                ],
                'utility' => [
                    'background' => 'styles.utility.background',
                    'text-color' => 'styles.utility.text-color'
                ]
            ],
            'page' => [
                'assets' => [
                    'favicon' => 'page.assets.favicon',
                    'touchicon' => 'page.assets.touchicon',
                    'css' => [
                        '*' => [
                            'name' => 'page.assets.css.*.name',
                            'location' => 'page.assets.css.*.location',
                            'inline' => 'page.assets.css.*.inline',
                            'extra' => 'page.assets.css.*.extra',
                            'priority' => 'page.assets.css.*.priority'
                        ]
                    ],
                    'javascript' => [
                        '*' => [
                            'name' => 'page.assets.javascript.*.name',
                            'location' => 'page.assets.javascript.*.location',
                            'inline' => 'page.assets.javascript.*.inline',
                            'in_footer' => 'page.assets.javascript.*.in_footer',
                            'extra' => 'page.assets.javascript.*.extra',
                            'priority' => 'page.assets.javascript.*.priority'
                        ]
                    ]
                ],
                'body' => [
                    'attribs' => [
                        'id' => 'page.body.attribs.id',
                        'class' => 'page.body.attribs.class',
                        'extra' => 'page.body.attribs.extra'
                    ],
                    'layout' => [
                        'sections' => 'page.body.layout.sections'
                    ],
                    'body_top' => 'page.body.body_top',
                    'body_bottom' => 'page.body.body_bottom'
                ],
                'head' => [
                    'meta' => 'page.head.meta',
                    'head_bottom' => 'page.head.head_bottom',
                    'atoms' => 'page.head.atoms'
                ]
            ],
            'pages' => [
                'blog_item' => [
                    'tabs' => 'pages.blog_item.tabs',
                    'content' => 'pages.blog_item.content',
                    'xss_check' => 'pages.blog_item.xss_check',
                    'header' => [
                        'title' => 'pages.blog_item.header.title',
                        'media_order' => 'pages.blog_item.header.media_order',
                        'published' => 'pages.blog_item.header.published',
                        'date' => 'pages.blog_item.header.date',
                        'publish_date' => 'pages.blog_item.header.publish_date',
                        'unpublish_date' => 'pages.blog_item.header.unpublish_date',
                        'metadata' => 'pages.blog_item.header.metadata',
                        'taxonomy' => 'pages.blog_item.header.taxonomy',
                        'body_classes' => 'pages.blog_item.header.body_classes',
                        'dateformat' => 'pages.blog_item.header.dateformat',
                        'menu' => 'pages.blog_item.header.menu',
                        'slug' => 'pages.blog_item.header.slug',
                        'redirect' => 'pages.blog_item.header.redirect',
                        'process' => 'pages.blog_item.header.process',
                        'twig_first' => 'pages.blog_item.header.twig_first',
                        'never_cache_twig' => 'pages.blog_item.header.never_cache_twig',
                        'child_type' => 'pages.blog_item.header.child_type',
                        'routable' => 'pages.blog_item.header.routable',
                        'cache_enable' => 'pages.blog_item.header.cache_enable',
                        'visible' => 'pages.blog_item.header.visible',
                        'debugger' => 'pages.blog_item.header.debugger',
                        'template' => 'pages.blog_item.header.template',
                        'append_url_extension' => 'pages.blog_item.header.append_url_extension',
                        'routes' => [
                            'default' => 'pages.blog_item.header.routes.default',
                            'canonical' => 'pages.blog_item.header.routes.canonical',
                            'aliases' => 'pages.blog_item.header.routes.aliases'
                        ],
                        'admin' => [
                            'children_display_order' => 'pages.blog_item.header.admin.children_display_order'
                        ],
                        'order_by' => 'pages.blog_item.header.order_by',
                        'order_manual' => 'pages.blog_item.header.order_manual',
                        'image' => [
                            'summary' => [
                                'enabled' => 'pages.blog_item.header.image.summary.enabled',
                                'file' => 'pages.blog_item.header.image.summary.file'
                            ],
                            'text' => [
                                'enabled' => 'pages.blog_item.header.image.text.enabled',
                                'file' => 'pages.blog_item.header.image.text.file'
                            ],
                            'width' => 'pages.blog_item.header.image.width',
                            'height' => 'pages.blog_item.header.image.height'
                        ],
                        'summary' => [
                            'enabled' => 'pages.blog_item.header.summary.enabled',
                            'format' => 'pages.blog_item.header.summary.format',
                            'size' => 'pages.blog_item.header.summary.size',
                            'delimiter' => 'pages.blog_item.header.summary.delimiter'
                        ],
                        'continue_link' => 'pages.blog_item.header.continue_link'
                    ],
                    'options' => 'pages.blog_item.options',
                    'publishing' => 'pages.blog_item.publishing',
                    'taxonomies' => 'pages.blog_item.taxonomies',
                    'advanced' => 'pages.blog_item.advanced',
                    'columns' => 'pages.blog_item.columns',
                    'column1' => 'pages.blog_item.column1',
                    'settings' => 'pages.blog_item.settings',
                    'folder' => 'pages.blog_item.folder',
                    'route' => 'pages.blog_item.route',
                    'name' => 'pages.blog_item.name',
                    'column2' => 'pages.blog_item.column2',
                    'order_title' => 'pages.blog_item.order_title',
                    'ordering' => 'pages.blog_item.ordering',
                    'order' => 'pages.blog_item.order',
                    'overrides' => 'pages.blog_item.overrides',
                    'routes_only' => 'pages.blog_item.routes_only',
                    'admin_only' => 'pages.blog_item.admin_only',
                    'blueprint' => 'pages.blog_item.blueprint',
                    'blog' => 'pages.blog_item.blog',
                    'header_image' => 'pages.blog_item.header_image',
                    'summary' => 'pages.blog_item.summary',
                    'readmore' => 'pages.blog_item.readmore'
                ],
                'blog_list' => [
                    'tabs' => 'pages.blog_list.tabs',
                    'content' => 'pages.blog_list.content',
                    'xss_check' => 'pages.blog_list.xss_check',
                    'header' => [
                        'title' => 'pages.blog_list.header.title',
                        'media_order' => 'pages.blog_list.header.media_order',
                        'published' => 'pages.blog_list.header.published',
                        'date' => 'pages.blog_list.header.date',
                        'publish_date' => 'pages.blog_list.header.publish_date',
                        'unpublish_date' => 'pages.blog_list.header.unpublish_date',
                        'metadata' => 'pages.blog_list.header.metadata',
                        'taxonomy' => 'pages.blog_list.header.taxonomy',
                        'body_classes' => 'pages.blog_list.header.body_classes',
                        'dateformat' => 'pages.blog_list.header.dateformat',
                        'menu' => 'pages.blog_list.header.menu',
                        'slug' => 'pages.blog_list.header.slug',
                        'redirect' => 'pages.blog_list.header.redirect',
                        'process' => 'pages.blog_list.header.process',
                        'twig_first' => 'pages.blog_list.header.twig_first',
                        'never_cache_twig' => 'pages.blog_list.header.never_cache_twig',
                        'child_type' => 'pages.blog_list.header.child_type',
                        'routable' => 'pages.blog_list.header.routable',
                        'cache_enable' => 'pages.blog_list.header.cache_enable',
                        'visible' => 'pages.blog_list.header.visible',
                        'debugger' => 'pages.blog_list.header.debugger',
                        'template' => 'pages.blog_list.header.template',
                        'append_url_extension' => 'pages.blog_list.header.append_url_extension',
                        'routes' => [
                            'default' => 'pages.blog_list.header.routes.default',
                            'canonical' => 'pages.blog_list.header.routes.canonical',
                            'aliases' => 'pages.blog_list.header.routes.aliases'
                        ],
                        'admin' => [
                            'children_display_order' => 'pages.blog_list.header.admin.children_display_order'
                        ],
                        'order_by' => 'pages.blog_list.header.order_by',
                        'order_manual' => 'pages.blog_list.header.order_manual',
                        'content' => [
                            'items' => 'pages.blog_list.header.content.items',
                            'leading' => 'pages.blog_list.header.content.leading',
                            'columns' => 'pages.blog_list.header.content.columns',
                            'limit' => 'pages.blog_list.header.content.limit',
                            'order' => [
                                'by' => 'pages.blog_list.header.content.order.by',
                                'dir' => 'pages.blog_list.header.content.order.dir'
                            ],
                            'show_date' => 'pages.blog_list.header.content.show_date',
                            'pagination' => 'pages.blog_list.header.content.pagination',
                            'url_taxonomy_filters' => 'pages.blog_list.header.content.url_taxonomy_filters'
                        ]
                    ],
                    'options' => 'pages.blog_list.options',
                    'publishing' => 'pages.blog_list.publishing',
                    'taxonomies' => 'pages.blog_list.taxonomies',
                    'advanced' => 'pages.blog_list.advanced',
                    'columns' => 'pages.blog_list.columns',
                    'column1' => 'pages.blog_list.column1',
                    'settings' => 'pages.blog_list.settings',
                    'folder' => 'pages.blog_list.folder',
                    'route' => 'pages.blog_list.route',
                    'name' => 'pages.blog_list.name',
                    'column2' => 'pages.blog_list.column2',
                    'order_title' => 'pages.blog_list.order_title',
                    'ordering' => 'pages.blog_list.ordering',
                    'order' => 'pages.blog_list.order',
                    'overrides' => 'pages.blog_list.overrides',
                    'routes_only' => 'pages.blog_list.routes_only',
                    'admin_only' => 'pages.blog_list.admin_only',
                    'blueprint' => 'pages.blog_list.blueprint',
                    'blog' => 'pages.blog_list.blog'
                ],
                'form' => 'pages.form',
                'modular' => [
                    'features' => [
                        'tabs' => 'pages.modular.features.tabs',
                        'advanced' => 'pages.modular.features.advanced',
                        'columns' => 'pages.modular.features.columns',
                        'column1' => 'pages.modular.features.column1',
                        'name' => 'pages.modular.features.name',
                        'overrides' => 'pages.modular.features.overrides',
                        'header' => [
                            'template' => 'pages.modular.features.header.template',
                            'features' => [
                                'icon' => 'pages.modular.features.header.features.icon',
                                'header' => 'pages.modular.features.header.features.header',
                                'text' => 'pages.modular.features.header.features.text'
                            ]
                        ],
                        'features' => 'pages.modular.features.features'
                    ],
                    'showcase' => [
                        'tabs' => 'pages.modular.showcase.tabs',
                        'advanced' => 'pages.modular.showcase.advanced',
                        'columns' => 'pages.modular.showcase.columns',
                        'column1' => 'pages.modular.showcase.column1',
                        'name' => 'pages.modular.showcase.name',
                        'overrides' => 'pages.modular.showcase.overrides',
                        'header' => [
                            'template' => 'pages.modular.showcase.header.template',
                            'buttons' => [
                                'text' => 'pages.modular.showcase.header.buttons.text',
                                'url' => 'pages.modular.showcase.header.buttons.url',
                                'primary' => 'pages.modular.showcase.header.buttons.primary'
                            ]
                        ],
                        'buttons' => 'pages.modular.showcase.buttons'
                    ],
                    'text' => [
                        'tabs' => 'pages.modular.text.tabs',
                        'advanced' => 'pages.modular.text.advanced',
                        'columns' => 'pages.modular.text.columns',
                        'column1' => 'pages.modular.text.column1',
                        'name' => 'pages.modular.text.name',
                        'overrides' => 'pages.modular.text.overrides',
                        'header' => [
                            'template' => 'pages.modular.text.header.template',
                            'image_align' => 'pages.modular.text.header.image_align'
                        ],
                        'content' => 'pages.modular.text.content',
                        'uploads' => 'pages.modular.text.uploads'
                    ]
                ]
            ]
        ],
        'dynamic' => [
            'pages.blog_item.name' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::pageTypes'
                ]
            ],
            'pages.blog_item.header.dateformat' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Utils::dateFormats'
                ]
            ],
            'pages.blog_item.header.process' => [
                'default' => [
                    'action' => 'config',
                    'params' => 'system.pages.process'
                ]
            ],
            'pages.blog_item.header.child_type' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::types'
                ]
            ],
            'pages.blog_list.name' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::pageTypes'
                ]
            ],
            'pages.blog_list.header.dateformat' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Utils::dateFormats'
                ]
            ],
            'pages.blog_list.header.process' => [
                'default' => [
                    'action' => 'config',
                    'params' => 'system.pages.process'
                ]
            ],
            'pages.blog_list.header.child_type' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::types'
                ]
            ],
            'pages.modular.features.name' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ],
            'pages.modular.features.header.template' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ],
            'pages.modular.showcase.name' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ],
            'pages.modular.showcase.header.template' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ],
            'pages.modular.text.name' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ],
            'pages.modular.text.header.template' => [
                'options' => [
                    'action' => 'data',
                    'params' => '\\Grav\\Common\\Page\\Pages::modularTypes'
                ]
            ]
        ],
        'filter' => [
            'validation' => true
        ],
        'configuration' => [
            'particles' => [
                'aos' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'audioplayer' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'blockcontent' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'calendar' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'carousel' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'casestudies' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'fixedheader' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'gridcontent' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'gridstatistic' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'imagegrid' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'infolist' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'newsletter' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'panelslider' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'pricingtable' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'simplecontent' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'simplecounter' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'simplemenu' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'testimonials' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'branding' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'copyright' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'custom' => [
                    'caching' => [
                        'type' => 'config_matches',
                        'values' => [
                            'twig' => '0',
                            'filter' => '0'
                        ]
                    ]
                ],
                'logo' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'menu' => [
                    'caching' => [
                        'type' => 'menu'
                    ]
                ],
                'mobile-menu' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'social' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'spacer' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ],
                'totop' => [
                    'caching' => [
                        'type' => 'static'
                    ]
                ]
            ]
        ]
    ]
];
