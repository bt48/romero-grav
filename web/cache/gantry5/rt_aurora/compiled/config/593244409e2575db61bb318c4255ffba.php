<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553165233,
    'checksum' => 'f5da287db2e9041aa66629b9c28d0c5b',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/assignments.yaml',
                'modified' => 1552956972
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/index.yaml',
                'modified' => 1553165229
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/layout.yaml',
                'modified' => 1553165229
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/styles.yaml',
                'modified' => 1552957068
            ]
        ],
        'user/data/gantry5/themes/rt_aurora/config/default' => [
            'content/archive/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/content.yaml',
                'modified' => 1548871662
            ],
            'content/archive/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/archive/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/heading.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/archive/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/archive/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/title.yaml',
                'modified' => 1548871662
            ],
            'content/blog/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/content.yaml',
                'modified' => 1548871662
            ],
            'content/blog/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/blog/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/heading.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/blog/query' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/query.yaml',
                'modified' => 1548871662
            ],
            'content/blog/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/blog/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/title.yaml',
                'modified' => 1548871662
            ],
            'content/general/wpautop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/general/wpautop.yaml',
                'modified' => 1548871662
            ],
            'content/page/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/page/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/title.yaml',
                'modified' => 1548871662
            ],
            'content/single/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/single/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/title.yaml',
                'modified' => 1548871662
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
                'modified' => 1552956792
            ],
            'particles/audioplayer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
                'modified' => 1552954183
            ],
            'particles/blockcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/blockcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/branding' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1552954183
            ],
            'particles/calendar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/calendar.yaml',
                'modified' => 1552954183
            ],
            'particles/carousel' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
                'modified' => 1552954183
            ],
            'particles/casestudies' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/casestudies.yaml',
                'modified' => 1552954183
            ],
            'particles/contactform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contactform.yaml',
                'modified' => 1548871662
            ],
            'particles/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/content.yaml',
                'modified' => 1552954183
            ],
            'particles/contentarray' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contentarray.yaml',
                'modified' => 1552954183
            ],
            'particles/copyright' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1552954183
            ],
            'particles/custom' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/custom.yaml',
                'modified' => 1552954183
            ],
            'particles/date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/date.yaml',
                'modified' => 1552954183
            ],
            'particles/gridcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/gridstatistic' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridstatistic.yaml',
                'modified' => 1552954183
            ],
            'particles/imagegrid' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/imagegrid.yaml',
                'modified' => 1552954183
            ],
            'particles/infolist' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/infolist.yaml',
                'modified' => 1552954183
            ],
            'particles/login' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/login.yaml',
                'modified' => 1552954183
            ],
            'particles/loginform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/loginform.yaml',
                'modified' => 1548871662
            ],
            'particles/logo' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1552954183
            ],
            'particles/menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
                'modified' => 1552954183
            ],
            'particles/messages' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/messages.yaml',
                'modified' => 1552954183
            ],
            'particles/mobile-menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/mobile-menu.yaml',
                'modified' => 1552954183
            ],
            'particles/newsletter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
                'modified' => 1552954183
            ],
            'particles/panelslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/panelslider.yaml',
                'modified' => 1552954183
            ],
            'particles/popupmodule' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/popupmodule.yaml',
                'modified' => 1548871662
            ],
            'particles/position' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/position.yaml',
                'modified' => 1552954183
            ],
            'particles/pricingtable' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/pricingtable.yaml',
                'modified' => 1552954183
            ],
            'particles/search' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/search.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecontent.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecounter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecounter.yaml',
                'modified' => 1552954183
            ],
            'particles/simplemenu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplemenu.yaml',
                'modified' => 1552954183
            ],
            'particles/social' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1552954183
            ],
            'particles/spacer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/spacer.yaml',
                'modified' => 1552954183
            ],
            'particles/testimonials' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
                'modified' => 1552954183
            ],
            'particles/totop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1552954183
            ],
            'particles/verticalslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/verticalslider.yaml',
                'modified' => 1552954183
            ],
            'particles/video' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/video.yaml',
                'modified' => 1552954183
            ],
            'particles/widget' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/widget.yaml',
                'modified' => 1548871662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1553164097
            ]
        ],
        'user/themes/rt_aurora/config/default' => [
            'page/assets' => [
                'file' => 'user/themes/rt_aurora/config/default/page/assets.yaml',
                'modified' => 1548871658
            ],
            'page/body' => [
                'file' => 'user/themes/rt_aurora/config/default/page/body.yaml',
                'modified' => 1548871658
            ],
            'page/head' => [
                'file' => 'user/themes/rt_aurora/config/default/page/head.yaml',
                'modified' => 1548871658
            ],
            'particles/branding' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1548871658
            ],
            'particles/copyright' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1548871658
            ],
            'particles/logo' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1548871658
            ],
            'particles/social' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1548871658
            ],
            'particles/totop' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1548871658
            ],
            'styles' => [
                'file' => 'user/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1548871658
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'aos' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'duration' => '1000',
                'once' => true,
                'delay' => '0',
                'easing' => 'ease',
                'offset' => '120'
            ],
            'audioplayer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'nowplaying' => 'Now Playing',
                'scrollbar' => 'noscrollbar',
                'overflow' => '350'
            ],
            'blockcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'linktarget' => '_self'
            ],
            'calendar' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'class' => '',
                'title' => '',
                'eventsheader' => '',
                'events' => [
                    
                ]
            ],
            'carousel' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'displayitems' => 5,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'readmore' => ''
                    ]
                ]
            ],
            'casestudies' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'cols-2',
                'class' => '',
                'title' => '',
                'cases' => [
                    
                ]
            ],
            'fixedheader' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'section' => '#g-navigation',
                'pinnedbg' => 'section',
                'custombg' => '#ffffff',
                'autohide' => 'enabled',
                'mobile' => 'disabled',
                'margin' => 115,
                'mobilemargin' => 115,
                'offset' => 100
            ],
            'gridcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridcontent-2cols'
            ],
            'gridstatistic' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridstatistic-2cols'
            ],
            'imagegrid' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-imagegrid-2cols',
                'layout' => 'g-imagegrid-standard'
            ],
            'infolist' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-1cols'
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'width' => 'g-newsletter-fullwidth',
                'layout' => 'g-newsletter-aside-compact',
                'style' => 'g-newsletter-rounded',
                'inputboxtext' => 'Your email address...',
                'buttontext' => 'Subscribe',
                'buttonclass' => 'button-xlarge button-block',
                'class' => '',
                'title' => '',
                'headtext' => '',
                'sidetext' => '',
                'buttonicon' => '',
                'uri' => ''
            ],
            'panelslider' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'displayitems' => 5,
                'nav' => 'enabled',
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'pricingtable' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'columns' => 'g-pricingtable-4-col'
            ],
            'simplecontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'simplecounter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => '1',
                'month' => '0',
                'class' => '',
                'title' => '',
                'desc' => '',
                'year' => '',
                'daytext' => '',
                'daystext' => '',
                'hourtext' => '',
                'hourstext' => '',
                'minutetext' => '',
                'minutestext' => '',
                'secondtext' => '',
                'secondstext' => ''
            ],
            'simplemenu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'title' => '',
                'class' => '',
                'menus' => [
                    
                ]
            ],
            'testimonials' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'disabled',
                'dots' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'class' => '',
                'title' => '',
                'autoplaySpeed' => '',
                'items' => [
                    
                ]
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2007',
                    'end' => 'now'
                ],
                'owner' => 'Romero Trust',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '1',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => '1',
                'url' => '',
                'image' => '',
                'svg' => '',
                'text' => 'aurora',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '1',
                'menu' => 'mainmenu',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0',
                'forceTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => 'https://www.facebook.com/RocketTheme',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => 'http://www.twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'search' => [
                'enabled' => '1',
                'title' => 'Search Form'
            ],
            'verticalslider' => [
                'enabled' => '1',
                'source' => 'particle',
                'presets' => 'disabled',
                'speed' => 400,
                'auto' => 'disabled',
                'pause' => 2000,
                'loop' => 'disabled',
                'controls' => 'enabled',
                'pager' => 'enabled',
                'height' => 800,
                'mobileheight' => 600,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'video' => [
                'enabled' => '1',
                'columns' => '1'
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'breadcrumbs' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'show',
                            'route' => ''
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '1',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'feed' => [
                'enabled' => true
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap2' => [
                    'enabled' => 0
                ],
                'bootstrap3' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'langswitcher' => [
                'enabled' => true
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'login' => [
                'enabled' => '1'
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'contactform' => [
                'enabled' => '1',
                'class' => '',
                'header' => '',
                'email' => '',
                'email_from' => '',
                'recaptcha' => [
                    'enabled' => '0',
                    'sitekey' => '',
                    'secretkey' => ''
                ]
            ],
            'loginform' => [
                'enabled' => '1',
                'class' => '',
                'title' => '<h3 class="g-title">Login</h3>',
                'greeting' => 'Hi, %s',
                'pretext' => '',
                'posttext' => ''
            ],
            'popupmodule' => [
                'enabled' => '1'
            ],
            'widget' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'styles' => [
            'above' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'accent' => [
                'color-1' => '#8a7224',
                'color-2' => '#c7b31c',
                'color-3' => '#f7f3d2'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'text-active-color' => '#31a594'
            ],
            'bottom' => [
                'background' => '#0d0d0d',
                'text-color' => '#ffffff'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'copyright' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'expanded' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'extension' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'font' => [
                'family-default' => 'muli'
            ],
            'footer' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'animation' => 'g-fade'
            ],
            'menustyle' => [
                'text-color' => '#2b2323',
                'text-color-alt' => '#8e9dab',
                'text-color-active' => '#8a7224',
                'background-active' => 'rgba(255,255,255, 0)',
                'sublevel-text-color' => '#000000',
                'sublevel-text-color-active' => '#31a594',
                'sublevel-background' => '#ffffff',
                'sublevel-background-active' => '#31a594'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'offcanvas' => [
                'background' => '#ff4b64',
                'text-color' => '#ffffff',
                'toggle-color' => '#ffffff',
                'width' => '10rem',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'slideshow' => [
                'background' => '#8a7224',
                'background-image' => 'gantry-media://fc3-1140x342.png',
                'text-color' => '#ffffff'
            ],
            'top' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'utility' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'preset1' => [
                'image' => 'gantry-admin://images/preset1.png',
                'description' => 'Preset 1',
                'colors' => [
                    0 => '#ff4b64',
                    1 => '#31a594'
                ],
                'styles' => [
                    'base' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666',
                        'text-active-color' => '#31a594'
                    ],
                    'accent' => [
                        'color-1' => '#ff4b64',
                        'color-2' => '#31a594',
                        'color-3' => '#508b70'
                    ],
                    'menustyle' => [
                        'text-color' => '#ffffff',
                        'text-color-alt' => '#8e9dab',
                        'text-color-active' => '#ffffff',
                        'background-active' => 'rgba(255,255,255, 0)',
                        'sublevel-text-color' => '#000000',
                        'sublevel-text-color-active' => '#31a594',
                        'sublevel-background' => '#ffffff',
                        'sublevel-background-active' => '#31a594'
                    ],
                    'font' => [
                        'family-default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif'
                    ],
                    'top' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'navigation' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'slideshow' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff',
                        'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg'
                    ],
                    'header' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'above' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'feature' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'showcase' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'utility' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'main' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'expanded' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'extension' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'bottom' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'footer' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'copyright' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'offcanvas' => [
                        'background' => '#ff4b64',
                        'text-color' => '#ffffff',
                        'toggle-color' => '#ffffff'
                    ],
                    'breakpoints' => [
                        'large-desktop-container' => '75rem',
                        'desktop-container' => '60rem',
                        'tablet-container' => '51rem',
                        'large-mobile-container' => '30rem',
                        'mobile-menu-breakpoint' => '51rem'
                    ]
                ]
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'doctype' => 'html',
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-3609',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/animate.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Animate CSS'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-1582'
                    ]
                ]
            ]
        ],
        'pages' => [
            'blog_item' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'default',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ]
                ],
                'name' => 'default'
            ],
            'blog_list' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'blog_item',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ],
                    'content' => [
                        'items' => '@self.children',
                        'leading' => 0,
                        'columns' => 2,
                        'limit' => 5,
                        'order' => [
                            'by' => 'date',
                            'dir' => 'desc'
                        ],
                        'show_date' => 0,
                        'pagination' => 1,
                        'url_taxonomy_filters' => 1
                    ]
                ],
                'name' => 'default'
            ],
            'modular' => [
                'features' => [
                    'name' => 'modular/features',
                    'header' => [
                        'template' => 'modular/features'
                    ]
                ],
                'showcase' => [
                    'name' => 'modular/showcase',
                    'header' => [
                        'template' => 'modular/showcase',
                        'buttons' => [
                            'primary' => 1
                        ]
                    ]
                ],
                'text' => [
                    'name' => 'modular/text',
                    'header' => [
                        'template' => 'modular/text',
                        'image_align' => 'left'
                    ]
                ]
            ]
        ],
        'content' => [
            'archive' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '1',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ],
            'blog' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '0',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '0',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'query' => [
                    'categories' => [
                        'include' => '',
                        'exclude' => ''
                    ]
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '1'
                ]
            ],
            'general' => [
                'wpautop' => [
                    'enabled' => '1'
                ]
            ],
            'page' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'title' => [
                    'enabled' => '0',
                    'link' => '0'
                ]
            ],
            'single' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ]
        ],
        'index' => [
            'name' => 'page_-_st_josephs',
            'timestamp' => 1553165229,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9021' => 'System Messages'
                ],
                'logo' => [
                    'logo-1317' => 'Logo',
                    'logo-9369' => 'SVG Pattern'
                ],
                'menu' => [
                    'menu-5165' => 'Menu'
                ],
                'simplecontent' => [
                    'simplecontent-7561' => 'Intro Text',
                    'simplecontent-2480' => 'Simple Content',
                    'simplecontent-5057' => 'contact module',
                    'simplecontent-2127' => 'Simple Content',
                    'simplecontent-8879' => 'Simple Content'
                ],
                'branding' => [
                    'branding-6761' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-9370' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'system-messages-9021' => 'system-messages-9828',
                    'navigation' => 'navigation',
                    'logo-1317' => 'logo-5992',
                    'menu-5165' => 'menu-2350',
                    'logo-9369' => 'logo-3146',
                    'header' => 'header',
                    'above' => 'above',
                    'utility' => 'utility',
                    'feature' => 'feature',
                    'expanded' => 'expanded',
                    'extension' => 'extension',
                    'bottom' => 'bottom',
                    'sidebar-footer' => 'sidebar-footer',
                    'simplecontent-5057' => 'simplecontent-5244',
                    'mainbar-footer' => 'mainbar-footer',
                    'simplecontent-2127' => 'simplecontent-8236',
                    'aside-footer' => 'aside-footer',
                    'simplecontent-8879' => 'simplecontent-4905',
                    'copyright' => 'copyright',
                    'branding-6761' => 'branding-1506',
                    'offcanvas' => 'offcanvas',
                    'mobile-menu-9370' => 'mobile-menu-7951'
                ],
                'layouts_-_full_width' => [
                    'sidebar' => 'sidebar',
                    'aside' => 'aside'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'layout' => [
                'top' => [
                    
                ],
                'navigation' => [
                    
                ],
                'header' => [
                    
                ],
                '/slideshow/' => [
                    0 => [
                        0 => 'simplecontent-7561'
                    ]
                ],
                'above' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                'utility' => [
                    
                ],
                'feature' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 13' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 74' => [
                                0 => [
                                    0 => 'simplecontent-2480'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 13' => [
                                
                            ]
                        ]
                    ]
                ],
                'expanded' => [
                    
                ],
                'extension' => [
                    
                ],
                'bottom' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'header' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-slideshow section-horizontal-paddings',
                        'variations' => ''
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings',
                        'variations' => ''
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => '',
                        'variations' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'aside' => [
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'simplecontent-7561' => [
                    'title' => 'Intro Text',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'St Joseph\'s Primary School – A Catholic Academy ',
                                'author' => '',
                                'leading_content' => 'Deep-rooted Gospel values are very much at the heart of the Trust’s vision.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Aurora combines elegance and simplicity in one professional template.'
                            ]
                        ]
                    ]
                ],
                'simplecontent-2480' => [
                    'title' => 'Simple Content',
                    'attributes' => [
                        'enabled' => 0,
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => '',
                                'author' => '',
                                'leading_content' => '',
                                'main_content' => '<h2>We believe that each person is unique and created in God’s image.  In our school, we provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.</h2>
<p>At St Joseph’s we hold Christ at the centre of everything we do and our children know that they are unique and created in God’s image. We celebrate each individual, supporting their journey through school and developing their gifts and talents. Our staff provide excellent opportunities for our children to enjoy and achieve in a happy, safe, and caring environment.</p>
<p>We know that your child will be happy at St Joseph’s and we hope that you as parents and carers will be fully involved in your child’s education and the life of the school in general.</p>
<p>If you need any further information please do not hesitate to look at our school brochure or contact the office and we will do our very best to help.</p>
<ul>
<li> w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a></li>
<li>e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a></li>
<li>t: +44 (0)1325 300 337</li>
</ul>',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Basic Block'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'assignments' => [
            'page' => [
                0 => [
                    'st-josephs' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                0 => [
                    'page' => true
                ]
            ]
        ]
    ]
];
