<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553164098,
    'checksum' => '88e8653cf440a0855cc2220f361f5625',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/_offline' => [
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/_offline/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/_offline/layout.yaml',
                'modified' => 1552956792
            ]
        ],
        'user/data/gantry5/themes/rt_aurora/config/default' => [
            'content/archive/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/content.yaml',
                'modified' => 1548871662
            ],
            'content/archive/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/archive/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/heading.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/archive/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/archive/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/title.yaml',
                'modified' => 1548871662
            ],
            'content/blog/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/content.yaml',
                'modified' => 1548871662
            ],
            'content/blog/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/blog/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/heading.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/blog/query' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/query.yaml',
                'modified' => 1548871662
            ],
            'content/blog/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/blog/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/title.yaml',
                'modified' => 1548871662
            ],
            'content/general/wpautop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/general/wpautop.yaml',
                'modified' => 1548871662
            ],
            'content/page/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/page/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/title.yaml',
                'modified' => 1548871662
            ],
            'content/single/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/single/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/title.yaml',
                'modified' => 1548871662
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
                'modified' => 1552956792
            ],
            'particles/audioplayer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
                'modified' => 1552954183
            ],
            'particles/blockcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/blockcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/branding' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1552954183
            ],
            'particles/calendar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/calendar.yaml',
                'modified' => 1552954183
            ],
            'particles/carousel' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
                'modified' => 1552954183
            ],
            'particles/casestudies' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/casestudies.yaml',
                'modified' => 1552954183
            ],
            'particles/contactform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contactform.yaml',
                'modified' => 1548871662
            ],
            'particles/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/content.yaml',
                'modified' => 1552954183
            ],
            'particles/contentarray' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contentarray.yaml',
                'modified' => 1552954183
            ],
            'particles/copyright' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1552954183
            ],
            'particles/custom' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/custom.yaml',
                'modified' => 1552954183
            ],
            'particles/date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/date.yaml',
                'modified' => 1552954183
            ],
            'particles/gridcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/gridstatistic' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridstatistic.yaml',
                'modified' => 1552954183
            ],
            'particles/imagegrid' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/imagegrid.yaml',
                'modified' => 1552954183
            ],
            'particles/infolist' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/infolist.yaml',
                'modified' => 1552954183
            ],
            'particles/login' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/login.yaml',
                'modified' => 1552954183
            ],
            'particles/loginform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/loginform.yaml',
                'modified' => 1548871662
            ],
            'particles/logo' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1552954183
            ],
            'particles/menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
                'modified' => 1552954183
            ],
            'particles/messages' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/messages.yaml',
                'modified' => 1552954183
            ],
            'particles/mobile-menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/mobile-menu.yaml',
                'modified' => 1552954183
            ],
            'particles/newsletter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
                'modified' => 1552954183
            ],
            'particles/panelslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/panelslider.yaml',
                'modified' => 1552954183
            ],
            'particles/popupmodule' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/popupmodule.yaml',
                'modified' => 1548871662
            ],
            'particles/position' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/position.yaml',
                'modified' => 1552954183
            ],
            'particles/pricingtable' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/pricingtable.yaml',
                'modified' => 1552954183
            ],
            'particles/search' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/search.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecontent.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecounter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecounter.yaml',
                'modified' => 1552954183
            ],
            'particles/simplemenu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplemenu.yaml',
                'modified' => 1552954183
            ],
            'particles/social' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1552954183
            ],
            'particles/spacer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/spacer.yaml',
                'modified' => 1552954183
            ],
            'particles/testimonials' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
                'modified' => 1552954183
            ],
            'particles/totop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1552954183
            ],
            'particles/verticalslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/verticalslider.yaml',
                'modified' => 1552954183
            ],
            'particles/video' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/video.yaml',
                'modified' => 1552954183
            ],
            'particles/widget' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/widget.yaml',
                'modified' => 1548871662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1553164097
            ]
        ],
        'user/themes/rt_aurora/config/default' => [
            'page/assets' => [
                'file' => 'user/themes/rt_aurora/config/default/page/assets.yaml',
                'modified' => 1548871658
            ],
            'page/body' => [
                'file' => 'user/themes/rt_aurora/config/default/page/body.yaml',
                'modified' => 1548871658
            ],
            'page/head' => [
                'file' => 'user/themes/rt_aurora/config/default/page/head.yaml',
                'modified' => 1548871658
            ],
            'particles/branding' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1548871658
            ],
            'particles/copyright' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1548871658
            ],
            'particles/logo' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1548871658
            ],
            'particles/social' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1548871658
            ],
            'particles/totop' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1548871658
            ],
            'styles' => [
                'file' => 'user/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1548871658
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'aos' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'duration' => '1000',
                'once' => true,
                'delay' => '0',
                'easing' => 'ease',
                'offset' => '120'
            ],
            'audioplayer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'nowplaying' => 'Now Playing',
                'scrollbar' => 'noscrollbar',
                'overflow' => '350'
            ],
            'blockcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'linktarget' => '_self'
            ],
            'calendar' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'class' => '',
                'title' => '',
                'eventsheader' => '',
                'events' => [
                    
                ]
            ],
            'carousel' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'displayitems' => 5,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'readmore' => ''
                    ]
                ]
            ],
            'casestudies' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'cols-2',
                'class' => '',
                'title' => '',
                'cases' => [
                    
                ]
            ],
            'fixedheader' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'section' => '#g-navigation',
                'pinnedbg' => 'section',
                'custombg' => '#ffffff',
                'autohide' => 'enabled',
                'mobile' => 'disabled',
                'margin' => 115,
                'mobilemargin' => 115,
                'offset' => 100
            ],
            'gridcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridcontent-2cols'
            ],
            'gridstatistic' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridstatistic-2cols'
            ],
            'imagegrid' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-imagegrid-2cols',
                'layout' => 'g-imagegrid-standard'
            ],
            'infolist' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-1cols'
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'width' => 'g-newsletter-fullwidth',
                'layout' => 'g-newsletter-aside-compact',
                'style' => 'g-newsletter-rounded',
                'inputboxtext' => 'Your email address...',
                'buttontext' => 'Subscribe',
                'buttonclass' => 'button-xlarge button-block',
                'class' => '',
                'title' => '',
                'headtext' => '',
                'sidetext' => '',
                'buttonicon' => '',
                'uri' => ''
            ],
            'panelslider' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'displayitems' => 5,
                'nav' => 'enabled',
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'pricingtable' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'columns' => 'g-pricingtable-4-col'
            ],
            'simplecontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'simplecounter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => '1',
                'month' => '0',
                'class' => '',
                'title' => '',
                'desc' => '',
                'year' => '',
                'daytext' => '',
                'daystext' => '',
                'hourtext' => '',
                'hourstext' => '',
                'minutetext' => '',
                'minutestext' => '',
                'secondtext' => '',
                'secondstext' => ''
            ],
            'simplemenu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'title' => '',
                'class' => '',
                'menus' => [
                    
                ]
            ],
            'testimonials' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'disabled',
                'dots' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'class' => '',
                'title' => '',
                'autoplaySpeed' => '',
                'items' => [
                    
                ]
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2007',
                    'end' => 'now'
                ],
                'owner' => 'Romero Trust',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '1',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => '1',
                'url' => '',
                'image' => '',
                'svg' => '',
                'text' => 'aurora',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '1',
                'menu' => 'mainmenu',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0',
                'forceTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => 'https://www.facebook.com/RocketTheme',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => 'http://www.twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'search' => [
                'enabled' => '1',
                'title' => 'Search Form'
            ],
            'verticalslider' => [
                'enabled' => '1',
                'source' => 'particle',
                'presets' => 'disabled',
                'speed' => 400,
                'auto' => 'disabled',
                'pause' => 2000,
                'loop' => 'disabled',
                'controls' => 'enabled',
                'pager' => 'enabled',
                'height' => 800,
                'mobileheight' => 600,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'video' => [
                'enabled' => '1',
                'columns' => '1'
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'breadcrumbs' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'show',
                            'route' => ''
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '1',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'feed' => [
                'enabled' => true
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap2' => [
                    'enabled' => 0
                ],
                'bootstrap3' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'langswitcher' => [
                'enabled' => true
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'login' => [
                'enabled' => '1'
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'contactform' => [
                'enabled' => '1',
                'class' => '',
                'header' => '',
                'email' => '',
                'email_from' => '',
                'recaptcha' => [
                    'enabled' => '0',
                    'sitekey' => '',
                    'secretkey' => ''
                ]
            ],
            'loginform' => [
                'enabled' => '1',
                'class' => '',
                'title' => '<h3 class="g-title">Login</h3>',
                'greeting' => 'Hi, %s',
                'pretext' => '',
                'posttext' => ''
            ],
            'popupmodule' => [
                'enabled' => '1'
            ],
            'widget' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'styles' => [
            'above' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'accent' => [
                'color-1' => '#8a7224',
                'color-2' => '#c7b31c',
                'color-3' => '#f7f3d2'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'text-active-color' => '#31a594'
            ],
            'bottom' => [
                'background' => '#0d0d0d',
                'text-color' => '#ffffff'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'copyright' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'expanded' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'extension' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'font' => [
                'family-default' => 'muli'
            ],
            'footer' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'animation' => 'g-fade'
            ],
            'menustyle' => [
                'text-color' => '#2b2323',
                'text-color-alt' => '#8e9dab',
                'text-color-active' => '#8a7224',
                'background-active' => 'rgba(255,255,255, 0)',
                'sublevel-text-color' => '#000000',
                'sublevel-text-color-active' => '#31a594',
                'sublevel-background' => '#ffffff',
                'sublevel-background-active' => '#31a594'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'offcanvas' => [
                'background' => '#ff4b64',
                'text-color' => '#ffffff',
                'toggle-color' => '#ffffff',
                'width' => '10rem',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'slideshow' => [
                'background' => '#0d0d0d',
                'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg',
                'text-color' => '#ffffff'
            ],
            'top' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'utility' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'preset1' => [
                'image' => 'gantry-admin://images/preset1.png',
                'description' => 'Preset 1',
                'colors' => [
                    0 => '#ff4b64',
                    1 => '#31a594'
                ],
                'styles' => [
                    'base' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666',
                        'text-active-color' => '#31a594'
                    ],
                    'accent' => [
                        'color-1' => '#ff4b64',
                        'color-2' => '#31a594',
                        'color-3' => '#508b70'
                    ],
                    'menustyle' => [
                        'text-color' => '#ffffff',
                        'text-color-alt' => '#8e9dab',
                        'text-color-active' => '#ffffff',
                        'background-active' => 'rgba(255,255,255, 0)',
                        'sublevel-text-color' => '#000000',
                        'sublevel-text-color-active' => '#31a594',
                        'sublevel-background' => '#ffffff',
                        'sublevel-background-active' => '#31a594'
                    ],
                    'font' => [
                        'family-default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif'
                    ],
                    'top' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'navigation' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'slideshow' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff',
                        'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg'
                    ],
                    'header' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'above' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'feature' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'showcase' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'utility' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'main' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'expanded' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'extension' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'bottom' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'footer' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'copyright' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'offcanvas' => [
                        'background' => '#ff4b64',
                        'text-color' => '#ffffff',
                        'toggle-color' => '#ffffff'
                    ],
                    'breakpoints' => [
                        'large-desktop-container' => '75rem',
                        'desktop-container' => '60rem',
                        'tablet-container' => '51rem',
                        'large-mobile-container' => '30rem',
                        'mobile-menu-breakpoint' => '51rem'
                    ]
                ]
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'doctype' => 'html',
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-3609',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/animate.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Animate CSS'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-1582'
                    ]
                ]
            ]
        ],
        'pages' => [
            'blog_item' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'default',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ]
                ],
                'name' => 'default'
            ],
            'blog_list' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'blog_item',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ],
                    'content' => [
                        'items' => '@self.children',
                        'leading' => 0,
                        'columns' => 2,
                        'limit' => 5,
                        'order' => [
                            'by' => 'date',
                            'dir' => 'desc'
                        ],
                        'show_date' => 0,
                        'pagination' => 1,
                        'url_taxonomy_filters' => 1
                    ]
                ],
                'name' => 'default'
            ],
            'modular' => [
                'features' => [
                    'name' => 'modular/features',
                    'header' => [
                        'template' => 'modular/features'
                    ]
                ],
                'showcase' => [
                    'name' => 'modular/showcase',
                    'header' => [
                        'template' => 'modular/showcase',
                        'buttons' => [
                            'primary' => 1
                        ]
                    ]
                ],
                'text' => [
                    'name' => 'modular/text',
                    'header' => [
                        'template' => 'modular/text',
                        'image_align' => 'left'
                    ]
                ]
            ]
        ],
        'content' => [
            'archive' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '1',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ],
            'blog' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '0',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '0',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'query' => [
                    'categories' => [
                        'include' => '',
                        'exclude' => ''
                    ]
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '1'
                ]
            ],
            'general' => [
                'wpautop' => [
                    'enabled' => '1'
                ]
            ],
            'page' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'title' => [
                    'enabled' => '0',
                    'link' => '0'
                ]
            ],
            'single' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ]
        ],
        'index' => [
            'name' => '_offline',
            'timestamp' => 1552956792,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => '_offline',
                'timestamp' => 1509009075
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9828' => 'System Messages'
                ],
                'logo' => [
                    'logo-5992' => 'Logo'
                ],
                'simplecontent' => [
                    'simplecontent-5551' => 'Offline Description'
                ],
                'content' => [
                    'system-content-7661' => 'Page Content'
                ],
                'blockcontent' => [
                    'blockcontent-4574' => 'Maintenance Mode'
                ],
                'branding' => [
                    'branding-1168' => 'Branding'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'navigation' => 'navigation',
                    'slideshow' => 'slideshow',
                    'extension' => 'extension',
                    'bottom' => 'bottom',
                    'copyright' => 'copyright',
                    'branding-1168' => 'branding-1506'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => '_offline',
                'timestamp' => 1509009075
            ],
            'layout' => [
                'top' => [
                    0 => [
                        0 => 'system-messages-9828'
                    ]
                ],
                'navigation' => [
                    0 => [
                        0 => 'logo-5992'
                    ]
                ],
                'slideshow' => [
                    0 => [
                        0 => 'simplecontent-5551'
                    ]
                ],
                '/header/' => [
                    
                ],
                '/above/' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/feature/' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 20' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 60' => [
                                0 => [
                                    0 => 'system-content-7661'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 20' => [
                                
                            ]
                        ]
                    ]
                ],
                '/expanded/' => [
                    
                ],
                'extension' => [
                    
                ],
                'bottom' => [
                    0 => [
                        0 => 'blockcontent-4574'
                    ]
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ],
                    'type' => 'section'
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-header'
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-above'
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-showcase'
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-utility'
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-feature'
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => ''
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'variations' => 'center'
                    ]
                ],
                'aside' => [
                    'attributes' => [
                        'class' => ''
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '0',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '1',
                        'class' => 'g-default-expanded'
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar'
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main'
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside'
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'logo-5992' => [
                    'block' => [
                        'variations' => 'nomarginall center'
                    ]
                ],
                'simplecontent-5551' => [
                    'title' => 'Offline Description',
                    'attributes' => [
                        'class' => '',
                        'title' => 'Offline',
                        'items' => [
                            0 => [
                                'layout' => 'header',
                                'created_date' => '',
                                'content_title' => 'Site offline',
                                'author' => '',
                                'leading_content' => 'We are working on new and exciting stuff.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Offline'
                            ]
                        ]
                    ],
                    'block' => [
                        'variations' => 'align-left'
                    ]
                ],
                'system-content-7661' => [
                    'block' => [
                        'variations' => 'title6'
                    ]
                ],
                'blockcontent-4574' => [
                    'title' => 'Maintenance Mode',
                    'attributes' => [
                        'class' => '',
                        'headline' => 'Maintenance Mode',
                        'description' => 'Sorry, our Website is Temporarily Down for Maintenance. Please Check Back Again Soon.'
                    ],
                    'block' => [
                        'class' => 'g-title-large',
                        'variations' => 'center'
                    ]
                ]
            ]
        ]
    ]
];
