<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553164100,
    'checksum' => '12b7b800c48caa03c31f0dc11dbc0ff6',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/pages_-_contact' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_contact/assignments.yaml',
                'modified' => 1552955721
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_contact/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_contact/layout.yaml',
                'modified' => 1552956792
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_contact/styles.yaml',
                'modified' => 1552956700
            ]
        ],
        'user/data/gantry5/themes/rt_aurora/config/default' => [
            'content/archive/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/content.yaml',
                'modified' => 1548871662
            ],
            'content/archive/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/archive/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/heading.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/archive/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/archive/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/title.yaml',
                'modified' => 1548871662
            ],
            'content/blog/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/content.yaml',
                'modified' => 1548871662
            ],
            'content/blog/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/blog/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/heading.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/blog/query' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/query.yaml',
                'modified' => 1548871662
            ],
            'content/blog/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/blog/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/title.yaml',
                'modified' => 1548871662
            ],
            'content/general/wpautop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/general/wpautop.yaml',
                'modified' => 1548871662
            ],
            'content/page/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/page/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/title.yaml',
                'modified' => 1548871662
            ],
            'content/single/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/single/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/title.yaml',
                'modified' => 1548871662
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
                'modified' => 1552956792
            ],
            'particles/audioplayer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
                'modified' => 1552954183
            ],
            'particles/blockcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/blockcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/branding' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1552954183
            ],
            'particles/calendar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/calendar.yaml',
                'modified' => 1552954183
            ],
            'particles/carousel' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
                'modified' => 1552954183
            ],
            'particles/casestudies' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/casestudies.yaml',
                'modified' => 1552954183
            ],
            'particles/contactform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contactform.yaml',
                'modified' => 1548871662
            ],
            'particles/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/content.yaml',
                'modified' => 1552954183
            ],
            'particles/contentarray' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contentarray.yaml',
                'modified' => 1552954183
            ],
            'particles/copyright' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1552954183
            ],
            'particles/custom' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/custom.yaml',
                'modified' => 1552954183
            ],
            'particles/date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/date.yaml',
                'modified' => 1552954183
            ],
            'particles/gridcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/gridstatistic' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridstatistic.yaml',
                'modified' => 1552954183
            ],
            'particles/imagegrid' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/imagegrid.yaml',
                'modified' => 1552954183
            ],
            'particles/infolist' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/infolist.yaml',
                'modified' => 1552954183
            ],
            'particles/login' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/login.yaml',
                'modified' => 1552954183
            ],
            'particles/loginform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/loginform.yaml',
                'modified' => 1548871662
            ],
            'particles/logo' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1552954183
            ],
            'particles/menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
                'modified' => 1552954183
            ],
            'particles/messages' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/messages.yaml',
                'modified' => 1552954183
            ],
            'particles/mobile-menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/mobile-menu.yaml',
                'modified' => 1552954183
            ],
            'particles/newsletter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
                'modified' => 1552954183
            ],
            'particles/panelslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/panelslider.yaml',
                'modified' => 1552954183
            ],
            'particles/popupmodule' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/popupmodule.yaml',
                'modified' => 1548871662
            ],
            'particles/position' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/position.yaml',
                'modified' => 1552954183
            ],
            'particles/pricingtable' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/pricingtable.yaml',
                'modified' => 1552954183
            ],
            'particles/search' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/search.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecontent.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecounter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecounter.yaml',
                'modified' => 1552954183
            ],
            'particles/simplemenu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplemenu.yaml',
                'modified' => 1552954183
            ],
            'particles/social' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1552954183
            ],
            'particles/spacer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/spacer.yaml',
                'modified' => 1552954183
            ],
            'particles/testimonials' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
                'modified' => 1552954183
            ],
            'particles/totop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1552954183
            ],
            'particles/verticalslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/verticalslider.yaml',
                'modified' => 1552954183
            ],
            'particles/video' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/video.yaml',
                'modified' => 1552954183
            ],
            'particles/widget' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/widget.yaml',
                'modified' => 1548871662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1553164097
            ]
        ],
        'user/themes/rt_aurora/config/default' => [
            'page/assets' => [
                'file' => 'user/themes/rt_aurora/config/default/page/assets.yaml',
                'modified' => 1548871658
            ],
            'page/body' => [
                'file' => 'user/themes/rt_aurora/config/default/page/body.yaml',
                'modified' => 1548871658
            ],
            'page/head' => [
                'file' => 'user/themes/rt_aurora/config/default/page/head.yaml',
                'modified' => 1548871658
            ],
            'particles/branding' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1548871658
            ],
            'particles/copyright' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1548871658
            ],
            'particles/logo' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1548871658
            ],
            'particles/social' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1548871658
            ],
            'particles/totop' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1548871658
            ],
            'styles' => [
                'file' => 'user/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1548871658
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'aos' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'duration' => '1000',
                'once' => true,
                'delay' => '0',
                'easing' => 'ease',
                'offset' => '120'
            ],
            'audioplayer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'nowplaying' => 'Now Playing',
                'scrollbar' => 'noscrollbar',
                'overflow' => '350'
            ],
            'blockcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'linktarget' => '_self'
            ],
            'calendar' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'class' => '',
                'title' => '',
                'eventsheader' => '',
                'events' => [
                    
                ]
            ],
            'carousel' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'displayitems' => 5,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'readmore' => ''
                    ]
                ]
            ],
            'casestudies' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'cols-2',
                'class' => '',
                'title' => '',
                'cases' => [
                    
                ]
            ],
            'fixedheader' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'section' => '#g-navigation',
                'pinnedbg' => 'section',
                'custombg' => '#ffffff',
                'autohide' => 'enabled',
                'mobile' => 'disabled',
                'margin' => 115,
                'mobilemargin' => 115,
                'offset' => 100
            ],
            'gridcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridcontent-2cols'
            ],
            'gridstatistic' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridstatistic-2cols'
            ],
            'imagegrid' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-imagegrid-2cols',
                'layout' => 'g-imagegrid-standard'
            ],
            'infolist' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-1cols'
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'width' => 'g-newsletter-fullwidth',
                'layout' => 'g-newsletter-aside-compact',
                'style' => 'g-newsletter-rounded',
                'inputboxtext' => 'Your email address...',
                'buttontext' => 'Subscribe',
                'buttonclass' => 'button-xlarge button-block',
                'class' => '',
                'title' => '',
                'headtext' => '',
                'sidetext' => '',
                'buttonicon' => '',
                'uri' => ''
            ],
            'panelslider' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'displayitems' => 5,
                'nav' => 'enabled',
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'pricingtable' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'columns' => 'g-pricingtable-4-col'
            ],
            'simplecontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'simplecounter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => '1',
                'month' => '0',
                'class' => '',
                'title' => '',
                'desc' => '',
                'year' => '',
                'daytext' => '',
                'daystext' => '',
                'hourtext' => '',
                'hourstext' => '',
                'minutetext' => '',
                'minutestext' => '',
                'secondtext' => '',
                'secondstext' => ''
            ],
            'simplemenu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'title' => '',
                'class' => '',
                'menus' => [
                    
                ]
            ],
            'testimonials' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'disabled',
                'dots' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'class' => '',
                'title' => '',
                'autoplaySpeed' => '',
                'items' => [
                    
                ]
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2007',
                    'end' => 'now'
                ],
                'owner' => 'Romero Trust',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '1',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => '1',
                'url' => '',
                'image' => '',
                'svg' => '',
                'text' => 'aurora',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '1',
                'menu' => 'mainmenu',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0',
                'forceTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => 'https://www.facebook.com/RocketTheme',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => 'http://www.twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'search' => [
                'enabled' => '1',
                'title' => 'Search Form'
            ],
            'verticalslider' => [
                'enabled' => '1',
                'source' => 'particle',
                'presets' => 'disabled',
                'speed' => 400,
                'auto' => 'disabled',
                'pause' => 2000,
                'loop' => 'disabled',
                'controls' => 'enabled',
                'pager' => 'enabled',
                'height' => 800,
                'mobileheight' => 600,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'video' => [
                'enabled' => '1',
                'columns' => '1'
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'breadcrumbs' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'show',
                            'route' => ''
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '1',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'feed' => [
                'enabled' => true
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap2' => [
                    'enabled' => 0
                ],
                'bootstrap3' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'langswitcher' => [
                'enabled' => true
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'login' => [
                'enabled' => '1'
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'contactform' => [
                'enabled' => '1',
                'class' => '',
                'header' => '',
                'email' => '',
                'email_from' => '',
                'recaptcha' => [
                    'enabled' => '0',
                    'sitekey' => '',
                    'secretkey' => ''
                ]
            ],
            'loginform' => [
                'enabled' => '1',
                'class' => '',
                'title' => '<h3 class="g-title">Login</h3>',
                'greeting' => 'Hi, %s',
                'pretext' => '',
                'posttext' => ''
            ],
            'popupmodule' => [
                'enabled' => '1'
            ],
            'widget' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'styles' => [
            'above' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'accent' => [
                'color-1' => '#8a7224',
                'color-2' => '#c7b31c',
                'color-3' => '#f7f3d2'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'text-active-color' => '#31a594'
            ],
            'bottom' => [
                'background' => '#0d0d0d',
                'text-color' => '#ffffff'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'copyright' => [
                'background' => '#f5f3e7',
                'text-color' => '#4a3309'
            ],
            'expanded' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'extension' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'font' => [
                'family-default' => 'muli'
            ],
            'footer' => [
                'background' => '#f5f3e7',
                'text-color' => '#4a3309'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'animation' => 'g-fade'
            ],
            'menustyle' => [
                'text-color' => '#2b2323',
                'text-color-alt' => '#8e9dab',
                'text-color-active' => '#8a7224',
                'background-active' => 'rgba(255,255,255, 0)',
                'sublevel-text-color' => '#000000',
                'sublevel-text-color-active' => '#31a594',
                'sublevel-background' => '#ffffff',
                'sublevel-background-active' => '#31a594'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'offcanvas' => [
                'background' => '#ff4b64',
                'text-color' => '#ffffff',
                'toggle-color' => '#ffffff',
                'width' => '10rem',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'slideshow' => [
                'background' => '#8a7224',
                'background-image' => 'gantry-media://RCET_Logo.jpg',
                'text-color' => '#ffffff'
            ],
            'top' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'utility' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'preset1' => [
                'image' => 'gantry-admin://images/preset1.png',
                'description' => 'Preset 1',
                'colors' => [
                    0 => '#ff4b64',
                    1 => '#31a594'
                ],
                'styles' => [
                    'base' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666',
                        'text-active-color' => '#31a594'
                    ],
                    'accent' => [
                        'color-1' => '#ff4b64',
                        'color-2' => '#31a594',
                        'color-3' => '#508b70'
                    ],
                    'menustyle' => [
                        'text-color' => '#ffffff',
                        'text-color-alt' => '#8e9dab',
                        'text-color-active' => '#ffffff',
                        'background-active' => 'rgba(255,255,255, 0)',
                        'sublevel-text-color' => '#000000',
                        'sublevel-text-color-active' => '#31a594',
                        'sublevel-background' => '#ffffff',
                        'sublevel-background-active' => '#31a594'
                    ],
                    'font' => [
                        'family-default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif'
                    ],
                    'top' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'navigation' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'slideshow' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff',
                        'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg'
                    ],
                    'header' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'above' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'feature' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'showcase' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'utility' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'main' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'expanded' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'extension' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'bottom' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'footer' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'copyright' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'offcanvas' => [
                        'background' => '#ff4b64',
                        'text-color' => '#ffffff',
                        'toggle-color' => '#ffffff'
                    ],
                    'breakpoints' => [
                        'large-desktop-container' => '75rem',
                        'desktop-container' => '60rem',
                        'tablet-container' => '51rem',
                        'large-mobile-container' => '30rem',
                        'mobile-menu-breakpoint' => '51rem'
                    ]
                ]
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'doctype' => 'html',
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-3609',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/animate.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Animate CSS'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-1582'
                    ]
                ]
            ]
        ],
        'pages' => [
            'blog_item' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'default',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ]
                ],
                'name' => 'default'
            ],
            'blog_list' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'blog_item',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ],
                    'content' => [
                        'items' => '@self.children',
                        'leading' => 0,
                        'columns' => 2,
                        'limit' => 5,
                        'order' => [
                            'by' => 'date',
                            'dir' => 'desc'
                        ],
                        'show_date' => 0,
                        'pagination' => 1,
                        'url_taxonomy_filters' => 1
                    ]
                ],
                'name' => 'default'
            ],
            'modular' => [
                'features' => [
                    'name' => 'modular/features',
                    'header' => [
                        'template' => 'modular/features'
                    ]
                ],
                'showcase' => [
                    'name' => 'modular/showcase',
                    'header' => [
                        'template' => 'modular/showcase',
                        'buttons' => [
                            'primary' => 1
                        ]
                    ]
                ],
                'text' => [
                    'name' => 'modular/text',
                    'header' => [
                        'template' => 'modular/text',
                        'image_align' => 'left'
                    ]
                ]
            ]
        ],
        'content' => [
            'archive' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '1',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ],
            'blog' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '0',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '0',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'query' => [
                    'categories' => [
                        'include' => '',
                        'exclude' => ''
                    ]
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '1'
                ]
            ],
            'general' => [
                'wpautop' => [
                    'enabled' => '1'
                ]
            ],
            'page' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'title' => [
                    'enabled' => '0',
                    'link' => '0'
                ]
            ],
            'single' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ]
        ],
        'index' => [
            'name' => 'pages_-_contact',
            'timestamp' => 1552956792,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1508514391
            ],
            'positions' => [
                'sidebar' => 'Sidebar',
                'aside' => 'Aside'
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'sidebar' => 'Sidebar',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'mainbar' => 'Main',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'simplecontent' => [
                    'simplecontent-9318' => 'Header - Contact us',
                    'simplecontent-5458' => 'contact module',
                    'simplecontent-5267' => 'Simple Content',
                    'simplecontent-2333' => 'Simple Content'
                ],
                'logo' => [
                    'logo-3428' => 'SVG Pattern',
                    'logo-3771' => 'Logo',
                    'logo-6190' => 'SVG Pattern'
                ],
                'content' => [
                    'system-content-5115' => 'Page Content'
                ],
                'messages' => [
                    'system-messages-5687' => 'System Messages'
                ],
                'menu' => [
                    'menu-1216' => 'Menu'
                ],
                'position' => [
                    'position-position-9232' => 'Sidebar',
                    'position-position-6232' => 'Aside'
                ],
                'branding' => [
                    'branding-4147' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-2720' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'navigation' => 'navigation',
                    'sidebar' => 'sidebar',
                    'aside' => 'aside',
                    'sidebar-footer' => 'sidebar-footer',
                    'mainbar-footer' => 'mainbar-footer',
                    'aside-footer' => 'aside-footer',
                    'copyright' => 'copyright',
                    'offcanvas' => 'offcanvas',
                    'system-messages-5687' => 'system-messages-9828',
                    'logo-3771' => 'logo-5992',
                    'menu-1216' => 'menu-2350',
                    'logo-6190' => 'logo-3146',
                    'position-position-9232' => 'position-position-9414',
                    'position-position-6232' => 'position-position-8807',
                    'simplecontent-5458' => 'simplecontent-5244',
                    'simplecontent-5267' => 'simplecontent-8236',
                    'simplecontent-2333' => 'simplecontent-4905',
                    'branding-4147' => 'branding-1506',
                    'mobile-menu-2720' => 'mobile-menu-7951'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1508514391
            ],
            'layout' => [
                'top' => [
                    
                ],
                'navigation' => [
                    
                ],
                '/slideshow/' => [
                    0 => [
                        0 => 'simplecontent-9318'
                    ]
                ],
                '/header/' => [
                    
                ],
                '/above/' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/feature/' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 26' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 49' => [
                                0 => [
                                    0 => 'logo-3428'
                                ],
                                1 => [
                                    0 => 'system-content-5115'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 25' => [
                                
                            ]
                        ]
                    ]
                ],
                '/expanded/' => [
                    
                ],
                '/extension/' => [
                    
                ],
                '/bottom/' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-slideshow section-horizontal-paddings'
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-header section-horizontal-paddings section-vertical-paddings-small'
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-above section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-utility section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-feature section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'subtype' => 'aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'subtype' => 'main',
                    'title' => 'Main',
                    'attributes' => [
                        'class' => 'section-horizontal-paddings'
                    ]
                ],
                'aside' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-expanded section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-extension section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-bottom section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'children',
                            1 => 'attributes'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'simplecontent-9318' => [
                    'title' => 'Header - Contact us',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'header',
                                'created_date' => '',
                                'content_title' => '<strong>Get</strong> in Touch!',
                                'author' => '',
                                'leading_content' => 'We are looking forward to hear from you.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Get in Touch!'
                            ]
                        ]
                    ]
                ],
                'logo-3428' => [
                    'title' => 'SVG Pattern',
                    'attributes' => [
                        'url' => '',
                        'link' => '0',
                        'svg' => '<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 2560 420">
<path
 d="M2141.000,240.000 L2560.533,240.000 L2561.000,212.001 L2561.000,300.000 L959.000,300.000 L216.000,300.000 L213.000,300.000 C196.432,300.000 183.000,286.569 183.000,270.000 C183.000,253.432 196.432,240.000 213.000,240.000 L216.000,240.000 L466.000,240.000 C482.569,240.000 496.000,226.568 496.000,210.000 C496.000,193.431 482.569,180.000 466.000,180.000 L1.000,180.000 L1.000,0.000 L407.000,0.000 L1502.000,0.000 L1931.000,0.000 C1947.569,0.000 1961.000,13.431 1961.000,30.000 C1961.000,46.568 1947.569,60.000 1931.000,60.000 L1541.000,60.000 C1524.431,60.000 1511.000,73.432 1511.000,90.000 C1511.000,106.569 1524.431,120.000 1541.000,120.000 L2561.000,120.000 L2561.000,180.000 L2141.000,180.000 C2124.431,180.000 2111.000,193.431 2111.000,210.000 C2111.000,226.568 2124.431,240.000 2141.000,240.000 ZM602.000,60.000 L407.000,60.000 L82.000,60.000 C65.431,60.000 52.000,73.432 52.000,90.000 C52.000,106.569 65.431,120.000 82.000,120.000 L602.000,120.000 C618.569,120.000 632.000,106.569 632.000,90.000 C632.000,73.432 618.569,60.000 602.000,60.000 ZM1139.000,120.000 L760.000,120.000 C743.431,120.000 730.000,133.431 730.000,150.000 C730.000,166.569 743.431,180.000 760.000,180.000 L1139.000,180.000 C1155.568,180.000 1169.000,166.569 1169.000,150.000 C1169.000,133.431 1155.568,120.000 1139.000,120.000 ZM1906.000,180.000 L1353.000,180.000 C1336.432,180.000 1323.000,193.431 1323.000,210.000 C1323.000,226.568 1336.432,240.000 1353.000,240.000 L1906.000,240.000 C1922.569,240.000 1936.000,226.568 1936.000,210.000 C1936.000,193.431 1922.569,180.000 1906.000,180.000 ZM2561.000,60.000 L2561.000,60.004 L2561.000,60.000 L2561.000,60.000 ZM115.000,390.000 C115.000,406.569 101.569,420.000 85.000,420.000 L0.000,420.000 L0.000,360.000 L85.000,360.000 C101.569,360.000 115.000,373.432 115.000,390.000 ZM1401.000,360.000 L1569.000,360.000 C1585.568,360.000 1599.000,373.432 1599.000,390.000 C1599.000,406.569 1585.568,420.000 1569.000,420.000 L1401.000,420.000 C1384.431,420.000 1371.000,406.569 1371.000,390.000 C1371.000,373.432 1384.431,360.000 1401.000,360.000 ZM2339.000,360.000 L2377.000,360.000 C2393.569,360.000 2407.000,373.432 2407.000,390.000 C2407.000,406.569 2393.569,420.000 2377.000,420.000 L2339.000,420.000 C2322.431,420.000 2309.000,406.569 2309.000,390.000 C2309.000,373.432 2322.431,360.000 2339.000,360.000 Z"></path>
</svg>',
                        'text' => ''
                    ],
                    'block' => [
                        'class' => 'svg-pattern-mainbar'
                    ]
                ]
            ]
        ],
        'assignments' => [
            'page' => [
                0 => [
                    'contact' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ]
    ]
];
