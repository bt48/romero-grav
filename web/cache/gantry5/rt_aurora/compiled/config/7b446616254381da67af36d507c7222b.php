<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553164099,
    'checksum' => 'e210045f37a4ffdbc4d3528c66b32f2c',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/assignments.yaml',
                'modified' => 1552956224
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/index.yaml',
                'modified' => 1552957980
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/layout.yaml',
                'modified' => 1552957980
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/styles.yaml',
                'modified' => 1552958916
            ]
        ],
        'user/data/gantry5/themes/rt_aurora/config/default' => [
            'content/archive/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/content.yaml',
                'modified' => 1548871662
            ],
            'content/archive/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/archive/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/heading.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/archive/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/archive/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/title.yaml',
                'modified' => 1548871662
            ],
            'content/blog/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/content.yaml',
                'modified' => 1548871662
            ],
            'content/blog/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/blog/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/heading.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/blog/query' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/query.yaml',
                'modified' => 1548871662
            ],
            'content/blog/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/blog/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/title.yaml',
                'modified' => 1548871662
            ],
            'content/general/wpautop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/general/wpautop.yaml',
                'modified' => 1548871662
            ],
            'content/page/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/page/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/title.yaml',
                'modified' => 1548871662
            ],
            'content/single/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/single/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/title.yaml',
                'modified' => 1548871662
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
                'modified' => 1552956792
            ],
            'particles/audioplayer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
                'modified' => 1552954183
            ],
            'particles/blockcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/blockcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/branding' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1552954183
            ],
            'particles/calendar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/calendar.yaml',
                'modified' => 1552954183
            ],
            'particles/carousel' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
                'modified' => 1552954183
            ],
            'particles/casestudies' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/casestudies.yaml',
                'modified' => 1552954183
            ],
            'particles/contactform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contactform.yaml',
                'modified' => 1548871662
            ],
            'particles/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/content.yaml',
                'modified' => 1552954183
            ],
            'particles/contentarray' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contentarray.yaml',
                'modified' => 1552954183
            ],
            'particles/copyright' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1552954183
            ],
            'particles/custom' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/custom.yaml',
                'modified' => 1552954183
            ],
            'particles/date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/date.yaml',
                'modified' => 1552954183
            ],
            'particles/gridcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/gridstatistic' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridstatistic.yaml',
                'modified' => 1552954183
            ],
            'particles/imagegrid' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/imagegrid.yaml',
                'modified' => 1552954183
            ],
            'particles/infolist' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/infolist.yaml',
                'modified' => 1552954183
            ],
            'particles/login' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/login.yaml',
                'modified' => 1552954183
            ],
            'particles/loginform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/loginform.yaml',
                'modified' => 1548871662
            ],
            'particles/logo' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1552954183
            ],
            'particles/menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
                'modified' => 1552954183
            ],
            'particles/messages' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/messages.yaml',
                'modified' => 1552954183
            ],
            'particles/mobile-menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/mobile-menu.yaml',
                'modified' => 1552954183
            ],
            'particles/newsletter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
                'modified' => 1552954183
            ],
            'particles/panelslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/panelslider.yaml',
                'modified' => 1552954183
            ],
            'particles/popupmodule' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/popupmodule.yaml',
                'modified' => 1548871662
            ],
            'particles/position' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/position.yaml',
                'modified' => 1552954183
            ],
            'particles/pricingtable' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/pricingtable.yaml',
                'modified' => 1552954183
            ],
            'particles/search' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/search.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecontent.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecounter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecounter.yaml',
                'modified' => 1552954183
            ],
            'particles/simplemenu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplemenu.yaml',
                'modified' => 1552954183
            ],
            'particles/social' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1552954183
            ],
            'particles/spacer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/spacer.yaml',
                'modified' => 1552954183
            ],
            'particles/testimonials' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
                'modified' => 1552954183
            ],
            'particles/totop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1552954183
            ],
            'particles/verticalslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/verticalslider.yaml',
                'modified' => 1552954183
            ],
            'particles/video' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/video.yaml',
                'modified' => 1552954183
            ],
            'particles/widget' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/widget.yaml',
                'modified' => 1548871662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1553164097
            ]
        ],
        'user/themes/rt_aurora/config/default' => [
            'page/assets' => [
                'file' => 'user/themes/rt_aurora/config/default/page/assets.yaml',
                'modified' => 1548871658
            ],
            'page/body' => [
                'file' => 'user/themes/rt_aurora/config/default/page/body.yaml',
                'modified' => 1548871658
            ],
            'page/head' => [
                'file' => 'user/themes/rt_aurora/config/default/page/head.yaml',
                'modified' => 1548871658
            ],
            'particles/branding' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1548871658
            ],
            'particles/copyright' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1548871658
            ],
            'particles/logo' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1548871658
            ],
            'particles/social' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1548871658
            ],
            'particles/totop' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1548871658
            ],
            'styles' => [
                'file' => 'user/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1548871658
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'aos' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'duration' => '1000',
                'once' => true,
                'delay' => '0',
                'easing' => 'ease',
                'offset' => '120'
            ],
            'audioplayer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'nowplaying' => 'Now Playing',
                'scrollbar' => 'noscrollbar',
                'overflow' => '350'
            ],
            'blockcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'linktarget' => '_self'
            ],
            'calendar' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'class' => '',
                'title' => '',
                'eventsheader' => '',
                'events' => [
                    
                ]
            ],
            'carousel' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'displayitems' => 5,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'readmore' => ''
                    ]
                ]
            ],
            'casestudies' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'cols-2',
                'class' => '',
                'title' => '',
                'cases' => [
                    
                ]
            ],
            'fixedheader' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'section' => '#g-navigation',
                'pinnedbg' => 'section',
                'custombg' => '#ffffff',
                'autohide' => 'enabled',
                'mobile' => 'disabled',
                'margin' => 115,
                'mobilemargin' => 115,
                'offset' => 100
            ],
            'gridcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridcontent-2cols'
            ],
            'gridstatistic' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridstatistic-2cols'
            ],
            'imagegrid' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-imagegrid-2cols',
                'layout' => 'g-imagegrid-standard'
            ],
            'infolist' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-1cols'
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'width' => 'g-newsletter-fullwidth',
                'layout' => 'g-newsletter-aside-compact',
                'style' => 'g-newsletter-rounded',
                'inputboxtext' => 'Your email address...',
                'buttontext' => 'Subscribe',
                'buttonclass' => 'button-xlarge button-block',
                'class' => '',
                'title' => '',
                'headtext' => '',
                'sidetext' => '',
                'buttonicon' => '',
                'uri' => ''
            ],
            'panelslider' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'displayitems' => 5,
                'nav' => 'enabled',
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'pricingtable' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'columns' => 'g-pricingtable-4-col'
            ],
            'simplecontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'simplecounter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => '1',
                'month' => '0',
                'class' => '',
                'title' => '',
                'desc' => '',
                'year' => '',
                'daytext' => '',
                'daystext' => '',
                'hourtext' => '',
                'hourstext' => '',
                'minutetext' => '',
                'minutestext' => '',
                'secondtext' => '',
                'secondstext' => ''
            ],
            'simplemenu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'title' => '',
                'class' => '',
                'menus' => [
                    
                ]
            ],
            'testimonials' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'disabled',
                'dots' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'class' => '',
                'title' => '',
                'autoplaySpeed' => '',
                'items' => [
                    
                ]
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2007',
                    'end' => 'now'
                ],
                'owner' => 'Romero Trust',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '1',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => '1',
                'url' => '',
                'image' => '',
                'svg' => '',
                'text' => 'aurora',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '1',
                'menu' => 'mainmenu',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0',
                'forceTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => 'https://www.facebook.com/RocketTheme',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => 'http://www.twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'search' => [
                'enabled' => '1',
                'title' => 'Search Form'
            ],
            'verticalslider' => [
                'enabled' => '1',
                'source' => 'particle',
                'presets' => 'disabled',
                'speed' => 400,
                'auto' => 'disabled',
                'pause' => 2000,
                'loop' => 'disabled',
                'controls' => 'enabled',
                'pager' => 'enabled',
                'height' => 800,
                'mobileheight' => 600,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'video' => [
                'enabled' => '1',
                'columns' => '1'
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'breadcrumbs' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'show',
                            'route' => ''
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '1',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'feed' => [
                'enabled' => true
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap2' => [
                    'enabled' => 0
                ],
                'bootstrap3' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'langswitcher' => [
                'enabled' => true
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'login' => [
                'enabled' => '1'
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'contactform' => [
                'enabled' => '1',
                'class' => '',
                'header' => '',
                'email' => '',
                'email_from' => '',
                'recaptcha' => [
                    'enabled' => '0',
                    'sitekey' => '',
                    'secretkey' => ''
                ]
            ],
            'loginform' => [
                'enabled' => '1',
                'class' => '',
                'title' => '<h3 class="g-title">Login</h3>',
                'greeting' => 'Hi, %s',
                'pretext' => '',
                'posttext' => ''
            ],
            'popupmodule' => [
                'enabled' => '1'
            ],
            'widget' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'styles' => [
            'above' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'accent' => [
                'color-1' => '#8a7224',
                'color-2' => '#c7b31c',
                'color-3' => '#f7f3d2'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'text-active-color' => '#31a594'
            ],
            'bottom' => [
                'background' => '#0d0d0d',
                'text-color' => '#ffffff'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'copyright' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'expanded' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'extension' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'font' => [
                'family-default' => 'muli'
            ],
            'footer' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'animation' => 'g-fade'
            ],
            'menustyle' => [
                'text-color' => '#2b2323',
                'text-color-alt' => '#8e9dab',
                'text-color-active' => '#8a7224',
                'background-active' => 'rgba(255,255,255, 0)',
                'sublevel-text-color' => '#000000',
                'sublevel-text-color-active' => '#31a594',
                'sublevel-background' => '#ffffff',
                'sublevel-background-active' => '#31a594'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'offcanvas' => [
                'background' => '#ff4b64',
                'text-color' => '#ffffff',
                'toggle-color' => '#ffffff',
                'width' => '10rem',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'slideshow' => [
                'background' => '#8a7224',
                'background-image' => 'gantry-media://pupils.jpg',
                'text-color' => '#fffff7'
            ],
            'top' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'utility' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'preset1' => [
                'image' => 'gantry-admin://images/preset1.png',
                'description' => 'Preset 1',
                'colors' => [
                    0 => '#ff4b64',
                    1 => '#31a594'
                ],
                'styles' => [
                    'base' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666',
                        'text-active-color' => '#31a594'
                    ],
                    'accent' => [
                        'color-1' => '#ff4b64',
                        'color-2' => '#31a594',
                        'color-3' => '#508b70'
                    ],
                    'menustyle' => [
                        'text-color' => '#ffffff',
                        'text-color-alt' => '#8e9dab',
                        'text-color-active' => '#ffffff',
                        'background-active' => 'rgba(255,255,255, 0)',
                        'sublevel-text-color' => '#000000',
                        'sublevel-text-color-active' => '#31a594',
                        'sublevel-background' => '#ffffff',
                        'sublevel-background-active' => '#31a594'
                    ],
                    'font' => [
                        'family-default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif'
                    ],
                    'top' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'navigation' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'slideshow' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff',
                        'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg'
                    ],
                    'header' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'above' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'feature' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'showcase' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'utility' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'main' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'expanded' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'extension' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'bottom' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'footer' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'copyright' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'offcanvas' => [
                        'background' => '#ff4b64',
                        'text-color' => '#ffffff',
                        'toggle-color' => '#ffffff'
                    ],
                    'breakpoints' => [
                        'large-desktop-container' => '75rem',
                        'desktop-container' => '60rem',
                        'tablet-container' => '51rem',
                        'large-mobile-container' => '30rem',
                        'mobile-menu-breakpoint' => '51rem'
                    ]
                ]
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'doctype' => 'html',
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-3609',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/animate.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Animate CSS'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-1582'
                    ]
                ]
            ]
        ],
        'pages' => [
            'blog_item' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'default',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ]
                ],
                'name' => 'default'
            ],
            'blog_list' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'blog_item',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ],
                    'content' => [
                        'items' => '@self.children',
                        'leading' => 0,
                        'columns' => 2,
                        'limit' => 5,
                        'order' => [
                            'by' => 'date',
                            'dir' => 'desc'
                        ],
                        'show_date' => 0,
                        'pagination' => 1,
                        'url_taxonomy_filters' => 1
                    ]
                ],
                'name' => 'default'
            ],
            'modular' => [
                'features' => [
                    'name' => 'modular/features',
                    'header' => [
                        'template' => 'modular/features'
                    ]
                ],
                'showcase' => [
                    'name' => 'modular/showcase',
                    'header' => [
                        'template' => 'modular/showcase',
                        'buttons' => [
                            'primary' => 1
                        ]
                    ]
                ],
                'text' => [
                    'name' => 'modular/text',
                    'header' => [
                        'template' => 'modular/text',
                        'image_align' => 'left'
                    ]
                ]
            ]
        ],
        'content' => [
            'archive' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '1',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ],
            'blog' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '0',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '0',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'query' => [
                    'categories' => [
                        'include' => '',
                        'exclude' => ''
                    ]
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '1'
                ]
            ],
            'general' => [
                'wpautop' => [
                    'enabled' => '1'
                ]
            ],
            'page' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'title' => [
                    'enabled' => '0',
                    'link' => '0'
                ]
            ],
            'single' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ]
        ],
        'index' => [
            'name' => 'page_-_st_johns',
            'timestamp' => 1552957980,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9138' => 'System Messages'
                ],
                'logo' => [
                    'logo-6511' => 'Logo',
                    'logo-4091' => 'SVG Pattern'
                ],
                'menu' => [
                    'menu-2983' => 'Menu'
                ],
                'simplecontent' => [
                    'simplecontent-7561' => 'Intro Text',
                    'simplecontent-5107' => 'Simple Content',
                    'simplecontent-6722' => 'contact module',
                    'simplecontent-9181' => 'Simple Content',
                    'simplecontent-5675' => 'Simple Content'
                ],
                'branding' => [
                    'branding-5737' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-7330' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'system-messages-9138' => 'system-messages-9828',
                    'navigation' => 'navigation',
                    'logo-6511' => 'logo-5992',
                    'menu-2983' => 'menu-2350',
                    'logo-4091' => 'logo-3146',
                    'header' => 'header',
                    'slideshow' => 'slideshow',
                    'above' => 'above',
                    'showcase' => 'showcase',
                    'utility' => 'utility',
                    'feature' => 'feature',
                    'expanded' => 'expanded',
                    'extension' => 'extension',
                    'bottom' => 'bottom',
                    'sidebar-footer' => 'sidebar-footer',
                    'simplecontent-6722' => 'simplecontent-5244',
                    'mainbar-footer' => 'mainbar-footer',
                    'simplecontent-9181' => 'simplecontent-8236',
                    'aside-footer' => 'aside-footer',
                    'simplecontent-5675' => 'simplecontent-4905',
                    'copyright' => 'copyright',
                    'branding-5737' => 'branding-1506',
                    'offcanvas' => 'offcanvas',
                    'mobile-menu-7330' => 'mobile-menu-7951'
                ],
                'layouts_-_full_width' => [
                    'simplecontent-7561' => 'simplecontent-7561',
                    'sidebar' => 'sidebar',
                    'aside' => 'aside'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'layout' => [
                'top' => [
                    
                ],
                'navigation' => [
                    
                ],
                'header' => [
                    
                ],
                'slideshow' => [
                    0 => [
                        0 => 'simplecontent-7561'
                    ]
                ],
                'above' => [
                    
                ],
                'showcase' => [
                    
                ],
                'utility' => [
                    
                ],
                'feature' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 13' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 74' => [
                                0 => [
                                    0 => 'simplecontent-5107'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 13' => [
                                
                            ]
                        ]
                    ]
                ],
                'expanded' => [
                    
                ],
                'extension' => [
                    
                ],
                'bottom' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'header' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => '',
                        'variations' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'aside' => [
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'simplecontent-7561' => [
                    'title' => 'Intro Text',
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block'
                        ]
                    ]
                ],
                'simplecontent-5107' => [
                    'title' => 'Simple Content',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => '',
                                'author' => '',
                                'leading_content' => '',
                                'main_content' => '<h2>At St John’s we pride ourselves on being A Learning Community Guided by Gospel Values, where each individual is valued and nurtured to grow in confidence and to embrace their God-given talents.  As a supportive and inclusive school, we respond to the changing needs of all our students across the age and ability range and we are committed to the highest standards of teaching and learning.</h2>
<p>By providing a wealth of exciting experiences and opportunities, we encourage our young people to be aspirational, fulfilling their potential and realising their dreams with determination and enthusiasm.</p>
<p>From sporting events at regional, national and international levels to Arts projects, foreign exchanges and residential trips, we strive to provide many opportunities for our young people to grow in confidence and to develop their skills.  We believe that our community is a very special place to learn, work and grow, supporting individuals to become independent and resilient learners who enjoy their education and are well-equipped to be the leaders of tomorrow.</p>
<p>Whilst our students participate in many activities that enrich their education and life experiences, we encourage them to recognise their role in helping those in need, both within and beyond our community. We support our young people to respect one another, to embrace diversity and to respond with compassion to those less fortunate than ourselves.</p>
<p>By working in partnership with parents/carers, parishes, feeder schools and the wider community, we create a positive, safe and stimulating environment in which each individual can succeed.</p>
<p>Our dedicated and highly-motivated staff are always available to offer help, support and guidance for our young people and their families, so please come and see our learning community in action.  Meet our students, who are our best ambassadors, and please take the opportunity to speak with them about their experiences when you visit our school.  In addition, spend time exploring our excellent facilities including our floodlit 3G pitch and recently refurbished swimming pool, our professional recording studio, and much, much more.</p>
<p>Please contact us at any time if you require any additional information:</p>
<h3>St John’s School & Sixth Form College – A Catholic Academy</h3>
<ul>
<li>w: <a href="https://www.stjohnsrc.org.uk">www.stjohns.org.uk</a></li>
<li>e: <a href="mailto:staff@stjohnsrc.org.uk">staff@stjohns.org.uk</a></li>
<li>t: 01388 603246</li>
</ul>',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Simple Block'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'assignments' => [
            'page' => [
                0 => [
                    'st-johns' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ]
    ]
];
