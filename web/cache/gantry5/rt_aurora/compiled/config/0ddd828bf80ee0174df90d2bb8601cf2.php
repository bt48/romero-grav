<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1552957006,
    'checksum' => '31e3a10a12b1447c62e6b5a78eb244dd',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/assignments.yaml',
                'modified' => 1552956143
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/layout.yaml',
                'modified' => 1552956792
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/styles.yaml',
                'modified' => 1552955449
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'page' => [
                0 => [
                    'st-josephs' => true,
                    'st-johns' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'index' => [
            'name' => 'layouts_-_full_width',
            'timestamp' => 1552956792,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'simplecontent' => [
                    'simplecontent-7561' => 'Intro Text',
                    'simplecontent-7366' => 'contact module',
                    'simplecontent-9754' => 'Simple Content',
                    'simplecontent-9809' => 'Simple Content'
                ],
                'content' => [
                    'system-content-6621' => 'Page Content'
                ],
                'messages' => [
                    'system-messages-8361' => 'System Messages'
                ],
                'logo' => [
                    'logo-5211' => 'Logo',
                    'logo-3323' => 'SVG Pattern'
                ],
                'menu' => [
                    'menu-4340' => 'Menu'
                ],
                'branding' => [
                    'branding-5506' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-6627' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'navigation' => 'navigation',
                    'header' => 'header',
                    'slideshow' => 'slideshow',
                    'above' => 'above',
                    'showcase' => 'showcase',
                    'utility' => 'utility',
                    'feature' => 'feature',
                    'expanded' => 'expanded',
                    'extension' => 'extension',
                    'bottom' => 'bottom',
                    'sidebar-footer' => 'sidebar-footer',
                    'mainbar-footer' => 'mainbar-footer',
                    'aside-footer' => 'aside-footer',
                    'copyright' => 'copyright',
                    'offcanvas' => 'offcanvas',
                    'system-messages-8361' => 'system-messages-9828',
                    'logo-5211' => 'logo-5992',
                    'menu-4340' => 'menu-2350',
                    'logo-3323' => 'logo-3146',
                    'simplecontent-7366' => 'simplecontent-5244',
                    'simplecontent-9754' => 'simplecontent-8236',
                    'simplecontent-9809' => 'simplecontent-4905',
                    'branding-5506' => 'branding-1506',
                    'mobile-menu-6627' => 'mobile-menu-7951'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'layout' => [
                'top' => [
                    
                ],
                'navigation' => [
                    
                ],
                'header' => [
                    
                ],
                'slideshow' => [
                    0 => [
                        0 => 'simplecontent-7561'
                    ]
                ],
                'above' => [
                    
                ],
                'showcase' => [
                    
                ],
                'utility' => [
                    
                ],
                'feature' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 13' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 74' => [
                                0 => [
                                    0 => 'system-content-6621'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 13' => [
                                
                            ]
                        ]
                    ]
                ],
                'expanded' => [
                    
                ],
                'extension' => [
                    
                ],
                'bottom' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'header' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => '',
                        'variations' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'aside' => [
                    'attributes' => [
                        'class' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'simplecontent-7561' => [
                    'title' => 'Intro Text',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'St John’s School & Sixth Form College – A Catholic Academy ',
                                'author' => '',
                                'leading_content' => 'Deep-rooted Gospel values are very much at the heart of the Trust’s vision.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Aurora combines elegance and simplicity in one professional template.'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'styles' => [
            'preset' => 'preset1',
            'menustyle' => [
                'background-active' => 'rgba(255,255,255, 0)'
            ],
            'slideshow' => [
                'background' => '#8a7224',
                'background-image' => 'gantry-media://pupils.jpg',
                'text-color' => '#ffffff'
            ]
        ]
    ]
];
