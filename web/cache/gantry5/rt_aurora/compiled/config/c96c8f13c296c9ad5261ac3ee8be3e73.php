<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553165216,
    'checksum' => 'dadd6666b70b987f1e21ff46076a820c',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/assignments.yaml',
                'modified' => 1552956972
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/index.yaml',
                'modified' => 1552957662
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/layout.yaml',
                'modified' => 1552957662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/styles.yaml',
                'modified' => 1552957068
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'page' => [
                0 => [
                    'st-josephs' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                0 => [
                    'page' => true
                ]
            ]
        ],
        'index' => [
            'name' => 'page_-_st_josephs',
            'timestamp' => 1552957662,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'sidebar' => 'Sidebar',
                'mainbar' => 'Mainbar',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9021' => 'System Messages'
                ],
                'logo' => [
                    'logo-1317' => 'Logo',
                    'logo-9369' => 'SVG Pattern'
                ],
                'menu' => [
                    'menu-5165' => 'Menu'
                ],
                'simplecontent' => [
                    'simplecontent-7561' => 'Intro Text',
                    'simplecontent-2480' => 'Simple Content',
                    'simplecontent-5057' => 'contact module',
                    'simplecontent-2127' => 'Simple Content',
                    'simplecontent-8879' => 'Simple Content'
                ],
                'branding' => [
                    'branding-6761' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-9370' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'system-messages-9021' => 'system-messages-9828',
                    'navigation' => 'navigation',
                    'logo-1317' => 'logo-5992',
                    'menu-5165' => 'menu-2350',
                    'logo-9369' => 'logo-3146',
                    'header' => 'header',
                    'above' => 'above',
                    'utility' => 'utility',
                    'feature' => 'feature',
                    'expanded' => 'expanded',
                    'extension' => 'extension',
                    'bottom' => 'bottom',
                    'sidebar-footer' => 'sidebar-footer',
                    'simplecontent-5057' => 'simplecontent-5244',
                    'mainbar-footer' => 'mainbar-footer',
                    'simplecontent-2127' => 'simplecontent-8236',
                    'aside-footer' => 'aside-footer',
                    'simplecontent-8879' => 'simplecontent-4905',
                    'copyright' => 'copyright',
                    'branding-6761' => 'branding-1506',
                    'offcanvas' => 'offcanvas',
                    'mobile-menu-9370' => 'mobile-menu-7951'
                ],
                'layouts_-_full_width' => [
                    'sidebar' => 'sidebar',
                    'aside' => 'aside'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'fullwidth',
                'timestamp' => 1508428073
            ],
            'layout' => [
                'top' => [
                    
                ],
                'navigation' => [
                    
                ],
                'header' => [
                    
                ],
                '/slideshow/' => [
                    0 => [
                        0 => 'simplecontent-7561'
                    ]
                ],
                'above' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                'utility' => [
                    
                ],
                'feature' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 13' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar 74' => [
                                0 => [
                                    0 => 'simplecontent-2480'
                                ]
                            ]
                        ],
                        2 => [
                            'aside 13' => [
                                
                            ]
                        ]
                    ]
                ],
                'expanded' => [
                    
                ],
                'extension' => [
                    
                ],
                'bottom' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'header' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-slideshow section-horizontal-paddings',
                        'variations' => ''
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings',
                        'variations' => ''
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => '',
                        'variations' => ''
                    ],
                    'block' => [
                        'class' => 'equal-height',
                        'fixed' => '1'
                    ]
                ],
                'aside' => [
                    'inherit' => [
                        'outline' => 'layouts_-_full_width',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ],
                    'block' => [
                        'fixed' => '1'
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'simplecontent-7561' => [
                    'title' => 'Intro Text',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'St Joseph\'s Primary School – A Catholic Academy ',
                                'author' => '',
                                'leading_content' => 'Deep-rooted Gospel values are very much at the heart of the Trust’s vision.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Aurora combines elegance and simplicity in one professional template.'
                            ]
                        ]
                    ]
                ],
                'simplecontent-2480' => [
                    'title' => 'Simple Content',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => '',
                                'author' => '',
                                'leading_content' => '',
                                'main_content' => '<h2>We believe that each person is unique and created in God’s image.  In our school, we provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.</h2>
<p>At St Joseph’s we hold Christ at the centre of everything we do and our children know that they are unique and created in God’s image. We celebrate each individual, supporting their journey through school and developing their gifts and talents. Our staff provide excellent opportunities for our children to enjoy and achieve in a happy, safe, and caring environment.</p>
<p>We know that your child will be happy at St Joseph’s and we hope that you as parents and carers will be fully involved in your child’s education and the life of the school in general.</p>
<p>If you need any further information please do not hesitate to look at our school brochure or contact the office and we will do our very best to help.</p>
<ul>
<li> w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a></li>
<li>e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a></li>
<li>t: +44 (0)1325 300 337</li>
</ul>',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Basic Block'
                            ]
                        ]
                    ]
                ]
            ]
        ],
        'styles' => [
            'preset' => 'preset1',
            'menustyle' => [
                'background-active' => 'rgba(255,255,255, 0)'
            ],
            'slideshow' => [
                'background' => '#8a7224',
                'background-image' => 'gantry-media://fc3-1140x342.png',
                'text-color' => '#ffffff'
            ]
        ]
    ]
];
