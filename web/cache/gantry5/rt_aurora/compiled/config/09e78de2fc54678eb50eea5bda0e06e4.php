<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1553164097,
    'checksum' => 'f0958f3acc59bce490dd5c8692ad5a4e',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/default' => [
            'content/archive/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/content.yaml',
                'modified' => 1548871662
            ],
            'content/archive/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/archive/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/heading.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/archive/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/archive/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/archive/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/archive/title.yaml',
                'modified' => 1548871662
            ],
            'content/blog/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/content.yaml',
                'modified' => 1548871662
            ],
            'content/blog/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/blog/heading' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/heading.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/blog/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/blog/query' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/query.yaml',
                'modified' => 1548871662
            ],
            'content/blog/read-more' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/read-more.yaml',
                'modified' => 1548871662
            ],
            'content/blog/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/blog/title.yaml',
                'modified' => 1548871662
            ],
            'content/general/wpautop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/general/wpautop.yaml',
                'modified' => 1548871662
            ],
            'content/page/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/page/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/page/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/page/title.yaml',
                'modified' => 1548871662
            ],
            'content/single/featured-image' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/featured-image.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-author' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-author.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-categories' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-categories.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-comments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-comments.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-date.yaml',
                'modified' => 1548871662
            ],
            'content/single/meta-tags' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/meta-tags.yaml',
                'modified' => 1548871662
            ],
            'content/single/title' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/content/single/title.yaml',
                'modified' => 1548871662
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/index.yaml',
                'modified' => 1552956792
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/layout.yaml',
                'modified' => 1552956792
            ],
            'particles/audioplayer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/audioplayer.yaml',
                'modified' => 1552954183
            ],
            'particles/blockcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/blockcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/branding' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1552954183
            ],
            'particles/calendar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/calendar.yaml',
                'modified' => 1552954183
            ],
            'particles/carousel' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/carousel.yaml',
                'modified' => 1552954183
            ],
            'particles/casestudies' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/casestudies.yaml',
                'modified' => 1552954183
            ],
            'particles/contactform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contactform.yaml',
                'modified' => 1548871662
            ],
            'particles/content' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/content.yaml',
                'modified' => 1552954183
            ],
            'particles/contentarray' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/contentarray.yaml',
                'modified' => 1552954183
            ],
            'particles/copyright' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1552954183
            ],
            'particles/custom' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/custom.yaml',
                'modified' => 1552954183
            ],
            'particles/date' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/date.yaml',
                'modified' => 1552954183
            ],
            'particles/gridcontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridcontent.yaml',
                'modified' => 1552954183
            ],
            'particles/gridstatistic' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/gridstatistic.yaml',
                'modified' => 1552954183
            ],
            'particles/imagegrid' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/imagegrid.yaml',
                'modified' => 1552954183
            ],
            'particles/infolist' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/infolist.yaml',
                'modified' => 1552954183
            ],
            'particles/login' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/login.yaml',
                'modified' => 1552954183
            ],
            'particles/loginform' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/loginform.yaml',
                'modified' => 1548871662
            ],
            'particles/logo' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1552954183
            ],
            'particles/menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/menu.yaml',
                'modified' => 1552954183
            ],
            'particles/messages' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/messages.yaml',
                'modified' => 1552954183
            ],
            'particles/mobile-menu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/mobile-menu.yaml',
                'modified' => 1552954183
            ],
            'particles/newsletter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/newsletter.yaml',
                'modified' => 1552954183
            ],
            'particles/panelslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/panelslider.yaml',
                'modified' => 1552954183
            ],
            'particles/popupmodule' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/popupmodule.yaml',
                'modified' => 1548871662
            ],
            'particles/position' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/position.yaml',
                'modified' => 1552954183
            ],
            'particles/pricingtable' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/pricingtable.yaml',
                'modified' => 1552954183
            ],
            'particles/search' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/search.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecontent' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecontent.yaml',
                'modified' => 1552954183
            ],
            'particles/simplecounter' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplecounter.yaml',
                'modified' => 1552954183
            ],
            'particles/simplemenu' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/simplemenu.yaml',
                'modified' => 1552954183
            ],
            'particles/social' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1552954183
            ],
            'particles/spacer' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/spacer.yaml',
                'modified' => 1552954183
            ],
            'particles/testimonials' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/testimonials.yaml',
                'modified' => 1552954183
            ],
            'particles/totop' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1552954183
            ],
            'particles/verticalslider' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/verticalslider.yaml',
                'modified' => 1552954183
            ],
            'particles/video' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/video.yaml',
                'modified' => 1552954183
            ],
            'particles/widget' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/particles/widget.yaml',
                'modified' => 1548871662
            ],
            'styles' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1553164097
            ]
        ],
        'user/themes/rt_aurora/config/default' => [
            'page/assets' => [
                'file' => 'user/themes/rt_aurora/config/default/page/assets.yaml',
                'modified' => 1548871658
            ],
            'page/body' => [
                'file' => 'user/themes/rt_aurora/config/default/page/body.yaml',
                'modified' => 1548871658
            ],
            'page/head' => [
                'file' => 'user/themes/rt_aurora/config/default/page/head.yaml',
                'modified' => 1548871658
            ],
            'particles/branding' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/branding.yaml',
                'modified' => 1548871658
            ],
            'particles/copyright' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/copyright.yaml',
                'modified' => 1548871658
            ],
            'particles/logo' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/logo.yaml',
                'modified' => 1548871658
            ],
            'particles/social' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/social.yaml',
                'modified' => 1548871658
            ],
            'particles/totop' => [
                'file' => 'user/themes/rt_aurora/config/default/particles/totop.yaml',
                'modified' => 1548871658
            ],
            'styles' => [
                'file' => 'user/themes/rt_aurora/config/default/styles.yaml',
                'modified' => 1548871658
            ]
        ]
    ],
    'data' => [
        'particles' => [
            'aos' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'duration' => '1000',
                'once' => true,
                'delay' => '0',
                'easing' => 'ease',
                'offset' => '120'
            ],
            'audioplayer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'nowplaying' => 'Now Playing',
                'scrollbar' => 'noscrollbar',
                'overflow' => '350'
            ],
            'blockcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'linktarget' => '_self'
            ],
            'calendar' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'class' => '',
                'title' => '',
                'eventsheader' => '',
                'events' => [
                    
                ]
            ],
            'carousel' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'displayitems' => 5,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'readmore' => ''
                    ]
                ]
            ],
            'casestudies' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'cols-2',
                'class' => '',
                'title' => '',
                'cases' => [
                    
                ]
            ],
            'fixedheader' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => true,
                'section' => '#g-navigation',
                'pinnedbg' => 'section',
                'custombg' => '#ffffff',
                'autohide' => 'enabled',
                'mobile' => 'disabled',
                'margin' => 115,
                'mobilemargin' => 115,
                'offset' => 100
            ],
            'gridcontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridcontent-2cols'
            ],
            'gridstatistic' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'readmoreclass' => 'button-3',
                'cols' => 'g-gridstatistic-2cols'
            ],
            'imagegrid' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-imagegrid-2cols',
                'layout' => 'g-imagegrid-standard'
            ],
            'infolist' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'cols' => 'g-1cols'
            ],
            'newsletter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'width' => 'g-newsletter-fullwidth',
                'layout' => 'g-newsletter-aside-compact',
                'style' => 'g-newsletter-rounded',
                'inputboxtext' => 'Your email address...',
                'buttontext' => 'Subscribe',
                'buttonclass' => 'button-xlarge button-block',
                'class' => '',
                'title' => '',
                'headtext' => '',
                'sidetext' => '',
                'buttonicon' => '',
                'uri' => ''
            ],
            'panelslider' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'source' => 'particle',
                'displayitems' => 5,
                'nav' => 'enabled',
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'pricingtable' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'columns' => 'g-pricingtable-4-col'
            ],
            'simplecontent' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'simplecounter' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => '1',
                'month' => '0',
                'class' => '',
                'title' => '',
                'desc' => '',
                'year' => '',
                'daytext' => '',
                'daystext' => '',
                'hourtext' => '',
                'hourstext' => '',
                'minutetext' => '',
                'minutestext' => '',
                'secondtext' => '',
                'secondstext' => ''
            ],
            'simplemenu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'title' => '',
                'class' => '',
                'menus' => [
                    
                ]
            ],
            'testimonials' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'animateOut' => 'fadeOut',
                'animateIn' => 'fadeIn',
                'nav' => 'disabled',
                'dots' => 'enabled',
                'loop' => 'enabled',
                'autoplay' => 'disabled',
                'pauseOnHover' => 'enabled',
                'class' => '',
                'title' => '',
                'autoplaySpeed' => '',
                'items' => [
                    
                ]
            ],
            'branding' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'content' => '&copy; 2018 by <a href="http://www.rockettheme.com/" title="RocketTheme" class="g-powered-by">RocketTheme</a>. All rights reserved.',
                'css' => [
                    'class' => ''
                ]
            ],
            'copyright' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'date' => [
                    'start' => '2007',
                    'end' => 'now'
                ],
                'owner' => 'Romero Trust',
                'link' => '',
                'target' => '_blank',
                'css' => [
                    'class' => ''
                ]
            ],
            'custom' => [
                'caching' => [
                    'type' => 'config_matches',
                    'values' => [
                        'twig' => '0',
                        'filter' => '0'
                    ]
                ],
                'enabled' => '1',
                'twig' => '0',
                'filter' => '0'
            ],
            'logo' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'target' => '_self',
                'link' => '1',
                'url' => '',
                'image' => '',
                'svg' => '',
                'text' => 'aurora',
                'class' => 'g-logo'
            ],
            'menu' => [
                'caching' => [
                    'type' => 'menu'
                ],
                'enabled' => '1',
                'menu' => 'mainmenu',
                'base' => '/',
                'startLevel' => '1',
                'maxLevels' => '0',
                'renderTitles' => '0',
                'hoverExpand' => '1',
                'mobileTarget' => '0',
                'forceTarget' => '0'
            ],
            'mobile-menu' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'social' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => ''
                ],
                'target' => '_blank',
                'display' => 'both',
                'title' => '',
                'items' => [
                    0 => [
                        'icon' => 'fa fa-facebook-square fa-fw',
                        'text' => '',
                        'link' => 'https://www.facebook.com/RocketTheme',
                        'name' => 'Facebook'
                    ],
                    1 => [
                        'icon' => 'fa fa-twitter-square fa-fw',
                        'text' => '',
                        'link' => 'http://www.twitter.com/rockettheme',
                        'name' => 'Twitter'
                    ]
                ]
            ],
            'spacer' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1'
            ],
            'totop' => [
                'caching' => [
                    'type' => 'static'
                ],
                'enabled' => '1',
                'css' => [
                    'class' => 'g-totop'
                ],
                'icon' => '',
                'content' => 'Top'
            ],
            'search' => [
                'enabled' => '1',
                'title' => 'Search Form'
            ],
            'verticalslider' => [
                'enabled' => '1',
                'source' => 'particle',
                'presets' => 'disabled',
                'speed' => 400,
                'auto' => 'disabled',
                'pause' => 2000,
                'loop' => 'disabled',
                'controls' => 'enabled',
                'pager' => 'enabled',
                'height' => 800,
                'mobileheight' => 600,
                'article' => [
                    'limit' => [
                        'total' => 5,
                        'start' => 0
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ],
                    'display' => [
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ]
                    ]
                ]
            ],
            'video' => [
                'enabled' => '1',
                'columns' => '1'
            ],
            'analytics' => [
                'enabled' => true,
                'ua' => [
                    'anonym' => false,
                    'ssl' => false,
                    'debug' => false
                ]
            ],
            'assets' => [
                'enabled' => true
            ],
            'breadcrumbs' => [
                'enabled' => true
            ],
            'content' => [
                'enabled' => '1'
            ],
            'contentarray' => [
                'enabled' => '1',
                'article' => [
                    'limit' => [
                        'total' => 2,
                        'columns' => 2,
                        'start' => 0
                    ],
                    'display' => [
                        'pagination_buttons' => '',
                        'image' => [
                            'enabled' => 'intro'
                        ],
                        'text' => [
                            'type' => 'intro',
                            'limit' => '',
                            'formatting' => 'text'
                        ],
                        'title' => [
                            'enabled' => 'show',
                            'limit' => ''
                        ],
                        'date' => [
                            'enabled' => 'published',
                            'format' => 'l, F d, Y'
                        ],
                        'read_more' => [
                            'enabled' => 'show',
                            'label' => '',
                            'css' => ''
                        ],
                        'author' => [
                            'enabled' => 'show'
                        ],
                        'category' => [
                            'enabled' => 'show',
                            'route' => ''
                        ]
                    ],
                    'sort' => [
                        'orderby' => 'default',
                        'ordering' => 'asc'
                    ]
                ],
                'css' => [
                    'class' => ''
                ],
                'extra' => [
                    
                ]
            ],
            'date' => [
                'enabled' => '1',
                'css' => [
                    'class' => 'date'
                ],
                'date' => [
                    'formats' => 'l, F d, Y'
                ]
            ],
            'feed' => [
                'enabled' => true
            ],
            'frameworks' => [
                'enabled' => true,
                'jquery' => [
                    'enabled' => 0,
                    'ui_core' => 0,
                    'ui_sortable' => 0
                ],
                'bootstrap2' => [
                    'enabled' => 0
                ],
                'bootstrap3' => [
                    'enabled' => 0
                ],
                'mootools' => [
                    'enabled' => 0,
                    'more' => 0
                ]
            ],
            'langswitcher' => [
                'enabled' => true
            ],
            'lightcase' => [
                'enabled' => true
            ],
            'login' => [
                'enabled' => '1'
            ],
            'messages' => [
                'enabled' => '1'
            ],
            'position' => [
                'enabled' => '1',
                'chrome' => ''
            ],
            'contactform' => [
                'enabled' => '1',
                'class' => '',
                'header' => '',
                'email' => '',
                'email_from' => '',
                'recaptcha' => [
                    'enabled' => '0',
                    'sitekey' => '',
                    'secretkey' => ''
                ]
            ],
            'loginform' => [
                'enabled' => '1',
                'class' => '',
                'title' => '<h3 class="g-title">Login</h3>',
                'greeting' => 'Hi, %s',
                'pretext' => '',
                'posttext' => ''
            ],
            'popupmodule' => [
                'enabled' => '1'
            ],
            'widget' => [
                'enabled' => '1',
                'chrome' => ''
            ]
        ],
        'styles' => [
            'above' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'accent' => [
                'color-1' => '#8a7224',
                'color-2' => '#c7b31c',
                'color-3' => '#f7f3d2'
            ],
            'base' => [
                'background' => '#ffffff',
                'text-color' => '#666666',
                'text-active-color' => '#31a594'
            ],
            'bottom' => [
                'background' => '#0d0d0d',
                'text-color' => '#ffffff'
            ],
            'breakpoints' => [
                'large-desktop-container' => '75rem',
                'desktop-container' => '60rem',
                'tablet-container' => '51rem',
                'large-mobile-container' => '30rem',
                'mobile-menu-breakpoint' => '51rem'
            ],
            'copyright' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'expanded' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'extension' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'feature' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'font' => [
                'family-default' => 'muli'
            ],
            'footer' => [
                'background' => '#f4f5e7',
                'text-color' => '#333333'
            ],
            'header' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'main' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'menu' => [
                'animation' => 'g-fade'
            ],
            'menustyle' => [
                'text-color' => '#2b2323',
                'text-color-alt' => '#8e9dab',
                'text-color-active' => '#8a7224',
                'background-active' => 'rgba(255,255,255, 0)',
                'sublevel-text-color' => '#000000',
                'sublevel-text-color-active' => '#31a594',
                'sublevel-background' => '#ffffff',
                'sublevel-background-active' => '#31a594'
            ],
            'navigation' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'offcanvas' => [
                'background' => '#ff4b64',
                'text-color' => '#ffffff',
                'toggle-color' => '#ffffff',
                'width' => '10rem',
                'toggle-visibility' => '1'
            ],
            'showcase' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'slideshow' => [
                'background' => '#0d0d0d',
                'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg',
                'text-color' => '#ffffff'
            ],
            'top' => [
                'background' => '#ffffff',
                'text-color' => '#2b2323'
            ],
            'utility' => [
                'background' => '#ffffff',
                'text-color' => '#666666'
            ],
            'preset1' => [
                'image' => 'gantry-admin://images/preset1.png',
                'description' => 'Preset 1',
                'colors' => [
                    0 => '#ff4b64',
                    1 => '#31a594'
                ],
                'styles' => [
                    'base' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666',
                        'text-active-color' => '#31a594'
                    ],
                    'accent' => [
                        'color-1' => '#ff4b64',
                        'color-2' => '#31a594',
                        'color-3' => '#508b70'
                    ],
                    'menustyle' => [
                        'text-color' => '#ffffff',
                        'text-color-alt' => '#8e9dab',
                        'text-color-active' => '#ffffff',
                        'background-active' => 'rgba(255,255,255, 0)',
                        'sublevel-text-color' => '#000000',
                        'sublevel-text-color-active' => '#31a594',
                        'sublevel-background' => '#ffffff',
                        'sublevel-background-active' => '#31a594'
                    ],
                    'font' => [
                        'family-default' => 'muli, Helvetica, Tahoma, Geneva, Arial, sans-serif'
                    ],
                    'top' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'navigation' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'slideshow' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff',
                        'background-image' => 'gantry-media://backgrounds/slideshow/img-01.jpg'
                    ],
                    'header' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'above' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'feature' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'showcase' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'utility' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'main' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'expanded' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'extension' => [
                        'background' => '#ffffff',
                        'text-color' => '#666666'
                    ],
                    'bottom' => [
                        'background' => '#0d0d0d',
                        'text-color' => '#ffffff'
                    ],
                    'footer' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'copyright' => [
                        'background' => '#eaf6f4',
                        'text-color' => '#50818b'
                    ],
                    'offcanvas' => [
                        'background' => '#ff4b64',
                        'text-color' => '#ffffff',
                        'toggle-color' => '#ffffff'
                    ],
                    'breakpoints' => [
                        'large-desktop-container' => '75rem',
                        'desktop-container' => '60rem',
                        'tablet-container' => '51rem',
                        'large-mobile-container' => '30rem',
                        'mobile-menu-breakpoint' => '51rem'
                    ]
                ]
            ],
            'preset' => 'preset1'
        ],
        'page' => [
            'body' => [
                'attribs' => [
                    'class' => 'gantry',
                    'id' => '',
                    'extra' => [
                        
                    ]
                ],
                'layout' => [
                    'sections' => '0'
                ],
                'doctype' => 'html',
                'body_top' => '',
                'body_bottom' => ''
            ],
            'assets' => [
                'favicon' => '',
                'touchicon' => '',
                'css' => [
                    
                ],
                'javascript' => [
                    
                ]
            ],
            'head' => [
                'meta' => [
                    
                ],
                'head_bottom' => '',
                'atoms' => [
                    0 => [
                        'id' => 'assets-3609',
                        'type' => 'assets',
                        'title' => 'Custom CSS / JS',
                        'attributes' => [
                            'enabled' => '1',
                            'css' => [
                                0 => [
                                    'location' => 'gantry-assets://css/animate.css',
                                    'inline' => '',
                                    'extra' => [
                                        
                                    ],
                                    'name' => 'Animate CSS'
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'type' => 'frameworks',
                        'title' => 'JavaScript Frameworks',
                        'attributes' => [
                            'enabled' => '1',
                            'jquery' => [
                                'enabled' => '1',
                                'ui_core' => '1',
                                'ui_sortable' => '0'
                            ],
                            'bootstrap' => [
                                'enabled' => '0'
                            ],
                            'mootools' => [
                                'enabled' => '0',
                                'more' => '0'
                            ]
                        ],
                        'id' => 'frameworks-1582'
                    ]
                ]
            ]
        ],
        'pages' => [
            'blog_item' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'default',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ]
                ],
                'name' => 'default'
            ],
            'blog_list' => [
                'header' => [
                    'process' => [
                        'markdown' => true,
                        'twig' => false
                    ],
                    'child_type' => 'blog_item',
                    'admin' => [
                        'children_display_order' => 'collection'
                    ],
                    'content' => [
                        'items' => '@self.children',
                        'leading' => 0,
                        'columns' => 2,
                        'limit' => 5,
                        'order' => [
                            'by' => 'date',
                            'dir' => 'desc'
                        ],
                        'show_date' => 0,
                        'pagination' => 1,
                        'url_taxonomy_filters' => 1
                    ]
                ],
                'name' => 'default'
            ],
            'modular' => [
                'features' => [
                    'name' => 'modular/features',
                    'header' => [
                        'template' => 'modular/features'
                    ]
                ],
                'showcase' => [
                    'name' => 'modular/showcase',
                    'header' => [
                        'template' => 'modular/showcase',
                        'buttons' => [
                            'primary' => 1
                        ]
                    ]
                ],
                'text' => [
                    'name' => 'modular/text',
                    'header' => [
                        'template' => 'modular/text',
                        'image_align' => 'left'
                    ]
                ]
            ]
        ],
        'content' => [
            'archive' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '1',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ],
            'blog' => [
                'content' => [
                    'type' => 'content',
                    'gexcerpt-length' => '50',
                    'columns' => 'size-50'
                ],
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'heading' => [
                    'enabled' => '0',
                    'text' => ''
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '0',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'query' => [
                    'categories' => [
                        'include' => '',
                        'exclude' => ''
                    ]
                ],
                'read-more' => [
                    'label' => 'Read More',
                    'mode' => 'auto'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '1'
                ]
            ],
            'general' => [
                'wpautop' => [
                    'enabled' => '1'
                ]
            ],
            'page' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-date' => [
                    'enabled' => '0',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'title' => [
                    'enabled' => '0',
                    'link' => '0'
                ]
            ],
            'single' => [
                'featured-image' => [
                    'enabled' => '1',
                    'width' => '',
                    'height' => '',
                    'position' => 'none'
                ],
                'meta-author' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Written by'
                ],
                'meta-categories' => [
                    'enabled' => '1',
                    'link' => '1',
                    'prefix' => 'Categories:'
                ],
                'meta-comments' => [
                    'enabled' => '1',
                    'link' => '0',
                    'prefix' => ''
                ],
                'meta-date' => [
                    'enabled' => '1',
                    'link' => '1',
                    'format' => 'j F Y',
                    'prefix' => 'Published:'
                ],
                'meta-tags' => [
                    'enabled' => '0',
                    'link' => '1',
                    'prefix' => 'Tags:'
                ],
                'title' => [
                    'enabled' => '1',
                    'link' => '0'
                ]
            ]
        ],
        'index' => [
            'name' => 'default',
            'timestamp' => 1552956792,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1509009075
            ],
            'positions' => [
                'sidebar' => 'Sidebar',
                'aside' => 'Aside'
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'showcase' => 'Showcase',
                'utility' => 'Utility',
                'feature' => 'Feature',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'sidebar' => 'Sidebar',
                'aside' => 'Aside',
                'aside-footer' => 'Footer Aside',
                'mainbar' => 'Main',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-9828' => 'System Messages'
                ],
                'logo' => [
                    'logo-5992' => 'Logo',
                    'logo-3146' => 'SVG Pattern'
                ],
                'menu' => [
                    'menu-2350' => 'Menu'
                ],
                'position' => [
                    'position-position-9414' => 'Sidebar',
                    'position-position-8807' => 'Aside'
                ],
                'simplecontent' => [
                    'simplecontent-5244' => 'contact module',
                    'simplecontent-8236' => 'Simple Content',
                    'simplecontent-4905' => 'Simple Content'
                ],
                'branding' => [
                    'branding-1506' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-7951' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'default',
                'timestamp' => 1509009075
            ],
            'layout' => [
                '/top/' => [
                    0 => [
                        0 => 'system-messages-9828'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'logo-5992 25',
                        1 => 'menu-2350 75'
                    ],
                    1 => [
                        0 => 'logo-3146'
                    ]
                ],
                '/slideshow/' => [
                    
                ],
                '/header/' => [
                    
                ],
                '/above/' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/feature/' => [
                    
                ],
                '/container-main/' => [
                    0 => [
                        0 => [
                            'sidebar 26' => [
                                0 => [
                                    0 => 'position-position-9414'
                                ]
                            ]
                        ],
                        1 => [
                            'mainbar 49' => [
                                
                            ]
                        ],
                        2 => [
                            'aside 25' => [
                                0 => [
                                    0 => 'position-position-8807'
                                ]
                            ]
                        ]
                    ]
                ],
                '/expanded/' => [
                    
                ],
                '/extension/' => [
                    
                ],
                '/bottom/' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            '/sidebar-footer/ 40' => [
                                0 => [
                                    0 => 'simplecontent-5244'
                                ]
                            ]
                        ],
                        1 => [
                            'mainbar-footer 30' => [
                                0 => [
                                    0 => 'simplecontent-8236'
                                ]
                            ]
                        ],
                        2 => [
                            '/aside-footer/ 30' => [
                                0 => [
                                    0 => 'simplecontent-4905'
                                ]
                            ]
                        ]
                    ]
                ],
                '/copyright/' => [
                    0 => [
                        0 => 'branding-1506'
                    ]
                ],
                'offcanvas' => [
                    0 => [
                        0 => 'mobile-menu-7951'
                    ]
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-top section-horizontal-paddings'
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-fluid-navigation'
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-slideshow section-horizontal-paddings'
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-header section-horizontal-paddings section-vertical-paddings-small'
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-above section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-showcase section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-utility section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-feature section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'sidebar' => [
                    'type' => 'section',
                    'subtype' => 'aside',
                    'attributes' => [
                        'class' => ''
                    ]
                ],
                'mainbar' => [
                    'type' => 'section',
                    'subtype' => 'main',
                    'title' => 'Main',
                    'attributes' => [
                        'class' => 'section-horizontal-paddings'
                    ]
                ],
                'aside' => [
                    'attributes' => [
                        'class' => ''
                    ]
                ],
                'container-main' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-main',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-expanded section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-extension section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-default-bottom section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => ''
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main'
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => ''
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => ''
                    ]
                ],
                'offcanvas' => [
                    'attributes' => [
                        'swipe' => '0'
                    ]
                ]
            ],
            'content' => [
                'logo-5992' => [
                    'attributes' => [
                        'image' => 'gantry-media://RCET_Logo.jpg'
                    ]
                ],
                'logo-3146' => [
                    'title' => 'SVG Pattern',
                    'attributes' => [
                        'link' => '0',
                        'svg' => '<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 2560 431">
<path
 d="M2141.000,1190.000 L2561.000,1190.000 L2561.000,1250.000 L959.000,1250.000 L216.000,1250.000 L213.000,1250.000 C196.432,1250.000 183.000,1236.569 183.000,1220.000 C183.000,1203.432 196.432,1190.000 213.000,1190.000 L216.000,1190.000 L466.000,1190.000 C482.569,1190.000 496.000,1176.568 496.000,1160.000 C496.000,1143.431 482.569,1130.000 466.000,1130.000 L1.134,1130.000 L1.134,1190.000 L1.134,1250.000 L1.000,1250.000 L1.000,950.000 L407.000,950.000 L1502.000,950.000 L1931.000,950.000 C1947.569,950.000 1961.000,963.431 1961.000,980.000 C1961.000,996.568 1947.569,1010.000 1931.000,1010.000 L1541.000,1010.000 C1524.431,1010.000 1511.000,1023.431 1511.000,1040.000 C1511.000,1056.568 1524.431,1070.000 1541.000,1070.000 L2561.000,1070.000 L2561.000,1130.000 L2141.000,1130.000 C2124.431,1130.000 2111.000,1143.431 2111.000,1160.000 C2111.000,1176.568 2124.431,1190.000 2141.000,1190.000 ZM602.000,1010.000 L407.000,1010.000 L82.000,1010.000 C65.431,1010.000 52.000,1023.431 52.000,1040.000 C52.000,1056.568 65.431,1070.000 82.000,1070.000 L602.000,1070.000 C618.569,1070.000 632.000,1056.568 632.000,1040.000 C632.000,1023.431 618.569,1010.000 602.000,1010.000 ZM1139.000,1070.000 L760.000,1070.000 C743.431,1070.000 730.000,1083.432 730.000,1100.000 C730.000,1116.569 743.431,1130.000 760.000,1130.000 L1139.000,1130.000 C1155.568,1130.000 1169.000,1116.569 1169.000,1100.000 C1169.000,1083.432 1155.568,1070.000 1139.000,1070.000 ZM1906.000,1130.000 L1353.000,1130.000 C1336.432,1130.000 1323.000,1143.431 1323.000,1160.000 C1323.000,1176.568 1336.432,1190.000 1353.000,1190.000 L1906.000,1190.000 C1922.569,1190.000 1936.000,1176.568 1936.000,1160.000 C1936.000,1143.431 1922.569,1130.000 1906.000,1130.000 ZM2560.381,950.000 L2561.000,950.000 L2561.000,1010.000 L2560.381,1010.000 L2560.381,950.000 ZM2560.491,360.000 L2560.491,420.000 L2477.000,420.000 C2460.431,420.000 2447.000,406.569 2447.000,390.000 C2447.000,373.432 2460.431,360.000 2477.000,360.000 L2560.491,360.000 ZM1161.000,360.000 C1177.569,360.000 1191.000,373.432 1191.000,390.000 C1191.000,406.569 1177.569,420.000 1161.000,420.000 L993.000,420.000 C976.432,420.000 963.000,406.569 963.000,390.000 C963.000,373.432 976.432,360.000 993.000,360.000 L1161.000,360.000 ZM223.000,360.000 C239.569,360.000 253.000,373.432 253.000,390.000 C253.000,406.569 239.569,420.000 223.000,420.000 L185.000,420.000 C168.431,420.000 155.000,406.569 155.000,390.000 C155.000,373.432 168.431,360.000 185.000,360.000 L223.000,360.000 ZM2560.585,240.000 L2560.873,240.000 L2560.873,180.000 L2096.000,180.000 C2079.431,180.000 2066.000,193.431 2066.000,210.000 C2066.000,226.568 2079.431,240.000 2096.000,240.000 L2346.000,240.000 L2349.000,240.000 C2365.568,240.000 2379.000,253.432 2379.000,270.000 C2379.000,286.569 2365.568,300.000 2349.000,300.000 L2346.000,300.000 L1603.000,300.000 L1.000,300.000 L1.000,60.000 L1.000,60.000 L1.000,120.000 L1021.000,120.000 C1037.569,120.000 1051.000,106.569 1051.000,90.000 C1051.000,73.432 1037.569,60.000 1021.000,60.000 L631.000,60.000 C614.431,60.000 601.000,46.568 601.000,30.000 C601.000,13.431 614.431,-0.000 631.000,-0.000 L1060.000,-0.000 L2155.000,-0.000 L2561.000,-0.000 L2561.000,300.000 L2560.585,300.000 L2560.585,240.000 ZM421.000,180.000 L1.465,180.000 L1.465,240.000 L421.000,240.000 C437.569,240.000 451.000,226.568 451.000,210.000 C451.000,193.431 437.569,180.000 421.000,180.000 ZM1209.000,180.000 L656.000,180.000 C639.431,180.000 626.000,193.431 626.000,210.000 C626.000,226.568 639.431,240.000 656.000,240.000 L1209.000,240.000 C1225.568,240.000 1239.000,226.568 1239.000,210.000 C1239.000,193.431 1225.568,180.000 1209.000,180.000 ZM1802.000,120.000 L1423.000,120.000 C1406.432,120.000 1393.000,133.431 1393.000,150.000 C1393.000,166.569 1406.432,180.000 1423.000,180.000 L1802.000,180.000 C1818.569,180.000 1832.000,166.569 1832.000,150.000 C1832.000,133.431 1818.569,120.000 1802.000,120.000 ZM2480.000,60.000 L2155.000,60.000 L1960.000,60.000 C1943.431,60.000 1930.000,73.432 1930.000,90.000 C1930.000,106.569 1943.431,120.000 1960.000,120.000 L2480.000,120.000 C2496.569,120.000 2510.000,106.569 2510.000,90.000 C2510.000,73.432 2496.569,60.000 2480.000,60.000 ZM115.000,1340.000 C115.000,1356.568 101.569,1370.000 85.000,1370.000 L0.401,1370.000 L0.401,1310.000 L85.000,1310.000 C101.569,1310.000 115.000,1323.431 115.000,1340.000 ZM1401.000,1310.000 L1569.000,1310.000 C1585.568,1310.000 1599.000,1323.431 1599.000,1340.000 C1599.000,1356.568 1585.568,1370.000 1569.000,1370.000 L1401.000,1370.000 C1384.431,1370.000 1371.000,1356.568 1371.000,1340.000 C1371.000,1323.431 1384.431,1310.000 1401.000,1310.000 ZM2339.000,1310.000 L2377.000,1310.000 C2393.569,1310.000 2407.000,1323.431 2407.000,1340.000 C2407.000,1356.568 2393.569,1370.000 2377.000,1370.000 L2339.000,1370.000 C2322.431,1370.000 2309.000,1356.568 2309.000,1340.000 C2309.000,1323.431 2322.431,1310.000 2339.000,1310.000 Z"/></path>
</svg>',
                        'text' => '',
                        'class' => ''
                    ],
                    'block' => [
                        'class' => 'svg-pattern-default'
                    ]
                ],
                'position-position-9414' => [
                    'title' => 'Sidebar',
                    'attributes' => [
                        'key' => 'sidebar'
                    ]
                ],
                'position-position-8807' => [
                    'title' => 'Aside',
                    'attributes' => [
                        'key' => 'aside'
                    ]
                ],
                'simplecontent-5244' => [
                    'title' => 'contact module',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'contact information',
                                'author' => '',
                                'leading_content' => 'You can find out more about the Romero Trust by contacting either of our schools:',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'contact information'
                            ]
                        ]
                    ],
                    'block' => [
                        'class' => 'nopaddingbottom'
                    ]
                ],
                'simplecontent-8236' => [
                    'title' => 'Simple Content',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'St John’s School & Sixth Form College – A Catholic Academy ',
                                'author' => '',
                                'leading_content' => 'w: <a href="https://www.stjohnsrc.org.uk">www.stjohnsrc.org.uk</a><br />
e: <a href="mailto:staff@stjohnsrc.org.uk">staff@stjohnsrc.org.uk</a></br />
t: 01388 603246',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'St John’s School & Sixth Form College – A Catholic Academy'
                            ]
                        ]
                    ]
                ],
                'simplecontent-4905' => [
                    'title' => 'Simple Content',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'St Joseph’s Primary School – A Catholic Academy',
                                'author' => '',
                                'leading_content' => 'w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a><br />
t: +44 (0)1325 300 337<br />
e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a>',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'St Josephs\'s'
                            ]
                        ]
                    ]
                ],
                'branding-1506' => [
                    'attributes' => [
                        'content' => '&copy; 2019 by Romero Trust. All rights reserved.'
                    ],
                    'block' => [
                        'variations' => 'center'
                    ]
                ]
            ]
        ]
    ]
];
