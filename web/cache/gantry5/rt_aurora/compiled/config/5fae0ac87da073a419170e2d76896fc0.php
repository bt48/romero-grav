<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1552957017,
    'checksum' => '496ae4c63d7a8b0a308785a9962222e8',
    'files' => [
        0 => [
            'home' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/home/assignments.yaml',
                'modified' => 1548871664
            ],
            'page_-_st_johns' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_johns/assignments.yaml',
                'modified' => 1552956224
            ],
            'pages_-_about_us' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_about_us/assignments.yaml',
                'modified' => 1552952309
            ],
            'layouts_-_left_sidebar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_left_sidebar/assignments.yaml',
                'modified' => 1548871664
            ],
            'layouts_-_right_sidebar' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_right_sidebar/assignments.yaml',
                'modified' => 1548871664
            ],
            'pages_-_contact' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_contact/assignments.yaml',
                'modified' => 1552955721
            ],
            'pages_-_offline' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/pages_-_offline/assignments.yaml',
                'modified' => 1548871664
            ],
            'layouts_-_full_width' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/layouts_-_full_width/assignments.yaml',
                'modified' => 1552957013
            ],
            'page_-_st_josephs' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/page_-_st_josephs/assignments.yaml',
                'modified' => 1552956972
            ]
        ]
    ],
    'data' => [
        'home' => [
            'page' => [
                0 => [
                    'home' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'page_-_st_johns' => [
            'page' => [
                0 => [
                    'st-johns' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'pages_-_about_us' => [
            'page' => [
                0 => [
                    'about-us' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'layouts_-_left_sidebar' => [
            'page' => [
                0 => [
                    'sample-layouts/left-sidebar' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'layouts_-_right_sidebar' => [
            'page' => [
                0 => [
                    'sample-layouts/right-sidebar' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'pages_-_contact' => [
            'page' => [
                0 => [
                    'contact' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'pages_-_offline' => [
            'page' => [
                0 => [
                    'pages/offline' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'layouts_-_full_width' => [
            'page' => [
                
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'page_-_st_josephs' => [
            'page' => [
                0 => [
                    'st-josephs' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                0 => [
                    'page' => true
                ]
            ]
        ]
    ]
];
