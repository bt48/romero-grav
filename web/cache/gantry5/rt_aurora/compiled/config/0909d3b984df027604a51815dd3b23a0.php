<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledConfig',
    'timestamp' => 1552957220,
    'checksum' => '6b31c031a8182b06a7ee385d5191a0e1',
    'files' => [
        'user/data/gantry5/themes/rt_aurora/config/home' => [
            'assignments' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/home/assignments.yaml',
                'modified' => 1548871664
            ],
            'index' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/home/index.yaml',
                'modified' => 1552956918
            ],
            'layout' => [
                'file' => 'user/data/gantry5/themes/rt_aurora/config/home/layout.yaml',
                'modified' => 1552956918
            ]
        ]
    ],
    'data' => [
        'assignments' => [
            'page' => [
                0 => [
                    'home' => true
                ]
            ],
            'language' => [
                
            ],
            'type' => [
                
            ]
        ],
        'index' => [
            'name' => 'home',
            'timestamp' => 1552956918,
            'version' => 7,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'home_-_particles',
                'timestamp' => 1508851818
            ],
            'positions' => [
                
            ],
            'sections' => [
                'top' => 'Top',
                'navigation' => 'Navigation',
                'slideshow' => 'Slideshow',
                'above' => 'Above',
                'feature' => 'Feature',
                'showcase' => 'Showcase',
                'expanded' => 'Expanded',
                'extension' => 'Extension',
                'utility' => 'Utility',
                'bottom' => 'Bottom',
                'sidebar-footer' => 'Footer Sidebar',
                'mainbar-footer' => 'Footer Main',
                'copyright' => 'Copyright',
                'header' => 'Header',
                'aside-footer' => 'Footer Aside',
                'offcanvas' => 'Offcanvas'
            ],
            'particles' => [
                'messages' => [
                    'system-messages-1675' => 'System Messages'
                ],
                'logo' => [
                    'logo-4214' => 'Logo'
                ],
                'menu' => [
                    'menu-7163' => 'Menu'
                ],
                'verticalslider' => [
                    'verticalslider-5007' => 'Vertical Slider'
                ],
                'simplecontent' => [
                    'simplecontent-1596' => 'Intro Text',
                    'simplecontent-4215' => 'Intro Lead',
                    'simplecontent-1208' => 'contact module',
                    'simplecontent-5935' => 'Simple Content',
                    'simplecontent-4318' => 'Simple Content'
                ],
                'branding' => [
                    'branding-8906' => 'Branding'
                ],
                'mobile-menu' => [
                    'mobile-menu-9619' => 'Mobile-menu'
                ]
            ],
            'inherit' => [
                'default' => [
                    'top' => 'top',
                    'system-messages-1675' => 'system-messages-9828',
                    'menu-7163' => 'menu-2350',
                    'sidebar-footer' => 'sidebar-footer',
                    'simplecontent-1208' => 'simplecontent-5244',
                    'simplecontent-5935' => 'simplecontent-8236',
                    'aside-footer' => 'aside-footer',
                    'simplecontent-4318' => 'simplecontent-4905',
                    'copyright' => 'copyright',
                    'branding-8906' => 'branding-1506',
                    'offcanvas' => 'offcanvas',
                    'mobile-menu-9619' => 'mobile-menu-7951'
                ]
            ]
        ],
        'layout' => [
            'version' => 2,
            'preset' => [
                'image' => 'gantry-admin://images/layouts/default.png',
                'name' => 'home_-_particles',
                'timestamp' => 1508851818
            ],
            'layout' => [
                'top' => [
                    0 => [
                        0 => 'system-messages-1675'
                    ]
                ],
                '/navigation/' => [
                    0 => [
                        0 => 'logo-4214 25',
                        1 => 'menu-7163 75'
                    ]
                ],
                '/slideshow/' => [
                    0 => [
                        0 => 'verticalslider-5007'
                    ]
                ],
                '/header/' => [
                    0 => [
                        0 => 'simplecontent-1596 60',
                        1 => 'simplecontent-4215 40'
                    ]
                ],
                '/above/' => [
                    
                ],
                '/feature/' => [
                    
                ],
                '/showcase/' => [
                    
                ],
                'expanded' => [
                    
                ],
                '/extension/' => [
                    
                ],
                '/utility/' => [
                    
                ],
                '/bottom/' => [
                    
                ],
                '/container-footer/' => [
                    0 => [
                        0 => [
                            'sidebar-footer 30' => [
                                
                            ]
                        ],
                        1 => [
                            'mainbar-footer 40' => [
                                0 => [
                                    0 => 'simplecontent-5935'
                                ]
                            ]
                        ],
                        2 => [
                            'aside-footer 30' => [
                                
                            ]
                        ]
                    ]
                ],
                'copyright' => [
                    
                ],
                'offcanvas' => [
                    
                ]
            ],
            'structure' => [
                'top' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes'
                        ]
                    ]
                ],
                'navigation' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'g-fluid-navigation g-absolute-navigation fp-navigation',
                        'variations' => ''
                    ]
                ],
                'slideshow' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '2',
                        'class' => 'fp-slideshow',
                        'variations' => ''
                    ]
                ],
                'header' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-header section-horizontal-paddings section-vertical-paddings-small'
                    ]
                ],
                'above' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-above'
                    ]
                ],
                'feature' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-feature section-horizontal-paddings'
                    ]
                ],
                'showcase' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-showcase'
                    ]
                ],
                'expanded' => [
                    'type' => 'section',
                    'attributes' => [
                        'class' => 'section-horizontal-paddings'
                    ]
                ],
                'extension' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-extension section-horizontal-paddings section-vertical-paddings'
                    ]
                ],
                'utility' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-vertical-paddings'
                    ]
                ],
                'bottom' => [
                    'type' => 'section',
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'fp-bottom section-vertical-paddings section-horizontal-paddings'
                    ]
                ],
                'sidebar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Sidebar',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'mainbar-footer' => [
                    'type' => 'section',
                    'title' => 'Footer Main',
                    'attributes' => [
                        'class' => '',
                        'variations' => ''
                    ]
                ],
                'aside-footer' => [
                    'title' => 'Footer Aside',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block',
                            2 => 'children'
                        ]
                    ]
                ],
                'container-footer' => [
                    'attributes' => [
                        'boxed' => '3',
                        'class' => 'section-horizontal-paddings section-vertical-paddings nopaddingbottom fp-footer',
                        'extra' => [
                            
                        ]
                    ]
                ],
                'copyright' => [
                    'type' => 'section',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ],
                'offcanvas' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'children'
                        ]
                    ]
                ]
            ],
            'content' => [
                'system-messages-1675' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block'
                        ],
                        'particle' => 'system-messages-9828'
                    ]
                ],
                'logo-4214' => [
                    'attributes' => [
                        'image' => 'gantry-media://RCET_Logo.jpg'
                    ]
                ],
                'menu-7163' => [
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block'
                        ],
                        'particle' => 'menu-2350'
                    ]
                ],
                'verticalslider-5007' => [
                    'title' => 'Vertical Slider',
                    'attributes' => [
                        'presets' => 'enabled',
                        'auto' => 'enabled',
                        'pause' => '8000',
                        'loop' => 'enabled',
                        'controls' => 'enabled',
                        'pager' => 'enabled',
                        'height' => '600',
                        'mobileheight' => '400',
                        'items' => [
                            0 => [
                                'image' => 'gantry-media://pupils.jpg',
                                'small_title' => 'A Learning Community Guided by Gospel Values',
                                'desc' => 'At St John’s we pride ourselves on being A Learning Community Guided by Gospel Values, where each individual is valued and nurtured to grow in confidence and to embrace their God-given talents. ',
                                'buttontext' => 'Find out more',
                                'buttonlink' => '/st-johns',
                                'buttontarget' => '_self',
                                'buttonclass' => '',
                                'title' => 'St John’s School & Sixth Form College – A Catholic Academy'
                            ],
                            1 => [
                                'image' => 'gantry-media://fc3-1140x342.png',
                                'small_title' => 'Welcome to St. Joseph\'s Primary School',
                                'desc' => 'We provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.',
                                'buttontext' => 'Find out more',
                                'buttonlink' => '#',
                                'buttontarget' => '_self',
                                'buttonclass' => '',
                                'title' => 'Welcome to St. Joseph\'s Primary School'
                            ]
                        ],
                        'article' => [
                            'filter' => [
                                'categories' => ''
                            ],
                            'limit' => [
                                'total' => '5',
                                'start' => '0'
                            ],
                            'sort' => [
                                'orderby' => 'default',
                                'ordering' => 'asc'
                            ]
                        ]
                    ],
                    'block' => [
                        'variations' => 'nomarginall nopaddingall'
                    ]
                ],
                'simplecontent-1596' => [
                    'title' => 'Intro Text',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => 'Deep-rooted Gospel values are very much at the heart of the Trust’s vision.',
                                'author' => '',
                                'leading_content' => '',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Aurora combines elegance and simplicity in one professional template.'
                            ]
                        ]
                    ]
                ],
                'simplecontent-4215' => [
                    'title' => 'Intro Lead',
                    'attributes' => [
                        'class' => '',
                        'title' => '',
                        'items' => [
                            0 => [
                                'layout' => 'standard',
                                'created_date' => '',
                                'content_title' => '',
                                'author' => '',
                                'leading_content' => 'The Romero Trust serves the Catholic and wider communities of South-West Durham, aiming to provide the best education and opportunities for young people of the area.',
                                'main_content' => '',
                                'readmore_label' => '',
                                'readmore_link' => '',
                                'readmore_class' => '',
                                'readmore_target' => '_self',
                                'title' => 'Intro Lead'
                            ]
                        ]
                    ]
                ],
                'simplecontent-5935' => [
                    'title' => 'Simple Content',
                    'inherit' => [
                        'outline' => 'default',
                        'include' => [
                            0 => 'attributes',
                            1 => 'block'
                        ],
                        'particle' => 'simplecontent-8236'
                    ]
                ]
            ]
        ]
    ]
];
