<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903f52f0a9c3.99673748',
    'content' => '<div id="simplecontent-9044-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                                                
                                        <div class="g-simplecontent-item-main-content"><p>Deep-rooted Gospel values are very much at the heart of the Trust’s vision, which serves the Catholic and wider communities of South-West Durham, aiming to provide the best education and opportunities for young people of the area. The Trust’s aim is to develop and grow strong partnerships so that every child is valued, supported and encouraged to develop their God-given talents in order to reach their full potential.</p>
<p>By working in partnership, we are stronger together and are committed to prioritising the welfare and well-being of our children. With outstanding pastoral care and greater access to specialist support, guidance and SEND expertise, we aim to provide a caring environment where children are nurtured and encouraged to flourish.</p>
<p>As a Trust we will continue to work with other schools and educational organisations on local and national initiatives, which will provide greater access to further opportunities and support for the benefit of children, staff and the individual schools involved.</p>
<p>The Romero Catholic Education Trust was established in December 2016 by St John’s and the Trust welcomed St Joseph’s Primary School, Newton Aycliffe, into the Trust on 1 April 2018. Currently, the Romero Catholic Education Trust has overarching accountability and governance of:<p>
<ul>
<li><a href="/st-johns">St John’s School & Sixth Form College – A Catholic Academy</a></li>
<li>St Joseph’s Primary School – A Catholic Academy
</ul>
<p>Being members of the Trust enhances our strong commitment to work collaboratively to remove barriers to learning, share expertise, resources and specialist services for the educational benefit of all children and staff. </p></div>
                                    </div>
            
        </div>
    </div>
            </div>'
];
