<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c9041d8e78b66.59064803',
    'content' => '<div id="simplecontent-5107-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                                                
                                        <div class="g-simplecontent-item-main-content"><h2>At St John’s we pride ourselves on being A Learning Community Guided by Gospel Values, where each individual is valued and nurtured to grow in confidence and to embrace their God-given talents.  As a supportive and inclusive school, we respond to the changing needs of all our students across the age and ability range and we are committed to the highest standards of teaching and learning.</h2>
<p>By providing a wealth of exciting experiences and opportunities, we encourage our young people to be aspirational, fulfilling their potential and realising their dreams with determination and enthusiasm.</p>
<p>From sporting events at regional, national and international levels to Arts projects, foreign exchanges and residential trips, we strive to provide many opportunities for our young people to grow in confidence and to develop their skills.  We believe that our community is a very special place to learn, work and grow, supporting individuals to become independent and resilient learners who enjoy their education and are well-equipped to be the leaders of tomorrow.</p>
<p>Whilst our students participate in many activities that enrich their education and life experiences, we encourage them to recognise their role in helping those in need, both within and beyond our community. We support our young people to respect one another, to embrace diversity and to respond with compassion to those less fortunate than ourselves.</p>
<p>By working in partnership with parents/carers, parishes, feeder schools and the wider community, we create a positive, safe and stimulating environment in which each individual can succeed.</p>
<p>Our dedicated and highly-motivated staff are always available to offer help, support and guidance for our young people and their families, so please come and see our learning community in action.  Meet our students, who are our best ambassadors, and please take the opportunity to speak with them about their experiences when you visit our school.  In addition, spend time exploring our excellent facilities including our floodlit 3G pitch and recently refurbished swimming pool, our professional recording studio, and much, much more.</p>
<p>Please contact us at any time if you require any additional information:</p>
<h3>St John’s School & Sixth Form College – A Catholic Academy</h3>
<ul>
<li>w: <a href="https://www.stjohnsrc.org.uk">www.stjohns.org.uk</a></li>
<li>e: <a href="mailto:staff@stjohnsrc.org.uk</a>staff@stjohns.org.uk</li>
<li>t: 01388 603246</li>
</ul></div>
                                    </div>
            
        </div>
    </div>
            </div>'
];
