<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903fb2ad4659.57156757',
    'content' => '<div id="calendar-9280-particle" class="g-content g-particle">            <div class="g-calendar-particle ">

        
        <div id="g-calendar-9280" class="g-calendar">
            <script id="g-calendar-9280-flux-clndr-template" type="text/template">

                <div class="controls">
                    <div class="clndr-previous-button"><i class="fa fa-fw fa-arrow-circle-left"></i></div><div class="month-year"><%= month %> <%= year %></div><div class="clndr-next-button"><i class="fa fa-fw fa-arrow-circle-right"></i></div>
                </div>

                <div class="days-container">
                    <div class="days">
                        <div class="headers">
                            <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
                        </div>
                        <% _.each(days, function(day) { %><div class="<%= day.classes %>" id="<%= day.id %>"><%= day.day %></div><% }); %>
                    </div>
                    <div class="events">
                        <div class="headers">
                            <div class="x-button"><i class="fa fa-fw fa-close"></i></div>
                            <div class="event-header">Events</div>
                        </div>
                        <div class="events-list">
                            <% _.each(eventsThisMonth, function(event) { %>
                            <div id="<%= event.id %>" class="event hidden">
                                <%
                                    var date_format = \'MMMM Do\';
                                    var event_dates = moment(event.startDate).format(date_format);
                                    if (event.endDate !== \'\' && event.endDate !== event.startDate) {
                                        event_dates += \' - \' + moment(event.endDate).format(date_format);
                                    }
                                %>
                                <% if (event.url !== \'\') { %>
                                <a target="<%= event.target %>" class="event-link" href="<%= event.url %>"><%= event_dates %>: <%= event.title %></a>
                                <% } else { %>
                                <%= event_dates %>: <%= event.title %>
                                <% } %>
                                <span class="event-desc"><%= event.desc %></span>
                            </div>
                            <% }); %>
                        </div>
                    </div>
                </div>

            </script>
        </div>
    </div>
            </div>',
    'frameworks' => [
        'jquery' => 1
    ],
    'scripts' => [
        'footer' => [
            '5e4e05b379b0eb64a1e19e91c0f68802598947bf9704238680649399fad017ef95fc04ed' => [
                ':type' => 'file',
                ':priority' => 0,
                'src' => '/user/themes/rt_aurora/js/moment-with-locales.min.js',
                'type' => 'text/javascript',
                'defer' => false,
                'async' => false,
                'handle' => ''
            ],
            '5bc537326f71eb9e4660368ce922638a502a2411bd6ce3a2bdcd83b457b3d01816a1fc5c' => [
                ':type' => 'file',
                ':priority' => 0,
                'src' => '/user/themes/rt_aurora/js/clndr.js',
                'type' => 'text/javascript',
                'defer' => false,
                'async' => false,
                'handle' => ''
            ],
            '467eb0c86c1ccf914a09a325be6b08f24b9a2d2198c6d0a5012f625f1e969b4ac69731e7' => [
                ':type' => 'inline',
                ':priority' => 0,
                'content' => '
        jQuery(document).ready(function () {
            moment.locale(\'en\');
            var weekdayNames = Array.apply(null, Array(7)).map(
                function (_, i) {
                    return moment(i, \'e\').format(\'ddd\');
                });

            var events = [
                            ];

            jQuery("#g-calendar-9280").clndr({
                template: jQuery(\'#g-calendar-9280-flux-clndr-template\').html(),
                moment: moment,
                daysOfTheWeek: weekdayNames,
                adjacentDaysChangeMonth: true,
                events: events,
                multiDayEvents: {
                    singleDay: \'startDate\',
                    endDate: \'endDate\',
                    startDate: \'startDate\'
                },
                clickEvents: {
                    click: function(target) {
                        if(target.events.length) {
                            var eventContainer;
                            var daysContainer = jQuery("#g-calendar-9280").find(\'.days-container\');
                            daysContainer.toggleClass(\'show-events\', true);

                            target.events.forEach(function(event) {
                                eventContainer = jQuery("#g-calendar-9280").find(\'#\' + event.id);
                                eventContainer.removeClass(\'hidden\', false);
                            });

                            jQuery("#g-calendar-9280").find(\'.x-button\').click( function() {
                                daysContainer.toggleClass(\'show-events\', false);

                                target.events.forEach(function(event) {
                                    eventContainer = jQuery("#g-calendar-9280").find(\'#\' + event.id);
                                    eventContainer.addClass(\'hidden\', false);
                                });
                            });
                        }
                    }
                },
            });
        });
    ',
                'type' => 'text/javascript'
            ]
        ]
    ]
];
