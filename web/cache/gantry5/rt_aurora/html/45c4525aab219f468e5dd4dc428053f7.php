<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903e38b6f2c3.84557534',
    'content' => '<div id="simplecontent-7561-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                            <div class="g-simplecontent-item-content-title">St John’s School & Sixth Form College – A Catholic Academy </div>
                                                                
                    <div class="g-simplecontent-item-leading-content">Deep-rooted Gospel values are very much at the heart of the Trust’s vision.</div>                    
                                    </div>
            
        </div>
    </div>
            </div>'
];
