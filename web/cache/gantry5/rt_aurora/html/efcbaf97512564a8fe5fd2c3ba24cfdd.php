<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c9040e2d707f0.63400986',
    'content' => '<div id="simplecontent-2480-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                                                
                                        <div class="g-simplecontent-item-main-content"><h2>We believe that each person is unique and created in God’s image.  In our school, we provide a distinctive Catholic Education, where each child is loved, nurtured, inspired and challenged to aspire excellence and develop their individual abilities for themselves and others.</h2>
<p>At St Joseph’s we hold Christ at the centre of everything we do and our children know that they are unique and created in God’s image. We celebrate each individual, supporting their journey through school and developing their gifts and talents. Our staff provide excellent opportunities for our children to enjoy and achieve in a happy, safe, and caring environment.</p>
<p>We know that your child will be happy at St Joseph’s and we hope that you as parents and carers will be fully involved in your child’s education and the life of the school in general.</p>
<p>If you need any further information please do not hesitate to look at our school brochure or contact the office and we will do our very best to help.</p>
<ul>
<li> w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a></li>
<li>e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a></li>
<li>t: +44 (0)1325 300 337</li>
</ul></div>
                                    </div>
            
        </div>
    </div>
            </div>'
];
