<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903fb2ae9a52.79899065',
    'content' => '<div id="simplecounter-8847-particle" class="g-content g-particle">            <div class="g-simplecounter ">

    
    <div class="g-simplecounter-content">
        
    </div>

    <div class="g-simplecounter-block">
        <div class="g-simplecounter-calendar" id="g-simplecounter-8847" data-simplecounter-id="g-simplecounter-8847" data-countdown="/1/1"  data-simplecounter-daytext="" data-simplecounter-daystext="" data-simplecounter-hourtext="" data-simplecounter-hourstext="" data-simplecounter-minutetext="" data-simplecounter-minutestext="" data-simplecounter-secondtext="" data-simplecounter-secondstext=""></div>
    </div>
</div>
            </div>',
    'frameworks' => [
        'jquery' => 1
    ],
    'scripts' => [
        'footer' => [
            'b7cfa007d55d3389921ad3ce1ff46cf943b96c8637506519927a74cecf3a3c348eeea84e' => [
                ':type' => 'file',
                ':priority' => 0,
                'src' => '/user/themes/rt_aurora/js/simplecounter.js',
                'type' => 'text/javascript',
                'defer' => false,
                'async' => false,
                'handle' => ''
            ],
            '9fce1a3f7c4a2f8c24f078ab8a704b7816498590d24edaca1196f729a5e5fbbf24147c57' => [
                ':type' => 'file',
                ':priority' => 0,
                'src' => '/user/themes/rt_aurora/js/simplecounter.init.js',
                'type' => 'text/javascript',
                'defer' => false,
                'async' => false,
                'handle' => ''
            ]
        ]
    ]
];
