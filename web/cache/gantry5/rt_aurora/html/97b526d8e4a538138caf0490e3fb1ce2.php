<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903f52f1a919.50490542',
    'content' => '<div id="simplecontent-7104-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                            <div class="g-simplecontent-item-content-title">St Joseph’s Primary School – A Catholic Academy</div>
                                                                
                    <div class="g-simplecontent-item-leading-content">w: <a href="https://www.stjosephsrcprimaryschool.net">www.stjosephsrcprimaryschool.net</a><br />
t: +44 (0)1325 300 337<br />
e: <a href="mailto:stjosephsnewtonaycliffe@durhamlearning.net">stjosephsnewtonaycliffe@durhamlearning.net</a></div>                    
                                    </div>
            
        </div>
    </div>
            </div>'
];
