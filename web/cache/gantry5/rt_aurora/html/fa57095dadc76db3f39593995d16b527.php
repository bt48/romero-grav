<?php
return [
    '_type' => 'Gantry\\Component\\Content\\Block\\HtmlBlock',
    '_version' => 1,
    'id' => '5c903e59781524.89337265',
    'content' => '<div id="simplecontent-2127-particle" class="g-content g-particle">            <div class="">
        <div class="g-simplecontent">

            
                            <div class="g-simplecontent-item g-simplecontent-layout-standard">
                    
                    
                    
                    
                                                                                            <div class="g-simplecontent-item-content-title">St John’s School & Sixth Form College – A Catholic Academy </div>
                                                                
                    <div class="g-simplecontent-item-leading-content">w: <a href="https://www.stjohnsrc.org.uk">www.stjohnsrc.org.uk</a><br />
e: <a href="mailto:staff@stjohnsrc.org.uk">staff@stjohnsrc.org.uk</a></br />
t: 01388 603246</div>                    
                                    </div>
            
        </div>
    </div>
            </div>'
];
